import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID,NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { SecurityModule } from './pages/Security/security.module';
import { PayrollModule } from './pages/Payroll/payroll.module';
import { PosSaleModule} from './pages/PosSale/possale.module';

import { SideNavOuterToolbarModule, SideNavInnerToolbarModule, SingleCardModule } from './layouts';
import { FooterModule, LoginFormModule } from './shared/components';
import { AuthService, ScreenService, AppInfoService } from './shared/services';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { JwtModule } from '@auth0/angular-jwt';
import { DatePipe } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { ErrorInterceptorProvider } from './shared/services/error.interceptor';

registerLocaleData(localeEs, 'es');

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function tokenGetterLocal(): string {
  return localStorage.getItem('token') || '';
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetterLocal,
        allowedDomains: [environment.apiUrlToken],
        disallowedRoutes: [environment.apiUrlToken + '/SEG_USUARIO/login']
      }
    }),
    SideNavOuterToolbarModule,
    SideNavInnerToolbarModule,
    SingleCardModule,
    FooterModule,
    LoginFormModule,
    PayrollModule,
    PosSaleModule,
    SecurityModule,
    AppRoutingModule
  ],
  providers: [ErrorInterceptorProvider, DatePipe, AuthService, ScreenService, AppInfoService,{ provide: LOCALE_ID, useValue: 'es'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
