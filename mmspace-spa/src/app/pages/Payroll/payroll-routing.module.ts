import { PlaSoliConvenioComponent } from './pla-soli-convenio/pla-soli-convenio.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AuthGuardService } from 'src/app/shared/services/auth.service';
import { AppCanDeactivateGuard } from 'src/app/app-candeactivate.guard';

import { PlaSoliPermisoComponent } from './pla-soli-permiso/pla-soli-permiso.component';
import { PlaSoliPrestamoComponent } from './pla-soli-prestamo/pla-soli-prestamo.component';
import { PlaSoliConstanciaComponent } from './pla-soli-constancia/pla-soli-constancia.component';
import { PlaSolicitudVacacionComponent } from './pla-solicitud-vacacion/pla-solicitud-vacacion.component';
import { PlaSoliHoraExtraComponent } from './pla-soli-hora-extra/pla-soli-hora-extra.component';
import { PlaSolicitudVacacionAutorizarComponent } from './pla-solicitud-vacacion-autorizar/pla-solicitud-vacacion-autorizar.component';
import { PlaSoliPermisoAutorizarComponent } from './pla-soli-permiso-autorizar/pla-soli-permiso-autorizar.component';
import { PlaPlanVacacionEmpleadoComponent } from './pla-plan-vacacion-empleado/pla-plan-vacacion-empleado.component';
import { PlaPlanVacacionEmpleadoAutorizarComponent }
 from './pla-plan-vacacion-empleado-autorizar/pla-plan-vacacion-empleado-autorizar.component';
import { PlaPlanVacacionEmpleadoListadoComponent } from './pla-plan-vacacion-empleado-listado/pla-plan-vacacion-empleado-listado.component';
import { PlaSoliHoraExtraAutorizarComponent } from './pla-soli-hora-extra-autorizar/pla-soli-hora-extra-autorizar.component';


const routes: Routes = [
  {
    path: 'pla-soli-permiso',
    component: PlaSoliPermisoComponent,
    data: { titulo: 'Permisos' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-soli-permiso/pla-soli-permiso.module').then(m => m.PlaSoliPermisoModule)
  },
  {
    path: 'pla-soli-permiso-autorizar',
    component: PlaSoliPermisoAutorizarComponent,
    data: { titulo: 'Autorizar Permisos' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-soli-permiso-autorizar/pla-soli-permiso-autorizar.module').then(m => m.PlaSoliPermisoAutorizarModule)
  },
  {
    path: 'pla-soli-prestamo',
    component: PlaSoliPrestamoComponent,
    data: { titulo: 'Prestamos' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-soli-prestamo/pla-soli-prestamo.module').then(m => m.PlaSoliPrestamoModule)
  },
  {
    path: 'pla-soli-constancia',
    component: PlaSoliConstanciaComponent,
    data: { titulo: 'Constancia' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-soli-constancia/pla-soli-constancia.module').then(m => m.PlaSoliConstanciaModule)
  },
  {
    path: 'pla-soli-convenio',
    component: PlaSoliConvenioComponent,
    data: { titulo: 'Convenio' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-soli-convenio/pla-soli-convenio.module').then(m => m.PlaSoliConvenioModule)
  },
  {
    path: 'pla-solicitud-vacacion',
    component: PlaSolicitudVacacionComponent,
    data: { titulo: 'Vacaciones' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-solicitud-vacacion/pla-solicitud-vacacion.module').then(m => m.PlaSolicitudVacacionModule)
  },
  {
    path: 'pla-solicitud-vacacion-autorizar',
    component: PlaSolicitudVacacionAutorizarComponent,
    data: { titulo: 'Autorizar Vacaciones' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-solicitud-vacacion-autorizar/pla-solicitud-vacacion-autorizar.module')
    .then(m => m.PlaSolicitudVacacionAutorizarModule)
  },
  {
    path: 'pla-soli-hora-extra',
    component: PlaSoliHoraExtraComponent,
    data: { titulo: 'Horas Extras' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-soli-hora-extra/pla-soli-hora-extra.module').then(m => m.PlaSoliHoraExtraModule)
  },
  {
    path: 'pla-plan-vacacion-empleado',
    component: PlaPlanVacacionEmpleadoComponent,
    data: { titulo: 'Programar mis vacaciones anuales' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-plan-vacacion-empleado/pla-plan-vacacion-empleado.module').then(m => m.PlaPlanVacacionEmpleadoModule)
  },
  {
    path: 'pla-plan-vacacion-empleado-autorizar',
    component: PlaPlanVacacionEmpleadoAutorizarComponent,
    data: { titulo: 'Autorizar programacion de vacaciones' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-plan-vacacion-empleado-autorizar/pla-plan-vacacion-empleado-autorizar.module')
    .then(m => m.PlaPlanVacacionEmpleadoAutorizarModule)
  },
  {
    path: 'pla-plan-vacacion-empleado-listado',
    component: PlaPlanVacacionEmpleadoListadoComponent,
    data: { titulo: 'Programa Anual de Vacaciones' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-plan-vacacion-empleado-listado/pla-plan-vacacion-empleado-listado.module')
    .then(m => m.PlaPlanVacacionEmpleadoListadoModule)
  }
  ,
  {
    path: 'pla-soli-hora-extra-autorizar',
    component: PlaSoliHoraExtraAutorizarComponent,
    data: { titulo: 'Horas Extras Autorizar' },
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pla-soli-hora-extra-autorizar/pla-soli-hora-extra-autorizar.module')
    .then(m => m.PlaSoliHoraExtraAutorizarModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PayrollRoutingModule { }
