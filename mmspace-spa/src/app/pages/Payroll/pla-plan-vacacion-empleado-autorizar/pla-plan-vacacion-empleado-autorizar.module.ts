import { NgModule } from '@angular/core';
import { PlaPlanVacacionEmpleadoAutorizarRoutingModule } from './pla-plan-vacacion-empleado-autorizar-routing.module';

@NgModule({
	imports: [
		PlaPlanVacacionEmpleadoAutorizarRoutingModule
	]
})
export class PlaPlanVacacionEmpleadoAutorizarModule { }
