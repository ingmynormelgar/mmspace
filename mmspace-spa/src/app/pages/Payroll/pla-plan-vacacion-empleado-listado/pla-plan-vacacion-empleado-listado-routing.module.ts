import { DxDateBoxModule } from 'devextreme-angular/ui/date-box';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { DxLoadPanelModule } from 'devextreme-angular/ui/load-panel';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxTabPanelModule } from 'devextreme-angular/ui/tab-panel';
import { DxDropDownBoxModule } from 'devextreme-angular/ui/drop-down-box';
import { DxCheckBoxModule } from 'devextreme-angular/ui/check-box';

import { PlaPlanVacacionEmpleadoListadoComponent } from './pla-plan-vacacion-empleado-listado.component';
import { DxToolbarModule } from 'devextreme-angular/ui/toolbar';




const routes: Routes = [{ path: '', component: PlaPlanVacacionEmpleadoListadoComponent }];

@NgModule({
	imports: [RouterModule.forChild(routes),
		CommonModule,
		DxFormModule,
		DxDataGridModule,
		DxButtonModule,
		DxLoadPanelModule,
    DxToolbarModule,
		DxDropDownBoxModule,
		DxCheckBoxModule,
		DxTabPanelModule,
    DxDateBoxModule
	],
	exports: [RouterModule],
	declarations: [
		PlaPlanVacacionEmpleadoListadoComponent
	]
})
export class PlaPlanVacacionEmpleadoListadoRoutingModule { }
