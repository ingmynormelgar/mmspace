import { NgModule } from '@angular/core';
import { PlaPlanVacacionEmpleadoListadoRoutingModule } from './pla-plan-vacacion-empleado-listado-routing.module';

@NgModule({
	imports: [
		PlaPlanVacacionEmpleadoListadoRoutingModule
	]
})
export class PlaPlanVacacionEmpleadoListadoModule { }
