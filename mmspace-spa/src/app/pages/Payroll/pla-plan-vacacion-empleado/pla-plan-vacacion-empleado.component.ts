/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, ViewChild } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { custom } from 'devextreme/ui/dialog';
import { locale, loadMessages } from 'devextreme/localization';
import esMessages from 'devextreme/localization/messages/es.json';
import notify from 'devextreme/ui/notify';
import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
import { RowStatus } from 'src/app/shared/models/RowStatus.enum';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { ActivatedRoute } from '@angular/router';

import { AppInfoService } from 'src/app/shared/services/app-info.service';
import { PlaPlanVacacionEmpleado } from './pla-plan-vacacion-empleado';
import { PlaPlanVacacionEmpleadoService } from './pla-plan-vacacion-empleado.service';
import { AnyAaaaRecord } from 'dns';

@Component({
	selector: 'app-pla-plan-vacacion-empleado',
	templateUrl: './pla-plan-vacacion-empleado.component.html',
	styleUrls: ['./pla-plan-vacacion-empleado.component.scss']
})
export class PlaPlanVacacionEmpleadoComponent implements OnInit {
	@ViewChild('fData', { static: false }) dataForm!: DxFormComponent;
	// @ViewChild('gData') dataGrid!: DxDataGridComponent;

	//#region <Declaraciones>
	tituloVentana = 'Plan Vacacion Empleado';
	subTituloVentana = '';
	urlOpcion = '/pla-plan-vacacion-empleado';
	banderaMtto = UpdateType.Browse;
	loadingVisible = false;
	permiteSalir = true;
	permisos = 'ABC';
	permiteAdd = false;
	permiteEdit = false;
	permiteDele = false;
	permitePrint = false;
	models: any;
	modelUpdate: any;
	model: PlaPlanVacacionEmpleado = {
		CORR_SUSCRIPCION: 1,
		CORR_CONFI_PAIS: 1,
		CORR_EMPRESA: 1,
		ANIO_PERIODO: this.appInfoService.toYear(new Date()),
		CORR_EMPLEADO: 0,
    NOMBRE_EMPLEADO: '',
		CORR_DEPARTAMENTO: 0,
    NOMBRE_DEPARTAMENTO: '',
		CORR_CENTRO_COSTO: 0,
    NOMBRE_CENTRO: '',
		FECHA_INGRESO_EMPLEADO: new Date(),
		DIAS_SALDO: 0,
		DIAS_PROG_1: 0,
		DIAS_PROG_2: 0,
		DIAS_PROG_3: 0,
		DIAS_PROG_4: 0,
		DIAS_PROG_5: 0,
		DIAS_PROG_6: 0,
		DIAS_PROG_7: 0,
		DIAS_PROG_8: 0,
		DIAS_PROG_9: 0,
		DIAS_PROG_10: 0,
		DIAS_PROG_11: 0,
		DIAS_PROG_12: 0,
		DIAS_ACUM_1: 0,
		DIAS_ACUM_2: 0,
		DIAS_ACUM_3: 0,
		DIAS_ACUM_4: 0,
		DIAS_ACUM_5: 0,
		DIAS_ACUM_6: 0,
		DIAS_ACUM_7: 0,
		DIAS_ACUM_8: 0,
		DIAS_ACUM_9: 0,
		DIAS_ACUM_10: 0,
		DIAS_ACUM_11: 0,
		DIAS_ACUM_12: 0,
		DIAS_DESC_1: 0,
		DIAS_DESC_2: 0,
		DIAS_DESC_3: 0,
		DIAS_DESC_4: 0,
		DIAS_DESC_5: 0,
		DIAS_DESC_6: 0,
		DIAS_DESC_7: 0,
		DIAS_DESC_8: 0,
		DIAS_DESC_9: 0,
		DIAS_DESC_10: 0,
		DIAS_DESC_11: 0,
		DIAS_DESC_12: 0,
		DIAS_PAGO: 0,
    SALDO_1: 0,
    SALDO_2: 0,
    SALDO_3: 0,
    SALDO_4: 0,
    SALDO_5: 0,
    SALDO_6: 0,
    SALDO_7: 0,
    SALDO_8: 0,
    SALDO_9: 0,
    SALDO_10: 0,
    SALDO_11: 0,
    SALDO_12: 0,
    DIAS_EFECT_1:0,
    DIAS_EFECT_2:0,
    DIAS_EFECT_3:0,
    DIAS_EFECT_4:0,
    DIAS_EFECT_5:0,
    DIAS_EFECT_6:0,
    DIAS_EFECT_7:0,
    DIAS_EFECT_8:0,
    DIAS_EFECT_9:0,
    DIAS_EFECT_10:0,
    DIAS_EFECT_11:0,
    DIAS_EFECT_12:0,
    TOTAL_EFECT:0,
    TOTAL_DESC:0,
    TOTAL_ACUM:0,
    TOTAL_PROG:0,
    TOTAL_SALDO:0,
    TOTAL_SALDO_ANTERIOR:0,
		ESTADO_PLAN: '',
    NOMBRE_ESTADO_PLAN: '',
		USUARIO_CREA: '',
		FECHA_CREA: new Date(),
		ESTACION_CREA: '',
		USUARIO_ACTU: '',
		FECHA_ACTU: new Date(),
		ESTACION_ACTU: ''
	};
	param: any = {
    CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
	};

  valorSaldo1=0;
  valorSaldo2=0;
  valorSaldo3=0;
  valorSaldo4=0;
  valorSaldo5=0;
  valorSaldo6=0;
  valorSaldo7=0;
  valorSaldo8=0;
  valorSaldo9=0;
  valorSaldo10=0;
  valorSaldo11=0;
  valorSaldo12=0;
  valorProg1=0;
  valorProg2=0;
  valorProg3=0;
  valorProg4=0;
  valorProg5=0;
  valorProg6=0;
  valorProg7=0;
  valorProg8=0;
  valorProg9=0;
  valorProg10=0;
  valorProg11=0;
  valorProg12=0;
  valorSaldoFinal=0;
	//#endregion

	constructor(
		private appInfoService: AppInfoService,
		private service: PlaPlanVacacionEmpleadoService,
		private router: ActivatedRoute
	) {
		this.tituloVentana = router.snapshot.data.titulo;
		loadMessages(esMessages);
		locale(this.appInfoService.getLocale);
		this.getPermisos();

		// Metodos como propiedades
		this.getPermiteEditar = this.getPermiteEditar.bind(this);
		this.getPermiteDele = this.getPermiteDele.bind(this);
		this.isBrowse = this.isBrowse.bind(this);
		this.editarClick = this.editarClick.bind(this);
    this.onValueChangedDIAS_PROG_1 = this.onValueChangedDIAS_PROG_1.bind(this);
    this.onValueChangedDIAS_PROG_2 = this.onValueChangedDIAS_PROG_2.bind(this);
    this.onValueChangedDIAS_PROG_3 = this.onValueChangedDIAS_PROG_3.bind(this);
    this.onValueChangedDIAS_PROG_4 = this.onValueChangedDIAS_PROG_4.bind(this);
    this.onValueChangedDIAS_PROG_5 = this.onValueChangedDIAS_PROG_5.bind(this);
    this.onValueChangedDIAS_PROG_6 = this.onValueChangedDIAS_PROG_6.bind(this);
    this.onValueChangedDIAS_PROG_7 = this.onValueChangedDIAS_PROG_7.bind(this);
    this.onValueChangedDIAS_PROG_8 = this.onValueChangedDIAS_PROG_8.bind(this);
    this.onValueChangedDIAS_PROG_9 = this.onValueChangedDIAS_PROG_9.bind(this);
    this.onValueChangedDIAS_PROG_10 = this.onValueChangedDIAS_PROG_10.bind(this);
    this.onValueChangedDIAS_PROG_11 = this.onValueChangedDIAS_PROG_11.bind(this);
    this.onValueChangedDIAS_PROG_12 = this.onValueChangedDIAS_PROG_12.bind(this);
	}

	ngOnInit(): void {
		this.inicializaOpciones();
		this.llenaComboBox();
		this.consultar();
	}

	//#region <Validadores>
	esValido(): boolean {
		return true;
	}
	// #endregion
	//#region <Inicializando Opciones>
	inicializaOpciones() {
		this.getEMPRESA();
	}
	getEMPRESA() {
		// this.dSService.enviarCorrEmpresaObservable.subscribe(empresa => {
		// 	this.param.CORR_EMPRESA = empresa;
		// });
	}

	// #endregion
	//#region <Manejo de Combos>
	llenaComboBox() {
		// this.getEstado();
	}

	// getEstado() {
		// this.param.TIPO_CONSULTA = 1;
		// this.param.CORR_LISTA = 1;
		// this.param.OPCION_CONSULTA = 0;
		// this.service.getEstados(this.param).pipe(take(1)).subscribe(
		//   (model: any[]) => {
		//     this.mEstadoPC = model;
		//   },
		//   (error: any) => {
		//     notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
		//   }
		// );
	// }
	//#endregion

	//#region <Metodos Browse>
	isBrowse(): boolean {
		if (this.banderaMtto === UpdateType.Browse) {
			return true;
	}
		return false;
	}

	isForm(): boolean {
		if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
			return true;
		}
		return false;
	}

	getPermisos() {
		this.permiteAdd = false;
		this.permiteEdit = false;
		this.permiteDele = false;
		this.permitePrint = false;
		this.permisos = this.appInfoService.getPermiso(this.urlOpcion);
		if (this.permisos.includes('C')) {
			this.permiteAdd = true;
		}
		if (this.permisos.includes('U')) {
			this.permiteEdit = true;
		}
		if (this.permisos.includes('D')) {
			this.permiteDele = true;
		}
	}

	getPermiteEditar(e: any) {
    if (e.row.data.ESTADO_PLAN === 'SO'){
		  if (this.permiteEdit) {
			  return true;
		  }
    }
		return false;
	}

	getPermiteDele(e: any) {
    if (e.row.data.ESTADO_PLAN === 'DI') {
		  if (this.permiteDele) {
		  	return true;
      }
		}
		return false;
	}

	focusedRowChanged(e: any) {
		this.model = e.row.data;
	}
	//#endregion

	//#region <Metodos Mtto>
	consultar() {
    this.param.CORR_SUSCRIPCION = this.appInfoService.CORR_SUSCRIPCION;
		this.param.CORR_CONFI_PAIS = this.appInfoService.CORR_CONFI_PAIS;
		this.param.CORR_EMPRESA = this.appInfoService.CORR_EMPRESA;
		this.service.getAll(this.param).pipe(take(1)).subscribe((model: any[]) => {
			this.models = model;
		});
	}

	editarClick(e: any) {
		e.event.preventDefault();
		this.modelUpdate = {
			CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.model.CORR_EMPRESA,
			ANIO_PERIODO: this.model.ANIO_PERIODO,
			CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
			CORR_DEPARTAMENTO: this.model.CORR_DEPARTAMENTO,
      NOMBRE_DEPARTAMENTO: this.model.NOMBRE_DEPARTAMENTO,
			CORR_CENTRO_COSTO: this.model.CORR_CENTRO_COSTO,
      NOMBRE_CENTRO: this.model.NOMBRE_CENTRO,
			FECHA_INGRESO_EMPLEADO: this.model.FECHA_INGRESO_EMPLEADO,
			DIAS_SALDO: this.model.DIAS_SALDO,
			DIAS_PROG_1: this.model.DIAS_PROG_1,
			DIAS_PROG_2: this.model.DIAS_PROG_2,
			DIAS_PROG_3: this.model.DIAS_PROG_3,
			DIAS_PROG_4: this.model.DIAS_PROG_4,
			DIAS_PROG_5: this.model.DIAS_PROG_5,
			DIAS_PROG_6: this.model.DIAS_PROG_6,
			DIAS_PROG_7: this.model.DIAS_PROG_7,
			DIAS_PROG_8: this.model.DIAS_PROG_8,
			DIAS_PROG_9: this.model.DIAS_PROG_9,
			DIAS_PROG_10: this.model.DIAS_PROG_10,
			DIAS_PROG_11: this.model.DIAS_PROG_11,
			DIAS_PROG_12: this.model.DIAS_PROG_12,
			DIAS_ACUM_1: this.model.DIAS_ACUM_1,
			DIAS_ACUM_2: this.model.DIAS_ACUM_2,
			DIAS_ACUM_3: this.model.DIAS_ACUM_3,
			DIAS_ACUM_4: this.model.DIAS_ACUM_4,
			DIAS_ACUM_5: this.model.DIAS_ACUM_5,
			DIAS_ACUM_6: this.model.DIAS_ACUM_6,
			DIAS_ACUM_7: this.model.DIAS_ACUM_7,
			DIAS_ACUM_8: this.model.DIAS_ACUM_8,
			DIAS_ACUM_9: this.model.DIAS_ACUM_9,
			DIAS_ACUM_10: this.model.DIAS_ACUM_10,
			DIAS_ACUM_11: this.model.DIAS_ACUM_11,
			DIAS_ACUM_12: this.model.DIAS_ACUM_12,
			DIAS_DESC_1: this.model.DIAS_DESC_1,
			DIAS_DESC_2: this.model.DIAS_DESC_2,
			DIAS_DESC_3: this.model.DIAS_DESC_3,
			DIAS_DESC_4: this.model.DIAS_DESC_4,
			DIAS_DESC_5: this.model.DIAS_DESC_5,
			DIAS_DESC_6: this.model.DIAS_DESC_6,
			DIAS_DESC_7: this.model.DIAS_DESC_7,
			DIAS_DESC_8: this.model.DIAS_DESC_8,
			DIAS_DESC_9: this.model.DIAS_DESC_9,
			DIAS_DESC_10: this.model.DIAS_DESC_10,
			DIAS_DESC_11: this.model.DIAS_DESC_11,
			DIAS_DESC_12: this.model.DIAS_DESC_12,
      SALDO_1:this.model.SALDO_1,
      SALDO_2:this.model.SALDO_2,
      SALDO_3:this.model.SALDO_3,
      SALDO_4:this.model.SALDO_4,
      SALDO_5:this.model.SALDO_5,
      SALDO_6:this.model.SALDO_6,
      SALDO_7:this.model.SALDO_7,
      SALDO_8:this.model.SALDO_8,
      SALDO_9:this.model.SALDO_9,
      SALDO_10:this.model.SALDO_10,
      SALDO_11:this.model.SALDO_11,
      SALDO_12:this.model.SALDO_12,
      DIAS_EFECT_1:this.model.DIAS_EFECT_1,
      DIAS_EFECT_2:this.model.DIAS_EFECT_2,
      DIAS_EFECT_3:this.model.DIAS_EFECT_3,
      DIAS_EFECT_4:this.model.DIAS_EFECT_4,
      DIAS_EFECT_5:this.model.DIAS_EFECT_5,
      DIAS_EFECT_6:this.model.DIAS_EFECT_6,
      DIAS_EFECT_7:this.model.DIAS_EFECT_7,
      DIAS_EFECT_8:this.model.DIAS_EFECT_8,
      DIAS_EFECT_9:this.model.DIAS_EFECT_9,
      DIAS_EFECT_10:this.model.DIAS_EFECT_10,
      DIAS_EFECT_11:this.model.DIAS_EFECT_11,
      DIAS_EFECT_12:this.model.DIAS_EFECT_12,
			DIAS_PAGO: this.model.DIAS_PAGO,
      TOTAL_EFECT:this.model.TOTAL_EFECT,
      TOTAL_DESC:this.model.TOTAL_DESC,
      TOTAL_ACUM:this.model.TOTAL_ACUM,
      TOTAL_PROG:this.model.TOTAL_PROG,
      TOTAL_SALDO:this.model.TOTAL_SALDO,
      TOTAL_SALDO_ANTERIOR:this.model.TOTAL_SALDO_ANTERIOR,
			ESTADO_PLAN: this.model.ESTADO_PLAN,
      NOMBRE_ESTADO_PLAN: this.model.NOMBRE_ESTADO_PLAN,
			USUARIO_CREA: this.model.USUARIO_CREA,
			FECHA_CREA: this.model.FECHA_CREA,
			ESTACION_CREA: this.model.ESTACION_CREA,
			USUARIO_ACTU: this.model.USUARIO_ACTU,
			FECHA_ACTU: this.model.FECHA_ACTU,
			ESTACION_ACTU: this.model.ESTACION_ACTU
		};
		this.permiteSalir = false;
		this.banderaMtto = UpdateType.Update;
		this.subTituloVentana = RowStatus.Update.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('NOMBRE_EMPLEADO')?.focus();
      this.valorProg1=this.model.DIAS_PROG_1;
      this.valorProg2=this.model.DIAS_PROG_2;
      this.valorProg3=this.model.DIAS_PROG_3;
      this.valorProg4=this.model.DIAS_PROG_4;
      this.valorProg5=this.model.DIAS_PROG_5;
      this.valorProg6=this.model.DIAS_PROG_6;
      this.valorProg7=this.model.DIAS_PROG_7;
      this.valorProg8=this.model.DIAS_PROG_8;
      this.valorProg9=this.model.DIAS_PROG_9;
      this.valorProg10=this.model.DIAS_PROG_10;
      this.valorProg11=this.model.DIAS_PROG_11;
      this.valorProg12=this.model.DIAS_PROG_12;
      this.valorSaldoFinal=this.model.SALDO_12-this.model.DIAS_PAGO;
		});
	}

	guardar(): void {
		if (!this.esValido()) {
			return;
		}

  if (this.model.SALDO_1 < 0 ||
      this.model.SALDO_2 < 0 ||
      this.model.SALDO_3 < 0 ||
      this.model.SALDO_4 < 0 ||
      this.model.SALDO_5 < 0 ||
      this.model.SALDO_6 < 0 ||
      this.model.SALDO_7 < 0 ||
      this.model.SALDO_8 < 0 ||
      this.model.SALDO_9 < 0 ||
      this.model.SALDO_10 < 0 ||
      this.model.SALDO_11 < 0 ||
      this.model.SALDO_12 < 0  )
  {
    notify(
      { message: 'Los dias programados no deben ser menores al saldo!'
        , width: 'auto'
        , shading: false
        , closeOnClick: true
        , closeOnOutsideClick: true
      }
      , 'error'
      , 500000
    );
    return;
  }

		this.loadingVisible = true;
		if (this.banderaMtto === UpdateType.Add) {
				this.service.insert(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.models.push(newModel);
					this.model = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro creado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true},'error', 500000);
					this.loadingVisible = false;
				}
			);
		} else
		if (this.banderaMtto === UpdateType.Update) {
			this.service.update(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.model = newModel;
					const vIndex = this.models.findIndex((item: any) => item.CORR_EMPLEADO === newModel.CORR_EMPLEADO);
					this.models[vIndex] = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro modificado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true},'error', 500000);
					this.loadingVisible = false;
				}
			);
		}
	}

	cancelar(): void {
    const cancelRow = () => {
      this.permiteSalir = true;
      this.banderaMtto = UpdateType.Browse;
      this.subTituloVentana = RowStatus.Not_Defined.toString();
      this.getPermisos();
    };
    if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
      const confirmacion = custom({
        title: 'Confirmación de Cancelar',
        messageHtml: '¿Quieres cancelar y perder los cambios realizados?',
        buttons: [{
          text: 'Si', onClick: (e: any) => true
        }, {
          text: 'No', onClick: (e: any) => false
        }]
      });

      confirmacion.show().then((cancel: boolean) => {
        if (cancel) {
          if(this.banderaMtto === UpdateType.Update){
          this.model = this.modelUpdate;
          const vIndex = this.models.findIndex((item: any) => item.CORR_EMPLEADO === this.modelUpdate.CORR_EMPLEADO);
          this.models[vIndex] = this.modelUpdate;
          }
          cancelRow();
        }
      });
    } else {
      cancelRow();
    }
	}

	rowRemoving(e: any) {
		this.service.delete(e.data.CORR_EMPLEADO, this.param).pipe(take(1)).subscribe(
			() => {
				notify({ message: 'Registro eliminado con exito!', width: 'auto', shading: false }, 'success', 1500);
				e.component.refresh();
			},
			(error: any) => {
				e.cancel = true;
				notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true }, 'error', 500000);
			}
		);
	}

	rowDblClick(e: any) {
		this.banderaMtto = UpdateType.Not_Defined;
		this.subTituloVentana = RowStatus.Browse.toString();
		setTimeout(() => {
			this.bloquear();
		});
	}

	bloquear(): void {
		this.dataForm.instance.getEditor('ANIO_PERIODO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_EMPLEADO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_DEPARTAMENTO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_CENTRO_COSTO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_INGRESO_EMPLEADO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_SALDO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_1')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_2')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_3')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_4')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_5')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_6')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_7')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_8')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_9')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_10')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_11')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PROG_12')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_1')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_2')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_3')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_4')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_5')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_6')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_7')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_8')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_9')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_10')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_11')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_ACUM_12')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_1')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_2')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_3')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_4')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_5')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_6')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_7')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_8')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_9')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_10')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_11')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_DESC_12')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PAGO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('ESTADO_PLAN')?.option('readOnly', true);
	}

	permitirSalir():
		| boolean
		| import('rxjs').Observable<boolean>
		| Promise<boolean> {
		if (this.permiteSalir) {
			return true;
		}
		const confirmacion = custom({
			title: 'Confirmación de Salida',
			messageHtml: '¿Quieres salir del formulario y perder los cambios realizados?',
			buttons: [{
				text: 'Si',
				onClick: (e: any) => true
			},
			{
				text: 'No',
				onClick: (e: any) => false
			}]

		});

		return confirmacion.show().then(() => {});
	}

 onValueChangedDIAS_PROG_1(e: any){
  this.valorProg1 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
}
  onValueChangedDIAS_PROG_2(e: any){
    this.valorProg2 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }
  onValueChangedDIAS_PROG_3(e: any){
    this.valorProg3 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
    }
  onValueChangedDIAS_PROG_4(e: any){
    this.valorProg4 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }
  onValueChangedDIAS_PROG_5(e: any){
    this.valorProg5 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }
  onValueChangedDIAS_PROG_6(e: any){
    this.valorProg6 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }
  onValueChangedDIAS_PROG_7(e: any){
    this.valorProg7 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }
  onValueChangedDIAS_PROG_8(e: any){
    this.valorProg8 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }
  onValueChangedDIAS_PROG_9(e: any){
    this.valorProg9 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }
  onValueChangedDIAS_PROG_10(e: any){
    this.valorProg10 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }
  onValueChangedDIAS_PROG_11(e: any){
    this.valorProg11 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }
  onValueChangedDIAS_PROG_12(e: any){
    this.valorProg12 = e.value;
    this.valorSaldo1 = this.model.DIAS_SALDO + this.model.DIAS_ACUM_1 - this.valorProg1;
    this.model.SALDO_1 = this.valorSaldo1;
    this.valorSaldo2 = this.valorSaldo1 + this.model.DIAS_ACUM_2 - this.valorProg2;
    this.model.SALDO_2 = this.valorSaldo2;
    this.valorSaldo3 = this.valorSaldo2 + this.model.DIAS_ACUM_3 - this.valorProg3;
    this.model.SALDO_3 = this.valorSaldo3;
    this.valorSaldo4 = this.valorSaldo3 + this.model.DIAS_ACUM_4 - this.valorProg4;
    this.model.SALDO_4 = this.valorSaldo4;
    this.valorSaldo5 = this.valorSaldo4 + this.model.DIAS_ACUM_5 -  this.valorProg5;
    this.model.SALDO_5 = this.valorSaldo5;
    this.valorSaldo6 = this.valorSaldo5 + this.model.DIAS_ACUM_6 - this.valorProg6;
    this.model.SALDO_6 = this.valorSaldo6;
    this.valorSaldo7 = this.valorSaldo6 + this.model.DIAS_ACUM_7 - this.valorProg7;
    this.model.SALDO_7 = this.valorSaldo7;
    this.valorSaldo8 = this.valorSaldo7 + this.model.DIAS_ACUM_8 - this.valorProg8;
    this.model.SALDO_8 = this.valorSaldo8;
    this.valorSaldo9 = this.valorSaldo8 + this.model.DIAS_ACUM_9 - this.valorProg9;
    this.model.SALDO_9 = this.valorSaldo9;
    this.valorSaldo10 = this.valorSaldo9 + this.model.DIAS_ACUM_10 - this.valorProg10;
    this.model.SALDO_10 = this.valorSaldo10;
    this.valorSaldo11 = this.valorSaldo10 + this.model.DIAS_ACUM_11 - this.valorProg11;
    this.model.SALDO_11 = this.valorSaldo11;
    this.valorSaldo12 = this.valorSaldo11 + this.model.DIAS_ACUM_12 - this.valorProg12;
    this.model.SALDO_12 = this.valorSaldo12;
    this.model.TOTAL_SALDO = this.valorSaldo12;
    this.valorSaldoFinal = this.valorSaldo12-this.model.DIAS_PAGO;
    this.model.TOTAL_SALDO_ANTERIOR = this.valorSaldoFinal;
    this.model.TOTAL_PROG = this.valorProg1+this.valorProg2+this.valorProg3+this.valorProg4+this.valorProg5+
      this.valorProg6+this.valorProg7+this.valorProg8+this.valorProg9+this.valorProg10+this.valorProg11+this.valorProg12;
  }

  SolicitarPro(): void {
    if (this.model.ESTADO_PLAN !== '') {
      const confirApli = custom({
        title: 'Confirmación de Solicitud de Programacion',
        messageHtml: '¿Realmente desea Solicitar la programacion de vacación?',
        buttons: [{
          text: 'Si',
          onClick: (e: any) => {
            console.log(e);
            if (this.model.ESTADO_PLAN === 'SO') {
              this.service.solicitarPro(this.model).subscribe(
                (newModel: any) => {
                  this.model = newModel;
                  const vIndex = this.models.findIndex((item: any) => item.CORR_EMPLEADO === newModel.CORR_EMPLEADO);
                  this.models[vIndex] = newModel;
                  notify({ message: 'Registro Programado con exito', width: 'auto', shading: false}, 'success', 1500);
                },
                (error: any) => {
                  notify({ message: error,
                    width: 'auto',
                    shading: false,
                    closeOnClick: true,
                    closeOnOutsideClick: true }, 'error', 500000
                  );
                }
              );
            } else {
              notify({ message: 'El registro debe estar Solicitado', width: 'auto', shading: false}, 'warning', 1500);
            }
          }
        },
        {
          text: 'No',
          onClick: (e: any) => false
        }]
      });
      confirApli.show().then((dialogResult: any) => {});
    } else {
      notify(
        {
          message: 'Debe seleccionar un registro ',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }
  }


	//#endregion

}
