import { NgModule } from '@angular/core';
import { PlaPlanVacacionEmpleadoRoutingModule } from './pla-plan-vacacion-empleado-routing.module';

@NgModule({
	imports: [
		PlaPlanVacacionEmpleadoRoutingModule
	]
})
export class PlaPlanVacacionEmpleadoModule { }