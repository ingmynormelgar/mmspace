import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class PlaPlanVacacionEmpleadoService {
	readonly urlMtto = environment.apiUrl + 'PLA_PLAN_VACACION_EMPLEADO/';

	constructor(private http: HttpClient) { }

  getAll(param: any): Observable<any[]> {

		let parametros = new HttpParams();

		if (param != null) {
      parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);;
		}

		return this.http.get<any[]>(this.urlMtto, { params: parametros });
	}

	get(id: number, param: any): Observable<any> {

		let parametros = new HttpParams();

		if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
      parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('ANIO_PERIODO', param.ANIO_PERIODO);
			parametros = parametros.append('CORR_EMPLEADO', param.CORR_EMPLEADO);
			parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
		}

		return this.http.get<any>(this.urlMtto + id , { params: parametros });
	}

	insert(model: any): any {
		return this.http.post(this.urlMtto, model).pipe(
			map((response: any) => response)
		);
	}

	update(model: any): any {
		return this.http.put(this.urlMtto, model).pipe(
			map((response: any) => response)
		);
	}

	delete(id: number, param: any): any {
		let parametros = new HttpParams();

		if (param != null) {
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
		}

		return this.http.delete(this.urlMtto + id, { params: parametros }).pipe(
		  map((response: any) => response)
		);
	}
  solicitarPro(model: any): any {
    return this.http.put(this.urlMtto + 'SolicitarPro', model).pipe(
      map((response: any) => response)
    );
  }
}
