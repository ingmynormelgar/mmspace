/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, ViewChild } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { custom } from 'devextreme/ui/dialog';
import { locale, loadMessages } from 'devextreme/localization';
import esMessages from 'devextreme/localization/messages/es.json';
import notify from 'devextreme/ui/notify';
import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
import { RowStatus } from 'src/app/shared/models/RowStatus.enum';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { DxDropDownBoxComponent } from 'devextreme-angular/ui/drop-down-box';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { ActivatedRoute } from '@angular/router';

import { AppInfoService } from 'src/app/shared/services/app-info.service';
import { PlaSoliConstancia } from './pla-soli-constancia';
import { PlaSoliConstanciaService } from './pla-soli-constancia.service';

@Component({
	selector: 'app-pla-soli-constancia',
	templateUrl: './pla-soli-constancia.component.html',
	styleUrls: ['./pla-soli-constancia.component.scss']
})
export class PlaSoliConstanciaComponent implements OnInit {
	@ViewChild('fData', { static: false }) dataForm!: DxFormComponent;
 @ ViewChild('estadoConstancia', { static: false }) estadoConstancia!: DxDropDownBoxComponent;
 	@ViewChild('dataEstadoConstancia', { static: false }) dataEstadoConstancia!: DxDataGridComponent;
	@ViewChild('corrEmpleado', { static: false }) corrEmpleado!: DxDropDownBoxComponent;
	@ViewChild('dataEmpleado', { static: false }) dataEmpleado!: DxDataGridComponent;
  @ViewChild('corrTipoConstancia', { static: false }) corrTipoConstancia!: DxDropDownBoxComponent;
	@ViewChild('dataTipoConstancia', { static: false }) dataTipoConstancia!: DxDataGridComponent;

	//#region <Declaraciones>
	tituloVentana = 'Solicitud de Constancia';
	subTituloVentana = '';
	urlOpcion = '/pla-soli-constancia';
	banderaMtto = UpdateType.Browse;
	loadingVisible = false;
	permiteSalir = true;
	permisos = 'ABC';
	permiteAdd = false;
	permiteEdit = false;
	permiteDele = false;
	permitePrint = false;
	models: any;
	modelUpdate: any;
	model: PlaSoliConstancia = {
		CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
		CORR_EMPLEADO: 0,
    NOMBRE_EMPLEADO: '',
		CORR_CONSTANCIA: 0,
		FECHA_SOLICITUD: this.appInfoService.getDate(),
		CORR_TIPO_CONSTANCIA: 0,
    NOMBRE_TIPO_CONSTANCIA: '',
		USUARIO_SOLICITA: '',
		USUARIO_AUTORIZA: '',
		ESTADO_CONSTANCIA: '',
    NOMBRE_ESTADO_CONSTANCIA: '',
		OBSERVACIONES: '',
		USUARIO_CREA: '',
		FECHA_CREA: new Date(),
		ESTACION_CREA: '',
		USUARIO_ACTU: '',
		FECHA_ACTU: new Date(),
		ESTACION_ACTU: ''
	};
	param: any = {
		TIPO_CONSULTA: 1,
		CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
    FECHA_INICIAL: new Date(this.appInfoService.getDate().getFullYear(), this.appInfoService.getDate().getMonth(), 1),
    FECHA_FINAL: this.appInfoService.getDate(),
		OPCION_CONSULTA: 0
	};

  mEstado: any;
  mEmpleado: any;
  mTipoConstancia: any;
  bloqLookup = false;
	//#endregion

	constructor(
		private appInfoService: AppInfoService,
		private service: PlaSoliConstanciaService,
		private router: ActivatedRoute
	) {
		this.tituloVentana = router.snapshot.data.titulo;
		loadMessages(esMessages);
		locale(this.appInfoService.getLocale);
		this.getPermisos();

		// Metodos como propiedades
		this.getPermiteEditar = this.getPermiteEditar.bind(this);
		this.getPermiteDele = this.getPermiteDele.bind(this);
		this.isBrowse = this.isBrowse.bind(this);
		this.editarClick = this.editarClick.bind(this);
	}

	ngOnInit(): void {
		this.inicializaOpciones();
		this.llenaComboBox();
		this.consultar();
	}

	//#region <Validadores>
	esValido(): boolean {
		if (this.model.ESTADO_CONSTANCIA === '' || this.model.ESTADO_CONSTANCIA === null) {
      notify(
        {
          message:
            'Error, Debe seleccionar el Estado de la Constancia.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.FECHA_SOLICITUD === null) {
      notify(
        {
          message:
            'Error, Debe seleccionar una Fecha.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.CORR_TIPO_CONSTANCIA === 0 || this.model.CORR_TIPO_CONSTANCIA === null) {
      notify(
        {
          message:
            'Error, Debe seleccionar el Tipo de Constancia.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
		return true;
	}
	// #endregion
	//#region <Inicializando Opciones>
	inicializaOpciones() {
		this.getEMPRESA();
	}
	getEMPRESA() {
		// this.dSService.enviarCorrEmpresaObservable.subscribe(empresa => {
		// 	this.param.CORR_EMPRESA = empresa;
		// });
	}

	// #endregion

	//#region <Manejo de Combos>
	llenaComboBox() {
		this.getEstados();
    this.getEmpleados();
    this.getTipoConstancias();
	}

	getEstados() {
	  this.param.TIPO_CONSULTA = 1;
	  this.param.CORR_LISTA = 1;
	  this.param.OPCION_CONSULTA = 0;
	  this.service.getEstados(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mEstado = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  getEmpleados() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 1;
	  this.service.getEmpleados(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mEmpleado = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  getTipoConstancias() {
    this.param.TIPO_CONSULTA = 2;
    this.param.OPCION_CONSULTA = 0;
    this.service.getTipoConstancias(this.param).pipe(take(1)).subscribe(
      (model: any[]) => {
        this.mTipoConstancia = model;
      },
      (error: any) => {
        notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
      }
    );
  }
	//#endregion

	//#region <Metodos Browse>
	isBrowse(): boolean {
		if (this.banderaMtto === UpdateType.Browse) {
			return true;
	}
		return false;
	}

	isForm(): boolean {
		if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
			return true;
		}
		return false;
	}

	getPermisos() {
		this.permiteAdd = false;
		this.permiteEdit = false;
		this.permiteDele = false;
		this.permitePrint = false;
		this.permisos = this.appInfoService.getPermiso(this.urlOpcion);
		if (this.permisos.includes('C')) {
			this.permiteAdd = true;
		}
		if (this.permisos.includes('U')) {
			this.permiteEdit = true;
		}
		if (this.permisos.includes('D')) {
			this.permiteDele = true;
		}
	}

	getPermiteEditar(e: any) {
    if (e.row.data.ESTADO_CONSTANCIA === 'DI') {
		  if (this.permiteEdit) {
        return true;
      }
		}
		return false;
	}

	getPermiteDele(e: any) {
		if (e.row.data.ESTADO_CONSTANCIA === 'DI') {
      if (this.permiteDele) {
        return true;
      }
		}
		return false;
	}

	focusedRowChanged(e: any) {
		this.model = e.row.data;
	}
	//#endregion

	//#region <Metodos Mtto>
	consultar() {
		this.param.TIPO_CONSULTA = 1;
		this.param.OPCION_CONSULTA = 1;
    localStorage.setItem('FechaInicial', this.param.FECHA_INICIAL.toISOString());
    localStorage.setItem('FechaFinal', this.param.FECHA_FINAL.toISOString());
		this.service.getAll(this.param).pipe(take(1)).subscribe((model: any[]) => {
			this.models = model;
		});
	}

	nuevo(): void {
		this.model = {
			CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
			CORR_EMPLEADO: this.mEmpleado[0].CORR_EMPLEADO,
      NOMBRE_EMPLEADO: '',
			CORR_CONSTANCIA: 0,
			FECHA_SOLICITUD: this.appInfoService.getDate(),
			CORR_TIPO_CONSTANCIA: 1,
      NOMBRE_TIPO_CONSTANCIA: '',
			USUARIO_SOLICITA: '',
			USUARIO_AUTORIZA: '',
			ESTADO_CONSTANCIA: 'DI',
      NOMBRE_ESTADO_CONSTANCIA: '',
			OBSERVACIONES: '',
			USUARIO_CREA: '',
			FECHA_CREA: new Date(),
			ESTACION_CREA: '',
			USUARIO_ACTU: '',
			FECHA_ACTU: new Date(),
			ESTACION_ACTU: ''
		};
    this.modelUpdate = {
			CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.model.CORR_EMPRESA,
			CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
			CORR_CONSTANCIA: this.model.CORR_CONSTANCIA,
			FECHA_SOLICITUD: this.model.FECHA_SOLICITUD,
			CORR_TIPO_CONSTANCIA: this.model.CORR_TIPO_CONSTANCIA,
      NOMBRE_TIPO_CONSTANCIA: this.model.NOMBRE_TIPO_CONSTANCIA,
			USUARIO_SOLICITA: this.model.USUARIO_SOLICITA,
			USUARIO_AUTORIZA: this.model.USUARIO_AUTORIZA,
			ESTADO_CONSTANCIA: this.model.ESTADO_CONSTANCIA,
      NOMBRE_ESTADO_CONSTANCIA: this.model.NOMBRE_ESTADO_CONSTANCIA,
			OBSERVACIONES: this.model.OBSERVACIONES,
			USUARIO_CREA: this.model.USUARIO_CREA,
			FECHA_CREA: this.model.FECHA_CREA,
			ESTACION_CREA: this.model.ESTACION_CREA,
			USUARIO_ACTU: this.model.USUARIO_ACTU,
			FECHA_ACTU: this.model.FECHA_ACTU,
			ESTACION_ACTU: this.model.ESTACION_ACTU
		};
    this.permiteSalir = false;
    this.bloqLookup = false;
		this.banderaMtto = UpdateType.Add;
		this.subTituloVentana = RowStatus.Add.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('NOMBRE_CONSTANCIA')?.focus();
		});
	}

	editarClick(e: any) {
		e.event.preventDefault();
    this.bloqLookup = false;
		this.modelUpdate = {
			CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.model.CORR_EMPRESA,
			CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
			CORR_CONSTANCIA: this.model.CORR_CONSTANCIA,
			FECHA_SOLICITUD: this.model.FECHA_SOLICITUD,
			CORR_TIPO_CONSTANCIA: this.model.CORR_TIPO_CONSTANCIA,
      NOMBRE_TIPO_CONSTANCIA: this.model.NOMBRE_TIPO_CONSTANCIA,
			USUARIO_SOLICITA: this.model.USUARIO_SOLICITA,
			USUARIO_AUTORIZA: this.model.USUARIO_AUTORIZA,
			ESTADO_CONSTANCIA: this.model.ESTADO_CONSTANCIA,
      NOMBRE_ESTADO_CONSTANCIA: this.model.NOMBRE_ESTADO_CONSTANCIA,
			OBSERVACIONES: this.model.OBSERVACIONES,
			USUARIO_CREA: this.model.USUARIO_CREA,
			FECHA_CREA: this.model.FECHA_CREA,
			ESTACION_CREA: this.model.ESTACION_CREA,
			USUARIO_ACTU: this.model.USUARIO_ACTU,
			FECHA_ACTU: this.model.FECHA_ACTU,
			ESTACION_ACTU: this.model.ESTACION_ACTU
		};
		this.permiteSalir = false;
		this.banderaMtto = UpdateType.Update;
		this.subTituloVentana = RowStatus.Update.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('NOMBRE_CONSTANCIA')?.focus();
		});
	}

	guardar(): void {
		if (!this.esValido()) {
			return;
		}

		this.loadingVisible = true;
		if (this.banderaMtto === UpdateType.Add) {
				this.service.insert(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.models.push(newModel);
					this.model = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro creado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true
            },
            'error',
            500000
          );
					this.loadingVisible = false;
				}
			);
		} else
		if (this.banderaMtto === UpdateType.Update) {
			this.service.update(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.model = newModel;
					const vIndex = this.models.findIndex((item: any) => item.CORR_CONSTANCIA === newModel.CORR_CONSTANCIA);
					this.models[vIndex] = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro modificado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true
            },
            'error',
            500000
          );
					this.loadingVisible = false;
				}
			);
		}
	}

	cancelar(): void {
    const cancelRow = () => {
      this.permiteSalir = true;
      this.bloqLookup = false;
      this.banderaMtto = UpdateType.Browse;
      this.subTituloVentana = RowStatus.Not_Defined.toString();
      this.getPermisos();
    };
    if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
      const confirmacion = custom({
        title: 'Confirmación de Cancelar',
        messageHtml: '¿Quieres cancelar y perder los cambios realizados?',
        buttons: [{
          text: 'Si', onClick: (e: any) => true
        }, {
          text: 'No', onClick: (e: any) => false
        }]
      });

      confirmacion.show().then((cancel: boolean) => {
        if (cancel) {
          this.model = this.modelUpdate;
          const vIndex = this.models.findIndex((item: any) => item.CORR_CONSTANCIA === this.modelUpdate.CORR_CONSTANCIA);
          this.models[vIndex] = this.modelUpdate;
          cancelRow();
        }
      });
    } else {
      cancelRow();
    }
	}

	rowRemoving(e: any) {
    this.param.CORR_EMPLEADO = e.data.CORR_EMPLEADO;
		this.service.delete(e.data.CORR_CONSTANCIA, this.param).pipe(take(1)).subscribe(
			() => {
				notify({ message: 'Registro eliminado con exito!', width: 'auto', shading: false }, 'success', 1500);
				e.component.refresh();
			},
			(error: any) => {
				e.cancel = true;
				notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true }, 'error', 500000);
			}
		);
	}

	rowDblClick(e: any) {
		this.banderaMtto = UpdateType.Not_Defined;
		this.subTituloVentana = RowStatus.Browse.toString();
		setTimeout(() => {
			this.bloquear();
		});
	}

	bloquear(): void {
		this.dataForm.instance.getEditor('CORR_EMPLEADO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_CONSTANCIA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_SOLICITUD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_TIPO_CONSTANCIA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('USUARIO_SOLICITA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('USUARIO_AUTORIZA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('ESTADO_CONSTANCIA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('OBSERVACIONES')?.option('readOnly', true);
    this.bloqLookup = true;
	}

	permitirSalir():
		| boolean
		| import('rxjs').Observable<boolean>
		| Promise<boolean> {
		if (this.permiteSalir) {
			return true;
		}
		const confirmacion = custom({
			title: 'Confirmación de Salida',
			messageHtml: '¿Quieres salir del formulario y perder los cambios realizados?',
			buttons: [{
				text: 'Si',
				onClick: (e: any) => true
			},
			{
				text: 'No',
				onClick: (e: any) => false
			}]

		});

		return confirmacion.show().then(() => {});
	}
	//#endregion

  //region Metodos Lookups
  valueChangedESTADO_CONSTANCIA(e: any) {
    if (this.model.ESTADO_CONSTANCIA === null) {
      this.dataEstadoConstancia.instance.clearSelection();
    }
  }

  selectionChangedESTADO_CONSTANCIA(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.ESTADO_CONSTANCIA = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickESTADO_CONSTANCIA(e: any, data: any) {
    this.estadoConstancia.instance.close();
    this.estadoConstancia.instance.focus();
  }

  valueChangedCORR_EMPLEADO(e: any) {
    if (this.model.CORR_EMPLEADO === null) {
      this.dataEmpleado.instance.clearSelection();
    }
  }

  selectionChangedCORR_EMPLEADO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_EMPLEADO = selectedRowKeys[0].CORR_EMPLEADO;
    }
  }

  rowClickCORR_EMPLEADO(e: any, data: any) {
    this.corrEmpleado.instance.close();
    this.corrEmpleado.instance.focus();
  }

  valueChangedCORR_TIPO_CONSTANCIA(e: any) {
    if (this.model.ESTADO_CONSTANCIA === null) {
      this.dataTipoConstancia.instance.clearSelection();
    }
  }

  selectionChangedCORR_TIPO_CONSTANCIA(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_TIPO_CONSTANCIA = selectedRowKeys[0].CORR_TIPO_CONSTANCIA;
    }
  }

  rowClickCORR_TIPO_CONSTANCIA(e: any, data: any) {
    this.corrTipoConstancia.instance.close();
    this.corrTipoConstancia.instance.focus();
  }
  //#endregion

  //solicitar
  Solicitar(): void {
    if (this.model.ESTADO_CONSTANCIA !== '') {
      const confirApli = custom({
        title: 'Confirmación de Solicitud',
        messageHtml: '¿Realmente desea Solicitar esta Constancia?',
        buttons: [{
          text: 'Si',
          onClick: (e: any) => {
            if (this.model.ESTADO_CONSTANCIA === 'DI') {
              this.service.solicitar(this.model).subscribe(
                (newModel: any) => {
                  this.model = newModel;
                  const vIndex = this.models.findIndex((item: any) => item.CORR_CONSTANCIA === newModel.CORR_CONSTANCIA);
                  this.models[vIndex] = newModel;
                  notify({ message: 'Registro Solicitado con exito', width: 'auto', shading: false}, 'success', 1500);
                },
                (error: any) => {
                  notify({ message: error,
                    width: 'auto',
                    shading: false,
                    closeOnClick: true,
                    closeOnOutsideClick: true }, 'error', 500000
                  );
                }
              );
            } else {
              notify({ message: 'La Solicitud debe estar en Digitado', width: 'auto', shading: false}, 'warning', 1500);
            }
          }
        },
        {
          text: 'No',
          onClick: (e: any) => false
        }]
      });
      confirApli.show().then((dialogResult: any) => {});
    } else {
      notify(
        {
          message: 'Debe seleccionar un registro ',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }
  }
}
