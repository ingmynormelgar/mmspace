import { NgModule } from '@angular/core';
import { PlaSoliConstanciaRoutingModule } from './pla-soli-constancia-routing.module';

@NgModule({
	imports: [
		PlaSoliConstanciaRoutingModule
	]
})
export class PlaSoliConstanciaModule { }