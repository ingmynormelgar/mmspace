/* eslint-disable @typescript-eslint/naming-convention */
export interface PlaSoliConstancia {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_EMPLEADO: number;
  NOMBRE_EMPLEADO: string;
	CORR_CONSTANCIA: number;
	FECHA_SOLICITUD: Date;
	CORR_TIPO_CONSTANCIA: number;
  NOMBRE_TIPO_CONSTANCIA: string;
	USUARIO_SOLICITA: string;
	USUARIO_AUTORIZA: string;
	ESTADO_CONSTANCIA: string;
  NOMBRE_ESTADO_CONSTANCIA: string;
	OBSERVACIONES: string;
	USUARIO_CREA: string;
	FECHA_CREA: Date;
	ESTACION_CREA: string;
	USUARIO_ACTU: string;
	FECHA_ACTU: Date;
	ESTACION_ACTU: string;
}
