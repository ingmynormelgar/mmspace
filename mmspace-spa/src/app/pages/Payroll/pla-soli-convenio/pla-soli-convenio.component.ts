/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { custom } from 'devextreme/ui/dialog';
import { locale, loadMessages } from 'devextreme/localization';
import esMessages from 'devextreme/localization/messages/es.json';
import notify from 'devextreme/ui/notify';
import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
import { RowStatus } from 'src/app/shared/models/RowStatus.enum';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { DxDropDownBoxComponent } from 'devextreme-angular/ui/drop-down-box';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { ActivatedRoute } from '@angular/router';

import { AppInfoService } from 'src/app/shared/services/app-info.service';
import { PlaSoliConvenio } from './pla-soli-convenio';
import { PlaSoliConvenioService } from './pla-soli-convenio.service';

@Component({
	selector: 'app-pla-soli-convenio',
	templateUrl: './pla-soli-convenio.component.html',
	styleUrls: ['./pla-soli-convenio.component.scss']
})
export class PlaSoliConvenioComponent implements OnInit {
	@ViewChild('fData', { static: false }) dataForm!: DxFormComponent;
  @ ViewChild('estadoConvenio', { static: false }) estadoConvenio!: DxDropDownBoxComponent;
  @ViewChild('dataEstadoConvenio', { static: false }) dataEstadoConvenio!: DxDataGridComponent;
  @ViewChild('corrEmpleado', { static: false }) corrEmpleado!: DxDropDownBoxComponent;
	@ViewChild('dataEmpleado', { static: false }) dataEmpleado!: DxDataGridComponent;
  @ViewChild('corrTipoConvenio', { static: false }) corrTipoConvenio!: DxDropDownBoxComponent;
	@ViewChild('dataTipoConvenio', { static: false }) dataTipoConvenio!: DxDataGridComponent;
  @ViewChild('corrMoneda', { static: false }) corrMoneda!: DxDropDownBoxComponent;
	@ViewChild('dataMoneda', { static: false }) dataMoneda!: DxDataGridComponent;
  @ViewChild('corrRubro', { static: false }) corrRubro!: DxDropDownBoxComponent;
	@ViewChild('dataRubro', { static: false }) dataRubro!: DxDataGridComponent;

	//#region <Declaraciones>
	tituloVentana = 'Solicitud de Convenio';
	subTituloVentana = '';
	urlOpcion = '/pla-soli-convenio';
	banderaMtto = UpdateType.Browse;
	loadingVisible = false;
	permiteSalir = true;
	permisos = 'ABC';
	permiteAdd = false;
	permiteEdit = false;
	permiteDele = false;
	permitePrint = false;
	models: any;
	modelUpdate: any;
	model: PlaSoliConvenio = {
	  CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
		CORR_EMPLEADO: 0,
    NOMBRE_EMPLEADO: '',
		CORR_CONVENIO: 0,
		FECHA_SOLICITUD: this.appInfoService.getDate(),
		CORR_RUBRO_CONVENIO: 0,
    NOMBRE_RUBRO: '',
		MONTO_CREDITO: 0,
		USUARIO_SOLICITA: '',
		USUARIO_AUTORIZA: '',
		FECHA_ENTREGA: this.appInfoService.getDate(),
		FECHA_VENCIMIENTO: this.appInfoService.getDate(),
		CORR_MONEDA: 0,
    NOMBRE_MONEDA: '',
		ESTADO_CONVENIO: '',
    NOMBRE_ESTADO_CONVENIO: '',
		OBSERVACIONES: '',
		USUARIO_CREA: '',
		FECHA_CREA: new Date(),
		ESTACION_CREA: '',
		USUARIO_ACTU: '',
		FECHA_ACTU: new Date(),
		ESTACION_ACTU: '',
		CORR_RUBRO_FIJO: 0,
		CLASE_CONVENIO: '',
    NOMBRE_CLASE_CONVENIO: '',
		NOMBRE_BENEFICIARIO: '',
		NUMERO_IDENTIFICACION_BENEFICIARIO: '',
    CORR_PERIODICIDAD: 0,
    NOMBRE_PERIODICIDAD: '',
    MONTO_CUOTA: 0,
    NUMERO_CUOTAS: 0,
    FECHA_INICIO: this.appInfoService.getDate(),
    MONTO_RUBRO_TOTAL: 0
	};
	param: any = {
    TIPO_CONSULTA: 1,
		CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
    FECHA_INICIAL: new Date(this.appInfoService.getDate().getFullYear(), this.appInfoService.getDate().getMonth(), 1),
    FECHA_FINAL: this.appInfoService.getDate(),
		OPCION_CONSULTA: 0
	};

  mEstado: any;
  mEmpleado: any;
  mMoneda: any;
  mRubro: any;
  mTipoConvenio: any;
  mMonedaEmpresa: any;
  bloqBeneficiario = true;
  bloqLookup = false;
	//#endregion

	constructor(
		private appInfoService: AppInfoService,
		private service: PlaSoliConvenioService,
		private router: ActivatedRoute,
    private cd: ChangeDetectorRef
	) {
		this.tituloVentana = router.snapshot.data.titulo;
		loadMessages(esMessages);
		locale(this.appInfoService.getLocale);
		this.getPermisos();

		// Metodos como propiedades
		this.getPermiteEditar = this.getPermiteEditar.bind(this);
		this.getPermiteDele = this.getPermiteDele.bind(this);
		this.isBrowse = this.isBrowse.bind(this);
		this.editarClick = this.editarClick.bind(this);
	}

	ngOnInit(): void {
		this.inicializaOpciones();
		this.llenaComboBox();
		this.consultar();
	}

	//#region <Validadores>
	esValido(): boolean {
    if (this.model.FECHA_SOLICITUD === null) {
      notify(
        {
          message:
            'Error, Debe seleccionar una Fecha de Solicitud.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    if (this.model.FECHA_ENTREGA === null) {
      notify(
        {
          message:
            'Error, Debe seleccionar una Fecha de Entrega.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    if (this.model.FECHA_VENCIMIENTO === null) {
      notify(
        {
          message:
            'Error, Debe seleccionar una Fecha de Vencimiento.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.MONTO_CREDITO === 0 ) {
      notify(
        {
          message:
            'Error, Debe ingresar un Credito mayor a 0.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.CORR_MONEDA === 0 || this.model.CORR_MONEDA === null) {
      notify(
        {
          message:
            'Error, Debe seleccionar una Moneda',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.CORR_RUBRO_CONVENIO === 0 || this.model.CORR_RUBRO_CONVENIO === null) {
      notify(
        {
          message:
            'Error, Debe seleccionar un Rubro',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.CLASE_CONVENIO === null) {
      notify(
        {
          message:
            'Error, Debe seleccionar una Clase de Convenio',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if(this.model.CLASE_CONVENIO === 'BN') {
      if (this.model.NOMBRE_BENEFICIARIO === '') {
        notify(
          {
            message:
              'Error, Debe ingresar un Beneficiario',
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true,
          },
          'error',
          500000
        );
        return false;
      }

      if (this.model.NUMERO_IDENTIFICACION_BENEFICIARIO === '') {
        notify(
          {
            message:
              'Error, Debe ingresar un No.Documento',
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true,
          },
          'error',
          500000
        );
        return false;
      }
    }
		return true;
	}
	// #endregion
	//#region <Inicializando Opciones>
	inicializaOpciones() {
		this.getEMPRESA();
	}
	getEMPRESA() {
		// this.dSService.enviarCorrEmpresaObservable.subscribe(empresa => {
		// 	this.param.CORR_EMPRESA = empresa;
		// });
	}

	// #endregion
	//#region <Manejo de Combos>
	llenaComboBox() {
		this.getEstados();
    this.getEmpleados();
    this.getTipoConvenio();
    this.getMonedas();
    this.getMonedaEmpresa();
    this.getRubros();
	}

  getEstados() {
	  this.param.TIPO_CONSULTA = 1;
	  this.param.CORR_LISTA = 1;
	  this.param.OPCION_CONSULTA = 0;
	  this.service.getEstados(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mEstado = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  getEmpleados() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 5;
	  this.service.getEmpleados(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mEmpleado = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
  }

  getTipoConvenio() {
    this.param.TIPO_CONSULTA = 1;
    this.param.CORR_LISTA = 3;
    this.param.OPCION_CONSULTA = 0;
    this.service.getTipoConvenio(this.param).pipe(take(1)).subscribe(
      (model: any[]) => {
        this.mTipoConvenio = model;
      },
      (error: any) => {
        notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
      }
    );
  }

  getMonedas() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 1;
	  this.service.getMonedas(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mMoneda = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
  }

  getMonedaEmpresa() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 2;
	  this.service.getMonedaEmpresas(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mMonedaEmpresa = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}


  getRubros() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 3;
	  this.service.getRubros(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mRubro = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
  }

  setMontoMaximo(vCORR_EMPLEADO: number) {
    this.param.CORR_EMPLEADO = vCORR_EMPLEADO;
	  this.service.getMontoMaximo(this.param).pipe(take(1)).subscribe(
	    (montoMaximo: number) => {
        this.model.MONTO_CREDITO = montoMaximo;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}


	//#endregion

	//#region <Metodos Browse>
	isBrowse(): boolean {
		if (this.banderaMtto === UpdateType.Browse) {
			return true;
	}
		return false;
	}

	isForm(): boolean {
		if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
			return true;
		}
		return false;
	}

	getPermisos() {
		this.permiteAdd = false;
		this.permiteEdit = false;
		this.permiteDele = false;
		this.permitePrint = false;
		this.permisos = this.appInfoService.getPermiso(this.urlOpcion);
		if (this.permisos.includes('C')) {
			this.permiteAdd = true;
		}
		if (this.permisos.includes('U')) {
			this.permiteEdit = true;
		}
		if (this.permisos.includes('D')) {
			this.permiteDele = true;
		}
	}

	getPermiteEditar(e: any) {
		if (this.permiteEdit) {
      if (e.row.data.ESTADO_CONVENIO === 'DI') {
        return true;
        }
		}
		return false;
	}

	getPermiteDele(e: any) {
    if (e.row.data.ESTADO_CONVENIO === 'DI') {
      if (this.permiteDele) {
        return true;
      }
    }
		return false;
	}

	focusedRowChanged(e: any) {
		this.model = e.row.data;
	}
	//#endregion

	//#region <Metodos Mtto>
	consultar() {
		this.param.TIPO_CONSULTA = 1;
    this.param.CORR_SUSCRIPCION = this.appInfoService.CORR_SUSCRIPCION;
		this.param.CORR_CONFI_PAIS = this.appInfoService.CORR_CONFI_PAIS;
		this.param.CORR_EMPRESA = this.appInfoService.CORR_EMPRESA;
    this.param.OPCION_CONSULTA = 1;
    localStorage.setItem('FechaInicial', this.param.FECHA_INICIAL.toISOString());
    localStorage.setItem('FechaFinal', this.param.FECHA_FINAL.toISOString());
		this.service.getAll(this.param).pipe(take(1)).subscribe((model: any[]) => {
			this.models = model;
		});
	}

	nuevo(): void {
		this.model = {
      CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
			CORR_EMPLEADO: this.mEmpleado[0].CORR_EMPLEADO,
      NOMBRE_EMPLEADO: '',
			CORR_CONVENIO: 0,
			FECHA_SOLICITUD: this.appInfoService.getDate(),
			CORR_RUBRO_CONVENIO: this.mRubro[0].CORR_RUBRO,
      NOMBRE_RUBRO: '',
			MONTO_CREDITO: 0,
			USUARIO_SOLICITA: '',
			USUARIO_AUTORIZA: '',
			FECHA_ENTREGA: this.appInfoService.getDate(),
			FECHA_VENCIMIENTO: this.appInfoService.getDate(),
			CORR_MONEDA: this.mMonedaEmpresa[0].CORR_MONEDA,
      NOMBRE_MONEDA: '',
			ESTADO_CONVENIO: 'DI',
      NOMBRE_ESTADO_CONVENIO: '',
			OBSERVACIONES: '',
			USUARIO_CREA: '',
			FECHA_CREA: new Date(),
			ESTACION_CREA: '',
			USUARIO_ACTU: '',
			FECHA_ACTU: new Date(),
			ESTACION_ACTU: '',
			CORR_RUBRO_FIJO: 0,
			CLASE_CONVENIO: 'PR',
      NOMBRE_CLASE_CONVENIO: '',
			NOMBRE_BENEFICIARIO: '',
			NUMERO_IDENTIFICACION_BENEFICIARIO: '',
      CORR_PERIODICIDAD: 0,
      NOMBRE_PERIODICIDAD: '',
      MONTO_CUOTA: 0,
      NUMERO_CUOTAS: 0,
      FECHA_INICIO: this.appInfoService.getDate(),
      MONTO_RUBRO_TOTAL: 0
		};
    this.modelUpdate = {
			CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.model.CORR_EMPRESA,
			CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
			CORR_CONVENIO: this.model.CORR_CONVENIO,
			FECHA_SOLICITUD: this.model.FECHA_SOLICITUD,
			CORR_RUBRO_CONVENIO: this.model.CORR_RUBRO_CONVENIO,
      NOMBRE_RUBRO: this.model.NOMBRE_RUBRO,
			MONTO_CREDITO: this.model.MONTO_CREDITO,
			USUARIO_SOLICITA: this.model.USUARIO_SOLICITA,
			USUARIO_AUTORIZA: this.model.USUARIO_AUTORIZA,
			FECHA_ENTREGA: this.model.FECHA_ENTREGA,
			FECHA_VENCIMIENTO: this.model.FECHA_VENCIMIENTO,
			CORR_MONEDA: this.model.CORR_MONEDA,
      NOMBRE_MONEDA: this.model.NOMBRE_MONEDA,
			ESTADO_CONVENIO: this.model.ESTADO_CONVENIO,
      NOMBRE_ESTADO_CONVENIO: this.model.NOMBRE_ESTADO_CONVENIO,
			OBSERVACIONES: this.model.OBSERVACIONES,
			USUARIO_CREA: this.model.USUARIO_CREA,
			FECHA_CREA: this.model.FECHA_CREA,
			ESTACION_CREA: this.model.ESTACION_CREA,
			USUARIO_ACTU: this.model.USUARIO_ACTU,
			FECHA_ACTU: this.model.FECHA_ACTU,
			ESTACION_ACTU: this.model.ESTACION_ACTU,
			CORR_RUBRO_FIJO: this.model.CORR_RUBRO_FIJO,
			CLASE_CONVENIO: this.model.CLASE_CONVENIO,
      NOMBRE_CLASE_CONVENIO: this.model.NOMBRE_CLASE_CONVENIO,
			NOMBRE_BENEFICIARIO: this.model.NOMBRE_BENEFICIARIO,
			NUMERO_IDENTIFICACION_BENEFICIARIO: this.model.NUMERO_IDENTIFICACION_BENEFICIARIO,
      CORR_PERIODICIDAD: this.model.CORR_PERIODICIDAD,
      NOMBRE_PERIODICIDAD: this.model.NOMBRE_PERIODICIDAD,
      MONTO_CUOTA: this.model.MONTO_CUOTA,
      NUMERO_CUOTAS: this.model.NUMERO_CUOTAS,
      FECHA_INICIO: this.model.FECHA_INICIO,
      MONTO_RUBRO_TOTAL: this.model.MONTO_RUBRO_TOTAL
		};
    this.setMontoMaximo(this.model.CORR_EMPLEADO);
    this.permiteSalir = false;
    this.bloqLookup = false;
		this.banderaMtto = UpdateType.Add;
		this.subTituloVentana = RowStatus.Add.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('NOMBRE_CONVENIO')?.focus();
		});
	}

	editarClick(e: any) {
		e.event.preventDefault();
		this.modelUpdate = {
			CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.model.CORR_EMPRESA,
			CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
			CORR_CONVENIO: this.model.CORR_CONVENIO,
			FECHA_SOLICITUD: this.model.FECHA_SOLICITUD,
			CORR_RUBRO_CONVENIO: this.model.CORR_RUBRO_CONVENIO,
      NOMBRE_RUBRO: this.model.NOMBRE_RUBRO,
			MONTO_CREDITO: this.model.MONTO_CREDITO,
			USUARIO_SOLICITA: this.model.USUARIO_SOLICITA,
			USUARIO_AUTORIZA: this.model.USUARIO_AUTORIZA,
			FECHA_ENTREGA: this.model.FECHA_ENTREGA,
			FECHA_VENCIMIENTO: this.model.FECHA_VENCIMIENTO,
			CORR_MONEDA: this.model.CORR_MONEDA,
      NOMBRE_MONEDA: this.model.NOMBRE_MONEDA,
			ESTADO_CONVENIO: this.model.ESTADO_CONVENIO,
      NOMBRE_ESTADO_CONVENIO: this.model.NOMBRE_ESTADO_CONVENIO,
			OBSERVACIONES: this.model.OBSERVACIONES,
			USUARIO_CREA: this.model.USUARIO_CREA,
			FECHA_CREA: this.model.FECHA_CREA,
			ESTACION_CREA: this.model.ESTACION_CREA,
			USUARIO_ACTU: this.model.USUARIO_ACTU,
			FECHA_ACTU: this.model.FECHA_ACTU,
			ESTACION_ACTU: this.model.ESTACION_ACTU,
			CORR_RUBRO_FIJO: this.model.CORR_RUBRO_FIJO,
			CLASE_CONVENIO: this.model.CLASE_CONVENIO,
      NOMBRE_CLASE_CONVENIO: this.model.NOMBRE_CLASE_CONVENIO,
			NOMBRE_BENEFICIARIO: this.model.NOMBRE_BENEFICIARIO,
			NUMERO_IDENTIFICACION_BENEFICIARIO: this.model.NUMERO_IDENTIFICACION_BENEFICIARIO,
      CORR_PERIODICIDAD: this.model.CORR_PERIODICIDAD,
      NOMBRE_PERIODICIDAD: this.model.NOMBRE_PERIODICIDAD,
      MONTO_CUOTA: this.model.MONTO_CUOTA,
      NUMERO_CUOTAS: this.model.NUMERO_CUOTAS,
      FECHA_INICIO: this.model.FECHA_INICIO,
      MONTO_RUBRO_TOTAL: this.model.MONTO_RUBRO_TOTAL
		};
    this.bloqLookup = false;
		this.permiteSalir = false;
		this.banderaMtto = UpdateType.Update;
		this.subTituloVentana = RowStatus.Update.toString();
		setTimeout(() => {
      this.mostrarBeneficiario();
			this.dataForm.instance.getEditor('NOMBRE_CONVENIO')?.focus();
		});
	}

	guardar(): void {
		if (!this.esValido()) {
			return;
		}

		this.loadingVisible = true;
		if (this.banderaMtto === UpdateType.Add) {
				this.service.insert(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.models.push(newModel);
					this.model = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro creado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify({ message: error, width: 'auto', shading: false,closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
					this.loadingVisible = false;
				}
			);
		} else
		if (this.banderaMtto === UpdateType.Update) {
			this.service.update(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.model = newModel;
					const vIndex = this.models.findIndex((item: any) => item.CORR_CONVENIO === newModel.CORR_CONVENIO);
					this.models[vIndex] = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro modificado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify({ message: error, width: 'auto', shading: false,closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
					this.loadingVisible = false;
				}
			);
		}
	}

	cancelar(): void {
    const cancelRow = () => {
      this.permiteSalir = true;
      this.bloqLookup = false;
      this.banderaMtto = UpdateType.Browse;
      this.subTituloVentana = RowStatus.Not_Defined.toString();
      this.getPermisos();
    };
    if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
      const confirmacion = custom({
        title: 'Confirmación de Cancelar',
        messageHtml: '¿Quieres cancelar y perder los cambios realizados?',
        buttons: [{
          text: 'Si', onClick: (e: any) => true
        }, {
          text: 'No', onClick: (e: any) => false
        }]
      });

      confirmacion.show().then((cancel: boolean) => {
        if (cancel) {
          this.model = this.modelUpdate;
          const vIndex = this.models.findIndex((item: any) => item.CORR_CONVENIO === this.modelUpdate.CORR_CONVENIO);
          this.models[vIndex] = this.modelUpdate;
          cancelRow();
        }
      });
    } else {
      cancelRow();
    }
	}

	rowRemoving(e: any) {
    this.param.CORR_EMPLEADO=e.data.CORR_EMPLEADO;
		this.service.delete(e.data.CORR_CONVENIO, this.param).pipe(take(1)).subscribe(
			() => {
				notify({ message: 'Registro eliminado con exito!', width: 'auto', shading: false }, 'success', 1500);
				e.component.refresh();
			},
			(error: any) => {
				e.cancel = true;
				notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true }, 'error', 500000);
			}
		);
	}

	rowDblClick(e: any) {
		this.banderaMtto = UpdateType.Not_Defined;
		this.subTituloVentana = RowStatus.Browse.toString();
		setTimeout(() => {
      this.mostrarBeneficiario();
			this.bloquear();
		});
	}

	bloquear(): void {
		this.dataForm.instance.getEditor('CORR_EMPLEADO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_CONVENIO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_SOLICITUD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_RUBRO_CONVENIO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('MONTO_CREDITO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('USUARIO_SOLICITA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('USUARIO_AUTORIZA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_ENTREGA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_VENCIMIENTO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_MONEDA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('ESTADO_CONVENIO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('OBSERVACIONES')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_RUBRO_FIJO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CLASE_CONVENIO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('NOMBRE_BENEFICIARIO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('NUMERO_IDENTIFICACION_BENEFICIARIO')?.option('readOnly', true);
    this.bloqLookup = true;
	}

	permitirSalir():
		| boolean
		| import('rxjs').Observable<boolean>
		| Promise<boolean> {
		if (this.permiteSalir) {
			return true;
		}
		const confirmacion = custom({
			title: 'Confirmación de Salida',
			messageHtml: '¿Quieres salir del formulario y perder los cambios realizados?',
			buttons: [{
				text: 'Si',
				onClick: (e: any) => true
			},
			{
				text: 'No',
				onClick: (e: any) => false
			}]

		});

		return confirmacion.show().then(() => {});
	}
	//#endregion

  //region Metodos Lookups
  valueChangedESTADO_CONVENIO(e: any) {
    if (this.model.ESTADO_CONVENIO === null) {
      this.dataEstadoConvenio.instance.clearSelection();
    }
  }

  selectionChangedESTADO_CONVENIO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.ESTADO_CONVENIO = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickESTADO_CONVENIO(e: any, data: any) {
    this.estadoConvenio.instance.close();
    this.estadoConvenio.instance.focus();
  }

  valueChangedCORR_EMPLEADO(e: any) {
    if (this.model.CORR_EMPLEADO === null) {
      this.dataEmpleado.instance.clearSelection();
    }
  }

  selectionChangedCORR_EMPLEADO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_EMPLEADO = selectedRowKeys[0].CORR_EMPLEADO;
    }
  }

  rowClickCORR_EMPLEADO(e: any, data: any) {
    this.corrEmpleado.instance.close();
    this.corrEmpleado.instance.focus();
  }

  valueChangedCORR_TIPO_CONVENIO(e: any) {
    this.mostrarBeneficiario();
    this.cd.detectChanges();
  }

  mostrarBeneficiario() {
    if(this.model.CLASE_CONVENIO === 'PR') {
      this.bloqBeneficiario = true;
    } else if(this.model.CLASE_CONVENIO === 'BN') {
      this.bloqBeneficiario = false;
    }
  }

  selectionChangedCORR_TIPO_CONVENIO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CLASE_CONVENIO = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickCORR_TIPO_CONVENIO(e: any, data: any) {
    this.corrTipoConvenio.instance.close();
    //this.corrTipoConvenio.instance.focus();
  }

  valueChangedCORR_MONEDA(e: any) {
    if (this.model.CORR_MONEDA === null) {
      this.dataMoneda.instance.clearSelection();
    }
  }

  selectionChangedCORR_MONEDA(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_MONEDA = selectedRowKeys[0].CORR_MONEDA;
    }
  }

  rowClickCORR_MONEDA(e: any, data: any) {
    this.corrMoneda.instance.close();
    this.corrMoneda.instance.focus();
  }

  valueChangedCORR_RUBRO_CONVENIO(e: any) {
    if (this.model.CORR_RUBRO_CONVENIO === null) {
      this.dataRubro.instance.clearSelection();
    }
  }

  selectionChangedCORR_RUBRO_CONVENIO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_RUBRO_CONVENIO = selectedRowKeys[0].CORR_RUBRO;
    }
  }

  rowClickCORR_RUBRO_CONVENIO(e: any, data: any) {
    this.corrRubro.instance.close();
    this.corrRubro.instance.focus();
  }
  //#endregion

  Solicitar(): void {
    if (this.model.ESTADO_CONVENIO !== '') {
      const confirApli = custom({
        title: 'Confirmación de Solicitud',
        messageHtml: '¿Realmente desea Sicitar este Convenio?',
        buttons: [{
          text: 'Si',
          onClick: (e: any) => {
            if (this.model.ESTADO_CONVENIO === 'DI') {
              this.service.solicitar(this.model).subscribe(
                (newModel: any) => {
                  this.model = newModel;
                  const vIndex = this.models.findIndex((item: any) => item.CORR_CONVENIO === newModel.CORR_CONVENIO);
                  this.models[vIndex] = newModel;
                  notify({ message: 'Registro Solicitado con exito', width: 'auto', shading: false}, 'success', 1500);
                },
                (error: any) => {
                  notify({ message: error,
                    width: 'auto',
                    shading: false,
                    closeOnClick: true,
                    closeOnOutsideClick: true }, 'error', 500000
                  );
                }
              );
            } else {
              notify({ message: 'La Solicitud debe estar en Digitado', width: 'auto', shading: false}, 'warning', 1500);
            }
          }
        },
        {
          text: 'No',
          onClick: (e: any) => false
        }]
      });
      confirApli.show().then((dialogResult: any) => {});
    } else {
      notify(
        {
          message: 'Debe seleccionar un registro ',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }
  }
}
