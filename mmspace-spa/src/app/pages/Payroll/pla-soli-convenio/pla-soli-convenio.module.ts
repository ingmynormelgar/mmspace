import { NgModule } from '@angular/core';
import { PlaSoliConvenioRoutingModule } from './pla-soli-convenio-routing.module';

@NgModule({
	imports: [
		PlaSoliConvenioRoutingModule
	]
})
export class PlaSoliConvenioModule { }