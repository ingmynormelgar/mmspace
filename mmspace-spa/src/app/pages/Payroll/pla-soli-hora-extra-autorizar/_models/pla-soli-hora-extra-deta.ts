import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
/* eslint-disable @typescript-eslint/naming-convention */
export interface PlaSoliHoraExtraDeta {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_EMPLEADO: number;
	CORR_SOLICITUD: number;
	CORR_SOLICITUD_DETA: number;
	CORR_RUBRO: number;
	FECHA_INICIAL_SOLI: Date;
  FECHA_INICIAL_SOLI_H: Date;
	FECHA_FINAL_SOLI: Date;
	TOTAL_HORAS_SOLI: number;
  TOTAL_MINUTOS_SOLI: number;
  FECHA_INICIAL_AUT: Date;
  FECHA_INICIAL_AUT_H: Date;
	FECHA_FINAL_AUT: Date;
	TOTAL_HORAS_AUT: number;
  TOTAL_MINUTOS_AUT: number;
  FECHA_INICIAL: Date;
  FECHA_INICIAL_H: Date;
	FECHA_FINAL: Date;
	TOTAL_HORAS: number;
  TOTAL_MINUTOS: number;
	ANIO_PERIODO: number;
	MES_PERIODO: number;
	CORR_RUBRO_MES: number;
  MTTO: UpdateType;
}
