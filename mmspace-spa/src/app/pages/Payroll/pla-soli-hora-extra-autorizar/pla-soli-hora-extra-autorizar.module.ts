import { NgModule } from '@angular/core';
import { PlaSoliHoraExtraAutorizarRoutingModule } from './pla-soli-hora-extra-autorizar-routing.module';

@NgModule({
	imports: [
		PlaSoliHoraExtraAutorizarRoutingModule
	]
})
export class PlaSoliHoraExtraAutorizarModule { }