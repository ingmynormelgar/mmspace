import {PlaSoliHoraExtraDeta} from './pla-soli-hora-extra-deta';
/* eslint-disable @typescript-eslint/naming-convention */
export interface PlaSoliHoraExtra {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_EMPLEADO: number;
  NOMBRE_EMPLEADO: string;
	CORR_SOLICITUD: number;
	FECHA_SOLICITUD: Date;
	USUARIO_AUTORIZA: string;
	ESTADO_SOLICITUD: string;
  NOMBRE_ESTADO_SOLICITUD: string;
	OBSERVACIONES: string;
	USUARIO_CREA: string;
	FECHA_CREA: Date;
	ESTACION_CREA: string;
	USUARIO_ACTU: string;
	FECHA_ACTU: Date;
	ESTACION_ACTU: string;
	CORR_MONEDA: number;
  NOMBRE_MONEDA: string;
  SOLI_HORA_EXTRA_DETA: PlaSoliHoraExtraDeta[];
}
