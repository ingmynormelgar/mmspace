/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, ViewChild } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { custom } from 'devextreme/ui/dialog';
import { locale, loadMessages } from 'devextreme/localization';
import esMessages from 'devextreme/localization/messages/es.json';
import notify from 'devextreme/ui/notify';
import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
import { RowStatus } from 'src/app/shared/models/RowStatus.enum';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { ActivatedRoute } from '@angular/router';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { DxDropDownBoxComponent } from 'devextreme-angular/ui/drop-down-box';


import { AppInfoService } from 'src/app/shared/services/app-info.service';
import { PlaSoliHoraExtra } from './_models/pla-soli-hora-extra';
import { PlaSoliHoraExtraService } from './pla-soli-hora-extra.service';
@Component({
	selector: 'app-pla-soli-hora-extra',
	templateUrl: './pla-soli-hora-extra.component.html',
	styleUrls: ['./pla-soli-hora-extra.component.scss']
})
export class PlaSoliHoraExtraComponent implements OnInit {
	@ViewChild('fData', { static: false }) dataForm!: DxFormComponent;
	@ViewChild('estadoSolicitud', { static: false }) estadoSolicitud!: DxDropDownBoxComponent;
 	@ViewChild('dataEstadoSolicitud', { static: false }) dataEstadoSolicitud!: DxDataGridComponent;
  @ViewChild('corrEmpleado', { static: false }) corrEmpleado!: DxDropDownBoxComponent;
  @ViewChild('dataEmpleado', { static: false }) dataEmpleado!: DxDataGridComponent;
	@ViewChild('corrTipoMoneda', { static: false }) corrTipoMoneda!: DxDropDownBoxComponent;
	@ViewChild('dataTipoMoneda', { static: false }) dataTipoMoneda!: DxDataGridComponent;
  @ViewChild('fDataDeta', { static: false }) dataDetaForm!: DxFormComponent;
  @ViewChild('fDataDeta2', { static: false }) dataDeta2Form!: DxFormComponent;
  @ViewChild('corrRubro', { static: false }) corrRubro!: DxDropDownBoxComponent;
	@ViewChild('dataRubro', { static: false }) dataRubro!: DxDataGridComponent;

	//#region <Declaraciones>
	tituloVentana = 'Soli Hora Extra';
	subTituloVentana = '';
	urlOpcion = '/pla-soli-hora-extra';
	banderaMtto = UpdateType.Browse;
	loadingVisible = false;
	permiteSalir = true;
	permisos = 'ABC';
	permiteAdd = false;
	permiteEdit = false;
	permiteDele = false;
	permitePrint = false;
  bloqLookups = false;
	models: any;
	modelUpdate: any;
	model: PlaSoliHoraExtra = {
		CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
		CORR_EMPLEADO: 0,
    NOMBRE_EMPLEADO: '',
		CORR_SOLICITUD: 0,
		FECHA_SOLICITUD: this.appInfoService.getDate(),
		USUARIO_AUTORIZA: '',
		ESTADO_SOLICITUD: '',
    NOMBRE_ESTADO_SOLICITUD: '',
		OBSERVACIONES: '',
		USUARIO_CREA: '',
		FECHA_CREA: new Date(),
		ESTACION_CREA: '',
		USUARIO_ACTU: '',
		FECHA_ACTU: new Date(),
		ESTACION_ACTU: '',
		CORR_MONEDA: 0,
    NOMBRE_MONEDA: '',
    SOLI_HORA_EXTRA_DETA:[{
      CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		  CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		  CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
      CORR_EMPLEADO: 0,
      CORR_SOLICITUD: 0,
      CORR_SOLICITUD_DETA: 0,
      CORR_RUBRO: 0,
      FECHA_INICIAL_SOLI: this.appInfoService.getDate(),
      FECHA_INICIAL_SOLI_H: this.appInfoService.getDate(),
      FECHA_FINAL_SOLI: this.appInfoService.getDate(),
      TOTAL_HORAS_SOLI: 0,
      TOTAL_MINUTOS_SOLI: 0,
      FECHA_INICIAL_AUT: this.appInfoService.getDate(),
      FECHA_INICIAL_AUT_H: this.appInfoService.getDate(),
      FECHA_FINAL_AUT: this.appInfoService.getDate(),
      TOTAL_HORAS_AUT: 0,
      TOTAL_MINUTOS_AUT: 0,
      FECHA_INICIAL: this.appInfoService.getDate(),
      FECHA_INICIAL_H: this.appInfoService.getDate(),
      FECHA_FINAL: this.appInfoService.getDate(),
      TOTAL_HORAS: 0,
      TOTAL_MINUTOS: 0,
      ANIO_PERIODO: 0,
      MES_PERIODO: 0,
      CORR_RUBRO_MES: 0,
      MTTO: UpdateType.Not_Defined
    }]
	};
	param: any = {
    CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
    CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
    FECHA_INICIAL: new Date(this.appInfoService.getDate().getFullYear(), this.appInfoService.getDate().getMonth(), 1),
    FECHA_FINAL: this.appInfoService.getDate(),
    CORR_EMPLEADO: 0,
    CORR_SOLICITUD: 0
	};

  mEstado: any;
  mEmpleado: any;
  mTipoMoneda: any;
  mMonedaEmpresa: any;
  mRubro: any;

  mostrarAut = false;
  mostrarReal = false;

  vCorrDeta = 0;
	//#endregion

	constructor(
		private appInfoService: AppInfoService,
		private service: PlaSoliHoraExtraService,
		private router: ActivatedRoute
	) {
		this.tituloVentana = router.snapshot.data.titulo;
		loadMessages(esMessages);
		locale(this.appInfoService.getLocale);
		this.getPermisos();

		// Metodos como propiedades
		this.getPermiteEditar = this.getPermiteEditar.bind(this);
		this.getPermiteDele = this.getPermiteDele.bind(this);
		this.isBrowse = this.isBrowse.bind(this);
		this.editarClick = this.editarClick.bind(this);
	}

	ngOnInit(): void {
		this.inicializaOpciones();
		this.llenaComboBox();
		this.consultar();
	}

	//#region <Validadores>
	esValido(): boolean {
		//Validando y devolviendo falso si no cumple una validacion
		return true;
	}
	// #endregion
	//#region <Inicializando Opciones>
	inicializaOpciones() {
		this.getEMPRESA();
	}
	getEMPRESA() {
		// this.dSService.enviarCorrEmpresaObservable.subscribe(empresa => {
		// 	this.param.CORR_EMPRESA = empresa;
		// });
	}

	// #endregion
	//#region <Manejo de Combos>
	llenaComboBox() {
		this.getEstado();
    this.getEmpleado();
    this.getMoneda();
    this.getMonedaEmpresa();
    this.getRubro();
  }

    getEstado() {
      this.service.getEstados(this.param).pipe(take(1)).subscribe(
        (model: any[]) => {
          this.mEstado = model;
        },
        (error: any) => {
          notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true
            },
            'error',
            500000
          );
        }
      );
    }

    getEmpleado() {
      this.service.getEmpleados(this.param).pipe(take(1)).subscribe(
        (model: any[]) => {
          this.mEmpleado = model;
        },
        (error: any) => {
          notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true
            },
            'error',
            500000
          );
        }
      );
    }

    getMoneda() {
      this.service.getTipoMonedas(this.param).pipe(take(1)).subscribe(
        (model: any[]) => {
          this.mTipoMoneda = model;
        },
        (error: any) => {
          notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true
            },
            'error',
            500000
          );
        }
      );
    }

    getMonedaEmpresa() {
      this.service.getMonedaEmpresas(this.param).pipe(take(1)).subscribe(
        (model: any[]) => {
          this.mMonedaEmpresa = model;
        },
        (error: any) => {
          notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true
            },
            'error',
            500000
          );
        }
      );
    }

    getRubro() {
      this.service.getRubro(this.param).pipe(take(1)).subscribe(
        (model: any[]) => {
          this.mRubro = model;
        },
        (error: any) => {
          notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true
            },
            'error',
            500000
          );
        }
      );
    }
	//#endregion

	//#region <Metodos Browse>
	isBrowse(): boolean {
		if (this.banderaMtto === UpdateType.Browse) {
			return true;
	}
		return false;
	}

	isForm(): boolean {
		if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
			return true;
		}
		return false;
	}

	getPermisos() {
		this.permiteAdd = false;
		this.permiteEdit = false;
		this.permiteDele = false;
		this.permitePrint = false;
		this.permisos = this.appInfoService.getPermiso(this.urlOpcion);
		if (this.permisos.includes('C')) {
			this.permiteAdd = true;
		}
		if (this.permisos.includes('U')) {
			this.permiteEdit = true;
		}
		if (this.permisos.includes('D')) {
			this.permiteDele = true;
		}
	}

	getPermiteEditar(e: any) {
		if (this.permiteEdit) {
			if (e.row.data.ESTADO_SOLICITUD === 'DI') {
        return true;
        }
		}
		return false;
	}

	getPermiteDele(e: any) {
		if (this.permiteDele) {
			if (e.row.data.ESTADO_SOLICITUD === 'DI') {
        return true;
      }
		}
		return false;
	}

	focusedRowChanged(e: any) {
		this.model = e.row.data;
	}
	//#endregion

	//#region <Metodos Mtto>
	consultar() {
    this.param.CORR_SUSCRIPCION = this.appInfoService.CORR_SUSCRIPCION;
		this.param.CORR_CONFI_PAIS = this.appInfoService.CORR_CONFI_PAIS;
		this.param.CORR_EMPRESA = this.appInfoService.CORR_EMPRESA;
    localStorage.setItem('FechaInicial', this.param.FECHA_INICIAL.toISOString());
    localStorage.setItem('FechaFinal', this.param.FECHA_FINAL.toISOString());
		this.service.getAll(this.param).pipe(take(1)).subscribe((model: any[]) => {
			this.models = model;
		});
	}

	nuevo(): void {
		this.permiteSalir = false;
    this.vCorrDeta = 0;
    this.bloqLookups = false;
    this.mostrarAut = false;
    this.mostrarReal = false;
		this.model = {
			CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
			CORR_EMPLEADO: this.mEmpleado[0].CORR_EMPLEADO,
      NOMBRE_EMPLEADO: '',
			CORR_SOLICITUD: 0,
			FECHA_SOLICITUD: this.appInfoService.getDate(),
			USUARIO_AUTORIZA: '',
			ESTADO_SOLICITUD: 'DI',
      NOMBRE_ESTADO_SOLICITUD: '',
			OBSERVACIONES: '',
			USUARIO_CREA: '',
			FECHA_CREA: new Date(),
			ESTACION_CREA: '',
			USUARIO_ACTU: '',
			FECHA_ACTU: new Date(),
			ESTACION_ACTU: '',
			CORR_MONEDA: this.mMonedaEmpresa[0].CORR_MONEDA,
      NOMBRE_MONEDA: '',
      SOLI_HORA_EXTRA_DETA:[{
        CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
        CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
        CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
        CORR_EMPLEADO: this.model.CORR_EMPLEADO,
        CORR_SOLICITUD: this.model.CORR_SOLICITUD,
        CORR_SOLICITUD_DETA: this.vCorrDeta,
        CORR_RUBRO: this.mRubro[0].CORR_RUBRO,
        FECHA_INICIAL_SOLI: this.appInfoService.getDate(),
        FECHA_INICIAL_SOLI_H: this.appInfoService.getDate(),
        FECHA_FINAL_SOLI: this.appInfoService.getDate(),
        TOTAL_HORAS_SOLI: 0,
        TOTAL_MINUTOS_SOLI: 0,
        FECHA_INICIAL_AUT: this.appInfoService.getDate(),
        FECHA_INICIAL_AUT_H: this.appInfoService.getDate(),
        FECHA_FINAL_AUT: this.appInfoService.getDate(),
        TOTAL_HORAS_AUT: 0,
        TOTAL_MINUTOS_AUT: 0,
        FECHA_INICIAL: this.appInfoService.getDate(),
        FECHA_INICIAL_H: this.appInfoService.getDate(),
        FECHA_FINAL: this.appInfoService.getDate(),
        TOTAL_HORAS: 0,
        TOTAL_MINUTOS: 0,
        ANIO_PERIODO: 0,
        MES_PERIODO: 0,
        CORR_RUBRO_MES: 0,
        MTTO: UpdateType.Add
      }]
		};

    this.modelUpdate = {
			CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.model.CORR_EMPRESA,
			CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
			CORR_SOLICITUD: this.model.CORR_SOLICITUD,
			FECHA_SOLICITUD: this.model.FECHA_SOLICITUD,
			USUARIO_AUTORIZA: this.model.USUARIO_AUTORIZA,
			ESTADO_SOLICITUD: this.model.ESTADO_SOLICITUD,
      NOMBRE_ESTADO_SOLICITUD: this.model.NOMBRE_ESTADO_SOLICITUD,
			OBSERVACIONES: this.model.OBSERVACIONES,
			USUARIO_CREA: this.model.USUARIO_CREA,
			FECHA_CREA: this.model.FECHA_CREA,
			ESTACION_CREA: this.model.ESTACION_CREA,
			USUARIO_ACTU: this.model.USUARIO_ACTU,
			FECHA_ACTU: this.model.FECHA_ACTU,
			ESTACION_ACTU: this.model.ESTACION_ACTU,
			CORR_MONEDA: this.model.CORR_MONEDA,
      NOMBRE_MONEDA: this.model.NOMBRE_MONEDA
		};
		this.banderaMtto = UpdateType.Add;
		this.subTituloVentana = RowStatus.Add.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('NOMBRE_SOLICITUD')?.focus();
		});
	}

	editarClick(e: any) {
		e.event.preventDefault();
    this.bloqLookups = false;
    this.consultarDeta();
		this.modelUpdate = {
			CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.model.CORR_EMPRESA,
			CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
			CORR_SOLICITUD: this.model.CORR_SOLICITUD,
			FECHA_SOLICITUD: this.model.FECHA_SOLICITUD,
			USUARIO_AUTORIZA: this.model.USUARIO_AUTORIZA,
			ESTADO_SOLICITUD: this.model.ESTADO_SOLICITUD,
      NOMBRE_ESTADO_SOLICITUD: this.model.NOMBRE_ESTADO_SOLICITUD,
			OBSERVACIONES: this.model.OBSERVACIONES,
			USUARIO_CREA: this.model.USUARIO_CREA,
			FECHA_CREA: this.model.FECHA_CREA,
			ESTACION_CREA: this.model.ESTACION_CREA,
			USUARIO_ACTU: this.model.USUARIO_ACTU,
			FECHA_ACTU: this.model.FECHA_ACTU,
			ESTACION_ACTU: this.model.ESTACION_ACTU,
			CORR_MONEDA: this.model.CORR_MONEDA,
      NOMBRE_MONEDA: this.model.NOMBRE_MONEDA
		};
		this.permiteSalir = false;
		this.banderaMtto = UpdateType.Update;
		this.subTituloVentana = RowStatus.Update.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('CORR_SOLICITUD')?.focus();
		});
	}

	guardar(): void {
		if (!this.esValido()) {
			return;
		}

		this.loadingVisible = true;
		if (this.banderaMtto === UpdateType.Add) {
				this.service.insert(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.models.push(newModel);
					this.model = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro creado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true,
            },
            'error',
            500000
          );
					this.loadingVisible = false;
				}
			);
		} else
		if (this.banderaMtto === UpdateType.Update) {
			this.service.update(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.model = newModel;
					const vIndex = this.models.findIndex((item: any) => item.CORR_SOLICITUD === newModel.CORR_SOLICITUD);
					this.models[vIndex] = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro modificado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true,
            },
            'error',
            500000
          );
					this.loadingVisible = false;
				}
			);
		}
	}

  cancelar(): void {
    const cancelRow = () => {
      this.permiteSalir = true;
      this.banderaMtto = UpdateType.Browse;
      this.subTituloVentana = RowStatus.Not_Defined.toString();
      this.getPermisos();
    };
    if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
      const confirmacion = custom({
        title: 'Confirmación de Cancelar',
        messageHtml: '¿Quieres cancelar y perder los cambios realizados?',
        buttons: [{
          text: 'Si', onClick: (e: any) => true
        }, {
          text: 'No', onClick: (e: any) => false
        }]
      });

      confirmacion.show().then((cancel: boolean) => {
        if (cancel) {
          this.model = this.modelUpdate;
          const vIndex = this.models.findIndex((item: any) => item.CORR_SOLICITUD=== this.modelUpdate.CORR_SOLICITUD);
          this.models[vIndex] = this.modelUpdate;
          this.vCorrDeta = 0;
          this.bloqLookups = false;
          cancelRow();
        }
      });
    } else {
      cancelRow();
    }
	}

	rowRemoving(e: any) {
    this.param.CORR_SUSCRIPCION = this.appInfoService.CORR_SUSCRIPCION;
    this.param.CORR_CONFI_PAIS = this.appInfoService.CORR_CONFI_PAIS;
		this.param.CORR_EMPRESA = this.appInfoService.CORR_EMPRESA;
    this.param.CORR_EMPLEADO = e.data.CORR_EMPLEADO;
		this.service.delete(e.data.CORR_SOLICITUD, this.param).pipe(take(1)).subscribe(
			() => {
				notify({ message: 'Registro eliminado con exito!', width: 'auto', shading: false }, 'success', 1500);
				e.component.refresh();
			},
			(error: any) => {
				e.cancel = true;
				notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true }, 'error', 500000);
			}
		);
	}

	rowDblClick(e: any) {
		this.banderaMtto = UpdateType.Not_Defined;
		this.subTituloVentana = RowStatus.Browse.toString();
    this.consultarDeta();
		setTimeout(() => {
			this.bloquear();
		});
	}

	bloquear(): void {
		this.dataForm.instance.getEditor('CORR_EMPLEADO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_SOLICITUD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_SOLICITUD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('USUARIO_AUTORIZA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('ESTADO_SOLICITUD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('OBSERVACIONES')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_MONEDA')?.option('readOnly', true);
    this.bloqLookups = true;
	}

	permitirSalir():
		| boolean
		| import('rxjs').Observable<boolean>
		| Promise<boolean> {
		if (this.permiteSalir) {
			return true;
		}
		const confirmacion = custom({
			title: 'Confirmación de Salida',
			messageHtml: '¿Quieres salir del formulario y perder los cambios realizados?',
			buttons: [{
				text: 'Si',
				onClick: (e: any) => true
			},
			{
				text: 'No',
				onClick: (e: any) => false
			}]

		});

		return confirmacion.show().then(() => {});
	}

  AgregarDeta(): void {
    this.vCorrDeta++;
    this.model.SOLI_HORA_EXTRA_DETA.push(
      {
      CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		  CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		  CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
      CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      CORR_SOLICITUD: this.model.CORR_SOLICITUD,
      CORR_SOLICITUD_DETA: this.vCorrDeta,
      CORR_RUBRO: this.mRubro[0].CORR_RUBRO,
      FECHA_INICIAL_SOLI: this.appInfoService.getDate(),
      FECHA_INICIAL_SOLI_H: this.appInfoService.getDate(),
      FECHA_FINAL_SOLI: this.appInfoService.getDate(),
      TOTAL_HORAS_SOLI: 0,
      TOTAL_MINUTOS_SOLI: 0,
      FECHA_INICIAL_AUT: this.appInfoService.getDate(),
      FECHA_INICIAL_AUT_H: this.appInfoService.getDate(),
      FECHA_FINAL_AUT: this.appInfoService.getDate(),
      TOTAL_HORAS_AUT: 0,
      TOTAL_MINUTOS_AUT: 0,
      FECHA_INICIAL: this.appInfoService.getDate(),
      FECHA_INICIAL_H: this.appInfoService.getDate(),
      FECHA_FINAL: this.appInfoService.getDate(),
      TOTAL_HORAS: 0,
      TOTAL_MINUTOS: 0,
      ANIO_PERIODO: 0,
      MES_PERIODO: 0,
      CORR_RUBRO_MES: 0,
      MTTO: UpdateType.Add
      }
    );
  }
	//#endregion
  valueChangedESTADO_SOLICITUD(e: any) {
    if (this.model.ESTADO_SOLICITUD === null) {
      this.dataEstadoSolicitud.instance.clearSelection();
    }
  }

  selectionChangedESTADO_SOLICITUD(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.ESTADO_SOLICITUD = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickESTADO_SOLICITUD(e: any, data: any) {
    this.estadoSolicitud.instance.close();
    this.estadoSolicitud.instance.focus();
  }

  valueChangedCORR_EMPLEADO(e: any) {
    if (this.model.CORR_EMPLEADO === null) {
      this.dataEmpleado.instance.clearSelection();
    }
  }

  selectionChangedCORR_EMPLEADO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_EMPLEADO = selectedRowKeys[0].CORR_EMPLEADO;
    }
  }

  rowClickCORR_EMPLEADO(e: any, data: any) {
    this.corrEmpleado.instance.close();
    this.corrEmpleado.instance.focus();
  }

  valueChangedCORR_MONEDA(e: any) {
    if (this.model.CORR_MONEDA === null) {
      this.dataTipoMoneda.instance.clearSelection();
    }
  }

  selectionChangedCORR_MONEDA(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_MONEDA = selectedRowKeys[0].CORR_MONEDA;
    }
  }

  rowClickCORR_MONEDA(e: any, data: any) {
    this.corrTipoMoneda.instance.close();
    this.corrTipoMoneda.instance.focus();
  }

  selectionChangedCORR_RUBRO(selectedRowKeys: any, i: number) {
    if (selectedRowKeys.length > 0) {
      this.model.SOLI_HORA_EXTRA_DETA[i].CORR_RUBRO = selectedRowKeys[0].CORR_RUBRO;
    }
  }

  rowClickCORR_RUBRO(e: any, cellInfo: any, dropDownBoxComponent: any) {
    dropDownBoxComponent.close();
    dropDownBoxComponent.focus();
  }

  //DETALLE
  consultarDeta(): void {
    if (this.model.ESTADO_SOLICITUD === 'DI') {
      this.mostrarAut = false;
      this.mostrarReal = false;
    } else if (this.model.ESTADO_SOLICITUD === 'SO' || this.model.ESTADO_SOLICITUD === 'AU' || this.model.ESTADO_SOLICITUD === 'DE'
    || this.model.ESTADO_SOLICITUD === 'AN') {
      this.mostrarAut = true;
      this.mostrarReal = false;
    }  else if (this.model.ESTADO_SOLICITUD === 'AP' || this.model.ESTADO_SOLICITUD === 'PA' || this.model.ESTADO_SOLICITUD === 'SA') {
      this.mostrarAut = true;
      this.mostrarReal = true;
    }

    this.param.CORR_EMPLEADO = this.model.CORR_EMPLEADO;
    this.param.CORR_SOLICITUD = this.model.CORR_SOLICITUD;
    this.service.getAllDeta(this.param).pipe(take(1)).subscribe(
      (model: any[]) => {
        this.model.SOLI_HORA_EXTRA_DETA = model;
        const modelDeta = this.model.SOLI_HORA_EXTRA_DETA.sort((a, b) => (a.CORR_SOLICITUD_DETA > b.CORR_SOLICITUD_DETA) ? 1 : -1);
        if (modelDeta.length > 0) {
          this.vCorrDeta = this.model.SOLI_HORA_EXTRA_DETA.length - 1;
          this.vCorrDeta = modelDeta[this.vCorrDeta].CORR_SOLICITUD_DETA;
        }
      }
    );
  }

  fDataDetaRowRemoving(e: any, i: number): void  {
    if (this.model.SOLI_HORA_EXTRA_DETA[i].MTTO !== UpdateType.Add) {
      this.param.CORR_EMPLEADO = this.model.CORR_EMPLEADO;
      this.param.CORR_SOLICITUD = this.model.CORR_SOLICITUD;
      this.service.deleteDeta(this.model.SOLI_HORA_EXTRA_DETA[i].CORR_SOLICITUD_DETA, this.param).pipe(take(1)).subscribe(
        () => {
          this.model.SOLI_HORA_EXTRA_DETA.splice(i,1);
          notify({ message: 'Registro eliminado con exito! ', width: 'auto', shading: false }, 'success', 1500);
        },
        (error: any) => {
          e.cancel = true;
          notify({ message: error.message, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true }, 'error', 500000);
        }
      );
    } else {
      this.model.SOLI_HORA_EXTRA_DETA.splice(i,1);
    }
  }

  fielDataChangedfDataDeta(e: any, i: number) {
    if (this.model.SOLI_HORA_EXTRA_DETA[i].MTTO === UpdateType.Browse) {
      this.model.SOLI_HORA_EXTRA_DETA[i].MTTO = UpdateType.Update;
    } else if (this.model.SOLI_HORA_EXTRA_DETA[i].MTTO === UpdateType.Not_Defined) {
      this.model.SOLI_HORA_EXTRA_DETA[i].MTTO = UpdateType.Add;
    }
    const fInicial = this.appInfoService.getDate(new Date(this.model.SOLI_HORA_EXTRA_DETA[i].FECHA_INICIAL_SOLI_H),
                                                 new Date(this.model.SOLI_HORA_EXTRA_DETA[i].FECHA_INICIAL_SOLI).getHours(),
                                                 new Date(this.model.SOLI_HORA_EXTRA_DETA[i].FECHA_INICIAL_SOLI).getMinutes(),
                                                 0);
    const fFinal = this.appInfoService.getDate(new Date(this.model.SOLI_HORA_EXTRA_DETA[i].FECHA_INICIAL_SOLI_H),
                                               new Date(this.model.SOLI_HORA_EXTRA_DETA[i].FECHA_FINAL_SOLI).getHours(),
                                               new Date(this.model.SOLI_HORA_EXTRA_DETA[i].FECHA_FINAL_SOLI).getMinutes(),
                                               0);
    this.model.SOLI_HORA_EXTRA_DETA[i].FECHA_INICIAL_SOLI = fInicial;
    this.model.SOLI_HORA_EXTRA_DETA[i].FECHA_FINAL_SOLI = fFinal;
    this.model.SOLI_HORA_EXTRA_DETA[i].TOTAL_HORAS_SOLI = this.appInfoService.getHours(fInicial,fFinal);
    this.model.SOLI_HORA_EXTRA_DETA[i].TOTAL_MINUTOS_SOLI = this.appInfoService.getMinutes(fInicial,fFinal) -
    (this.model.SOLI_HORA_EXTRA_DETA[i].TOTAL_HORAS_SOLI  * 60);
  }

  //solicitar
  Solicitar(): void {
    if (this.model.ESTADO_SOLICITUD !== '') {
      const confirApli = custom({
        title: 'Confirmación de Solicitud',
        messageHtml: '¿Quiere Solicitar el siguiente registro?',
        buttons: [{
          text: 'Si',
          onClick: (e: any) => {
            if (this.model.ESTADO_SOLICITUD === 'DI') {
              this.service.solicitar(this.model).subscribe(
                (newModel: any) => {
                  this.model = newModel;
                  const vIndex = this.models.findIndex((item: any) => item.CORR_SOLICITUD === newModel.CORR_SOLICITUD);
                  this.models[vIndex] = newModel;
                  notify({ message: 'Registro Solicitado con exito', width: 'auto', shading: false}, 'success', 1500);
                },
                (error: any) => {
                  notify({ message: error,
                    width: 'auto',
                    shading: false,
                    closeOnClick: true,
                    closeOnOutsideClick: true }, 'error', 500000
                  );
                }
              );
            } else {
              notify({ message: 'La Solicitud debe estar en Digitado', width: 'auto', shading: false}, 'warning', 1500);
            }
          }
        },
        {
          text: 'No',
          onClick: (e: any) => false
        }]
      });
      confirApli.show().then((dialogResult: any) => {});
    } else {
      notify(
        {
          message: 'Debe seleccionar un registro ',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }
  }
}
