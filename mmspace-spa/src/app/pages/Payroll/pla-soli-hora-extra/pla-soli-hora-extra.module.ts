import { NgModule } from '@angular/core';
import { PlaSoliHoraExtraRoutingModule } from './pla-soli-hora-extra-routing.module';

@NgModule({
	imports: [
		PlaSoliHoraExtraRoutingModule
	]
})
export class PlaSoliHoraExtraModule { }