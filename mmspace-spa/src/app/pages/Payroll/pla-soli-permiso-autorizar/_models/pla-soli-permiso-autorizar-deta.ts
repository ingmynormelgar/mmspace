import { UpdateType } from './../../../../shared/models/UpdateType.enum';
/* eslint-disable @typescript-eslint/naming-convention */
export interface PlaSoliPermisoAutorizarDeta {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_EMPLEADO: number;
	CORR_PERMISO: number;
	CORR_PERMISO_DETA: number;
  FECHA_INICIAL_H: Date;
	FECHA_INICIAL: Date;
	FECHA_FINAL: Date;
	TOTAL_DIAS: number;
	TOTAL_HORAS: number;
	TOTAL_MIN: number;
	USUARIO_CREA: string;
	FECHA_CREA: Date;
	ESTACION_CREA: string;
	USUARIO_ACTU: string;
	FECHA_ACTU: Date;
	ESTACION_ACTU: string;
	CORR_ACCION: number;
	CLASE_SOLICITUD_PERMISO_DETA: string;
  MTTO: UpdateType;
  VISIBLE_DIA: boolean;
  CLASE_OPEND: boolean;
}
