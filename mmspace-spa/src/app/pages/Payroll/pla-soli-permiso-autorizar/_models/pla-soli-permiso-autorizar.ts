import { PlaSoliPermisoAutorizarDeta } from './pla-soli-permiso-autorizar-deta';
/* eslint-disable @typescript-eslint/naming-convention */
export interface PlaSoliPermiso {
  CORR_SUSCRIPCION: number;
  CORR_CONFI_PAIS: number;
  CORR_EMPRESA: number;
  CORR_EMPLEADO: number;
  NOMBRE_EMPLEADO: string;
  CORR_PERMISO: number;
  FECHA_SOLICITUD: Date;
  CORR_MOTIVO_PERMISO: number;
  NOMBRE_MOTIVO_PERMISO: string;
  CON_GOCE_SALARIAL: boolean;
  USUARIO_SOLICITA: string;
  USUARIO_AUTORIZA: string;
  ESTADO_PERMISO: string;
  NOMBRE_ESTADO_PERMISO: string;
  OBSERVACIONES: string;
  USUARIO_CREA: string;
  FECHA_CREA: Date;
  ESTACION_CREA: string;
  USUARIO_ACTU: string;
  FECHA_ACTU: Date;
  ESTACION_ACTU: string;
  SOLI_PERMISO_DETA: PlaSoliPermisoAutorizarDeta[];
}
