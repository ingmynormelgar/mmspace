/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { custom } from 'devextreme/ui/dialog';
import { locale, loadMessages } from 'devextreme/localization';
import esMessages from 'devextreme/localization/messages/es.json';
import notify from 'devextreme/ui/notify';
import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
import { RowStatus } from 'src/app/shared/models/RowStatus.enum';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { ActivatedRoute } from '@angular/router';

import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { DxDropDownBoxComponent } from 'devextreme-angular/ui/drop-down-box';

import { AppInfoService } from 'src/app/shared/services/app-info.service';
import { PlaSoliPermiso } from './_models/pla-soli-permiso-autorizar';
import { PlaSoliPermisoAutorizarService } from './pla-soli-permiso-autorizar.service';

@Component({
	selector: 'app-pla-soli-permiso-autorizar',
	templateUrl: './pla-soli-permiso-autorizar.component.html',
	styleUrls: ['./pla-soli-permiso-autorizar.component.scss']
})
export class PlaSoliPermisoAutorizarComponent implements OnInit {
	@ViewChild('fData', { static: false }) dataForm!: DxFormComponent;
	@ViewChild('corrMotivoPermiso', { static: false }) corrMotivoPermiso!: DxDropDownBoxComponent;
  @ViewChild('dataMotivoPermiso', { static: false }) dataMotivoPermiso!: DxDataGridComponent;
  @ViewChild('estadoSolicitud', { static: false }) estadoSolicitud!: DxDropDownBoxComponent;
  @ViewChild('dataEstadoSolicitud', { static: false }) dataEstadoSolicitud!: DxDataGridComponent;
  @ViewChild('corrUsuario', { static: false }) corrUsuario!: DxDropDownBoxComponent;
  @ViewChild('dataUsuario', { static: false }) dataUsuario!: DxDataGridComponent;
  @ViewChild('fDataDeta', { static: false }) dataDetaForm!: DxFormComponent;
  @ViewChild('fDataDeta2', { static: false }) dataDeta2Form!: DxFormComponent;

	//#region <Declaraciones>
	tituloVentana = 'Autorizar Permiso';
	subTituloVentana = '';
	urlOpcion = '/pla-soli-permiso-autorizar';
	banderaMtto = UpdateType.Browse;
	loadingVisible = false;
	permiteSalir = true;
	permisos = 'ABC';
	permiteAdd = false;
	permiteEdit = false;
	permiteDele = false;
	permitePrint = false;
  bloquearTodo = false;
  bloquearDetaSiempre = true;


  count = 1;

  FECHA_INICIAL2 = new Date(2018, 9, 16, 15, 8, 12);
	models: any;
	modelUpdate: any;
	model: PlaSoliPermiso = {
		CORR_SUSCRIPCION: 7,
		CORR_CONFI_PAIS: 7,
		CORR_EMPRESA: 1,
		CORR_EMPLEADO: 0,
    NOMBRE_EMPLEADO: '',
		CORR_PERMISO: 0,
		FECHA_SOLICITUD: new Date(),
		CORR_MOTIVO_PERMISO: 1,
    NOMBRE_MOTIVO_PERMISO: '',
		CON_GOCE_SALARIAL: true,
		USUARIO_SOLICITA: '',
		USUARIO_AUTORIZA: '',
		ESTADO_PERMISO: 'DI',
    NOMBRE_ESTADO_PERMISO: '',
		OBSERVACIONES: '',
		USUARIO_CREA: '',
		FECHA_CREA: new Date(),
		ESTACION_CREA: '',
		USUARIO_ACTU: '',
		FECHA_ACTU: new Date(),
		ESTACION_ACTU: '',
    SOLI_PERMISO_DETA:[{
      CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
      CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
      CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
      CORR_EMPLEADO: 0,
      CORR_PERMISO: 1,
      CORR_PERMISO_DETA: 1,
      FECHA_INICIAL_H: this.appInfoService.getDate(),
      FECHA_INICIAL: this.appInfoService.getDate(),
      FECHA_FINAL: this.appInfoService.getDate(),
      TOTAL_DIAS: 10,
      TOTAL_HORAS: 10,
      TOTAL_MIN: 10,
      USUARIO_CREA: '',
      FECHA_CREA: new Date(),
      ESTACION_CREA: '',
      USUARIO_ACTU: '',
      FECHA_ACTU: new Date(),
      ESTACION_ACTU: '',
      CORR_ACCION: 0,
      CLASE_SOLICITUD_PERMISO_DETA:'DI',
      MTTO: UpdateType.Not_Defined,
      VISIBLE_DIA: true,
      CLASE_OPEND: false
    }]
	};

	param: any = {
		CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
    CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
    CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
    FECHA_INICIAL: new Date(this.appInfoService.getDate().getFullYear(), this.appInfoService.getDate().getMonth(), 1),
    FECHA_FINAL: this.appInfoService.getDate(),
    CORR_EMPLEADO: 0
	};

  mMotivoPermiso: any;
  mEstado: any;
  mUsuario: any;
  mClaseSolicitudDeta: any;
  vCorrDeta = 0;
	//#endregion

	constructor(
		private appInfoService: AppInfoService,
		private service: PlaSoliPermisoAutorizarService,
		private router: ActivatedRoute,
    private cd: ChangeDetectorRef
	) {
		this.tituloVentana = router.snapshot.data.titulo;
		loadMessages(esMessages);
		locale(this.appInfoService.getLocale);
		this.getPermisos();
		// Metodos como propiedades
		this.isBrowse = this.isBrowse.bind(this);
	}

	ngOnInit(): void {
		this.inicializaOpciones();

		this.llenaComboBox();
		this.consultar();
	}

	// #endregion

	//#region <Inicializando Opciones>
	inicializaOpciones() {
		this.getEMPRESA();
	}
	getEMPRESA() {
		// this.dSService.enviarCorrEmpresaObservable.subscribe(empresa => {
		// 	this.param.CORR_EMPRESA = empresa;
		// });
	}

	// #endregion

	//#region <Manejo de Combos>
	llenaComboBox() {
		this.getMotivoPermiso();
    this.getEstado();
    this.getClaseSolicitudDeta();
    this.getEmpleado();
	}

	getMotivoPermiso() {
	  this.param.TIPO_CONSULTA = 1;
	  this.param.OPCION_CONSULTA = 0;
	  this.service.getMotivosPermisosAutorizar(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mMotivoPermiso = model;
	    },
	    (error: any) => {
	      notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
	    }
	  );
	}

  getEstado() {
	  this.service.getEstadosAutorizar(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mEstado = model;
	    },
	    (error: any) => {
	      notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
	    }
	  );
	}

  getEmpleado() {
	  this.service.getEmpleadosAutorizar(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mUsuario = model;
	    },
	    (error: any) => {
	      notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
	    }
	  );
	}

  getClaseSolicitudDeta() {
	  this.service.getClaseSolicitudAutorizar(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mClaseSolicitudDeta = model;
	    },
	    (error: any) => {
	      notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
	    }
	  );
	}
	//#endregion

	//#region <Metodos Browse>
	isBrowse(): boolean {
		if (this.banderaMtto === UpdateType.Browse) {
			return true;
	}
		return false;
	}

	isForm(): boolean {
		if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
			return true;
		}
		return false;
	}

	getPermisos() {
		this.permiteAdd = false;
		this.permiteEdit = false;
		this.permiteDele = false;
		this.permitePrint = false;
		this.permisos = this.appInfoService.getPermiso(this.urlOpcion);
		if (this.permisos.includes('C')) {
			this.permiteAdd = true;
		}
		if (this.permisos.includes('U')) {
			this.permiteEdit = true;
		}
		if (this.permisos.includes('D')) {
			this.permiteDele = true;
		}
	}
	focusedRowChanged(e: any) {
		this.model = e.row.data;
	}
	//#endregion

	//#region <Metodos Mtto>
	consultar() {
    localStorage.setItem('FechaInicial', this.param.FECHA_INICIAL.toISOString());
    localStorage.setItem('FechaFinal', this.param.FECHA_FINAL.toISOString());
		this.service.getAllAutorizar(this.param).pipe(take(1)).subscribe((model: any[]) => {
			this.models = model;
		});
	}

	cancelar(): void {
    const cancelRow = () => {
      this.permiteSalir = true;
      this.banderaMtto = UpdateType.Browse;
      this.subTituloVentana = RowStatus.Not_Defined.toString();
      this.getPermisos();
    };
    if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
      const confirmacion = custom({
        title: 'Confirmación de Cancelar',
        messageHtml: '¿Quieres cancelar y perder los cambios realizados?',
        buttons: [{
          text: 'Si', onClick: (e: any) => true
        }, {
          text: 'No', onClick: (e: any) => false
        }]
      });

      confirmacion.show().then((cancel: boolean) => {
        if (cancel) {
          this.model = this.modelUpdate;
          const vIndex = this.models.findIndex((item: any) => item.CORR_PERMISO === this.modelUpdate.CORR_PERMISO);
          this.models[vIndex] = this.modelUpdate;
          this.vCorrDeta = 0;
          cancelRow();
        }
      });
    } else {
      cancelRow();
    }
	}

	rowDblClick(e: any) {
		this.banderaMtto = UpdateType.Not_Defined;
		this.subTituloVentana = RowStatus.Browse.toString();
    this.consultarDeta();
		setTimeout(() => {
			this.bloquear();
		});
	}

	bloquear(): void {
    this.dataForm.instance.getEditor('CORR_EMPLEADO')?.option('readOnly', true);
    this.dataForm.instance.getEditor('CORR_PERMISO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_SOLICITUD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_MOTIVO_PERMISO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CON_GOCE_SALARIAL')?.option('readOnly', true);
		this.dataForm.instance.getEditor('ESTADO_PERMISO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('OBSERVACIONES')?.option('readOnly', true);
    this.bloquearTodo = true;
    this.bloquearDetaSiempre = true;
	}

	permitirSalir():
		| boolean
		| import('rxjs').Observable<boolean>
		| Promise<boolean> {
		if (this.permiteSalir) {
			return true;
		}
		const confirmacion = custom({
			title: 'Confirmación de Salida',
			messageHtml: '¿Quieres salir del formulario y perder los cambios realizados?',
			buttons: [{
				text: 'Si',
				onClick: (e: any) => true
			},
			{
				text: 'No',
				onClick: (e: any) => false
			}]

		});

		return confirmacion.show().then(() => {});
	}
	//#endregion

  valueChangedCORR_MOTIVO_PERMISO(e: any) {
    if (this.model.CORR_MOTIVO_PERMISO === null) {
      this.dataMotivoPermiso.instance.clearSelection();
    }
  }

  selectionChangedCORR_MOTIVO_PERMISO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_MOTIVO_PERMISO = selectedRowKeys[0].CORR_MOTIVO_PERMISO;
      this.model.CON_GOCE_SALARIAL  = selectedRowKeys[0].CON_GOCE_SALARIAL;
    }
  }

  rowClickCORR_MOTIVO_PERMISO(e: any, data: any) {
    this.corrMotivoPermiso.instance.close();
    this.corrMotivoPermiso.instance.focus();
  }

  valueChangedESTADO_PERMISO(e: any) {
    if (this.model.ESTADO_PERMISO === null) {
      this.dataEstadoSolicitud.instance.clearSelection();
    }
  }

  selectionChangedESTADO_PERMISO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.ESTADO_PERMISO = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickESTADO_PERMISO(e: any, data: any) {
    this.estadoSolicitud.instance.close();
    this.estadoSolicitud.instance.focus();
  }

  valueChangedCORR_USUARIO(e: any) {
    if (this.model.CORR_EMPLEADO === null) {
      this.dataUsuario.instance.clearSelection();
    }
  }

  selectionChangedCORR_USUARIO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_EMPLEADO = selectedRowKeys[0].CORR_EMPLEADO;
    }
  }

  rowClickCORR_USUARIO(e: any, data: any) {
    this.corrUsuario.instance.close();
    this.corrUsuario.instance.focus();
  }

  valueChangedCON_GOCE_SALARIAL(e: any) {
    if (this.model.CON_GOCE_SALARIAL === false) {
      setTimeout(() => {
        this.dataForm.instance.getEditor('CON_GOCE_SALARIAL')?.option('readOnly', true);
      });
    }
  }
  //#region <Metodos Detalle>

  valueChangedCLASE_SOLICITUD_DETA(e: any, i: number) {
    if (e.value === 'DI') {
      this.model.SOLI_PERMISO_DETA[i].VISIBLE_DIA = true;
    }
    else if (e.value === 'HO')
    {
      this.model.SOLI_PERMISO_DETA[i].VISIBLE_DIA = false;
    }
    this.cd.detectChanges();
  }

  selectionChangedCLASE_SOLICITUD_DETA(selectedRowKeys: any, i: number) {
    if (selectedRowKeys.length > 0) {
      this.model.SOLI_PERMISO_DETA[i].CLASE_SOLICITUD_PERMISO_DETA = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickCLASE_SOLICITUD_DETA(e: any, cellInfo: any, dropDownBoxComponent: any) {
    // dropDownBoxComponent.close();
    // dropDownBoxComponent.focus();
  }

  consultarDeta(): void {
    this.param.CORR_EMPLEADO = this.model.CORR_EMPLEADO;
    this.param.CORR_PERMISO = this.model.CORR_PERMISO;
    this.service.getAllDetaAutorizar(this.param).pipe(take(1)).subscribe(
      (model: any[]) => {
        this.model.SOLI_PERMISO_DETA = model;
        const modelDeta = this.model.SOLI_PERMISO_DETA.sort((a, b) => (a.CORR_PERMISO_DETA > b.CORR_PERMISO_DETA) ? 1 : -1);
        if (modelDeta.length > 0) {
          this.vCorrDeta = this.model.SOLI_PERMISO_DETA.length - 1;
          this.vCorrDeta = modelDeta[this.vCorrDeta].CORR_PERMISO_DETA;
        }
      }
    );
  }

  fielDataChangedfDataDeta(e: any, i: number) {

    if (this.model.SOLI_PERMISO_DETA[i].MTTO === UpdateType.Browse) {
      this.model.SOLI_PERMISO_DETA[i].MTTO = UpdateType.Update;
    } else if (this.model.SOLI_PERMISO_DETA[i].MTTO === UpdateType.Not_Defined) {
      this.model.SOLI_PERMISO_DETA[i].MTTO = UpdateType.Add;
    }

      if (this.model.SOLI_PERMISO_DETA[i].CLASE_SOLICITUD_PERMISO_DETA === 'DI') {
        this.model.SOLI_PERMISO_DETA[i].TOTAL_DIAS = this.appInfoService.getDays(new Date(this.model.SOLI_PERMISO_DETA[i].FECHA_INICIAL),
                                                                                 new Date(this.model.SOLI_PERMISO_DETA[i].FECHA_FINAL));
      } else
      if (this.model.SOLI_PERMISO_DETA[i].CLASE_SOLICITUD_PERMISO_DETA === 'HO') {
        const fInicial = this.appInfoService.getDate(new Date(this.model.SOLI_PERMISO_DETA[i].FECHA_INICIAL_H),
                                                    new Date(this.model.SOLI_PERMISO_DETA[i].FECHA_INICIAL).getHours(),
                                                    new Date(this.model.SOLI_PERMISO_DETA[i].FECHA_INICIAL).getMinutes(),
                                                    0);
        const fFinal = this.appInfoService.getDate(new Date(this.model.SOLI_PERMISO_DETA[i].FECHA_INICIAL_H),
                                                  new Date(this.model.SOLI_PERMISO_DETA[i].FECHA_FINAL).getHours(),
                                                  new Date(this.model.SOLI_PERMISO_DETA[i].FECHA_FINAL).getMinutes(),
                                                  0);
        this.model.SOLI_PERMISO_DETA[i].TOTAL_HORAS = this.appInfoService.getHours(fInicial,fFinal);
        this.model.SOLI_PERMISO_DETA[i].TOTAL_MIN = Math.floor(((this.appInfoService.getMinutes(fInicial,fFinal)/ 60)
                                                    - this.model.SOLI_PERMISO_DETA[i].TOTAL_HORAS) * 60);
      }
  }

  onGridBoxOptionChanged(e: any, i: number){
    if (e.name === 'value'){
      this.model.SOLI_PERMISO_DETA[i].CLASE_OPEND = false;
      this.cd.detectChanges();
    }
  }

  //Autorizar
  Autorizar(): void {
    if (this.model.ESTADO_PERMISO !== '') {
      const confirApli = custom({
        title: 'Confirmación de Autorizar',
        messageHtml: '¿Quiere Autorizar este permiso?',
        buttons: [{
          text: 'Si',
          onClick: (e: any) => {
            if (this.model.ESTADO_PERMISO === 'SO') {
              this.service.autorizar(this.model).subscribe(
                (id: number) => {
                  const vIndex = this.models.findIndex((item: any) => item.CORR_PERMISO === id);
                  this.models.splice(vIndex,1);
                  this.banderaMtto = UpdateType.Browse;
                  this.subTituloVentana = RowStatus.Not_Defined.toString();
                  this.loadingVisible = false;
                  this.permiteSalir = true;
                  notify({ message: 'Registro Autorizado con exito', width: 'auto', shading: false}, 'success', 1500);
                },
                (error: any) => {
                  notify({ message: error,
                    width: 'auto',
                    shading: false,
                    closeOnClick: true,
                    closeOnOutsideClick: true }, 'error', 500000
                  );
                }
              );
            } else {
              notify({ message: 'La Solicitud debe estar en Solicitado', width: 'auto', shading: false}, 'warning', 1500);
            }
          }
        },
        {
          text: 'No',
          onClick: (e: any) => false
        }]
      });
      confirApli.show().then((dialogResult: any) => {});
    } else {
      notify(
        {
          message: 'Debe seleccionar un registro ',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }
  }


  Denegar(): void {
    if (this.model.ESTADO_PERMISO !== '') {
      const confirApli = custom({
        title: 'Confirmación Denegar',
        messageHtml: '¿Quiere Denegar este permiso?',
        buttons: [{
          text: 'Si',
          onClick: (e: any) => {
            if (this.model.ESTADO_PERMISO === 'SO') {
              this.service.denegar(this.model).subscribe(
                (id: number) => {
                  const vIndex = this.models.findIndex((item: any) => item.CORR_PERMISO === id);
                  this.models.splice(vIndex,1);
                  this.banderaMtto = UpdateType.Browse;
                  this.subTituloVentana = RowStatus.Not_Defined.toString();
                  this.loadingVisible = false;
                  this.permiteSalir = true;
                  notify({ message: 'Registro Denegado con exito', width: 'auto', shading: false}, 'success', 1500);
                },
                (error: any) => {
                  notify({ message: error,
                    width: 'auto',
                    shading: false,
                    closeOnClick: true,
                    closeOnOutsideClick: true }, 'error', 500000
                  );
                }
              );
            } else {
              notify({ message: 'La Solicitud debe estar en Solicitado', width: 'auto', shading: false}, 'warning', 1500);
            }
          }
        },
        {
          text: 'No',
          onClick: (e: any) => false
        }]
      });
      confirApli.show().then((dialogResult: any) => {});
    } else {
      notify(
        {
          message: 'Debe seleccionar un registro ',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }
  }
}
