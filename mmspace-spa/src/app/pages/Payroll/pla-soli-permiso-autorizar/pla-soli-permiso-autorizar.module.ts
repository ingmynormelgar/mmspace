import { NgModule } from '@angular/core';
import { PlaSoliPermisoAutorizarRoutingModule } from './pla-soli-permiso-autorizar-routing.module';

@NgModule({
	imports: [
		PlaSoliPermisoAutorizarRoutingModule
	]
})
export class PlaSoliPermisoAutorizarModule { }
