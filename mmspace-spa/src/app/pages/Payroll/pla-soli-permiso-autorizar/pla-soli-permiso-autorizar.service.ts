import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class PlaSoliPermisoAutorizarService {
	readonly urlMtto = environment.apiUrl + 'PLA_SOLI_PERMISO/';

	constructor(private http: HttpClient) { }

	getAllAutorizar(param: any): Observable<any[]> {

		let parametros = new HttpParams();

		if (param != null) {
			parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      		parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
			parametros = parametros.append('FECHA_INICIAL', param.FECHA_INICIAL.toISOString());
			parametros = parametros.append('FECHA_FINAL', param.FECHA_FINAL.toISOString());
		}
		return this.http.get<any[]>(this.urlMtto + 'GetAllAutorizar', { params: parametros });
	}

  getMotivosPermisosAutorizar(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
			parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
    }

    return this.http.get<any[]>(this.urlMtto + 'GetMotivosPermisosAutorizar', { params: parametros });
  }

  getEstadosAutorizar(param: any): Observable<any[]> {
    return this.http.get<any[]>(this.urlMtto + 'GetEstadosAutorizar');
  }

  getClaseSolicitudAutorizar(param: any): Observable<any[]> {
    return this.http.get<any[]>(this.urlMtto + 'GetClaseSolicitudesAutorizar');
  }

  getEmpleadosAutorizar(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
    }
    return this.http.get<any[]>(this.urlMtto + 'GetEmpleadosAutorizar', { params: parametros });
  }

  getAllDetaAutorizar(param: any): Observable<any[]> {

		let parametros = new HttpParams();

		if (param != null) {
			parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('CORR_EMPLEADO', param.CORR_EMPLEADO);
      parametros = parametros.append('CORR_PERMISO', param.CORR_PERMISO);
		}
		return this.http.get<any[]>(this.urlMtto + 'GetAllSoliPermisoDetaAutorizar', { params: parametros });
	}

  autorizar(model: any): any {
    return this.http.put(this.urlMtto + 'Autorizar', model).pipe(
      map((response: any) => response)
    );
  }
  denegar(model: any): any {
    return this.http.put(this.urlMtto + 'Denegar', model).pipe(
      map((response: any) => response)
    );
  }

}
