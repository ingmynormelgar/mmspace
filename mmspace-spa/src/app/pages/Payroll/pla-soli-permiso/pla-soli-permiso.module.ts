import { NgModule } from '@angular/core';
import { PlaSoliPermisoRoutingModule } from './pla-soli-permiso-routing.module';

@NgModule({
	imports: [
		PlaSoliPermisoRoutingModule
	]
})
export class PlaSoliPermisoModule { }
