import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class PlaSoliPermisoService {
	readonly urlMtto = environment.apiUrl + 'PLA_SOLI_PERMISO/';

	constructor(private http: HttpClient) { }

	getAll(param: any): Observable<any[]> {

		let parametros = new HttpParams();

		if (param != null) {
			parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('FECHA_INICIAL', param.FECHA_INICIAL.toISOString());
      parametros = parametros.append('FECHA_FINAL', param.FECHA_FINAL.toISOString());
		}
		return this.http.get<any[]>(this.urlMtto, { params: parametros });
	}

	get(id: number, param: any): Observable<any> {

		let parametros = new HttpParams();

		if (param != null) {
			parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
		}

		return this.http.get<any>(this.urlMtto + id , { params: parametros });
	}

	insert(model: any): any {
		return this.http.post(this.urlMtto, model).pipe(
			map((response: any) => response)
		);
	}

	update(model: any): any {
		return this.http.put(this.urlMtto, model).pipe(
			map((response: any) => response)
		);
	}

	delete(id: number, param: any): any {
		let parametros = new HttpParams();

		if (param != null) {
			parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('CORR_EMPLEADO', param.CORR_EMPLEADO);
		}

		return this.http.delete(this.urlMtto + id, { params: parametros }).pipe(
			map((response: any) => response)
		);
	}

  getMotivosPermisos(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
			parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
    }

    return this.http.get<any[]>(this.urlMtto + 'GetMotivosPermisos', { params: parametros });
  }

  getEstados(param: any): Observable<any[]> {
    return this.http.get<any[]>(this.urlMtto + 'GetEstados');
  }

  getClaseSolicitud(param: any): Observable<any[]> {
    return this.http.get<any[]>(this.urlMtto + 'GetClaseSolicitudes');
  }

  getEmpleados(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
    }
    return this.http.get<any[]>(this.urlMtto + 'GetEmpleados', { params: parametros });
  }

  getAllDeta(param: any): Observable<any[]> {

		let parametros = new HttpParams();

		if (param != null) {
			parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('CORR_EMPLEADO', param.CORR_EMPLEADO);
      parametros = parametros.append('CORR_PERMISO', param.CORR_PERMISO);
		}
		return this.http.get<any[]>(this.urlMtto + 'GetAllSoliPermisoDeta', { params: parametros });
	}

  // eslint-disable-next-line @typescript-eslint/naming-convention
  DeleteSoliPermisoDeta(id: number, param: any): any {
		let parametros = new HttpParams();

		if (param != null) {
			parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('CORR_EMPLEADO', param.CORR_EMPLEADO);
      parametros = parametros.append('CORR_PERMISO', param.CORR_PERMISO);
		}
		return this.http.delete(this.urlMtto + 'DeleteSoliPermisoDeta/' + id, { params: parametros }).pipe(
			map((response: any) => response)
		);
	}

  solicitar(model: any): any {
    return this.http.put(this.urlMtto + 'Solicitar', model).pipe(
      map((response: any) => response)
    );
  }

}
