import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { DxLoadPanelModule } from 'devextreme-angular/ui/load-panel';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxTabPanelModule } from 'devextreme-angular/ui/tab-panel';
import { DxDropDownBoxModule } from 'devextreme-angular/ui/drop-down-box';
import { DxCheckBoxModule } from 'devextreme-angular/ui/check-box';
import { DxToolbarModule } from 'devextreme-angular/ui/toolbar';
import { DxDateBoxModule } from 'devextreme-angular/ui/date-box';
import { DxNumberBoxModule } from 'devextreme-angular/ui/number-box';
import { DxTextAreaModule } from 'devextreme-angular/ui/text-area';

import { PlaSoliPrestamoComponent } from './pla-soli-prestamo.component';


const routes: Routes = [{ path: '', component: PlaSoliPrestamoComponent }];

@NgModule({
	imports: [RouterModule.forChild(routes),
		CommonModule,
		DxFormModule,
		DxDataGridModule,
		DxButtonModule,
		DxLoadPanelModule,
		DxDropDownBoxModule,
		DxCheckBoxModule,
		DxTabPanelModule,
		DxToolbarModule,
    DxDateBoxModule,
    DxNumberBoxModule,
    DxTextAreaModule
	],
	exports: [RouterModule],
	declarations: [
		PlaSoliPrestamoComponent
	]
})
export class PlaSoliPrestamoRoutingModule { }
