/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, ViewChild } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { custom } from 'devextreme/ui/dialog';
import { locale, loadMessages } from 'devextreme/localization';
import esMessages from 'devextreme/localization/messages/es.json';
import notify from 'devextreme/ui/notify';
import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
import { RowStatus } from 'src/app/shared/models/RowStatus.enum';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { ActivatedRoute } from '@angular/router';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { DxDropDownBoxComponent } from 'devextreme-angular/ui/drop-down-box';

import { AppInfoService } from 'src/app/shared/services/app-info.service';
import { PlaSoliPrestamo } from './pla-soli-prestamo';
import { PlaSoliPrestamoService } from './pla-soli-prestamo.service';

@Component({
	selector: 'app-pla-soli-prestamo',
	templateUrl: './pla-soli-prestamo.component.html',
	styleUrls: ['./pla-soli-prestamo.component.scss']
})
export class PlaSoliPrestamoComponent implements OnInit {
	@ViewChild('fData', { static: false }) dataForm!: DxFormComponent;
	@ViewChild('estadoSolicitud', { static: false }) estadoSolicitud!: DxDropDownBoxComponent;
 	@ViewChild('dataEstadoSolicitud', { static: false }) dataEstadoSolicitud!: DxDataGridComponent;
	@ViewChild('corrEmpleado', { static: false }) corrEmpleado!: DxDropDownBoxComponent;
	@ViewChild('dataEmpleado', { static: false }) dataEmpleado!: DxDataGridComponent;
	@ViewChild('corrRubro', { static: false }) corrRubro!: DxDropDownBoxComponent;
	@ViewChild('dataRubro', { static: false }) dataRubro!: DxDataGridComponent;
	@ViewChild('corrPeriodicidad', { static: false }) corrPeriodicidad!: DxDropDownBoxComponent;
	@ViewChild('dataPeriodicidad', { static: false }) dataPeriodicidad!: DxDataGridComponent;
	@ViewChild('corrTipoMoneda', { static: false }) corrTipoMoneda!: DxDropDownBoxComponent;
	@ViewChild('dataTipoMoneda', { static: false }) dataTipoMoneda!: DxDataGridComponent;
	// @ViewChild('gData') dataGrid!: DxDataGridComponent;

	//#region <Declaraciones>
	tituloVentana = 'Solicitud Prestamos';
	subTituloVentana = '';
	urlOpcion = '/pla-soli-prestamo';
	banderaMtto = UpdateType.Browse;
	loadingVisible = false;
	permiteSalir = true;
	permisos = 'ABC';
	permiteAdd = false;
	permiteEdit = false;
	permiteDele = false;
	permitePrint = false;
  bloqLookups = false;
	models: any;
	modelUpdate: any;
	model: PlaSoliPrestamo = {
		CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
		CORR_EMPLEADO: 0,
    NOMBRE_EMPLEADO: '',
		CORR_PRESTAMO: 0,
		FECHA_SOLICITUD: new Date(),
		CORR_RUBRO_PRESTAMO: 0,
    NOMBRE_RUBRO: '',
		FECHA_INICIO: new Date(),
		CORR_PERIODICIDAD: 0,
    NOMBRE_PERIODICIDAD: '',
		MONTO_PRESTAMO: 0,
    NUMERO_CUOTAS: 0,
		MONTO_CUOTA: 0,
		USUARIO_SOLICITA: '',
		USUARIO_AUTORIZA: '',
		CORR_MONEDA: 0,
    NOMBRE_MONEDA: '',
		ESTADO_PRESTAMO: '',
    NOMBRE_ESTADO_PRESTAMO: '',
		OBSERVACIONES: '',
		USUARIO_CREA: '',
		FECHA_CREA: new Date(),
		ESTACION_CREA: '',
		USUARIO_ACTU: '',
		FECHA_ACTU: new Date(),
		ESTACION_ACTU: '',
		CORR_RUBRO_FIJO: 0
	};
	param: any = {
		TIPO_CONSULTA: 1,
    CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
		FECHA_INICIAL: new Date(this.appInfoService.getDate().getFullYear(), this.appInfoService.getDate().getMonth(), 1),
    FECHA_FINAL: this.appInfoService.getDate(),
		OPCION_CONSULTA: 0
	};

  mEstado: any;
  mEmpleado: any;
  mRubro: any;
  mPeriodicidad: any;
  mTipoMoneda: any;
  mMonedaEmpresa: any;

	constructor(
		private appInfoService: AppInfoService,
		private service: PlaSoliPrestamoService,
		private router: ActivatedRoute
	) {
		this.tituloVentana = router.snapshot.data.titulo;
		loadMessages(esMessages);
		locale(this.appInfoService.getLocale);
		this.getPermisos();

		// Metodos como propiedades
		this.getPermiteEditar = this.getPermiteEditar.bind(this);
		this.getPermiteDele = this.getPermiteDele.bind(this);
		this.isBrowse = this.isBrowse.bind(this);
		this.editarClick = this.editarClick.bind(this);
	}

	ngOnInit(): void {
		this.inicializaOpciones();
		this.llenaComboBox();
		this.consultar();
	}

	//#region <Validadores>
	esValido(): boolean {
		if (this.model.ESTADO_PRESTAMO === '') {
      notify(
        {
          message:
            'Error, Debe seleccionar el estado del Prestamo.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    if (this.model.CORR_PERIODICIDAD === 0) {
        notify(
          {
            message:
              'Error, Debe seleccionar una periodicidad.',
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true,
          },
          'error',
          500000
        );
        return false;
    }
    if (this.model.CORR_EMPLEADO === 0) {
      notify(
        {
          message:
            'Error, Debe seleccionar un Empleado.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.MONTO_PRESTAMO <= 0) {
      notify(
        {
          message:
            'Error, Debe Ingresar un Monto Préstamo mayor a cero.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.MONTO_CUOTA <= 0) {
      notify(
        {
          message:
            'Error, Debe Ingresar un Monto Cuota mayor a cero.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.CORR_MONEDA === 0) {
      notify(
        {
          message: 'Error, Debe Seleccionar una Moneda.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if (this.model.CORR_RUBRO_PRESTAMO === 0) {
      notify(
        {
          message: 'Error, Debe seleccionar un Rubro.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

		return true;
	}
	// #endregion
	//#region <Inicializando Opciones>
	inicializaOpciones() {
		this.getEMPRESA();
	}
	getEMPRESA() {
		// this.dSService.enviarCorrEmpresaObservable.subscribe(empresa => {
		// 	this.param.CORR_EMPRESA = empresa;
		// });
	}

	// #endregion
	//#region <Manejo de Combos>
	llenaComboBox() {
		this.getEstado();
    this.getEmpleado();
    this.getRubro();
    this.getPeriodicidad();
    this.getMoneda();
	  this.getMonedaEmpresa();
	}

	getEstado() {
	  this.param.TIPO_CONSULTA = 1;
	  this.param.CORR_LISTA = 1;
	  this.param.OPCION_CONSULTA = 0;
	  this.service.getEstados(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mEstado = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  getEmpleado() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 5;
	  this.service.getEmpleados(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mEmpleado = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  getRubro() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 10;
	  this.service.getRubros(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mRubro = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  getPeriodicidad() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 1;
	  this.service.getPeriodicidad(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mPeriodicidad = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  getMoneda() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 1;
	  this.service.getTipoMonedas(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mTipoMoneda = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  getMonedaEmpresa() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 2;
	  this.service.getMonedaEmpresas(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mMonedaEmpresa = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  setPeriodicidadEmpleado(vCORR_EMPLEADO: number) {
	  this.param.TIPO_CONSULTA = 3;
    this.param.CORR_EMPLEADO = vCORR_EMPLEADO;
	  this.param.OPCION_CONSULTA = 1;
	  this.service.getPeriodicidadEmpleado(this.param).pipe(take(1)).subscribe(
	    (model: any) => {
        this.model.CORR_PERIODICIDAD = model.CORR_PERIODICIDAD;
        this.model.NUMERO_CUOTAS = model.NUMERO_CUOTAS;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  setMontoMaximo() {
	  this.service.getMontoMaximo(this.param).pipe(take(1)).subscribe(
	    (montoMaximo: number) => {
        this.model.MONTO_PRESTAMO = montoMaximo;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}
	//#endregion

	//#region <Metodos Browse>
	isBrowse(): boolean {
		if (this.banderaMtto === UpdateType.Browse) {
			return true;
	}
		return false;
	}

	isForm(): boolean {
		if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
			return true;
		}
		return false;
	}

	getPermisos() {
		this.permiteAdd = false;
		this.permiteEdit = false;
		this.permiteDele = false;
		this.permitePrint = false;
		this.permisos = this.appInfoService.getPermiso(this.urlOpcion);
		if (this.permisos.includes('C')) {
			this.permiteAdd = true;
		}
		if (this.permisos.includes('U')) {
			this.permiteEdit = true;
		}
		if (this.permisos.includes('D')) {
			this.permiteDele = true;
		}
	}

	getPermiteEditar(e: any) {
		if (this.permiteEdit) {
      if (e.row.data.ESTADO_PRESTAMO === 'DI') {
			return true;
      }
		}
		return false;
	}

	getPermiteDele(e: any) {
    if (e.row.data.ESTADO_PRESTAMO === 'DI') {
      if (this.permiteDele) {
        return true;
      }
    }
		return false;
	}

	focusedRowChanged(e: any) {
		this.model = e.row.data;
	}
	//#endregion

	//#region <Metodos Mtto>
	consultar() {
		this.param.TIPO_CONSULTA = 1;
    this.param.CORR_SUSCRIPCION = this.appInfoService.CORR_SUSCRIPCION;
		this.param.CORR_CONFI_PAIS = this.appInfoService.CORR_CONFI_PAIS;
		this.param.CORR_EMPRESA = this.appInfoService.CORR_EMPRESA;
    this.param.OPCION_CONSULTA = 1;
		localStorage.setItem('FechaInicial', this.param.FECHA_INICIAL.toISOString());
    localStorage.setItem('FechaFinal', this.param.FECHA_FINAL.toISOString());
		this.service.getAll(this.param).pipe(take(1)).subscribe((model: any[]) => {
			this.models = model;
		});
	}

	nuevo(): void {
		this.model = {
      CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
      CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
      CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
			CORR_EMPLEADO: this.mEmpleado[0].CORR_EMPLEADO,
      NOMBRE_EMPLEADO: '',
			CORR_PRESTAMO: 0,
			FECHA_SOLICITUD: this.appInfoService.getDate(),
			CORR_RUBRO_PRESTAMO: this.mRubro[0].CORR_RUBRO,
      NOMBRE_RUBRO:'',
			FECHA_INICIO: this.appInfoService.getDate(),
			CORR_PERIODICIDAD: 0,
      NOMBRE_PERIODICIDAD: '',
			MONTO_PRESTAMO: 0,
      NUMERO_CUOTAS: 0,
			MONTO_CUOTA: 0,
			USUARIO_SOLICITA: '',
			USUARIO_AUTORIZA: '',
			CORR_MONEDA: this.mMonedaEmpresa[0].CORR_MONEDA,
     	NOMBRE_MONEDA: '',
			ESTADO_PRESTAMO: 'DI',
      NOMBRE_ESTADO_PRESTAMO: '',
			OBSERVACIONES: '',
			USUARIO_CREA: '',
			FECHA_CREA: new Date(),
			ESTACION_CREA: '',
			USUARIO_ACTU: '',
			FECHA_ACTU: new Date(),
			ESTACION_ACTU: '',
			CORR_RUBRO_FIJO: 0
		};
    this.modelUpdate = {
      CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
      CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
      CORR_EMPRESA: this.model.CORR_EMPRESA,
      CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
      CORR_PRESTAMO: this.model.CORR_PRESTAMO,
      FECHA_SOLICITUD: this.model.FECHA_SOLICITUD,
      CORR_RUBRO_PRESTAMO: this.model.CORR_RUBRO_PRESTAMO,
      NOMBRE_RUBRO: this.model.NOMBRE_RUBRO,
      FECHA_INICIO: this.model.FECHA_INICIO,
      CORR_PERIODICIDAD: this.model.CORR_PERIODICIDAD,
      NOMBRE_PERIODICIDAD: this.model.NOMBRE_PERIODICIDAD,
      MONTO_PRESTAMO: this.model.MONTO_PRESTAMO,
      NUMERO_CUOTAS: this.model.NUMERO_CUOTAS,
      MONTO_CUOTA: this.model.MONTO_CUOTA,
      USUARIO_SOLICITA: this.model.USUARIO_SOLICITA,
      USUARIO_AUTORIZA: this.model.USUARIO_AUTORIZA,
      CORR_MONEDA: this.model.CORR_MONEDA,
      NOMBRE_MONEDA: this.model.NOMBRE_MONEDA,
      ESTADO_PRESTAMO: this.model.ESTADO_PRESTAMO,
      NOMBRE_ESTADO_PRESTAMO: this.model.NOMBRE_ESTADO_PRESTAMO,
      OBSERVACIONES: this.model.OBSERVACIONES,
      USUARIO_CREA: this.model.USUARIO_CREA,
      FECHA_CREA: this.model.FECHA_CREA,
      ESTACION_CREA: this.model.ESTACION_CREA,
      USUARIO_ACTU: this.model.USUARIO_ACTU,
      FECHA_ACTU: this.model.FECHA_ACTU,
      ESTACION_ACTU: this.model.ESTACION_ACTU,
      CORR_RUBRO_FIJO: this.model.CORR_RUBRO_FIJO
    };
    this.permiteSalir = false;
    this.bloqLookups = false;
    this.setPeriodicidadEmpleado(this.mEmpleado[0].CORR_EMPLEADO);
    this.setMontoMaximo();
		this.banderaMtto = UpdateType.Add;
		this.subTituloVentana = RowStatus.Add.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('NOMBRE_PRESTAMO')?.focus();
		});
	}

	editarClick(e: any) {
    e.event.preventDefault();
    this.bloqLookups = false;
    this.modelUpdate = {
      CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
      CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
      CORR_EMPRESA: this.model.CORR_EMPRESA,
      CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
      CORR_PRESTAMO: this.model.CORR_PRESTAMO,
      FECHA_SOLICITUD: this.model.FECHA_SOLICITUD,
      CORR_RUBRO_PRESTAMO: this.model.CORR_RUBRO_PRESTAMO,
      NOMBRE_RUBRO: this.model.NOMBRE_RUBRO,
      FECHA_INICIO: this.model.FECHA_INICIO,
      CORR_PERIODICIDAD: this.model.CORR_PERIODICIDAD,
      NOMBRE_PERIODICIDAD: this.model.NOMBRE_PERIODICIDAD,
      MONTO_PRESTAMO: this.model.MONTO_PRESTAMO,
      NUMERO_CUOTAS: this.model.NUMERO_CUOTAS,
      MONTO_CUOTA: this.model.MONTO_CUOTA,
      USUARIO_SOLICITA: this.model.USUARIO_SOLICITA,
      USUARIO_AUTORIZA: this.model.USUARIO_AUTORIZA,
      CORR_MONEDA: this.model.CORR_MONEDA,
      NOMBRE_MONEDA: this.model.NOMBRE_MONEDA,
      ESTADO_PRESTAMO: this.model.ESTADO_PRESTAMO,
      NOMBRE_ESTADO_PRESTAMO: this.model.NOMBRE_ESTADO_PRESTAMO,
      OBSERVACIONES: this.model.OBSERVACIONES,
      USUARIO_CREA: this.model.USUARIO_CREA,
      FECHA_CREA: this.model.FECHA_CREA,
      ESTACION_CREA: this.model.ESTACION_CREA,
      USUARIO_ACTU: this.model.USUARIO_ACTU,
      FECHA_ACTU: this.model.FECHA_ACTU,
      ESTACION_ACTU: this.model.ESTACION_ACTU,
      CORR_RUBRO_FIJO: this.model.CORR_RUBRO_FIJO
    };
    this.permiteSalir = false;
    this.banderaMtto = UpdateType.Update;
    this.subTituloVentana = RowStatus.Update.toString();
    setTimeout(() => {
      this.dataForm.instance.getEditor('NOMBRE_PRESTAMO')?.focus();
    });
	}

	guardar(): void {
		if (!this.esValido()) {
			return;
		}

		this.loadingVisible = true;
		if (this.banderaMtto === UpdateType.Add) {
				this.service.insert(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.models.push(newModel);
					this.model = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro creado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true,
            },
            'error',
            500000
          );
					this.loadingVisible = false;
				}
			);
		} else
		if (this.banderaMtto === UpdateType.Update) {
			this.service.update(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.model = newModel;
					const vIndex = this.models.findIndex((item: any) => item.CORR_PRESTAMO === newModel.CORR_PRESTAMO);
					this.models[vIndex] = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro modificado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true,
            },
            'error',
            500000
          );
					this.loadingVisible = false;
				}
			);
		}
	}

	cancelar(): void {
    const cancelRow = () => {
      this.permiteSalir = true;
      this.bloqLookups = false;
      this.banderaMtto = UpdateType.Browse;
      this.subTituloVentana = RowStatus.Not_Defined.toString();
      this.getPermisos();
    };
    if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
      const confirmacion = custom({
        title: 'Confirmación de Cancelar',
        messageHtml: '¿Quieres cancelar y perder los cambios realizados?',
        buttons: [{
          text: 'Si', onClick: (e: any) => true
        }, {
          text: 'No', onClick: (e: any) => false
        }]
      });

      confirmacion.show().then((cancel: boolean) => {
        if (cancel) {
          this.model = this.modelUpdate;
          const vIndex = this.models.findIndex((item: any) => item.CORR_PRESTAMO === this.modelUpdate.CORR_PRESTAMO);
          this.models[vIndex] = this.modelUpdate;
          cancelRow();
        }
      });
    } else {
      cancelRow();
    }
	}

	rowRemoving(e: any) {
    if (e.data.ESTADO_PRESTAMO === 'DI') {
      this.param.CORR_EMPLEADO = e.data.CORR_EMPLEADO;
      this.service.delete(e.data.CORR_PRESTAMO, this.param).pipe(take(1)).subscribe(
        () => {
          notify({ message: 'Registro eliminado con exito!', width: 'auto', shading: false }, 'success', 1500);
          e.component.refresh();
        },
        (error: any) => {
          e.cancel = true;
          notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true }, 'error', 500000);
        }
      );
    } else {
      e.cancel = true;
      notify(
        {
          message: 'El préstamo debe de estar en digitado para eliminarlo.',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }

	}

	rowDblClick(e: any) {
		this.banderaMtto = UpdateType.Not_Defined;
		this.subTituloVentana = RowStatus.Browse.toString();
		setTimeout(() => {
			this.bloquear();
		});
	}

	bloquear(): void {
		this.dataForm.instance.getEditor('CORR_EMPLEADO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_PRESTAMO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_SOLICITUD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_RUBRO_PRESTAMO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_INICIO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_PERIODICIDAD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('MONTO_PRESTAMO')?.option('readOnly', true);
    this.dataForm.instance.getEditor('NUMERO_CUOTAS')?.option('readOnly', true);
		this.dataForm.instance.getEditor('MONTO_CUOTA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('USUARIO_SOLICITA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('USUARIO_AUTORIZA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_MONEDA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('ESTADO_PRESTAMO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('OBSERVACIONES')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_RUBRO_FIJO')?.option('readOnly', true);
    this.bloqLookups = true;
	}

	permitirSalir():
		| boolean
		| import('rxjs').Observable<boolean>
		| Promise<boolean> {
		if (this.permiteSalir) {
			return true;
		}
		const confirmacion = custom({
			title: 'Confirmación de Salida',
			messageHtml: '¿Quieres salir del formulario y perder los cambios realizados?',
			buttons: [{
				text: 'Si',
				onClick: (e: any) => true
			},
			{
				text: 'No',
				onClick: (e: any) => false
			}]

		});

		return confirmacion.show().then(() => {});
	}
	//#endregion

	valueChangedESTADO_PRESTAMO(e: any) {
    if (this.model.ESTADO_PRESTAMO === null) {
      this.dataEstadoSolicitud.instance.clearSelection();
    }
  }

  selectionChangedESTADO_PRESTAMO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.ESTADO_PRESTAMO = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickESTADO_PRESTAMO(e: any, data: any) {
    this.estadoSolicitud.instance.close();
    this.estadoSolicitud.instance.focus();
  }

  valueChangedCORR_EMPLEADO(e: any) {
    if (this.model.CORR_EMPLEADO === null) {
      this.dataEmpleado.instance.clearSelection();
    } else {
      this.setPeriodicidadEmpleado(this.model.CORR_EMPLEADO);
    }
  }

  selectionChangedCORR_EMPLEADO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_EMPLEADO = selectedRowKeys[0].CORR_EMPLEADO;
    }
  }

  rowClickCORR_EMPLEADO(e: any, data: any) {
    this.corrEmpleado.instance.close();
    this.corrEmpleado.instance.focus();
  }

  valueChangedCORR_RUBRO_PRESTAMO(e: any) {
    if (this.model.CORR_RUBRO_PRESTAMO === null) {
      this.dataRubro.instance.clearSelection();
    }
  }

  selectionChangedCORR_RUBRO_PRESTAMO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_RUBRO_PRESTAMO = selectedRowKeys[0].CORR_RUBRO;
    }
  }

  rowClickCORR_RUBRO_PRESTAMO(e: any, data: any) {
    this.corrRubro.instance.close();
    this.corrRubro.instance.focus();
  }

  valueChangedCORR_PERIODICIDAD(e: any) {
    if (this.model.CORR_PERIODICIDAD === null) {
      this.dataPeriodicidad.instance.clearSelection();
    }
  }

  selectionChangedCORR_PERIODICIDAD(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_PERIODICIDAD = selectedRowKeys[0].CORR_PERIODICIDAD;
    }
  }

  rowClickCORR_PERIODICIDAD(e: any, data: any) {
    this.corrPeriodicidad.instance.close();
    this.corrPeriodicidad.instance.focus();
  }

  valueChangedCORR_MONEDA(e: any) {
    if (this.model.CORR_MONEDA === null) {
      this.dataTipoMoneda.instance.clearSelection();
    }
  }

  selectionChangedCORR_MONEDA(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_MONEDA = selectedRowKeys[0].CORR_MONEDA;
    }
  }

  rowClickCORR_MONEDA(e: any, data: any) {
    this.corrTipoMoneda.instance.close();
    this.corrTipoMoneda.instance.focus();
  }

  fieldDataChanged(e: any) {
    if (e.dataField === 'MONTO_PRESTAMO') {
      if (this.model.NUMERO_CUOTAS > 0) {
        this.model.MONTO_CUOTA = e.value / this.model.NUMERO_CUOTAS;
      }
    }
  }

  //solicitar
  Solicitar(): void {
    if (this.model.ESTADO_PRESTAMO !== '') {
      const confirApli = custom({
        title: 'Confirmación de Solicitud',
        messageHtml: '¿Quiere Solicitar esta Respuesta?',
        buttons: [{
          text: 'Si',
          onClick: (e: any) => {
            if (this.model.ESTADO_PRESTAMO === 'DI') {
              this.service.solicitar(this.model).subscribe(
                (newModel: any) => {
                  this.model = newModel;
                  const vIndex = this.models.findIndex((item: any) => item.CORR_PRESTAMO === newModel.CORR_PRESTAMO);
                  this.models[vIndex] = newModel;
                  notify({ message: 'Registro Solicitado con exito', width: 'auto', shading: false}, 'success', 1500);
                },
                (error: any) => {
                  notify({ message: error,
                    width: 'auto',
                    shading: false,
                    closeOnClick: true,
                    closeOnOutsideClick: true }, 'error', 500000
                  );
                }
              );
            } else {
              notify({ message: 'La Solicitud debe estar en Digitado', width: 'auto', shading: false}, 'warning', 1500);
            }
          }
        },
        {
          text: 'No',
          onClick: (e: any) => false
        }]
      });
      confirApli.show().then((dialogResult: any) => {});
    } else {
      notify(
        {
          message: 'Debe seleccionar un registro ',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }
  }
}
