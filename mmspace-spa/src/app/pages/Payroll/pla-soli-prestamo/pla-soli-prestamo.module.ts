import { NgModule } from '@angular/core';
import { PlaSoliPrestamoRoutingModule } from './pla-soli-prestamo-routing.module';

@NgModule({
	imports: [
		PlaSoliPrestamoRoutingModule
	]
})
export class PlaSoliPrestamoModule { }
