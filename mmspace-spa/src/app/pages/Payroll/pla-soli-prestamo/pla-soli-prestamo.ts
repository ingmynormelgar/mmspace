/* eslint-disable @typescript-eslint/naming-convention */
export interface PlaSoliPrestamo {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_EMPLEADO: number;
  NOMBRE_EMPLEADO: string;
	CORR_PRESTAMO: number;
	FECHA_SOLICITUD: Date;
	CORR_RUBRO_PRESTAMO: number;
  NOMBRE_RUBRO: string;
	FECHA_INICIO: Date;
	CORR_PERIODICIDAD: number;
  NOMBRE_PERIODICIDAD: string;
	MONTO_PRESTAMO: number;
  NUMERO_CUOTAS: number;
	MONTO_CUOTA: number;
	USUARIO_SOLICITA: string;
	USUARIO_AUTORIZA: string;
	CORR_MONEDA: number;
  NOMBRE_MONEDA: string;
	ESTADO_PRESTAMO: string;
  NOMBRE_ESTADO_PRESTAMO: string;
	OBSERVACIONES: string;
	USUARIO_CREA: string;
	FECHA_CREA: Date;
	ESTACION_CREA: string;
	USUARIO_ACTU: string;
	FECHA_ACTU: Date;
	ESTACION_ACTU: string;
	CORR_RUBRO_FIJO: number;
}
