/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, ViewChild } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { custom } from 'devextreme/ui/dialog';
import { locale, loadMessages } from 'devextreme/localization';
import esMessages from 'devextreme/localization/messages/es.json';
import notify from 'devextreme/ui/notify';
import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
import { RowStatus } from 'src/app/shared/models/RowStatus.enum';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { ActivatedRoute } from '@angular/router';

import { AppInfoService } from 'src/app/shared/services/app-info.service';
import { PlaSolicitudVacacionAutorizar } from './pla-solicitud-vacacion-autorizar';
import { PlaSolicitudVacacionAutorizarService } from './pla-solicitud-vacacion-autorizar.service';
import { DxDropDownBoxComponent } from 'devextreme-angular/ui/drop-down-box';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { split } from '@angular-devkit/core';

@Component({
	selector: 'app-pla-solicitud-vacacion-autorizar',
	templateUrl: './pla-solicitud-vacacion-autorizar.component.html',
	styleUrls: ['./pla-solicitud-vacacion-autorizar.component.scss']
})
export class PlaSolicitudVacacionAutorizarComponent implements OnInit {
	@ViewChild('fData', { static: false }) dataForm!: DxFormComponent;
	@ViewChild('corrEmpleado', { static: false }) corrEmpleado!: DxDropDownBoxComponent;
	@ViewChild('dataEmpleado', { static: false }) dataEmpleado!: DxDataGridComponent;
	@ViewChild('corrEstado', { static: false }) corrEstado!: DxDropDownBoxComponent;
	@ViewChild('dataEstado', { static: false }) dataEstado!: DxDataGridComponent;
	@ViewChild('corrTipoMoneda', { static: false }) corrTipoMoneda!: DxDropDownBoxComponent;
	@ViewChild('dataTipoMoneda', { static: false }) dataTipoMoneda!: DxDataGridComponent;
  // @ViewChild('gData') dataGrid!: DxDataGridComponent;

	//#region <Declaraciones>
	tituloVentana = 'Autorizar Solicitud de Vacaciones';
	subTituloVentana = '';
	urlOpcion = '/pla-solicitud-vacacion-autorizar';
	banderaMtto = UpdateType.Browse;
	loadingVisible = false;
	permiteSalir = true;
	permisos = 'ABC';
	permiteAdd = false;
	permiteEdit = false;
	permiteDele = false;
	permitePrint = false;
  bloqLookups = true;
  mostrarFechaAprobacion = false;
  totalDiasVacaciones = 0;
	models: any;
	modelUpdate: any;
	model: PlaSolicitudVacacionAutorizar = {
		CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
		CORR_SOLI_VACACION: 0,
		CORR_EMPLEADO: 0,
    NOMBRE_EMPLEADO: '',
		FECHA_SOLICITUD: this.appInfoService.getDate(),
		FECHA_INICIA_VACACION: this.appInfoService.getDate(),
		FECHA_FINAL_VACACION: this.appInfoService.getDate(),
		FECHA_INICIA_LABORES: this.appInfoService.getDate(),
		DIAS_VACACION: 0,
		DIAS_GOZAR: 0,
		DIAS_PAGO: 0,
		OBSERVACIONES: '',
		FECHA_APROBADA: this.appInfoService.getDate(),
		ESTADO_SOLICITUD: '',
    NOMBRE_ESTADO_SOLICITUD: '',
		USUARIO_CREA: '',
		FECHA_CREA: new Date(),
		ESTACION_CREA: '',
		USUARIO_ACTU: '',
		FECHA_ACTU: new Date(),
		ESTACION_ACTU: '',
		CORR_MONEDA: 0,
		NOMBRE_MONEDA: '',
    SIMBOLO: '',
		CORR_ACCION: 0,
		USUARIO_SOLICITA: '',
		USUARIO_AUTORIZA: ''
	};
	param: any = {
		TIPO_CONSULTA: 1,
    CORR_SUSCRIPCION: this.appInfoService.CORR_SUSCRIPCION,
		CORR_CONFI_PAIS: this.appInfoService.CORR_CONFI_PAIS,
		CORR_EMPRESA: this.appInfoService.CORR_EMPRESA,
    CORR_EMPLEADO:0,
    FECHA_INICIAL: new Date(this.appInfoService.getDate().getFullYear(), this.appInfoService.getDate().getMonth(), 1),
    FECHA_FINAL: this.appInfoService.getDate(),
		OPCION_CONSULTA: 0
	};

  mEmpleado: any;
  mEstado: any;
  mTipoMoneda: any;
  mMonedaEmpresa: any;
	//#endregion

	constructor(
		private appInfoService: AppInfoService,
		private service: PlaSolicitudVacacionAutorizarService,
		private router: ActivatedRoute
	) {
		this.tituloVentana = router.snapshot.data.titulo;
		loadMessages(esMessages);
		locale(this.appInfoService.getLocale);
		this.getPermisos();

		// Metodos como propiedades
		this.getPermiteEditar = this.getPermiteEditar.bind(this);
		this.getPermiteDele = this.getPermiteDele.bind(this);
		this.isBrowse = this.isBrowse.bind(this);
		this.editarClick = this.editarClick.bind(this);
	}

	ngOnInit(): void {
		this.inicializaOpciones();
		this.llenaComboBox();
		this.consultar();
	}

	//#region <Validadores>
	esValido(): boolean {
		//Validando y devolviendo falso si no cumple una validacion
    if(this.model.DIAS_VACACION <= 0){
      notify(
        {
          message:
            'Error, los Días de Vacación deben ser mayor que cero.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    if(this.model.DIAS_GOZAR <= 0){
      notify(
        {
          message:
            'Error, los Días Gozar deben ser mayor que cero.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    if(this.model.FECHA_SOLICITUD === null){
      notify(
        {
          message:
            'Error, la Fecha de Solicitud es obligatoria.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }

    if( this.model.FECHA_INICIA_VACACION === null ) {
      notify({ message: 'Ingresar Fecha Inicia Vacación.', width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
      return false;
    };

    if( this.model.FECHA_FINAL_VACACION === null ) {
      notify({ message: 'Ingresar Fecha Final Vacación.', width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
      return false;
    }

    console.log(this.model.FECHA_INICIA_LABORES);
    if(typeof this.model.FECHA_INICIA_LABORES === 'undefined' || this.model.FECHA_INICIA_LABORES === null){
      notify({ message: 'Ingresar Fecha Inicia Labores.', width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
      return false;
    }

    if( this.model.FECHA_FINAL_VACACION < this.model.FECHA_INICIA_VACACION ) {
      notify({ message: 'Ingresar rango de fecha válido. Fecha inicial debe ser menor que la fecha final.', width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
      return false;
    };

    if(this.model.FECHA_INICIA_LABORES <= this.model.FECHA_FINAL_VACACION ){
      notify({ message: 'Ingresar Fecha Inicio Labores válido. Fecha de inicio de labores debe ser mayor que la Fecha Final Vacación.', width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
      return false;
    }



		return true;
	}
	// #endregion
	//#region <Inicializando Opciones>

  inicializaOpciones() {
		this.getEMPRESA();
	}
	getEMPRESA() {
		// this.dSService.enviarCorrEmpresaObservable.subscribe(empresa => {
		// 	this.param.CORR_EMPRESA = empresa;
		// });
	}

	// #endregion
	//#region <Manejo de Combos>
	llenaComboBox() {
		this.getEstado();
    this.getEmpleados();
    //this.getTipoMoneda();
    //this.getMonedaEmpresa();
	}

  getEstado() {
	  this.param.TIPO_CONSULTA = 1;
	  this.param.CORR_LISTA = 1;
	  this.param.OPCION_CONSULTA = 0;
	  this.service.getEstados(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mEstado = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}

  getEmpleados() {
    this.service.getEmpleados(this.param).pipe(take(1)).subscribe(
      (model: any[]) => {
        this.mEmpleado = model;
      },
    (error: any) => {
        notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
      }
    );
  }

  getTipoMoneda() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 1;
	  this.service.getTipoMonedas(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mTipoMoneda = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}
  getMonedaEmpresa() {
	  this.param.TIPO_CONSULTA = 2;
	  this.param.OPCION_CONSULTA = 2;
	  this.service.getMonedaEmpresas(this.param).pipe(take(1)).subscribe(
	    (model: any[]) => {
	      this.mMonedaEmpresa = model;
	    },
	    (error: any) => {
	      notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true
          },
          'error',
          500000
        );
	    }
	  );
	}
	//#endregion

	//#region <Metodos Browse>
	isBrowse(): boolean {
		if (this.banderaMtto === UpdateType.Browse) {
			return true;
	}
		return false;
	}

	isForm(): boolean {
		if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
			return true;
		}
		return false;
	}

	getPermisos() {
		this.permiteAdd = false;
		this.permiteEdit = false;
		this.permiteDele = false;
		this.permitePrint = false;
		this.permisos = this.appInfoService.getPermiso(this.urlOpcion);
		if (this.permisos.includes('C')) {
			this.permiteAdd = true;
		}
		if (this.permisos.includes('U')) {
			this.permiteEdit = true;
		}
		if (this.permisos.includes('D')) {
			this.permiteDele = true;
		}
	}

	getPermiteEditar(e: any) {
		if (this.permiteEdit) {
      if (e.row.data.ESTADO_SOLICITUD === 'DI') {
        return true;
        }
		}
		return false;
	}

	getPermiteDele(e: any) {
		if (this.permiteDele) {
			if (e.row.data.ESTADO_SOLICITUD === 'DI') {
        return true;
      }
		}
		return false;
	}

	focusedRowChanged(e: any) {
		this.model = e.row.data;
	}
	//#endregion

	//#region <Metodos Mtto>
	consultar() {
    this.param.CORR_SUSCRIPCION = this.appInfoService.CORR_SUSCRIPCION;
		this.param.CORR_CONFI_PAIS = this.appInfoService.CORR_CONFI_PAIS;
		this.param.CORR_EMPRESA = this.appInfoService.CORR_EMPRESA;
    localStorage.setItem('FechaInicial', this.param.FECHA_INICIAL.toISOString());
    localStorage.setItem('FechaFinal', this.param.FECHA_FINAL.toISOString());
		this.service.getAll(this.param).pipe(take(1)).subscribe((model: any[]) => {
			this.models = model;
		});
	}


	editarClick(e: any) {
		e.event.preventDefault();
    this.mostrarFechaAprobada();
		this.modelUpdate = {
			CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.model.CORR_EMPRESA,
			CORR_SOLI_VACACION: this.model.CORR_SOLI_VACACION,
			CORR_EMPLEADO: this.model.CORR_EMPLEADO,
      NOMBRE_EMPLEADO: this.model.NOMBRE_EMPLEADO,
			FECHA_SOLICITUD: this.model.FECHA_SOLICITUD,
			FECHA_INICIA_VACACION: this.model.FECHA_INICIA_VACACION,
			FECHA_FINAL_VACACION: this.model.FECHA_FINAL_VACACION,
			FECHA_INICIA_LABORES: this.model.FECHA_INICIA_LABORES,
			DIAS_VACACION: this.model.DIAS_VACACION,
			DIAS_GOZAR: this.model.DIAS_GOZAR,
			DIAS_PAGO: this.model.DIAS_PAGO,
			OBSERVACIONES: this.model.OBSERVACIONES,
			FECHA_APROBADA: this.model.FECHA_APROBADA,
			ESTADO_SOLICITUD: this.model.ESTADO_SOLICITUD,
      NOMBRE_ESTADO_SOLICITUD: this.model.NOMBRE_ESTADO_SOLICITUD,
			USUARIO_CREA: this.model.USUARIO_CREA,
			FECHA_CREA: this.model.FECHA_CREA,
			ESTACION_CREA: this.model.ESTACION_CREA,
			USUARIO_ACTU: this.model.USUARIO_ACTU,
			FECHA_ACTU: this.model.FECHA_ACTU,
			ESTACION_ACTU: this.model.ESTACION_ACTU,
			CORR_MONEDA: this.model.CORR_MONEDA,
      NOMBRE_MONEDA: this.model.NOMBRE_MONEDA,
      SIMBOLO: this.model.SIMBOLO,
			CORR_ACCION: this.model.CORR_ACCION,
			USUARIO_SOLICITA: this.model.USUARIO_SOLICITA,
			USUARIO_AUTORIZA: this.model.USUARIO_AUTORIZA
		};
		this.permiteSalir = false;
		this.banderaMtto = UpdateType.Update;
		this.subTituloVentana = RowStatus.Update.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('NOMBRE_SOLI_VACACION')?.focus();
		});
	}



	cancelar(): void {
    const cancelRow = () => {
      this.permiteSalir = true;
      this.mostrarFechaAprobacion = false;
      this.banderaMtto = UpdateType.Browse;
      this.subTituloVentana = RowStatus.Not_Defined.toString();
      this.getPermisos();
    };
		if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
      const confirmacion = custom({
        title: 'Confirmación de Cancelar',
        messageHtml: '¿Quieres cancelar y perder los cambios realizados?',
        buttons: [{
          text: 'Si', onClick: (e: any) => true
        }, {
          text: 'No', onClick: (e: any) => false
        }]
      });

      confirmacion.show().then((cancel: boolean) => {
        if (cancel) {
          if (this.banderaMtto === UpdateType.Update){
            this.model = this.modelUpdate;
            const vIndex = this.models.findIndex((item: any) => item.CORR_SOLI_VACACION === this.modelUpdate.CORR_SOLI_VACACION);
            this.models[vIndex] = this.modelUpdate;
          }
          cancelRow();
        }
      });
    } else {
      cancelRow();
    }
	}

	rowDblClick(e: any) {
		this.banderaMtto = UpdateType.Not_Defined;
		this.subTituloVentana = RowStatus.Browse.toString();
    this.mostrarFechaAprobada();
		setTimeout(() => {
			this.bloquear();
		});
	}

	bloquear(): void {
		this.dataForm.instance.getEditor('CORR_SOLI_VACACION')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_EMPLEADO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_SOLICITUD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_INICIA_VACACION')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_FINAL_VACACION')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_INICIA_LABORES')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_VACACION')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_GOZAR')?.option('readOnly', true);
		this.dataForm.instance.getEditor('DIAS_PAGO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('OBSERVACIONES')?.option('readOnly', true);
		this.dataForm.instance.getEditor('FECHA_APROBADA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('ESTADO_SOLICITUD')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_MONEDA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_ACCION')?.option('readOnly', true);
		this.dataForm.instance.getEditor('USUARIO_SOLICITA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('USUARIO_AUTORIZA')?.option('readOnly', true);
		this.dataForm.instance.getEditor('ANIO_PERIODO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('MES_PERIODO')?.option('readOnly', true);
		this.dataForm.instance.getEditor('CORR_MOVIMIENTO')?.option('readOnly', true);
	}

	permitirSalir():
		| boolean
		| import('rxjs').Observable<boolean>
		| Promise<boolean> {
		if (this.permiteSalir) {
			return true;
		}
		const confirmacion = custom({
			title: 'Confirmación de Salida',
			messageHtml: '¿Quieres salir del formulario y perder los cambios realizados?',
			buttons: [{
				text: 'Si',
				onClick: (e: any) => true
			},
			{
				text: 'No',
				onClick: (e: any) => false
			}]

		});

		return confirmacion.show().then(() => {});
	}
	//#endregion


  //#region <lookups>
  //Empleado
  valueChangedCORR_EMPLEADO(e: any) {
    if (this.model.CORR_EMPLEADO === null) {
      this.dataEmpleado.instance.clearSelection();
    }
  }
  selectionChangedCORR_EMPLEADO(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_EMPLEADO = selectedRowKeys[0].CORR_EMPLEADO;
    }
  }
  rowClickCORR_EMPLEADO(e: any, data: any) {
    this.corrEmpleado.instance.close();
    this.corrEmpleado.instance.focus();
  }

  //Estado Solicitud
  valueChangedESTADO_SOLICITUD(e: any) {
    if (this.model.ESTADO_SOLICITUD === null) {
      this.dataEstado.instance.clearSelection();
    }
  }

  selectionChangedESTADO_SOLICITUD(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.ESTADO_SOLICITUD = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickESTADO_SOLICITUD(e: any, data: any) {
    this.corrEstado.instance.close();
    this.corrEstado.instance.focus();
  }

  //Tipo Moneda
  valueChangedCORR_MONEDA(e: any) {
    if (this.model.CORR_MONEDA === null) {
      this.dataTipoMoneda.instance.clearSelection();
    }
  }

  selectionChangedCORR_MONEDA(selectedRowKeys: any) {
    if (selectedRowKeys.length > 0) {
      this.model.CORR_MONEDA = selectedRowKeys[0].CORR_MONEDA;
    }
  }

  rowClickCORR_MONEDA(e: any, data: any) {
    this.corrTipoMoneda.instance.close();
    this.corrTipoMoneda.instance.focus();
  }

  mostrarFechaAprobada(){
    if(this.model.ESTADO_SOLICITUD === 'AP'){
      this.mostrarFechaAprobacion = true;
    }else{
      this.mostrarFechaAprobacion = false;
    };
  }
  ////#endregion

  autorizar(): void {
    if (this.model.ESTADO_SOLICITUD !== '') {
      const confirApli = custom({
        title: 'Confirmación de Solicitud',
        messageHtml: '¿Realmente desea Autorizar esta Solicitud?',
        buttons: [{
          text: 'Si',
          onClick: (e: any) => {
            if (this.model.ESTADO_SOLICITUD === 'SO') {
              this.service.autorizar(this.model).subscribe(
                (id: number) => {
                  const vIndex = this.models.findIndex((item: any) => item.CORR_SOLI_VACACION === id);
                  this.models.splice(vIndex,1);
                  this.banderaMtto = UpdateType.Browse;
                  this.subTituloVentana = RowStatus.Not_Defined.toString();
                  this.loadingVisible = false;
                  this.permiteSalir = true;
                  notify({ message: 'Registro Autorizado con exito', width: 'auto', shading: false}, 'success', 1500);
                },
                (error: any) => {
                  notify({ message: error,
                    width: 'auto',
                    shading: false,
                    closeOnClick: true,
                    closeOnOutsideClick: true }, 'error', 500000
                  );
                }
              );
            } else {
              notify({ message: 'La Solicitud debe estar en Solicitado', width: 'auto', shading: false}, 'warning', 1500);
            }
          }
        },
        {
          text: 'No',
          onClick: (e: any) => false
        }]
      });
      confirApli.show().then((dialogResult: any) => {});
    } else {
      notify(
        {
          message: 'Debe seleccionar un registro ',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }
  }

  rechazar(): void {
    if (this.model.ESTADO_SOLICITUD !== '') {
      const confirApli = custom({
        title: 'Confirmación de Solicitud',
        messageHtml: '¿Realmente desea Denegar esta Solicitud?',
        buttons: [{
          text: 'Si',
          onClick: (e: any) => {
            if (this.model.ESTADO_SOLICITUD === 'SO') {
              this.service.rechazar(this.model).subscribe(
                (id: number) => {
                  const vIndex = this.models.findIndex((item: any) => item.CORR_SOLI_VACACION === id);
                  this.models.splice(vIndex,1);
                  this.banderaMtto = UpdateType.Browse;
                  this.subTituloVentana = RowStatus.Not_Defined.toString();
                  this.loadingVisible = false;
                  this.permiteSalir = true;
                  notify({ message: 'Registro Denegado con exito', width: 'auto', shading: false}, 'success', 1500);
                },
                (error: any) => {
                  notify({ message: error,
                    width: 'auto',
                    shading: false,
                    closeOnClick: true,
                    closeOnOutsideClick: true }, 'error', 500000
                  );
                }
              );
            } else {
              notify({ message: 'La Solicitud debe estar en Solicitado', width: 'auto', shading: false}, 'warning', 1500);
            }
          }
        },
        {
          text: 'No',
          onClick: (e: any) => false
        }]
      });
      confirApli.show().then((dialogResult: any) => {});
    } else {
      notify(
        {
          message: 'Debe seleccionar un registro ',
          width: 'auto',
          shading: false ,
          closeOnClick: true,
          closeOnOutsideClick: true
        },
        'error',
        500000
      );
    }
  }

}
