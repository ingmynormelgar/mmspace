import { NgModule } from '@angular/core';
import { PlaSolicitudVacacionAutorizarRoutingModule } from './pla-solicitud-vacacion-autorizar-routing.module';

@NgModule({
	imports: [
		PlaSolicitudVacacionAutorizarRoutingModule
	]
})
export class PlaSolicitudVacacionAutorizarModule { }
