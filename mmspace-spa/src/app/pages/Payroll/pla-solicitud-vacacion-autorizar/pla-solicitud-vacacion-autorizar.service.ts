import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class PlaSolicitudVacacionAutorizarService {
	readonly urlMtto = environment.apiUrl + 'PLA_SOLICITUD_VACACION/';

	constructor(private http: HttpClient) { }

	getAll(param: any): Observable<any[]> {

		let parametros = new HttpParams();

		if (param != null) {
      parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('FECHA_INICIAL', param.FECHA_INICIAL.toISOString());
      parametros = parametros.append('FECHA_FINAL', param.FECHA_FINAL.toISOString());
		}

		return this.http.get<any[]>(this.urlMtto + 'GetAllAutorizar', { params: parametros });
	}

	get(id: number, param: any): Observable<any> {

		let parametros = new HttpParams();

		if (param != null) {
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
		}

		return this.http.get<any>(this.urlMtto + id , { params: parametros });
	}

  getEstados(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
			parametros = parametros.append('CORR_LISTA', param.CORR_LISTA);
			parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
    }

    return this.http.get<any[]>(this.urlMtto + 'GetEstadosAutorizar', { params: parametros });
  }

  getEmpleados(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
    }
    return this.http.get<any[]>(this.urlMtto + 'GetColaborador', { params: parametros });
  }

  getTipoMonedas(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
      parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
			parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
    }
    return this.http.get<any[]>(this.urlMtto + 'GetTipoMonedas', { params: parametros });
  }

  getMonedaEmpresas(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
      parametros = parametros.append('CORR_SUSCRIPCION', param.CORR_SUSCRIPCION);
      parametros = parametros.append('CORR_CONFI_PAIS', param.CORR_CONFI_PAIS);
			parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
			parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
    }
    return this.http.get<any[]>(this.urlMtto + 'GetMonedaEmpresa', { params: parametros });
  }

  autorizar(model: any): any {
    return this.http.put(this.urlMtto + 'Autorizar', model).pipe(
      map((response: any) => response)
    );
  }

  rechazar(model: any): any {
    return this.http.put(this.urlMtto + 'Rechazar', model).pipe(
      map((response: any) => response)
    );
  }

}
