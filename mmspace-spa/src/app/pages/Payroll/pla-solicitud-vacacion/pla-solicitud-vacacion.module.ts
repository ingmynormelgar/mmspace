import { NgModule } from '@angular/core';
import { PlaSolicitudVacacionRoutingModule } from './pla-solicitud-vacacion-routing.module';

@NgModule({
	imports: [
		PlaSolicitudVacacionRoutingModule
	]
})
export class PlaSolicitudVacacionModule { }
