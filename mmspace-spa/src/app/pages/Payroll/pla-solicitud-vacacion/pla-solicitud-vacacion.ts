/* eslint-disable @typescript-eslint/naming-convention */
export interface PlaSolicitudVacacion {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_SOLI_VACACION: number;
	CORR_EMPLEADO: number;
  NOMBRE_EMPLEADO: string;
	FECHA_SOLICITUD: Date;
	FECHA_INICIA_VACACION: Date;
	FECHA_FINAL_VACACION: Date;
	FECHA_INICIA_LABORES: Date;
	DIAS_VACACION: number;
	DIAS_GOZAR: number;
	DIAS_PAGO: number;
	OBSERVACIONES: string;
	FECHA_APROBADA: Date;
	ESTADO_SOLICITUD: string;
  NOMBRE_ESTADO_SOLICITUD: string;
	USUARIO_CREA: string;
	FECHA_CREA: Date;
	ESTACION_CREA: string;
	USUARIO_ACTU: string;
	FECHA_ACTU: Date;
	ESTACION_ACTU: string;
	CORR_MONEDA: number;
  NOMBRE_MONEDA: string;
  SIMBOLO: string;
	CORR_ACCION: number;
	USUARIO_SOLICITA: string;
	USUARIO_AUTORIZA: string;
}
