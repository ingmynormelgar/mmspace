import { NgModule } from '@angular/core';
import { PosBodegaRoutingModule } from './pos-bodega-routing.module';

@NgModule({
	imports: [
		PosBodegaRoutingModule
	]
})
export class PosBodegaModule { }
