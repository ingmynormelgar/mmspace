/* eslint-disable @typescript-eslint/naming-convention */
export interface PosBodega {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_BODEGA: number;
	NOMBRE_BODEGA: string;
}
