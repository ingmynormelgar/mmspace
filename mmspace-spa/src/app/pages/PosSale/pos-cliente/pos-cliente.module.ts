import { NgModule } from '@angular/core';
import { PosClienteRoutingModule } from './pos-cliente-routing.module';

@NgModule({
	imports: [
		PosClienteRoutingModule
	]
})
export class PosClienteModule { }