/* eslint-disable @typescript-eslint/naming-convention */
export interface PosCliente {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_CLIENTE: number;
	NOMBRE_CLIENTE: string;
	NRC: string;
}