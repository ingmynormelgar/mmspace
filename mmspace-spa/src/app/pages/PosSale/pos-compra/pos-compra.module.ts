import { NgModule } from '@angular/core';
import { PosCompraRoutingModule } from './pos-compra-routing.module';

@NgModule({
	imports: [
		PosCompraRoutingModule
	]
})
export class PosCompraModule { }