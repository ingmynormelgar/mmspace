/* eslint-disable @typescript-eslint/naming-convention */
export interface PosCompra {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_COMPRA: number;
	CORR_PROVEEDOR: number;
	FECHA_COMPRA: number;
	ESTADO_COMPRA: string;
}