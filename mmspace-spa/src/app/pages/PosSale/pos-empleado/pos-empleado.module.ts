import { NgModule } from '@angular/core';
import { PosEmpleadoRoutingModule } from './pos-empleado-routing.module';

@NgModule({
	imports: [
		PosEmpleadoRoutingModule
	]
})
export class PosEmpleadoModule { }