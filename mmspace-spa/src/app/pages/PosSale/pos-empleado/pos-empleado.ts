/* eslint-disable @typescript-eslint/naming-convention */
export interface PosEmpleado {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_EMPLEADO: number;
	PRIMER_NOMBRE: string;
	SEGUNDO_NOMBRE: string;
	PRIMER_APELLIDO: string;
	SEGUNDO_APELLIDO: string;
	DUI: string;
	TELEFONO: number;
	CARGO: string;
}