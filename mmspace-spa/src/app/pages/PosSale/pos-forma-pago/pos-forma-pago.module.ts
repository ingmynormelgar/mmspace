import { NgModule } from '@angular/core';
import { PosFormaPagoRoutingModule } from './pos-forma-pago-routing.module';

@NgModule({
	imports: [
		PosFormaPagoRoutingModule
	]
})
export class PosFormaPagoModule { }