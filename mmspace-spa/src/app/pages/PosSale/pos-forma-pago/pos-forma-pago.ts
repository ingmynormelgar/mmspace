/* eslint-disable @typescript-eslint/naming-convention */
export interface PosFormaPago {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_FORMA_PAGO: number;
	NOMBRE_FORMA_PAGO: string;
}