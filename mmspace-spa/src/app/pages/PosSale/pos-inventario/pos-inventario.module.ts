import { NgModule } from '@angular/core';
import { PosInventarioRoutingModule } from './pos-inventario-routing.module';

@NgModule({
	imports: [
		PosInventarioRoutingModule
	]
})
export class PosInventarioModule { }