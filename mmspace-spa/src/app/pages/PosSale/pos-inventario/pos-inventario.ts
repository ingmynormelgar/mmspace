/* eslint-disable @typescript-eslint/naming-convention */
export interface PosInventario {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	PERIODO_ANIO: number;
	PERIODO_MES: number;
	CORR_INVENTARIO: number;
	CORR_BODEGA: number;
}