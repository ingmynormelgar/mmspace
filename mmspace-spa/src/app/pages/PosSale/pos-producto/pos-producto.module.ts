import { NgModule } from '@angular/core';
import { PosProductoRoutingModule } from './pos-producto-routing.module';

@NgModule({
	imports: [
		PosProductoRoutingModule
	]
})
export class PosProductoModule { }