/* eslint-disable @typescript-eslint/naming-convention */
export interface PosProducto {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_PRODUCTO: number;
	CODIGO_PRODUCTO: number;
	NOMBRE_PRODUCTO: string;
	PRECIO_VENTA: number;
}