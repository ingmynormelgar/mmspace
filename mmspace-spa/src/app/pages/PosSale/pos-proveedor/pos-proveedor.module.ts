import { NgModule } from '@angular/core';
import { PosProveedorRoutingModule } from './pos-proveedor-routing.module';

@NgModule({
	imports: [
		PosProveedorRoutingModule
	]
})
export class PosProveedorModule { }