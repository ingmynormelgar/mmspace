/* eslint-disable @typescript-eslint/naming-convention */
export interface PosProveedor {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_PROVEEDOR: number;
	NOMBRE_PROVEEDOR: string;
	TELEFONO_PROVEEDOR: string;
	DIRECCION_PROVEEDOR: string;
}