/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, ViewChild } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { custom } from 'devextreme/ui/dialog';
import { locale, loadMessages } from 'devextreme/localization';
import esMessages from 'devextreme/localization/messages/es.json';
import notify from 'devextreme/ui/notify';
import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
import { RowStatus } from 'src/app/shared/models/RowStatus.enum';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { ActivatedRoute } from '@angular/router';

import { AppInfoService } from 'src/app/shared/services/app-info.service';
import { PosTipoFactura } from './pos-tipo-factura';
import { PosTipoFacturaService } from './pos-tipo-factura.service';

@Component({
	selector: 'app-pos-tipo-factura',
	templateUrl: './pos-tipo-factura.component.html',
	styleUrls: ['./pos-tipo-factura.component.scss']
})
export class PosTipoFacturaComponent implements OnInit {
	@ViewChild('fData', { static: false }) dataForm!: DxFormComponent;
	// @ViewChild('gData') dataGrid!: DxDataGridComponent;
	
	//#region <Declaraciones>
	tituloVentana = 'Tipo Factura';
	subTituloVentana = '';
	urlOpcion = '/pos-tipo-factura';
	banderaMtto = UpdateType.Browse;
	loadingVisible = false;
	permiteSalir = true;
	permisos = 'ABC';
	permiteAdd = false;
	permiteEdit = false;
	permiteDele = false;
	permitePrint = false;
	models: any;
	modelUpdate: any;
	model: PosTipoFactura = {
		CORR_SUSCRIPCION: 1,
		CORR_CONFI_PAIS: 1,
		CORR_EMPRESA: 1,
		CORR_TIPO_FACTURA: 0,
		NOMBRE_TIPO_FACTURA: '',
		NUMERO_ULT_RECIBO: 0
	};
	param: any = {
		TIPO_CONSULTA: 1,
		CORR_EMPRESA: 1,
		OPCION_CONSULTA: 0
	};
	//#endregion
	
	constructor(
		private appInfoService: AppInfoService,
		private service: PosTipoFacturaService,
		private router: ActivatedRoute
	) {
		this.tituloVentana = router.snapshot.data.titulo;
		loadMessages(esMessages);
		locale(this.appInfoService.getLocale);
		this.getPermisos();
		
		// Metodos como propiedades
		this.getPermiteEditar = this.getPermiteEditar.bind(this);
		this.getPermiteDele = this.getPermiteDele.bind(this);
		this.isBrowse = this.isBrowse.bind(this);
		this.editarClick = this.editarClick.bind(this);
	}
	
	ngOnInit(): void {
		this.inicializaOpciones();
		this.llenaComboBox();
		this.consultar();
	}
	
	//#region <Validadores>
	esValido() :boolean {
		//Validando y devolviendo falso si no cumple una validacion
		return true;
	}
	// #endregion
	//#region <Inicializando Opciones>
	inicializaOpciones() {
		this.getEMPRESA();
	}
	getEMPRESA() {
		// this.dSService.enviarCorrEmpresaObservable.subscribe(empresa => {
		// 	this.param.CORR_EMPRESA = empresa;
		// });
	}
	
	// #endregion
	//#region <Manejo de Combos>
	llenaComboBox() {
		// this.getEstado();
	}
	
	// getEstado() {
		// this.param.TIPO_CONSULTA = 1;
		// this.param.CORR_LISTA = 1;
		// this.param.OPCION_CONSULTA = 0;
		// this.service.getEstados(this.param).pipe(take(1)).subscribe(
		//   (model: any[]) => {
		//     this.mEstadoPC = model;
		//   },
		//   (error: any) => {
		//     notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
		//   }
		// );
	// }
	//#endregion
	
	//#region <Metodos Browse>
	isBrowse(): boolean {
		if (this.banderaMtto === UpdateType.Browse) {
			return true;
	}
		return false;
	}
	
	isForm(): boolean {
		if (this.banderaMtto === UpdateType.Add || this.banderaMtto === UpdateType.Update) {
			return true;
		}
		return false;
	}
	
	getPermisos() {
		this.permiteAdd = false;
		this.permiteEdit = false;
		this.permiteDele = false;
		this.permitePrint = false;
		this.permisos = this.appInfoService.getPermiso(this.urlOpcion);
		if (this.permisos.includes('C')) {
			this.permiteAdd = true;
		}
		if (this.permisos.includes('U')) {
			this.permiteEdit = true;
		}
		if (this.permisos.includes('D')) {
			this.permiteDele = true;
		}
	}
	
	getPermiteEditar(e: any) {
		if (this.permiteEdit) {
			return true;
		}
		return false;
	}
	
	getPermiteDele(e: any) {
		if (this.permiteDele) {
			return true;
		}
		return false;
	}
	
	focusedRowChanged(e: any) {
		this.model = e.row.data;
	}
	//#endregion
	
	//#region <Metodos Mtto>
	consultar() {
		this.param.TIPO_CONSULTA = 1;
		this.param.OPCION_CONSULTA = 0;
		this.service.getAll(this.param).pipe(take(1)).subscribe((model: any[]) => {
			this.models = model;
		});
	}
	
	nuevo() :void {
		this.permiteSalir = false;
		this.model = {
			CORR_SUSCRIPCION: 1,
			CORR_CONFI_PAIS: 1,
			CORR_EMPRESA: 1,
			CORR_TIPO_FACTURA: 0,
			NOMBRE_TIPO_FACTURA: '',
			NUMERO_ULT_RECIBO: 0
		};
		this.banderaMtto = UpdateType.Add;
		this.subTituloVentana = RowStatus.Add.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('NOMBRE_TIPO_FACTURA')?.focus();
		});
	}
	
	editarClick(e: any) {
		e.event.preventDefault();
		this.modelUpdate = {
			CORR_SUSCRIPCION: this.model.CORR_SUSCRIPCION,
			CORR_CONFI_PAIS: this.model.CORR_CONFI_PAIS,
			CORR_EMPRESA: this.model.CORR_EMPRESA,
			CORR_TIPO_FACTURA: this.model.CORR_TIPO_FACTURA,
			NOMBRE_TIPO_FACTURA: this.model.NOMBRE_TIPO_FACTURA,
			NUMERO_ULT_RECIBO: this.model.NUMERO_ULT_RECIBO
		};
		this.permiteSalir = false;
		this.banderaMtto = UpdateType.Update;
		this.subTituloVentana = RowStatus.Update.toString();
		setTimeout(() => {
			this.dataForm.instance.getEditor('NOMBRE_TIPO_FACTURA')?.focus();
		});
	}
	
	guardar(): void {
		if (!this.esValido()) {
			return;
		}
		
		this.loadingVisible = true;
		if (this.banderaMtto === UpdateType.Add) {
				this.service.insert(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.models.push(newModel);
					this.model = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro creado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
					this.loadingVisible = false;
				}
			);
		} else
		if (this.banderaMtto === UpdateType.Update) {
			this.service.update(this.model).pipe(take(1)).subscribe(
				(newModel: any) => {
					this.model = newModel;
					const vIndex = this.models.findIndex((item: any) => item.CORR_TIPO_FACTURA === newModel.CORR_TIPO_FACTURA);
					this.models[vIndex] = newModel;
					this.banderaMtto = UpdateType.Browse;
					this.subTituloVentana = RowStatus.Not_Defined.toString();
					this.loadingVisible = false;
					this.permiteSalir = true;
					notify({ message: 'Registro modificado con exito!', width: 'auto', shading: false}, 'success', 1500);
				},
				(error: any) => {
					notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
					this.loadingVisible = false;
				}
			);
		}
	}
	
	cancelar(): void {
		this.model = this.modelUpdate;
		const vIndex = this.models.findIndex((item: any) => item.CORR_TIPO_FACTURA === this.modelUpdate.CORR_TIPO_FACTURA);
		this.models[vIndex] = this.modelUpdate;
		this.permiteSalir = true;
		this.banderaMtto = UpdateType.Browse;
		this.subTituloVentana = RowStatus.Not_Defined.toString();
		this.getPermisos();
	}
	
	rowRemoving(e: any) {
		this.service.delete(e.data.CORR_TIPO_FACTURA, this.param).pipe(take(1)).subscribe(
			() => {
				notify({ message: 'Registro eliminado con exito!', width: 'auto', shading: false }, 'success', 1500);
				e.component.refresh();
			},
			(error: any) => {
				e.cancel = true;
				notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true }, 'error', 500000);
			}
		);
	}
	
	rowDblClick(e: any) {
		this.banderaMtto = UpdateType.Not_Defined;
		this.subTituloVentana = RowStatus.Browse.toString();
		setTimeout(() => {
			this.bloquear();
		});
	}
	
	bloquear(): void {
		this.dataForm.instance.getEditor('CORR_TIPO_FACTURA')?.option('ReadOnly', true);
		this.dataForm.instance.getEditor('NOMBRE_TIPO_FACTURA')?.option('ReadOnly', true);
		this.dataForm.instance.getEditor('NUMERO_ULT_RECIBO')?.option('ReadOnly', true);
	}
	
	permitirSalir():
		| boolean
		| import('rxjs').Observable<boolean>
		| Promise<boolean> {
		if (this.permiteSalir) {
			return true;
		}
		const confirmacion = custom({
			title: 'Confirmación de Salida',
			messageHtml: '¿Quieres salir del formulario y perder los cambios realizados?',
			buttons: [{
				text: 'Si',
				onClick: (e: any) => true
			},
			{
				text: 'No',
				onClick: (e: any) => false
			}]
			
		});
		
		return confirmacion.show().then(() => {});
	}
	//#endregion

}