import { NgModule } from '@angular/core';
import { PosTipoFacturaRoutingModule } from './pos-tipo-factura-routing.module';

@NgModule({
	imports: [
		PosTipoFacturaRoutingModule
	]
})
export class PosTipoFacturaModule { }