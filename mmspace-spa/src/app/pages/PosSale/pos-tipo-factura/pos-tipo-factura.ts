/* eslint-disable @typescript-eslint/naming-convention */
export interface PosTipoFactura {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	CORR_TIPO_FACTURA: number;
	NOMBRE_TIPO_FACTURA: string;
	NUMERO_ULT_RECIBO: number;
}