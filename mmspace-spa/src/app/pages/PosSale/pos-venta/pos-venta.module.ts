import { NgModule } from '@angular/core';
import { PosVentaRoutingModule } from './pos-venta-routing.module';

@NgModule({
	imports: [
		PosVentaRoutingModule
	]
})
export class PosVentaModule { }