/* eslint-disable @typescript-eslint/naming-convention */
export interface PosVenta {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CORR_EMPRESA: number;
	PERIODO_ANIO: number;
	PERIODO_MES: number;
	CORR_VENTA: number;
	CORR_EMPLEADO: number;
	CORR_FORMA_PAGO: number;
	CORR_CLIENTE: number;
	CORR_TIPO_FACTURA: number;
	NUMERO_RECIBO: number;
	TOTAL_VENTA: number;
	TOTAL_DESCUENTO: number;
	TOTAL_IVA: number;
	EXENTO_IVA: boolean;
}