import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AuthGuardService } from 'src/app/shared/services/auth.service';
import { AppCanDeactivateGuard } from 'src/app/app-candeactivate.guard';

import { PosBodegaComponent,} from './pos-bodega/pos-bodega.component';
import { PosClienteComponent,} from './pos-cliente/pos-cliente.component';
import { PosCompraComponent,} from './pos-compra/pos-compra.component';
import { PosEmpleadoComponent,} from './pos-empleado/pos-empleado.component';
import { PosFormaPagoComponent,} from './pos-forma-pago/pos-forma-pago.component';
import { PosInventarioComponent,} from './pos-inventario/pos-inventario.component';
import { PosProductoComponent,} from './pos-producto/pos-producto.component';
import { PosTipoFacturaComponent,} from './pos-tipo-factura/pos-tipo-factura.component';
import { PosVentaComponent,} from './pos-venta/pos-venta.component';
import { PosProveedorComponent,} from './pos-proveedor/pos-proveedor.component';

const routes: Routes = [
  {
    path: 'pos-bodega',
    component: PosBodegaComponent,
    data: { titulo: 'Bodegas'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-bodega/pos-bodega.module').then(m => m.PosBodegaModule)
  },
  {
    path: 'pos-cliente',
    component: PosClienteComponent,
    data: { titulo: 'Clientes'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-cliente/pos-cliente.module').then(m => m.PosClienteModule)
  },
  {
    path: 'pos-compra',
    component: PosCompraComponent,
    data: { titulo: 'Compras'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-compra/pos-compra.module').then(m => m.PosCompraModule)
  },
  {
    path: 'pos-empleado',
    component: PosEmpleadoComponent,
    data: { titulo: 'Empleado'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-empleado/pos-empleado.module').then(m => m.PosEmpleadoModule)
  },
  {
    path: 'pos-forma-pago',
    component: PosFormaPagoComponent,
    data: { titulo: 'Formas de pago'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-forma-pago/pos-forma-pago.module').then(m => m.PosFormaPagoModule)
  },
  {
    path: 'pos-inventario',
    component: PosInventarioComponent,
    data: { titulo: 'Inventario'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-inventario/pos-inventario.module').then(m => m.PosInventarioModule)
  },
  {
    path: 'pos-producto',
    component: PosProductoComponent,
    data: { titulo: 'Productos'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-producto/pos-producto.module').then(m => m.PosProductoModule)
  },
  {
    path: 'pos-tipo-factura',
    component: PosTipoFacturaComponent,
    data: { titulo: 'Tipos de facturas'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-tipo-factura/pos-tipo-factura.module').then(m => m.PosTipoFacturaModule)
  },
  {
    path: 'pos-venta',
    component: PosVentaComponent,
    data: { titulo: 'Ventas'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-venta/pos-venta.module').then(m => m.PosVentaModule)
  },
  {
    path: 'pos-proveedor',
    component: PosProveedorComponent,
    data: { titulo: 'Proveedores'},
    canActivate: [ AuthGuardService ],
    canDeactivate: [ AppCanDeactivateGuard ],
    loadChildren: () => import('./pos-proveedor/pos-proveedor.module').then(m => m.PosProveedorModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PosSaleRoutingModule { }
