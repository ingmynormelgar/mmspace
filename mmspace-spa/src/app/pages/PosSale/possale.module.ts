import { NgModule } from '@angular/core';
import { PosSaleRoutingModule } from './possale-routing.module';

@NgModule({
  imports: [
    PosSaleRoutingModule
  ]
})
export class PosSaleModule { }
