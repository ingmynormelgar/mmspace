import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DxFormModule } from 'devextreme-angular/ui/form';

import { ProfileComponent } from './profile.component';

const routes: Routes = [{ path: '', component: ProfileComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes),
    CommonModule,
    DxFormModule
  ],
  exports: [RouterModule],
  declarations: [
    ProfileComponent
  ]
})
export class ProfileRoutingModule { }
