import { NgModule } from '@angular/core';
import { SegConfigOpcionRoutingModule } from './seg-config-opcion-routing.module';


@NgModule({
  imports: [
    SegConfigOpcionRoutingModule
  ]
})
export class SegConfigOpcionModule { }
