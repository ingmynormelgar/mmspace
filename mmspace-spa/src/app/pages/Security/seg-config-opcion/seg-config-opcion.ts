/* eslint-disable @typescript-eslint/naming-convention */
export interface SegConfigOpcion {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CODIGO_SISTEMA: string;
	CODIGO_MENU: string;
	CODIGO_OPCION: string;
	ORDEN_SISTEMA: number;
	ORDEN_MENU: number;
	ORDEN_OPCION: number;
	USUARIO_CREA: string;
	FECHA_CREA: Date;
	ESTACION_CREA: string;
	USUARIO_ACTU: string;
	FECHA_ACTU: Date;
	ESTACION_ACTU: string;
}
