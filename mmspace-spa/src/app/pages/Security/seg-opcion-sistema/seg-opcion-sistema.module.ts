import { NgModule } from '@angular/core';
import { SegOpcionSistemaRoutingModule } from './seg-opcion-sistema-routing.module';


@NgModule({
  imports: [
    SegOpcionSistemaRoutingModule
  ]
})
export class SegOpcionSistemaModule { }
