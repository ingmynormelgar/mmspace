import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SegOpcionSistemaService {
  readonly urlMtto = environment.apiUrl + 'SEG_OPCION_SISTEMA/';

  constructor(private http: HttpClient) {}

  getAll(param: any): Observable<any[]> {
    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
      parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
    }

    return this.http.get<any[]>(this.urlMtto, { params: parametros });
  }

  get(id: number, param: any): Observable<any> {
    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
    }

    return this.http.get<any>(this.urlMtto + id, { params: parametros });
  }

  insert(model: any): any {
    return this.http.post(this.urlMtto, model).pipe(
      map((response: any) => response)
    );
  }

  update(model: any): any {
    return this.http.put(this.urlMtto, model).pipe(
      map((response: any) => response)
    );
  }

  delete(id: number, param: any): any {
    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
    }

    return this.http.delete(this.urlMtto + id, { params: parametros }).pipe(
      map((response: any) => response)
    );
  }
}
