/* eslint-disable @typescript-eslint/naming-convention */
export interface SegOpcionSistema {
	CORR_SUSCRIPCION: number;
	CORR_CONFI_PAIS: number;
	CODIGO_OPCION: string;
	NOMBRE_OPCION: string;
	URL_OPCION: string;
	IMAGEN_OPCION: string;
	USUARIO_CREA: string;
	FECHA_CREA: Date;
	ESTACION_CREA: string;
	USUARIO_ACTU: string;
	FECHA_ACTU: Date;
	ESTACION_ACTU: string;
}
