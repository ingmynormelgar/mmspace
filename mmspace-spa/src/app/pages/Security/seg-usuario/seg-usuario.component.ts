/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, ViewChild } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { custom } from 'devextreme/ui/dialog';
import { locale, loadMessages } from 'devextreme/localization';
import esMessages from 'devextreme/localization/messages/es.json';
import notify from 'devextreme/ui/notify';
import { UpdateType } from 'src/app/shared/models/UpdateType.enum';
import { RowStatus } from 'src/app/shared/models/RowStatus.enum';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { ActivatedRoute } from '@angular/router';

import { DxDropDownBoxComponent } from 'devextreme-angular/ui/drop-down-box';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';

import { AppInfoService } from 'src/app/shared/services/app-info.service';
import { SegUsuario } from 'src/app/pages/Security/seg-usuario/_models/seg-usuario';
import { SegUsuarioService } from './seg-usuario.service';


@Component({
  selector: 'app-seg-usuario',
  templateUrl: './seg-usuario.component.html',
  styleUrls: ['./seg-usuario.component.scss'],
})
export class SegUsuarioComponent implements OnInit {
  @ViewChild('fData', { static: false }) dataForm!: DxFormComponent;
  @ViewChild('tipoUsuario', { static: false }) tipoUsuario!: DxDropDownBoxComponent;
  @ViewChild('dataTipoUsuario', { static: false }) dataTipoUsuario!: DxDataGridComponent;
  @ViewChild('estadoUsuario', { static: false }) estadoUsuario!: DxDropDownBoxComponent;
  @ViewChild('dataEstadoUsuario', { static: false }) dataEstadoUsuario!: DxDataGridComponent;
  @ViewChild('gDataUsuarioOpcion', { static: false }) gDataUsuarioOpcion!: DxDataGridComponent;

  //#region <Declaraciones>
  tituloVentana = 'Usuario';
  subTituloVentana = '';
  urlOpcion = '/seg-usuario';
  banderaMtto = UpdateType.Browse;
  loadingVisible = false;
  permiteSalir = true;
  permisos = 'ABC';
  permiteAdd = true;
  permiteEdit = true;
  permiteDele = true;
  permitePrint = true;
  models: any;
  modelUpdate: any;
  model: SegUsuario = {
    LOGIN_SISTEMA: '',
    NOMBRE_USUARIO: '',
    CORREO_ELECTRONICO: '',
    TIPO_USUARIO: 0,
    NOMBRE_TIPO_USUARIO: '',
    ESTADO_USUARIO: 0,
    NOMBRE_ESTADO_USUARIO: '',
    IDIOMA: '',
    USUARIO_CREA: '',
    FECHA_CREA: new Date(),
    ESTACION_CREA: '',
    USUARIO_ACTU: '',
    FECHA_ACTU: new Date(),
    ESTACION_ACTU: '',
    DETALLE: [
      {
        LOGIN_SISTEMA: '',
        CORR_SUSCRIPCION: 7,
        CORR_CONFI_PAIS: 7,
        CODIGO_SISTEMA: '',
        CODIGO_MENU: '',
        CODIGO_OPCION: '',
        NUEVO: false,
        MODIFICAR: false,
        ELIMINAR: false,
        IMPRIMIR: false,
        USUARIO_CREA: '',
        FECHA_CREA: new Date(),
        ESTACION_CREA: '',
        USUARIO_ACTU: '',
        FECHA_ACTU: new Date(),
        ESTACION_ACTU: '',
        SELECCION: false,
        MTTO: UpdateType.Add,
      },
    ],
  };

  cambiarClave: any = {
    LOGIN_SISTEMA: '',
    CLAVE_USUARIO: '',
    CLAVE_USUARIO_NUEVA: '',
    CLAVE_CONFIRMAR: ''
  };

  param: any = {
    TIPO_CONSULTA: 1,
    CORR_EMPRESA: 1,
    OPCION_CONSULTA: 0,
  };

  mTipoUsuario: any;
  mEstadoUsuario: any;
  vFocucedRow: any;

  popupVisible = false;
  buttonClaveNueva: any;
  buttonConfirmClave: any;
  modeClaveNuev = '';
  modeConfimClave = '';
  //#endregion

  constructor(
    private appInfoService: AppInfoService,
    private service: SegUsuarioService,
    private router: ActivatedRoute
  ) {
    this.tituloVentana = router.snapshot.data.titulo;
    loadMessages(esMessages);
    locale(this.appInfoService.getLocale);
    this.getPermisos();

    // Metodos como propiedades
    this.getPermiteEditar = this.getPermiteEditar.bind(this);
    this.getPermiteDele = this.getPermiteDele.bind(this);
    this.isBrowse = this.isBrowse.bind(this);
    this.editarClick = this.editarClick.bind(this);
    this.isToolbar = this.isToolbar.bind(this);
  }

  ngOnInit(): void {
    this.inicializaOpciones();
    this.consultar();
    this.llenaComboBox();
  }

  //#region <Validadores>
  esValido(): boolean {
    //Validando y devolviendo falso si no cumple una validacion
    if (this.model.LOGIN_SISTEMA === '') {
      notify(
        {
          message: 'Error, Debe ingresar el nombre de inicio de sesion',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    if (this.model.NOMBRE_USUARIO === '') {
      notify(
        {
          message: 'Error, Debe ingresar el Nombre del Usuario.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    if (this.model.CORREO_ELECTRONICO === '') {
      notify(
        {
          message: 'Error, Debe ingresar un correo valido.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    if (this.model.TIPO_USUARIO === 0 || this.model.TIPO_USUARIO === null) {
      notify(
        {
          message: 'Error, Debe seleccionar un Tipo de USuario.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    if (this.model.ESTADO_USUARIO === null) {
      notify(
        {
          message: 'Error, Debe seleccionar un Estado de usuario.',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        500000
      );
      return false;
    }
    return true;
  }
  // #endregion
  //#region <Inicializando Opciones>
  inicializaOpciones() {
    this.getEMPRESA();
  }
  getEMPRESA() {
    // this.dSService.enviarCorrEmpresaObservable.subscribe(empresa => {
    // 	this.param.CORR_EMPRESA = empresa;
    // });
  }

  // #endregion
  //#region <Manejo de Combos>
  llenaComboBox() {
    this.getTipoUsuario();
    this.getEstadoUsuario();
  }

  getTipoUsuario(): void {
    this.param.TIPO_CONSULTA = 2;
    this.param.OPCION_CONSULTA = 1;
    this.param.CORR_LISTA = 10;
    this.service.getTipoUsuario(this.param).pipe(take(1)).subscribe(
      (model: any[]) => {
        this.mTipoUsuario = model;
      },
      (error: any) => {
        notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true,
          },
          'error',
          500000
        );
      }
    );
  }

  getEstadoUsuario(): void {
    this.param.TIPO_CONSULTA = 2;
    this.param.OPCION_CONSULTA = 1;
    this.param.CORR_LISTA = 11;
    this.service.getEstadoUsuario(this.param).pipe(take(1)).subscribe(
      (model: any[]) => {
        this.mEstadoUsuario = model;
      },
      (error: any) => {
        notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true,
          },
          'error',
          500000
        );
      }
    );
  }
  //#endregion

  //#region <Metodos Browse>
  isBrowse(): boolean {
    if (this.banderaMtto === UpdateType.Browse) {
      return true;
    }
    return false;
  }

  isForm(): boolean {
    if (
      this.banderaMtto === UpdateType.Add ||
      this.banderaMtto === UpdateType.Update
    ) {
      return true;
    }
    return false;
  }

  getPermisos() {
    this.permisos = this.appInfoService.getPermiso(this.urlOpcion);
    if (this.permisos.includes('C')) {
      this.permiteAdd = true;
    }
    if (this.permisos.includes('U')) {
      this.permiteEdit = true;
    }
    if (this.permisos.includes('D')) {
      this.permiteDele = true;
    }
  }

  getPermiteEditar(e: any) {
    if (this.permiteEdit) {
      return true;
    }
    return false;
  }

  getPermiteDele(e: any) {
    if (this.permiteDele) {
      return true;
    }
    return false;
  }

  focusedRowChanged(e: any) {
    this.model = e.row.data;
  }
  //#endregion

  //#region <Metodos Mtto>
  consultar() {
    this.param.TIPO_CONSULTA = 1;
    this.param.OPCION_CONSULTA = 0;
    this.service.getAll(this.param).pipe(take(1)).subscribe((model: any[]) => {
      this.models = model;
    });
  }

  nuevo(): void {
    this.permiteAdd = true;
    this.permiteEdit = true;
    this.permiteDele = true;
    this.permiteSalir = false;
    this.model = {
      LOGIN_SISTEMA: '',
      NOMBRE_USUARIO: '',
      CORREO_ELECTRONICO: '',
      TIPO_USUARIO: 1,
      NOMBRE_TIPO_USUARIO: '',
      ESTADO_USUARIO: 0,
      NOMBRE_ESTADO_USUARIO: '',
      IDIOMA: 'es-SV',
      USUARIO_CREA: '',
      FECHA_CREA: new Date(),
      ESTACION_CREA: '',
      USUARIO_ACTU: '',
      FECHA_ACTU: new Date(),
      ESTACION_ACTU: '',
      DETALLE: [
        {
          LOGIN_SISTEMA: '',
          CORR_SUSCRIPCION: 1,
          CORR_CONFI_PAIS: 1,
          CODIGO_SISTEMA: '',
          CODIGO_MENU: '',
          CODIGO_OPCION: '',
          NUEVO: false,
          MODIFICAR: false,
          ELIMINAR: false,
          IMPRIMIR: false,
          USUARIO_CREA: '',
          FECHA_CREA: new Date(),
          ESTACION_CREA: '',
          USUARIO_ACTU: '',
          FECHA_ACTU: new Date(),
          ESTACION_ACTU: '',
          SELECCION: false,
          MTTO: UpdateType.Not_Defined,
        },
      ],
    };
    this.banderaMtto = UpdateType.Add;
    this.subTituloVentana = RowStatus.Add.toString();
    setTimeout(() => {
      this.dataForm.instance.getEditor('LOGIN_SISTEMA')?.focus();
    });
  }

  editarClick(e: any) {
    e.event.preventDefault();
    this.modelUpdate = {
      LOGIN_SISTEMA: this.model.LOGIN_SISTEMA,
      NOMBRE_USUARIO: this.model.NOMBRE_USUARIO,
      CORREO_ELECTRONICO: this.model.CORREO_ELECTRONICO,
      TIPO_USUARIO: this.model.TIPO_USUARIO,
      NOMBRE_TIPO_USUARIO: this.model.NOMBRE_TIPO_USUARIO,
      ESTADO_USUARIO: this.model.ESTADO_USUARIO,
      NOMBRE_ESTADO_USUARIO: this.model.NOMBRE_ESTADO_USUARIO,
      IDIOMA: this.model.IDIOMA,
      USUARIO_CREA: this.model.USUARIO_CREA,
      FECHA_CREA: this.model.FECHA_CREA,
      ESTACION_CREA: this.model.ESTACION_CREA,
      USUARIO_ACTU: this.model.USUARIO_ACTU,
      FECHA_ACTU: this.model.FECHA_ACTU,
      ESTACION_ACTU: this.model.ESTACION_ACTU
    };
    this.permiteSalir = false;
    this.permiteAdd = true;
    this.permiteEdit = true;
    this.permiteDele = true;
    this.banderaMtto = UpdateType.Update;
    this.subTituloVentana = RowStatus.Update.toString();
    setTimeout(() => {
      this.dataForm.instance
        .getEditor('LOGIN_SISTEMA')
        ?.option('readOnly', true);
      this.dataForm.instance.getEditor('NOMBRE_USUARIO')?.focus();
      this.consultarUsuarioOpcion(this.model.LOGIN_SISTEMA);
    });
  }

  guardar(): void {
    if (!this.esValido()) {
      return;
    }

    this.loadingVisible = true;
    if (this.banderaMtto === UpdateType.Add) {
      this.model.DETALLE = this.getRowsUpdatedUsuarioOpcion();
      this.service.insert(this.model).pipe(take(1)).subscribe(
        (newModel: any) => {
          this.models.push(newModel);
          this.model = newModel;
          this.banderaMtto = UpdateType.Update;
          this.subTituloVentana = RowStatus.Update.toString();
          this.consultarUsuarioOpcion(this.model.LOGIN_SISTEMA);
          this.loadingVisible = false;
          this.permiteSalir = true;
          notify(
            {
              message: 'Registro creado con exito!',
              width: 'auto',
              shading: false,
            },
            'success',
            1500
          );
        },
        (error: any) => {
          notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true,
            },
            'error',
            500000
          );
          this.loadingVisible = false;
        }
      );
    } else if (this.banderaMtto === UpdateType.Update) {
      this.model.DETALLE = this.getRowsUpdatedUsuarioOpcion();
      this.service.update(this.model).pipe(take(1)).subscribe(
        (newModel: any) => {
          this.model = newModel;
          const vIndex = this.models.findIndex(
            (item: any) => item.LOGIN_SISTEMA === newModel.LOGIN_SISTEMA
          );
          this.models[vIndex] = newModel;
          this.banderaMtto = UpdateType.Browse;
          this.subTituloVentana = RowStatus.Not_Defined.toString();
          this.loadingVisible = false;
          this.permiteSalir = true;
          notify(
            {
              message: 'Registro modificado con exito!',
              width: 'auto',
              shading: false,
            },
            'success',
            1500
          );
        },
        (error: any) => {
          notify(
            {
              message: error,
              width: 'auto',
              shading: false,
              closeOnClick: true,
              closeOnOutsideClick: true,
            },
            'error',
            500000
          );
          this.loadingVisible = false;
        }
      );
    }
  }

  cancelar(): void {
    if (this.banderaMtto === UpdateType.Update) {
      this.model = this.modelUpdate;
      const vIndex = this.models.findIndex((item: any) => item.LOGIN_SISTEMA === this.modelUpdate.LOGIN_SISTEMA);
      this.models[vIndex] = this.modelUpdate;
    }
    this.permiteSalir = true;
    this.banderaMtto = UpdateType.Browse;
    this.subTituloVentana = RowStatus.Not_Defined.toString();
    this.permiteAdd = true;
    this.permiteDele = true;
    this.permiteEdit = true;
  }

  rowRemoving(e: any) {
    this.service.delete(e.data.LOGIN_SISTEMA, this.param).pipe(take(1)).subscribe(
      () => {
        notify(
          {
            message: 'Registro eliminado con exito!',
            width: 'auto',
            shading: false,
          },
          'success',
          1500
        );
        e.component.refresh();
      },
      (error: any) => {
        e.cancel = true;
        notify(
          {
            message: error,
            width: 'auto',
            shading: false,
            closeOnClick: true,
            closeOnOutsideClick: true,
          },
          'error',
          500000
        );
      }
    );
  }

  rowDblClick(e: any) {
    this.banderaMtto = UpdateType.Not_Defined;
    this.subTituloVentana = RowStatus.Browse.toString();
    setTimeout(() => {
      this.bloquear();
    });
    this.consultarUsuarioOpcion(this.model.LOGIN_SISTEMA);
  }

  bloquear(): void {
    this.dataForm.instance.getEditor('LOGIN_SISTEMA')?.option('readOnly', true);
    this.dataForm.instance.getEditor('NOMBRE_USUARIO')?.option('readOnly', true);
    this.dataForm.instance.getEditor('CORREO_ELECTRONICO')?.option('readOnly', true);
    this.dataForm.instance.getEditor('USUARIO_CREA')?.option('readOnly', true);
    this.dataForm.instance.getEditor('FECHA_CREA')?.option('readOnly', true);
    this.dataForm.instance.getEditor('ESTACION_CREA')?.option('readOnly', true);
    this.dataForm.instance.getEditor('USUARIO_ACTU')?.option('readOnly', true);
    this.dataForm.instance.getEditor('FECHA_ACTU')?.option('readOnly', true);
    this.dataForm.instance.getEditor('ESTACION_ACTU')?.option('readOnly', true);
    this.tipoUsuario.readOnly = true;
    this.estadoUsuario.readOnly = true;
    this.permiteAdd = false;
    this.permiteDele = false;
    this.permiteEdit = false;
  }

  permitirSalir():
    | boolean
    | import('rxjs').Observable<boolean>
    | Promise<boolean> {
    if (this.permiteSalir) {
      return true;
    }
    const confirmacion = custom({
      title: 'Confirmación de Salida',
      messageHtml:
        '¿Quieres salir del formulario y perder los cambios realizados?',
      buttons: [
        {
          text: 'Si',
          onClick: (e: any) => true,
        },
        {
          text: 'No',
          onClick: (e: any) => false,
        },
      ],
    });

    return confirmacion.show().then(() => {});
  }
  //#endregion

  valueChangeTIPO_USUARIO(e: any): void {
    if (this.model.TIPO_USUARIO === 0) {
      this.dataTipoUsuario.instance.clearSelection();
    }
  }

  selectionChangedTIPO_USUARIO(selectedRowKeys: any): void {
    if (selectedRowKeys.length > 0) {
      this.model.TIPO_USUARIO = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickTIPO_USUARIO(e: any, data: any): void {
    this.tipoUsuario.instance.close();
    this.tipoUsuario.instance.focus();
  }

  valueChangeESTADO_USUARIO(e: any): void {
    if (this.model.ESTADO_USUARIO === 0) {
      this.dataEstadoUsuario.instance.clearSelection();
    }
  }

  selectionChangedESTADO_USUARIO(selectedRowKeys: any): void {
    if (selectedRowKeys.length > 0) {
      this.model.ESTADO_USUARIO = selectedRowKeys[0].CODIGO;
    }
  }

  rowClickESTADO_USUARIO(e: any, data: any): void {
    this.estadoUsuario.instance.close();
    this.estadoUsuario.instance.focus();
  }

  //#region <Metodos Usuario Opcion>

  gDataUsuarioOpcionRowUpdated(e: any) {
    if (e.data.MTTO !== UpdateType.Add) {
      e.data.MTTO = UpdateType.Update;
    }
  }

  gDataUsuarioOpcionRowRemoving(e: any) {
    if (e.data.MTTO !== UpdateType.Add) {
      this.param.LOGIN_SISTEMA = this.model.LOGIN_SISTEMA;
      this.service
        .deleteUsuarioOpcion(e.data.LOGIN_SISTEMA, this.param)
        .subscribe(
          () => {
            notify(
              {
                message: 'Registro eliminado con exito! ',
                width: 'auto',
                shading: false,
              },
              'success',
              1500
            );
          },
          (error: any) => {
            e.cancel = true;
            notify(
              {
                message: error,
                width: 'auto',
                shading: false,
                closeOnClick: true,
                closeOnOutsideClick: true,
              },
              'error',
              500000
            );
          }
        );
    }
  }

  consultarUsuarioOpcion(loginSiStema: string) {
    this.param.TIPO_CONSULTA = 1;
    this.param.LOGIN_SISTEMA = loginSiStema;
    this.param.OPCION_CONSULTA = 0;
    this.service.getAllUsuarioOpcion(this.param).pipe(take(1)).subscribe((model: any[]) => {
      this.model.DETALLE = model;
    });
  }

  rowUpdated(e: any) {
    e.data.LOGIN_SISTEMA = this.model.LOGIN_SISTEMA;
    if (e.data.MTTO === UpdateType.Browse) {
      e.data.MTTO = UpdateType.Update;
    } else if (e.data.MTTO === UpdateType.Not_Defined) {
      e.data.MTTO = UpdateType.Delete;
    }
  }

  focusedRowChangedUsuarioOpcion(e: any) {
    this.vFocucedRow = e.row;
  }

  getRowsUpdatedUsuarioOpcion(): any[] {
    let modelUpdated: any[] = [];
    if (this.gDataUsuarioOpcion.instance.getDataSource() !== null) {
      modelUpdated = this.gDataUsuarioOpcion.instance
        .getDataSource()
        .items()
        .filter(
          (x) => x.MTTO === UpdateType.Add || x.MTTO === UpdateType.Update
        );
      return modelUpdated;
    }

    return modelUpdated;
  }

  valueChangedPermisos(e: any, cellinfo: any) {
    cellinfo.row.data.LOGIN_SISTEMA = this.model.LOGIN_SISTEMA;
    if (e.value === true) {
      cellinfo.row.data.NUEVO = true;
      cellinfo.row.data.MODIFICAR = true;
      cellinfo.row.data.ELIMINAR = true;
      cellinfo.row.data.IMPRIMIR = true;
      cellinfo.row.data.MTTO = UpdateType.Add;
    } else {
      cellinfo.row.data.NUEVO = false;
      cellinfo.row.data.MODIFICAR = false;
      cellinfo.row.data.ELIMINAR = false;
      cellinfo.row.data.IMPRIMIR = false;
      cellinfo.row.data.MTTO = UpdateType.Update;
    }
  }

  MarcarTodos(e: any) {
    this.model.DETALLE.forEach((x) => {
      x.LOGIN_SISTEMA = this.model.LOGIN_SISTEMA;
      x.SELECCION = true;
      x.NUEVO = true;
      x.MODIFICAR = true;
      x.ELIMINAR = true;
      x.IMPRIMIR = true;
      x.MTTO = UpdateType.Add;
    });
  }

  DesMarcarTodos(e: any) {
    this.model.DETALLE.forEach((x) => {
      x.LOGIN_SISTEMA = this.model.LOGIN_SISTEMA;
      x.SELECCION = false;
      x.NUEVO = false;
      x.MODIFICAR = false;
      x.ELIMINAR = false;
      x.IMPRIMIR = false;
      x.MTTO = UpdateType.Update;
    });
  }

  onToolbarPreparing(e: any) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
      },
      {
        location: 'before',
        widget: 'dxButton',
        visible: this.permiteEdit,
        options: {
          stylingMode: 'contained',
          type: 'success',
          icon: 'check',
          width: 'auto',
          text: 'Todos',
          onClick: this.MarcarTodos.bind(this),
        },
      },
      {
        location: 'before',
        widget: 'dxButton',
        visible: this.permiteEdit,
        options: {
          stylingMode: 'contained',
          type: 'danger',
          icon: 'clear',
          width: 'auto',
          text: 'Ninguno',
          onClick: this.DesMarcarTodos.bind(this),
        },
      }
    );
  }

  isToolbar(): boolean {
    if (this.banderaMtto !== UpdateType.Add) {
      return true;
    }
    return false;
  }
  //#endregion

  mostrarPopup() {
/*     if (typeof this.vFocusedRow !== 'undefined') { */
      this.popupVisible = true;
    /* } else {
      // tslint:disable-next-line: max-line-length
      notify(
        {
          message:
            'Debe seleccionar el usuario al que se le reasignara la clave',
          width: 'auto',
          shading: false,
          closeOnClick: true,
          closeOnOutsideClick: true,
        },
        'error',
        50000
      );
    } */
  }

  hidePopup() {
    this.popupVisible = false;
    this.cambiarClave.LOGIN_SISTEMA = '';
    this.cambiarClave.CLAVE_USUARIO_NUEVA = '';
    this.cambiarClave.CLAVE_CONFIRMAR = '';
  }

  reasignarPass(vFocusedRow: any) {
    this.cambiarClave.LOGIN_SISTEMA = 'mmhuezo';//vFocusedRow.data.LOGIN_SISTEMA;
    this.cambiarClave.CLAVE_USUARIO_NUEVA = 'prueba22';
    this.cambiarClave.CLAVE_CONFIRMAR = 'prueba22';
/*     if (this.cambiarClave.CLAVE_USUARIO_NUEVA !== this.cambiarClave.CLAVE_CONFIRMAR) {
      // tslint:disable-next-line: max-line-length
      return notify({ message: 'Las claves no coinciden', width: 'auto', shading: false }, 'warning', 2000);
    } */
    this.service.reasignarClave(this.cambiarClave).subscribe(
      () => {
        notify({ message: 'Clave cambiada con exito!', width: 'auto', shading: false }, 'success', 1500);
        //this.popupVisible = false;
        this.cambiarClave.LOGIN_SISTEMA = '';
        this.cambiarClave.CLAVE_USUARIO_NUEVA = '';
        this.cambiarClave.CLAVE_CONFIRMAR = '';
      },
      (error: any) => {
        notify({ message: error, width: 'auto', shading: false, closeOnClick: true, closeOnOutsideClick: true}, 'error', 50000);
      }
    );
  }
}
