import { NgModule } from '@angular/core';
import { SegUsuarioRoutingModule } from './seg-usuario-routing.module';

@NgModule({
  imports: [
    SegUsuarioRoutingModule
  ]
})
export class SegUsuarioModule { }
