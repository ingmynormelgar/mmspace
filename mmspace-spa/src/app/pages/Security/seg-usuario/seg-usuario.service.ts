import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class SegUsuarioService {
  readonly urlMtto = environment.apiUrl + 'SEG_USUARIO/';
  jwtHelper = new JwtHelperService();
  decodedToken: any;
  public urlIntentaAcceder = '';


  constructor(private http: HttpClient) {}

  getAll(param: any): Observable<any[]> {
    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
      parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
    }

    return this.http.get<any[]>(this.urlMtto, { params: parametros });
  }

  get(id: number, param: any): Observable<any> {
    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
    }

    return this.http.get<any>(this.urlMtto + id, { params: parametros });
  }

  insert(model: any): any {
    return this.http.post(this.urlMtto, model).pipe(
      map((response: any) => response)
    );
  }

  update(model: any): any {
    return this.http.put(this.urlMtto, model).pipe(
      map((response: any) => response)
    );
  }

  delete(id: number, param: any): any {
    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
    }

    return this.http.delete(this.urlMtto + id, { params: parametros }).pipe(
      map((response: any) => response)
    );
  }

  //#region <Opciones>

  getAllUsuarioOpcion(param: any): Observable<any[]> {
    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
      parametros = parametros.append('LOGIN_SISTEMA', param.LOGIN_SISTEMA);
      parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
    }

    return this.http.get<any[]>(this.urlMtto + 'GetAllUsuarioOpcion', { params: parametros });
  }

  getUsuarioOpcion(id: number, param: any): Observable<any> {
    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('LOGIN_SISTEMA', param.LOGIN_SISTEMA);
    }

    return this.http.get<any>(this.urlMtto + id, { params: parametros });
  }

  insertUsuarioOpcion(model: any): any {
    return this.http.post(this.urlMtto, model).pipe(
      map((response: any) => response)
    );
  }

  updateUsuarioOpcion(model: any): any {
    return this.http.put(this.urlMtto, model).pipe(
      map((response: any) => response)
    );
  }

  deleteUsuarioOpcion(id: number, param: any): any {
    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_EMPRESA', param.CORR_EMPRESA);
      parametros = parametros.append('LOGIN_SISTEMA', param.LOGIN_SISTEMA);
    }

    return this.http.delete(this.urlMtto + 'DeleteUsuarioOpcion/' + id, { params: parametros }).pipe(
      map((response: any) => response)
    );
  }

  getTipoUsuario(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
      parametros = parametros.append('CORR_LISTA', param.CORR_LISTA);
      parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
    }

    return this.http.get<any[]>(this.urlMtto + 'GetTipoUsuario', { params: parametros });
  }

  getEstadoUsuario(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
      parametros = parametros.append('CORR_LISTA', param.CORR_LISTA);
      parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
    }

    return this.http.get<any[]>(this.urlMtto + 'GetEstadoUsuario', { params: parametros });
  }

  cambioClave(model: any) {
    return this.http.post(this.urlMtto + 'CambioClave', model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          localStorage.setItem('token', user.token);
          this.decodedToken = this.jwtHelper.decodeToken(user.token);
        }
      })
    );
  }

  reasignarClave(model: any) {
    return this.http.post(this.urlMtto + 'ReasignarClave', model).pipe(
      map((response: any) => {
      })
    );
  }
  //#endregion
}
