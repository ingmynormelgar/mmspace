/* eslint-disable @typescript-eslint/naming-convention */
import { Component, NgModule, Output, Input, EventEmitter, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { take } from 'rxjs/internal/operators/take';
import { DxTreeViewModule, DxTreeViewComponent } from 'devextreme-angular/ui/tree-view';

import * as events from 'devextreme/events';
import { AuthService } from '../../services';

@Component({
  selector: 'app-side-navigation-menu',
  templateUrl: './side-navigation-menu.component.html',
  styleUrls: ['./side-navigation-menu.component.scss']
})
export class SideNavigationMenuComponent implements AfterViewInit, OnDestroy {
  @ViewChild(DxTreeViewComponent, { static: true })
  menu!: DxTreeViewComponent;

  @Output()
  selectedItemChanged = new EventEmitter<string>();

  @Output()
  openMenu = new EventEmitter<any>();

  private SelectedItem = '';
  @Input()
  set selectedItem(value: string) {
    this.SelectedItem = value;
    if (!this.menu.instance) {
      return;
    }

    this.menu.instance.selectItem(value);
  }

  private Items: any;
  get items(): any {
    if (this.Items === undefined) {
      this.Items = ''; // Solo para que deje de ser undefined y solo ingrese una vez.
      this.authService.getMenu().pipe(take(1)).subscribe((menu: any[]) => {
        this.Items = menu.map((item) => {
          if (item.path && !(/^\//.test(item.path))) {
            item.path = `/${item.path}`;
          }
          return { ...item, expanded: !this.CompactMode };
        });

        return this.Items;
      });
    }

    return this.Items;
  }

  private CompactMode = false;
  @Input()
  get compactMode(): boolean {
    return this.CompactMode;
  }
  set compactMode(val) {
    this.CompactMode = val;

    if (!this.menu.instance) {
      return;
    }

    if (val) {
      this.menu.instance.collapseAll();
    } else {
      this.menu.instance.expandItem(this.SelectedItem);
    }
  }

  constructor(private elementRef: ElementRef, private authService: AuthService) { }

  onItemClick(event: any): void {
    this.selectedItemChanged.emit(event);
  }

  ngAfterViewInit(): void {
    events.on(this.elementRef.nativeElement, 'dxclick', (e: any) => {
      this.openMenu.next(e);
    });
  }

  ngOnDestroy(): void {
    events.off(this.elementRef.nativeElement, 'dxclick');
  }
}

@NgModule({
  imports: [ DxTreeViewModule ],
  declarations: [ SideNavigationMenuComponent ],
  exports: [ SideNavigationMenuComponent ]
})
export class SideNavigationMenuModule { }
