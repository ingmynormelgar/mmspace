/* eslint-disable @typescript-eslint/naming-convention */
export enum RowStatus {
  Add = 'Adicionando Registro',
  Update = 'Modificando Registro',
  Delete = 'Eliminando Registro',
  Browse = 'Consultando Registro',
  Not_Defined = ''
}
