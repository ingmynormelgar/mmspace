/* eslint-disable @typescript-eslint/naming-convention */
export enum UpdateType {
  Add = 1,
  Update = 2,
  Delete = 3,
  Browse = 4,
  Not_Defined = 5
}
