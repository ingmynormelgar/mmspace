export interface Resultado {
  isOk: boolean;
  data?: any;
  message?: string;
}
