/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { DatePipe } from '@angular/common';

@Injectable()
export class AppInfoService {
  constructor(private authService: AuthService,
              private datePipe: DatePipe
              ) {}

  public get title(): string {
    return 'MmSpace';
  }

  CORR_SUSCRIPCION = 7;
  CORR_CONFI_PAIS = 7;
  CORR_EMPRESA = 1;

  public get currentYear(): number {
    return new Date().getFullYear();
  }

  public get getLocale() {
    return 'es-SV';
  }

  getPermiso(opcion: string): string {
    return this.authService.decodedToken[opcion];
  }

  toDate(date: any) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  toMonthComplete(date: any) {
    return this.datePipe.transform(date, 'd MMMM y');
  }

  toDateFMT(date: any, fmt: string) {
    return this.datePipe.transform(date, fmt);
  }

  toYear(date: any) {
    const vYear = this.datePipe.transform(date, 'yyyy') || 0;
    return +vYear;
  }

  toMonth(date: any) {
    const vMonth = this.datePipe.transform(date, 'MM') || 0;
    return +vMonth;
  }

  toDay(date: any) {
    const vDay = this.datePipe.transform(date, 'dd') || 0;
    return +vDay;
  }

  getDate(date?: Date, hours?: number, min?: number, sec?: number) {
    if (date === undefined) { date=new Date(); };
    if (hours === undefined) { hours=0; };
    if (min === undefined) { min=0; };
    if (sec === undefined) { sec=0; };

    const d = new Date(date.setHours(0, 0, 0, 0));

    return new Date(d.setHours(hours, min, sec, 0));
  }

  getDays(from: Date, to: Date) {
    if (from === null || to === null) {
      return 0;
    }

    return Math.floor((to.valueOf() - from.valueOf()) / 1000 / 60 / 60 / 24 + 1);
  }

  getHours(from: Date, to: Date) {
    if (from === null || to === null) {
      return 0;
    }

    return Math.floor((to.valueOf() - from.valueOf()) / 1000 / 60 / 60);
  }

  getMinutes(from: Date, to: Date) {
    if (from === null || to === null) {
      return 0;
    }

    return Math.floor((to.valueOf() - from.valueOf()) / 1000 / 60);
  }

/**
 * Adds time to a date. Modelled after MySQL DATE_ADD function.
 * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
 * https://stackoverflow.com/a/1214753/18511
 *
 * @param date  Date to start with
 * @param interval  One of: year, quarter, month, week, day, hour, minute, second
 * @param units  Number of units of the given interval to add.
 */
  dateAdd(date: Date, interval: string, units: number) {
    if(!(date instanceof Date))
      {return new Date();}

    let ret = new Date(date); //don't change original date

    const checkRollover = () => { if(ret.getDate() !== date.getDate()) {ret.setDate(0);}};
    switch(String(interval).toLowerCase()) {
      case 'year'   :  ret.setFullYear(ret.getFullYear() + units); checkRollover();  break;
      case 'quarter':  ret.setMonth(ret.getMonth() + 3*units); checkRollover();  break;
      case 'month'  :  ret.setMonth(ret.getMonth() + units); checkRollover();  break;
      case 'week'   :  ret.setDate(ret.getDate() + 7*units);  break;
      case 'day'    :  ret.setDate(ret.getDate() + units);  break;
      case 'hour'   :  ret.setTime(ret.getTime() + units*3600000);  break;
      case 'minute' :  ret.setTime(ret.getTime() + units*60000);  break;
      case 'second' :  ret.setTime(ret.getTime() + units*1000);  break;
      default       :  ret = new Date(date); break;
    }
    return ret;
  }
}
