/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { map } from 'rxjs/operators';
import { Resultado } from '../models/result';
import { Observable } from 'rxjs';
import notify from 'devextreme/ui/notify';

const defaultPath = '/';
@Injectable()
export class AuthService {
  readonly urlMtto = environment.apiUrl + 'SEG_USUARIO/';
  jwtHelper = new JwtHelperService();
  decodedToken: any;
  public urlIntentaAcceder = '';

  // private _user = defaultUser;
  // get loggedIn(): boolean {
  //   return !!this._user;
  // }

  // eslint-disable-next-line @typescript-eslint/naming-convention
  private LastAuthenticatedPath: string = defaultPath;
  set lastAuthenticatedPath(value: string) {
    this.LastAuthenticatedPath = value;
  }

  constructor(private router: Router,
              private http: HttpClient) { }

  logIn(login: string, password: string): any {

    return this.http.post(this.urlMtto + 'login', {LOGIN_SISTEMA: login, CLAVE_USUARIO: password} ).pipe(
      map((response: any) => {
        if (response) {
          localStorage.setItem('token', response.token);
          this.decodedToken = this.jwtHelper.decodeToken(response.token);
        }

        return response;
      })
    );
  }

  // async getUser() {
  //   try {
  //     // Send request

  //     return {
  //       isOk: true,
  //       data: this._user
  //     };
  //   }
  //   catch {
  //     return {
  //       isOk: false
  //     };
  //   }
  // }

  get loggedIn(): boolean {
    const token = localStorage.getItem('token') || '';

    if (!this.jwtHelper.isTokenExpired(token)) {
      return true;
    }
    return false;
  }

  async logOut(): Promise<void> {
    localStorage.removeItem('token');
    this.router.navigate(['/login-form']);
  }

  getMenu(): Observable<any[]> {
    return this.http.get<any[]>(this.urlMtto + 'menu/', { });
  }

  // async getMenu(): Promise<any[]> {
  //   return this.http.get<any[]>(this.urlMtto + 'menu/', { });
  // }
}

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const isLoggedIn = this.authService.loggedIn;
    const isLoginForm = route.routeConfig?.path === 'login-form';
    let isAuthorized = false;
    let routerUrl: string;
    let routerList: Array<any>;

    // eslint-disable-next-line prefer-const
    routerList = state.url.slice(1).split('/');
    // eslint-disable-next-line prefer-const
    routerUrl = '/' + routerList[0];

    if (isLoggedIn && isLoginForm) {
      this.authService.lastAuthenticatedPath = defaultPath;
      this.router.navigate([defaultPath]);
      return false;
    }

    if (!isLoggedIn && !isLoginForm) {
      this.router.navigate(['/login-form']);
    }

    if (isLoggedIn) {
      if (routerUrl === '/' || routerUrl === '/home' || routerUrl === '/profile') {
        isAuthorized = true;
      } else
      if (this.authService.decodedToken[routerUrl] &&
        typeof this.authService.decodedToken[routerUrl] === 'string') {
        if (this.authService.decodedToken[routerUrl].includes('R')) {
          this.authService.lastAuthenticatedPath = route.routeConfig?.path || '';
          isAuthorized = true;
        }
      }
    }

    if ((isAuthorized === false || isLoggedIn === false) && isLoginForm === false) {
      if (routerUrl !== '/' && routerUrl !== '/home') {
        notify({ message: 'ACCESO NO AUTORIZADO!',
              width: 'auto', shading: false,
              closeOnClick: true, closeOnOutsideClick: true}, 'error', 500000);
      }
    }

    return (isLoggedIn && isAuthorized) || isLoginForm;
  }
}
