import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GenListaDetaService {
  readonly urlMtto = environment.apiUrl + 'GEN_LISTA_DETA/';

  constructor(private http: HttpClient) { }

  getAll(param: any): Observable<any[]> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('TIPO_CONSULTA', param.TIPO_CONSULTA);
      parametros = parametros.append('CORR_LISTA', param.CORR_LISTA);
      parametros = parametros.append('OPCION_CONSULTA', param.OPCION_CONSULTA);
    }

    return this.http.get<any[]>(this.urlMtto, { params: parametros });
  }

  get(id: number, param: any): Observable<any> {

    let parametros = new HttpParams();

    if (param != null) {
      parametros = parametros.append('CORR_LISTA', param.CORR_LISTA);
    }

    return this.http.get<any>(this.urlMtto + id, { params: parametros });
  }

}
