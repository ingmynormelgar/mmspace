export const environment = {
  production: true,
  apiUrl: 'https://mmspace-api.azurewebsites.net/api/',
  apiUrlToken: 'mmspace-api.azurewebsites.net'
};
