using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.AssetManagement;
using mmspace.api.Data.Parameters.AssetManagement;
using mmspace.api.Models.AssetManagement;

namespace mmspace.api.Data.Controllers.AssetManagement
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class GEN_DEPTOController : ControllerBase
	{
		private readonly IGEN_DEPTORepository _repo;
		
		public GEN_DEPTOController(IGEN_DEPTORepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/gen-pais|R")]
		public async Task<List<V_GEN_DEPTO>> GetAll([FromQuery]DATA_GEN_DEPTO Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_DEPTO}")]
		[Authorize(Policy = "/gen-pais|R")]
		public async Task<V_GEN_DEPTO> Get(Int32 CORR_DEPTO,[FromQuery]DATA_GEN_DEPTO Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_PAIS,CORR_DEPTO);
		}
		
		[HttpPost]
		[Authorize(Policy = "/gen-pais|C")]
		public async Task<IActionResult> Post(GEN_DEPTO Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_PAIS, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/gen-pais|U")]
		public async Task<IActionResult> Put(GEN_DEPTO Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_PAIS, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_DEPTO}")]
		[Authorize(Policy = "/gen-pais|D")]
		public async Task<IActionResult> Delete(Int32 CORR_DEPTO,[FromQuery]DATA_GEN_DEPTO Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_PAIS,CORR_DEPTO, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}