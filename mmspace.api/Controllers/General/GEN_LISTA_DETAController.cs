using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Data.Parameters.General;
using mmspace.api.Models.General;

namespace mmspace.api.Data.Controllers.General
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class GEN_LISTA_DETAController : ControllerBase
	{
		private readonly IGEN_LISTA_DETARepository _repo;
		
		public GEN_LISTA_DETAController(IGEN_LISTA_DETARepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[AllowAnonymous]
		public async Task<List<V_GEN_LISTA_DETA>> GetAll([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CODIGO}")]
		public async Task<V_GEN_LISTA_DETA> Get(String CODIGO,[FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			return await _repo.Get(Data.CORR_LISTA,CODIGO);
		}
	}
}