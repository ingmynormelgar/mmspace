using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Data.Parameters.General;
using mmspace.api.Models.General;

namespace mmspace.api.Data.Controllers.General
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class GEN_MONEDAController : ControllerBase
	{
		private readonly IGEN_MONEDARepository _repo;
		
		public GEN_MONEDAController(IGEN_MONEDARepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/gen-moneda|R")]
		public async Task<List<V_GEN_MONEDA>> GetAll([FromQuery]DATA_GEN_MONEDA Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_MONEDA}")]
		[Authorize(Policy = "/gen-moneda|R")]
		public async Task<V_GEN_MONEDA> Get(Int32 CORR_MONEDA,[FromQuery]DATA_GEN_MONEDA Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,CORR_MONEDA);
		}
		
		[HttpPost]
		[Authorize(Policy = "/gen-moneda|C")]
		public async Task<IActionResult> Post(GEN_MONEDA Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/gen-moneda|U")]
		public async Task<IActionResult> Put(GEN_MONEDA Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_MONEDA}")]
		[Authorize(Policy = "/gen-moneda|D")]
		public async Task<IActionResult> Delete(Int32 CORR_MONEDA,[FromQuery]DATA_GEN_MONEDA Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,CORR_MONEDA, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}