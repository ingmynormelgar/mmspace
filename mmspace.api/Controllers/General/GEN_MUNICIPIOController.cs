using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.AssetManagement;
using mmspace.api.Data.Parameters.AssetManagement;
using mmspace.api.Models.AssetManagement;

namespace mmspace.api.Data.Controllers.AssetManagement
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class GEN_MUNICIPIOController : ControllerBase
	{
		private readonly IGEN_MUNICIPIORepository _repo;
		
		public GEN_MUNICIPIOController(IGEN_MUNICIPIORepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/gen-pais|R")]
		public async Task<List<V_GEN_MUNICIPIO>> GetAll([FromQuery]DATA_GEN_MUNICIPIO Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_MUNICIPIO}")]
		[Authorize(Policy = "/gen-pais|R")]
		public async Task<V_GEN_MUNICIPIO> Get(Int32 CORR_MUNICIPIO,[FromQuery]DATA_GEN_MUNICIPIO Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_PAIS,Data.CORR_DEPTO,CORR_MUNICIPIO);
		}
		
		[HttpPost]
		[Authorize(Policy = "/gen-pais|C")]
		public async Task<IActionResult> Post(GEN_MUNICIPIO Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_PAIS, Data.CORR_DEPTO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/gen-pais|U")]
		public async Task<IActionResult> Put(GEN_MUNICIPIO Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_PAIS, Data.CORR_DEPTO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_MUNICIPIO}")]
		[Authorize(Policy = "/gen-pais|D")]
		public async Task<IActionResult> Delete(Int32 CORR_MUNICIPIO,[FromQuery]DATA_GEN_MUNICIPIO Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_PAIS,Data.CORR_DEPTO,CORR_MUNICIPIO, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}