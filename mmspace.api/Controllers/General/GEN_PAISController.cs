using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.AssetManagement;
using mmspace.api.Data.Parameters.AssetManagement;
using mmspace.api.Models.AssetManagement;

namespace mmspace.api.Data.Controllers.AssetManagement
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class GEN_PAISController : ControllerBase
	{
		private readonly IGEN_PAISRepository _repo;
		
		public GEN_PAISController(IGEN_PAISRepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/gen-pais|R")]
		public async Task<List<V_GEN_PAIS>> GetAll([FromQuery]DATA_GEN_PAIS Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_PAIS}")]
		[Authorize(Policy = "/gen-pais|R")]
		public async Task<V_GEN_PAIS> Get(Int32 CORR_PAIS,[FromQuery]DATA_GEN_PAIS Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,CORR_PAIS);
		}
		
		[HttpPost]
		[Authorize(Policy = "/gen-pais|C")]
		public async Task<IActionResult> Post(GEN_PAIS Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/gen-pais|U")]
		public async Task<IActionResult> Put(GEN_PAIS Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_PAIS}")]
		[Authorize(Policy = "/gen-pais|D")]
		public async Task<IActionResult> Delete(Int32 CORR_PAIS,[FromQuery]DATA_GEN_PAIS Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,CORR_PAIS, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}