using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Data.Parameters.General;
using mmspace.api.Models.General;

namespace mmspace.api.Data.Controllers.General
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class GEN_TIPO_DIPController : ControllerBase
	{
		private readonly IGEN_TIPO_DIPRepository _repo;
		
		public GEN_TIPO_DIPController(IGEN_TIPO_DIPRepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/gen-tipo-dip|R")]
		public async Task<List<V_GEN_TIPO_DIP>> GetAll([FromQuery]DATA_GEN_TIPO_DIP Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_TIPO_DIP}")]
		[Authorize(Policy = "/gen-tipo-dip|R")]
		public async Task<V_GEN_TIPO_DIP> Get(Int32 CORR_TIPO_DIP,[FromQuery]DATA_GEN_TIPO_DIP Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_TIPO_DIP);
		}
		
		[HttpPost]
		[Authorize(Policy = "/gen-tipo-dip|C")]
		public async Task<IActionResult> Post(GEN_TIPO_DIP Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/gen-tipo-dip|U")]
		public async Task<IActionResult> Put(GEN_TIPO_DIP Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_TIPO_DIP}")]
		[Authorize(Policy = "/gen-tipo-dip|D")]
		public async Task<IActionResult> Delete(Int32 CORR_TIPO_DIP,[FromQuery]DATA_GEN_TIPO_DIP Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_TIPO_DIP, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}