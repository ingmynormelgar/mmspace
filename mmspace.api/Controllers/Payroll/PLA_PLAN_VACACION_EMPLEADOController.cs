using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Payroll;
using mmspace.api.Data.Parameters.Payroll;
using mmspace.api.Models.Payroll;

namespace mmspace.api.Data.Controllers.Payroll
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class PLA_PLAN_VACACION_EMPLEADOController : ControllerBase
	{
		private readonly IPLA_PLAN_VACACION_EMPLEADORepository _repo;
		
		public PLA_PLAN_VACACION_EMPLEADOController(IPLA_PLAN_VACACION_EMPLEADORepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pla-plan-vacacion-empleado|R")]
		public async Task<List<V_PLA_PLAN_VACACION_EMPLEADO>> GetAll([FromQuery]DATA_PLA_PLAN_VACACION_EMPLEADO Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.OPCION_CONSULTA = 1;
			Data.LOGIN_SISTEMA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_EMPLEADO}")]
		[Authorize(Policy = "/pla-plan-vacacion-empleado|R")]
		public async Task<V_PLA_PLAN_VACACION_EMPLEADO> Get(Int32 CORR_EMPLEADO,[FromQuery]DATA_PLA_PLAN_VACACION_EMPLEADO Data)
		{
			Data.LOGIN_SISTEMA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.ANIO_PERIODO,CORR_EMPLEADO,1);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pla-plan-vacacion-empleado|C")]
		public async Task<IActionResult> Post(PLA_PLAN_VACACION_EMPLEADO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA,Data.ANIO_PERIODO, (Int32)(resultado.CodeHelper),1);
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pla-plan-vacacion-empleado|U")]
		public async Task<IActionResult> Put(PLA_PLAN_VACACION_EMPLEADO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA,Data.ANIO_PERIODO, (Int32)(resultado.CodeHelper),1);
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_EMPLEADO}")]
		[Authorize(Policy = "/pla-plan-vacacion-empleado|D")]
		public async Task<IActionResult> Delete(Int32 CORR_EMPLEADO,[FromQuery]DATA_PLA_PLAN_VACACION_EMPLEADO Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.ANIO_PERIODO,CORR_EMPLEADO, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpPut("SolicitarPro")]
		[Authorize(Policy = "/pla-plan-vacacion-empleado|R")]
		public async Task<IActionResult> SolicitarPro(PLA_PLAN_VACACION_EMPLEADO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.ESTACION_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.SolicitarPro(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.ANIO_PERIODO, (Int32)(resultado.CodeHelper),1);
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		} 

		[HttpPut("Autorizar")]
		[Authorize(Policy = "/pla-plan-vacacion-empleado-autorizar|R")]
		public async Task<IActionResult> Autorizar(List<PLA_PLAN_VACACION_EMPLEADO> Data)
		{
			foreach (var Autorizar in Data)
			{
				Autorizar.USUARIO_ACTU = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
				Autorizar.ESTACION_CREA = Autorizar.ESTACION_CREA;
				Autorizar.FECHA_ACTU = DateTime.Now;
				Autorizar.ESTACION_ACTU = Autorizar.ESTACION_CREA;
			}
			
			var resultado = await _repo.Autorizar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return StatusCode(201);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		} 

		[HttpGet("GetAllAutorizar")]
		[Authorize(Policy = "/pla-plan-vacacion-empleado-autorizar|R")]
		public async Task<List<V_PLA_PLAN_VACACION_EMPLEADO>> GetAllAutorizar([FromQuery]DATA_PLA_PLAN_VACACION_EMPLEADO Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.OPCION_CONSULTA = 2;
			Data.LOGIN_SISTEMA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}

		[HttpGet("GetAllListado")]
		[Authorize(Policy = "/pla-plan-vacacion-empleado-listado|R")]
		public async Task<List<V_PLA_PLAN_VACACION_EMPLEADO>> GetAllListado([FromQuery]DATA_PLA_PLAN_VACACION_EMPLEADO Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.OPCION_CONSULTA = 3;
			Data.LOGIN_SISTEMA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}
	}
}