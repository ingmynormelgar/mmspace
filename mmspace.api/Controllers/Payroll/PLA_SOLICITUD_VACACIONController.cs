using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Payroll;
using mmspace.api.Data.Parameters.Payroll;
using mmspace.api.Data.Parameters.General;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Models.General;

namespace mmspace.api.Data.Controllers.Payroll
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class PLA_SOLICITUD_VACACIONController : ControllerBase
	{
		private readonly IPLA_SOLICITUD_VACACIONRepository _repo;
		private readonly IGEN_LISTA_DETARepository _repoEstado;
		private readonly IGEN_EMPLEADORepository _repoEmpleado;
		private readonly IGEN_TIPO_MONEDARepository _repoTipoMoneda;
		private readonly IGEN_EMPRESARepository _repoEmpresa;

		public PLA_SOLICITUD_VACACIONController(IPLA_SOLICITUD_VACACIONRepository repo,
												IGEN_EMPLEADORepository repoEmpleado,
												IGEN_LISTA_DETARepository repoEstado,
												IGEN_TIPO_MONEDARepository repoTipoMoneda,
												IGEN_EMPRESARepository repoEmpresa)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
			_repoEmpleado = repoEmpleado ?? throw new ArgumentNullException(nameof(_repoEmpleado));
			_repoEstado = repoEstado ?? throw new ArgumentNullException(nameof(_repoEstado));
			_repoTipoMoneda = repoTipoMoneda ?? throw new ArgumentNullException(nameof(_repoTipoMoneda));
			_repoEmpresa = repoEmpresa ?? throw new ArgumentNullException(nameof(_repoEmpresa));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pla-solicitud-vacacion|R")]
		public async Task<List<V_PLA_SOLICITUD_VACACION>> GetAll([FromQuery]DATA_PLA_SOLICITUD_VACACION Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.OPCION_CONSULTA = 2;
			Data.LOGIN_SISTEMA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}

    	[HttpGet ("GetAllAutorizar")]
		[Authorize(Policy = "/pla-solicitud-vacacion-autorizar|R")]
		public async Task<List<V_PLA_SOLICITUD_VACACION>> GetAllAutorizar([FromQuery]DATA_PLA_SOLICITUD_VACACION Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.OPCION_CONSULTA = 3;
			Data.LOGIN_SISTEMA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_SOLI_VACACION}")]
		[Authorize(Policy = "/pla-solicitud-vacacion|R")]
		public async Task<V_PLA_SOLICITUD_VACACION> Get(Int32 CORR_SOLI_VACACION,[FromQuery]DATA_PLA_SOLICITUD_VACACION Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_SOLI_VACACION,Data.CORR_EMPLEADO);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pla-solicitud-vacacion|C")]
		public async Task<IActionResult> Post(PLA_SOLICITUD_VACACION Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper) );
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pla-solicitud-vacacion|U")]
		public async Task<IActionResult> Put(PLA_SOLICITUD_VACACION Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_SOLI_VACACION}")]
		[Authorize(Policy = "/pla-solicitud-vacacion|D")]
		public async Task<IActionResult> Delete(Int32 CORR_SOLI_VACACION,[FromQuery]DATA_PLA_SOLICITUD_VACACION Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA, Data.CORR_EMPLEADO, CORR_SOLI_VACACION, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	
		[HttpGet("GetEstados")]
		[Authorize(Policy = "/pla-solicitud-vacacion|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstados([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.OPCION_CONSULTA = 0;
			return await _repoEstado.GetAll(Data);
		} 

		[HttpGet("GetEstadosAutorizar")]
		[Authorize(Policy = "/pla-solicitud-vacacion-autorizar|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstadosAutorizar([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.OPCION_CONSULTA = 0;
			return await _repoEstado.GetAll(Data);
		} 

		[HttpGet("GetEmpleados")]
		[Authorize(Policy = "/pla-solicitud-vacacion|R")]
		public async Task<List<V_GEN_EMPLEADO>> GetEmpleados([FromQuery]DATA_GEN_EMPLEADO Data)
		{
			Data.TIPO_CONSULTA = 2;
			Data.OPCION_CONSULTA = 5;
			Data.LOGIN_SISTEMA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repoEmpleado.GetAll(Data);
		}

    	[HttpGet("GetColaborador")]
		[Authorize(Policy = "/pla-solicitud-vacacion-autorizar|R")]
		public async Task<List<V_GEN_EMPLEADO>> GetColaborador([FromQuery]DATA_GEN_EMPLEADO Data)
		{
			Data.TIPO_CONSULTA = 2;
			Data.OPCION_CONSULTA = 6;
			Data.LOGIN_SISTEMA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repoEmpleado.GetAll(Data);
		}

		[HttpGet("GetTipoMonedas")]
		[Authorize(Policy = "/pla-solicitud-vacacion|R")]
		public async Task<List<V_GEN_TIPO_MONEDA>> GetAllTipoMonedas([FromQuery]DATA_GEN_TIPO_MONEDA Data)
		{
			Data.TIPO_CONSULTA = 2;
			Data.OPCION_CONSULTA = 1;
			return await _repoTipoMoneda.GetAll(Data);
		}

		[HttpGet("GetMonedaEmpresa")]
		[Authorize(Policy = "/pla-solicitud-vacacion|R")]
		public async Task<List<V_GEN_EMPRESA>> GetMonedaEmpresa([FromQuery]DATA_GEN_EMPRESA Data)
		{
			Data.TIPO_CONSULTA = 2;
			Data.OPCION_CONSULTA = 2;
			return await _repoEmpresa.GetAll(Data);
		}
	
		[HttpPut("Solicitar")]
		[Authorize(Policy = "/pla-solicitud-vacacion|R")]
		public async Task<IActionResult> Solicitar(PLA_SOLICITUD_VACACION Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.Solicitar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		} 

    	[HttpPut("Autorizar")]
		[Authorize(Policy = "/pla-solicitud-vacacion-autorizar|R")]
		public async Task<IActionResult> Autorizar(PLA_SOLICITUD_VACACION Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.Aplicar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return StatusCode(201, (Int32)(resultado.CodeHelper));
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		} 

    	[HttpPut("Rechazar")]
		[Authorize(Policy = "/pla-solicitud-vacacion-autorizar|R")]
		public async Task<IActionResult> Rechazar(PLA_SOLICITUD_VACACION Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.Rechazar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return StatusCode(201, (Int32)(resultado.CodeHelper));
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	
	}
}