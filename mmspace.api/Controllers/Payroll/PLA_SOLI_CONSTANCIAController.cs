using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Payroll;
using mmspace.api.Data.Parameters.Payroll;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;
using mmspace.api.Data.Repositories.Security;
using mmspace.api.Models.Security;
using mmspace.api.Data.Parameters.Security;

namespace mmspace.api.Data.Controllers.Payroll
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class PLA_SOLI_CONSTANCIAController : ControllerBase
	{
		private readonly IPLA_SOLI_CONSTANCIARepository _repo;
		private readonly IGEN_LISTA_DETARepository _repoEstado;
		private readonly ISEG_USUARIORepository _repoUsuario;
		private readonly IPLA_TIPO_CONSTANCIARepository _repoTipoConstancia;
		
		public PLA_SOLI_CONSTANCIAController(IPLA_SOLI_CONSTANCIARepository repo,
											 IGEN_LISTA_DETARepository repoEstado,
											 ISEG_USUARIORepository repoUsuario,
											 IPLA_TIPO_CONSTANCIARepository repoTipoConstancia)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
			_repoEstado = repoEstado ?? throw new ArgumentNullException(nameof(_repoEstado));
			_repoUsuario = repoUsuario ?? throw new ArgumentNullException(nameof(_repoUsuario));
			_repoTipoConstancia = repoTipoConstancia ?? throw new ArgumentNullException(nameof(_repoTipoConstancia));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pla-soli-constancia|R")]
		public async Task<List<V_PLA_SOLI_CONSTANCIA>> GetAll([FromQuery]DATA_PLA_SOLI_CONSTANCIA Data)
		{
			Data.USUARIO_SOLICITA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_CONSTANCIA}")]
		[Authorize(Policy = "/pla-soli-constancia|R")]
		public async Task<V_PLA_SOLI_CONSTANCIA> Get(Int32 CORR_CONSTANCIA,[FromQuery]DATA_PLA_SOLI_CONSTANCIA Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_CONSTANCIA);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pla-soli-constancia|C")]
		public async Task<IActionResult> Post(PLA_SOLI_CONSTANCIA Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pla-soli-constancia|U")]
		public async Task<IActionResult> Put(PLA_SOLI_CONSTANCIA Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_CONSTANCIA}")]
		[Authorize(Policy = "/pla-soli-constancia|D")]
		public async Task<IActionResult> Delete(Int32 CORR_CONSTANCIA,[FromQuery]DATA_PLA_SOLI_CONSTANCIA Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_CONSTANCIA, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpGet("GetEstados")]
		[Authorize(Policy = "/pla-soli-constancia|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstados([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			return await _repoEstado.GetAll(Data);
		} 

		[HttpGet("GetEmpleados")]
		[Authorize(Policy = "/pla-soli-constancia|R")]
		public async Task<List<V_SEG_USUARIO>> GetEmpleados([FromQuery]DATA_SEG_USUARIO Data)
		{
			Data.LOGIN_SISTEMA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repoUsuario.GetAll(Data);
		}

		[HttpGet("GetTipoConstancias")]
		[Authorize(Policy = "/pla-soli-constancia|R")]
		public async Task<List<V_PLA_TIPO_CONSTANCIA>> GetTIPO_CONSTANCIAs([FromQuery]DATA_PLA_TIPO_CONSTANCIA Data)
		{
			return await _repoTipoConstancia.GetAll(Data);
		}

		[HttpPut("Solicitar")]
		[Authorize(Policy = "/pla-soli-constancia|U")]
		public async Task<IActionResult> Solicitar(PLA_SOLI_CONSTANCIA Data)
		{
			Data.USUARIO_ACTU = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_ACTU = DateTime.Now;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.Solicitar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

	}
}