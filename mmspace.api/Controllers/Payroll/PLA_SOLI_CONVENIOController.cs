using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Payroll;
using mmspace.api.Data.Parameters.Payroll;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;
using mmspace.api.Data.Repositories.Security;
using mmspace.api.Models.Security;
using mmspace.api.Data.Parameters.Security;

namespace mmspace.api.Data.Controllers.Payroll
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class PLA_SOLI_CONVENIOController : ControllerBase
	{
		private readonly IPLA_SOLI_CONVENIORepository _repo;
		private readonly IGEN_LISTA_DETARepository _repoEstado;
		private readonly IGEN_EMPLEADORepository _repoEmpleado;
		private readonly IGEN_LISTA_DETARepository _repoTipoConvenio;
		private readonly IGEN_MONEDARepository _repoMoneda;
		private readonly IGEN_EMPRESARepository _repoEmpresa;
		private readonly IPLA_RUBRORepository _repoRubro;

		
		public PLA_SOLI_CONVENIOController(IPLA_SOLI_CONVENIORepository repo,
										   IGEN_LISTA_DETARepository repoEstado,
										   IGEN_EMPLEADORepository repoEmpleado,
										   IGEN_LISTA_DETARepository repoTipoConvenio,
										   IGEN_MONEDARepository repoMoneda,
										   IGEN_EMPRESARepository repoEmpresa,
										   IPLA_RUBRORepository repoRubro)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
			_repoEstado = repoEstado ?? throw new ArgumentNullException(nameof(_repoEstado));
			_repoEmpleado = repoEmpleado ?? throw new ArgumentNullException(nameof(_repoEmpleado));
			_repoTipoConvenio = repoTipoConvenio ?? throw new ArgumentNullException(nameof(_repoTipoConvenio));
			_repoMoneda = repoMoneda ?? throw new ArgumentNullException(nameof(_repoMoneda));
			_repoEmpresa = repoEmpresa ?? throw new ArgumentNullException(nameof(_repoEmpresa));
			_repoRubro = repoRubro ?? throw new ArgumentNullException(nameof(_repoRubro));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pla-soli-convenio|R")]
		public async Task<List<V_PLA_SOLI_CONVENIO>> GetAll([FromQuery]DATA_PLA_SOLI_CONVENIO Data)
		{
			Data.USUARIO_SOLICITA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_CONVENIO}")]
		[Authorize(Policy = "/pla-soli-convenio|R")]
		public async Task<V_PLA_SOLI_CONVENIO> Get(Int32 CORR_CONVENIO,[FromQuery]DATA_PLA_SOLI_CONVENIO Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_CONVENIO);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pla-soli-convenio|C")]
		public async Task<IActionResult> Post(PLA_SOLI_CONVENIO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pla-soli-convenio|U")]
		public async Task<IActionResult> Put(PLA_SOLI_CONVENIO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_CONVENIO}")]
		[Authorize(Policy = "/pla-soli-convenio|D")]
		public async Task<IActionResult> Delete(Int32 CORR_CONVENIO,[FromQuery]DATA_PLA_SOLI_CONVENIO Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_CONVENIO, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpGet("GetEstados")]
		[Authorize(Policy = "/pla-soli-convenio|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstados([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			return await _repoEstado.GetAll(Data);
		} 

		[HttpGet("GetEmpleados")]
		[Authorize(Policy = "/pla-soli-convenio|R")]
		public async Task<List<V_GEN_EMPLEADO>> GetEmpleados([FromQuery]DATA_GEN_EMPLEADO Data)
		{
			Data.LOGIN_SISTEMA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repoEmpleado.GetAll(Data);
		}

		[HttpGet("GetTipoConvenio")]
		[Authorize(Policy = "/pla-soli-convenio|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetTipoConvenio([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			return await _repoTipoConvenio.GetAll(Data);
		}

		[HttpGet("GetMonedas")]
		[Authorize(Policy = "/pla-soli-convenio|R")]
		public async Task<List<V_GEN_MONEDA>> GetMonedas([FromQuery]DATA_GEN_MONEDA Data)
		{
			return await _repoMoneda.GetAll(Data);
		}

		[HttpGet("GetMonedaEmpresa")]
		[Authorize(Policy = "/pla-soli-convenio|R")]
		public async Task<List<V_GEN_EMPRESA>> GetMonedaEmpresa([FromQuery]DATA_GEN_EMPRESA Data)
		{
			return await _repoEmpresa.GetAll(Data);
		}

		[HttpGet("GetRubros")]
		[Authorize(Policy = "/pla-soli-convenio|R")]
		public async Task<List<V_PLA_RUBRO>> GetRubros([FromQuery]DATA_PLA_RUBRO Data)
		{
			Data.CLASE_RUBRO = "CON";
			return await _repoRubro.GetAll(Data);
		}

		[HttpGet("GetMontoMaximo")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<Decimal> GetMontoMaximo([FromQuery]DATA_PLA_SOLI_CONVENIO Data)
		{
			return (Decimal) await _repo.GetScalar(Data, 0);
		}

		[HttpPut("Solicitar")]
		[Authorize(Policy = "/pla-soli-convenio|R")]
		public async Task<IActionResult> Solicitar(PLA_SOLI_CONVENIO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.Solicitar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		} 

	}
}