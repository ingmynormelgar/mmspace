using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Payroll;
using mmspace.api.Data.Parameters.Payroll;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Data.Parameters.General;
using mmspace.api.Models.General;

namespace mmspace.api.Data.Controllers.Payroll
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class PLA_SOLI_HORA_EXTRAController : ControllerBase
	{
		private readonly IPLA_SOLI_HORA_EXTRARepository _repo;
		private readonly IGEN_EMPLEADORepository _repoEmpleado;
		private readonly IGEN_LISTA_DETARepository _repoEstado;
		private readonly IGEN_TIPO_MONEDARepository _repoTipoMoneda;
    	private readonly IGEN_EMPRESARepository _repoEmpresa;
		private readonly IPLA_SOLI_HORA_EXTRA_DETARepository _repoDeta;
		private readonly IPLA_RUBRORepository _repoRubro;
		
		public PLA_SOLI_HORA_EXTRAController(IPLA_SOLI_HORA_EXTRARepository repo,
											IGEN_EMPLEADORepository repoEmpleado,
											IGEN_LISTA_DETARepository repoEstado,
											IGEN_TIPO_MONEDARepository repoTipoMoneda,
                      						IGEN_EMPRESARepository repoEmpresa,
											IPLA_SOLI_HORA_EXTRA_DETARepository repoDeta,
											IPLA_RUBRORepository repoRubro)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
			_repoEmpleado = repoEmpleado ?? throw new ArgumentNullException(nameof(_repoEmpleado));
			_repoEstado = repoEstado ?? throw new ArgumentNullException(nameof(_repoEstado));
			_repoTipoMoneda = repoTipoMoneda ?? throw new ArgumentNullException(nameof(_repoTipoMoneda));
      		_repoEmpresa = repoEmpresa ?? throw new ArgumentNullException(nameof(_repoEmpresa));
			_repoDeta = repoDeta ?? throw new ArgumentNullException(nameof(repoDeta));
			_repoRubro = repoRubro ?? throw new ArgumentNullException(nameof(_repoRubro));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pla-soli-hora-extra|R")]
		public async Task<List<V_PLA_SOLI_HORA_EXTRA>> GetAll([FromQuery]DATA_PLA_SOLI_HORA_EXTRA Data)
		{
			Data.TIPO_CONSULTA=1;
			Data.OPCION_CONSULTA=2;
			Data.USUARIO_SOLICITA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_SOLICITUD}")]
		[Authorize(Policy = "/pla-soli-hora-extra|R")]
		public async Task<V_PLA_SOLI_HORA_EXTRA> Get(Int32 CORR_SOLICITUD,[FromQuery]DATA_PLA_SOLI_HORA_EXTRA Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_SOLICITUD);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pla-soli-hora-extra|C")]
		public async Task<IActionResult> Post(PLA_SOLI_HORA_EXTRA Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pla-soli-hora-extra|U")]
		public async Task<IActionResult> Put(PLA_SOLI_HORA_EXTRA Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_SOLICITUD}")]
		[Authorize(Policy = "/pla-soli-hora-extra|D")]
		public async Task<IActionResult> Delete(Int32 CORR_SOLICITUD,[FromQuery]DATA_PLA_SOLI_HORA_EXTRA Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_SOLICITUD, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpGet("GetEstados")]
		[Authorize(Policy = "/pla-soli-hora-extra|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstados([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.CORR_LISTA = 1;
			Data.OPCION_CONSULTA = 0;
			return await _repoEstado.GetAll(Data);
		} 

		[HttpGet("GetEmpleados")]
		[Authorize(Policy = "/pla-soli-hora-extra|R")]
		public async Task<List<V_GEN_EMPLEADO>> GetEmpleados([FromQuery]DATA_GEN_EMPLEADO Data)
		{
			Data.TIPO_CONSULTA = 2;
      		Data.OPCION_CONSULTA = 5;
			Data.LOGIN_SISTEMA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repoEmpleado.GetAll(Data);
		}

		[HttpGet("GetTipoMonedas")]
		[Authorize(Policy = "/pla-soli-hora-extra|R")]
		public async Task<List<V_GEN_TIPO_MONEDA>> GetAllTipoMonedas([FromQuery]DATA_GEN_TIPO_MONEDA Data)
		{
			Data.TIPO_CONSULTA = 2;
      		Data.OPCION_CONSULTA = 0;
			return await _repoTipoMoneda.GetAll(Data);
		}

		[HttpGet("GetMonedaEmpresa")]
		[Authorize(Policy = "/pla-soli-hora-extra|R")]
		public async Task<List<V_GEN_EMPRESA>> GetMonedaEmpresa([FromQuery]DATA_GEN_EMPRESA Data)
		{
			Data.TIPO_CONSULTA = 2;
      		Data.OPCION_CONSULTA = 2;
			return await _repoEmpresa.GetAll(Data);
		}

		[HttpGet("GetRubro")]
		[Authorize(Policy = "/pla-soli-hora-extra|R")]
		public async Task<List<V_PLA_RUBRO>> GetRubro([FromQuery]DATA_PLA_RUBRO Data)
		{
			Data.TIPO_CONSULTA = 2;
			Data.CLASE_RUBRO = "HEX";
      		Data.OPCION_CONSULTA = 3;
			return await _repoRubro.GetAll(Data);
		}

		//metodos Detalle

		[HttpGet("GetAllSoliHoraExtraDeta")]
		[Authorize(Policy = "/pla-soli-hora-extra|R")]
		public async Task<List<V_PLA_SOLI_HORA_EXTRA_DETA>> GetAll([FromQuery]DATA_PLA_SOLI_HORA_EXTRA_DETA Data)
		{
			Data.TIPO_CONSULTA=1;
			Data.OPCION_CONSULTA=0;
			return await _repoDeta.GetAll(Data);
		}


		[HttpDelete("DeleteSoliHoraExtraDeta/{CORR_SOLICITUD_DETA}")]
		[Authorize(Policy = "/pla-soli-hora-extra|D")]
		public async Task<IActionResult> Delete(Int32 CORR_SOLICITUD_DETA,[FromQuery]DATA_PLA_SOLI_HORA_EXTRA_DETA Data)
		{
			var resultado = await _repoDeta.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,Data.CORR_SOLICITUD,CORR_SOLICITUD_DETA, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpPut("Solicitar")]
		[Authorize(Policy = "/pla-soli-hora-extra|R")]
		public async Task<IActionResult> Solicitar(PLA_SOLI_HORA_EXTRA Data)
		{
			Data.USUARIO_ACTU = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_ACTU = DateTime.Now;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			var resultado = await _repo.Solicitar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		//opcion de autorizar
		[HttpGet("GetAllAutorizar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|R")]
		public async Task<List<V_PLA_SOLI_HORA_EXTRA>> GetAllAutorizar([FromQuery]DATA_PLA_SOLI_HORA_EXTRA Data)
		{
			Data.TIPO_CONSULTA=1;
			Data.OPCION_CONSULTA=3;
			Data.USUARIO_SOLICITA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}
		

		
		[HttpPut("PutAutorizar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|U")]
		public async Task<IActionResult> PutAutorizar(PLA_SOLI_HORA_EXTRA Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		

		[HttpGet("GetEstadosAutorizar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstadosAutorizar([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.CORR_LISTA = 1;
			Data.OPCION_CONSULTA = 0;
			return await _repoEstado.GetAll(Data);
		} 

		[HttpGet("GetEmpleadosAutorizar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|R")]
		public async Task<List<V_GEN_EMPLEADO>> GetEmpleadosAutorizar([FromQuery]DATA_GEN_EMPLEADO Data)
		{
			Data.TIPO_CONSULTA = 2;
      		Data.OPCION_CONSULTA = 6;
			Data.LOGIN_SISTEMA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repoEmpleado.GetAll(Data);
		}

		[HttpGet("GetTipoMonedasAutorizar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|R")]
		public async Task<List<V_GEN_TIPO_MONEDA>> GetAllTipoMonedasAutorizar([FromQuery]DATA_GEN_TIPO_MONEDA Data)
		{
			Data.TIPO_CONSULTA = 2;
      		Data.OPCION_CONSULTA = 0;
			return await _repoTipoMoneda.GetAll(Data);
		}

		[HttpGet("GetMonedaEmpresaAutorizar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|R")]
		public async Task<List<V_GEN_EMPRESA>> GetMonedaEmpresaAutorizar([FromQuery]DATA_GEN_EMPRESA Data)
		{
			Data.TIPO_CONSULTA = 2;
      		Data.OPCION_CONSULTA = 2;
			return await _repoEmpresa.GetAll(Data);
		}

		[HttpGet("GetRubroAutorizar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|R")]
		public async Task<List<V_PLA_RUBRO>> GetRubroAutorizar([FromQuery]DATA_PLA_RUBRO Data)
		{
			Data.TIPO_CONSULTA = 2;
			Data.CLASE_RUBRO = "HEX";
      		Data.OPCION_CONSULTA = 3;
			return await _repoRubro.GetAll(Data);
		}

		//metodos Detalle opcion autorizar

		[HttpGet("GetAllSoliHoraExtraDetaAutorizar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|R")]
		public async Task<List<V_PLA_SOLI_HORA_EXTRA_DETA>> GetAllAutorizar([FromQuery]DATA_PLA_SOLI_HORA_EXTRA_DETA Data)
		{
			Data.TIPO_CONSULTA=1;
			Data.OPCION_CONSULTA=0;
			return await _repoDeta.GetAll(Data);
		}

		[HttpPut("Autorizar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|R")]
		public async Task<IActionResult> Autorizar(PLA_SOLI_HORA_EXTRA Data)
		{
			Data.USUARIO_ACTU = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_ACTU = DateTime.Now;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			var resultado = await _repo.Autorizar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return StatusCode(201, (Int32)(resultado.CodeHelper));
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpPut("Denegar")]
		[Authorize(Policy = "/pla-soli-hora-extra-autorizar|R")]
		public async Task<IActionResult> Denegar(PLA_SOLI_HORA_EXTRA Data)
		{
			Data.USUARIO_ACTU = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_ACTU = DateTime.Now;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			var resultado = await _repo.Denegar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return StatusCode(201, (Int32)(resultado.CodeHelper));
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}