using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Payroll;
using mmspace.api.Data.Parameters.Payroll;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Data.Parameters.General;
using mmspace.api.Models.General;
using mmspace.api.Data.Repositories.Security;
using mmspace.api.Data.Parameters.Security;
using mmspace.api.Models.Security;

namespace mmspace.api.Data.Controllers.Payroll
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class PLA_SOLI_PERMISOController : ControllerBase
	{
		private readonly IPLA_SOLI_PERMISORepository _repo;
		private readonly IPLA_MOTIVO_PERMISORepository _repoMotivoPermiso;
		private readonly IGEN_LISTA_DETARepository _repoEstado;
		private readonly IGEN_EMPLEADORepository _repoEmpleado;
		private readonly IPLA_SOLI_PERMISO_DETARepository _repoSoli;

		public PLA_SOLI_PERMISOController(IPLA_SOLI_PERMISORepository repo,
		IPLA_MOTIVO_PERMISORepository repoMotivoPermiso,
		IGEN_LISTA_DETARepository repoEstado,
		IGEN_EMPLEADORepository repoEmpleado,
		IPLA_SOLI_PERMISO_DETARepository repoSoli)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
			_repoMotivoPermiso = repoMotivoPermiso ?? throw new ArgumentNullException(nameof(_repoMotivoPermiso));
			_repoEstado = repoEstado ?? throw new ArgumentNullException(nameof(_repoEstado));
			_repoEmpleado = repoEmpleado ?? throw new ArgumentNullException(nameof(_repoEmpleado));
			_repoSoli = repoSoli ?? throw new ArgumentNullException(nameof(_repoSoli));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pla-soli-permiso|R")]
		public async Task<List<V_PLA_SOLI_PERMISO>> GetAll([FromQuery]DATA_PLA_SOLI_PERMISO Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.OPCION_CONSULTA = 1;
			Data.USUARIO_SOLICITA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_PERMISO}")]
		[Authorize(Policy = "/pla-soli-permiso|R")]
		public async Task<V_PLA_SOLI_PERMISO> Get(Int32 CORR_PERMISO,[FromQuery]DATA_PLA_SOLI_PERMISO Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_PERMISO);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pla-soli-permiso|C")]
		public async Task<IActionResult> Post(PLA_SOLI_PERMISO Data)
		{	
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA,Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pla-soli-permiso|U")]
		public async Task<IActionResult> Put(PLA_SOLI_PERMISO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_PERMISO}")]
		[Authorize(Policy = "/pla-soli-permiso|D")]
		public async Task<IActionResult> Delete(Int32 CORR_PERMISO,[FromQuery]DATA_PLA_SOLI_PERMISO Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_PERMISO, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpGet("GetMotivosPermisos")]
		[Authorize(Policy = "/pla-soli-permiso|R")]
		public async Task<List<V_PLA_MOTIVO_PERMISO>> GetMotivosPermisos([FromQuery]DATA_PLA_MOTIVO_PERMISO Data)
		{
			Data.TIPO_CONSULTA = 1;
      		Data.OPCION_CONSULTA = 0;
			return await _repoMotivoPermiso.GetAll(Data);
		}

		[HttpGet("GetEstados")]
		[Authorize(Policy = "/pla-soli-permiso|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstados([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.CORR_LISTA = 1;
			Data.OPCION_CONSULTA = 0;
			return await _repoEstado.GetAll(Data);
		}

    	[HttpGet("GetClaseSolicitudes")]
		[Authorize(Policy = "/pla-soli-permiso|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetClaseSolicitud([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.CORR_LISTA = 2;
			Data.OPCION_CONSULTA = 0;
			return await _repoEstado.GetAll(Data);
		}

		[HttpGet("GetEmpleados")]
		[Authorize(Policy = "/pla-soli-permiso|R")]
		public async Task<List<V_GEN_EMPLEADO>> GetEmpleados([FromQuery]DATA_GEN_EMPLEADO Data)
		{
			Data.TIPO_CONSULTA = 2;
      		Data.OPCION_CONSULTA = 5;
			Data.LOGIN_SISTEMA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repoEmpleado.GetAll(Data);
		}

		[HttpGet("GetAllSoliPermisoDeta")]
		[Authorize(Policy = "/pla-soli-permiso|R")]
		public async Task<List<V_PLA_SOLI_PERMISO_DETA>> GetAll([FromQuery]DATA_PLA_SOLI_PERMISO_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
      		Data.OPCION_CONSULTA = 0;
			return await _repoSoli.GetAll(Data);
		}

		[HttpDelete("DeleteSoliPermisoDeta/{CORR_PERMISO_DETA}")]
		[Authorize(Policy = "/pla-soli-permiso|D")]
		public async Task<IActionResult> Delete(Int32 CORR_PERMISO_DETA,[FromQuery]DATA_PLA_SOLI_PERMISO_DETA Data)
		{
			var resultado = await _repoSoli.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,Data.CORR_PERMISO,CORR_PERMISO_DETA, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpPut("Solicitar")]
		[Authorize(Policy = "/pla-soli-permiso|R")]
		public async Task<IActionResult> Solicitar(PLA_SOLI_PERMISO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.Solicitar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}


		//METODOS PANTALLA AUTORIZAR PERMISOS
		[HttpGet("GetAllAutorizar")]
		[Authorize(Policy = "/pla-soli-permiso-autorizar|R")]
		public async Task<List<V_PLA_SOLI_PERMISO>> GetAllAutorizar([FromQuery]DATA_PLA_SOLI_PERMISO Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.OPCION_CONSULTA = 3;
			Data.USUARIO_SOLICITA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}

		[HttpGet("GetMotivosPermisosAutorizar")]
		[Authorize(Policy = "/pla-soli-permiso-autorizar|R")]
		public async Task<List<V_PLA_MOTIVO_PERMISO>> GetMotivosPermisosAutorizar([FromQuery]DATA_PLA_MOTIVO_PERMISO Data)
		{
			Data.TIPO_CONSULTA = 1;
      		Data.OPCION_CONSULTA = 0;
			return await _repoMotivoPermiso.GetAll(Data);
		}

		[HttpGet("GetEstadosAutorizar")]
		[Authorize(Policy = "/pla-soli-permiso-autorizar|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstadosAutorizar([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.CORR_LISTA = 1;
			Data.OPCION_CONSULTA = 0;
			return await _repoEstado.GetAll(Data);
		}

    	[HttpGet("GetClaseSolicitudesAutorizar")]
		[Authorize(Policy = "/pla-soli-permiso-autorizar|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetClaseSolicitudesAutorizar([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
			Data.CORR_LISTA = 2;
			Data.OPCION_CONSULTA = 0;
			return await _repoEstado.GetAll(Data);
		}

		[HttpGet("GetEmpleadosAutorizar")]
		[Authorize(Policy = "/pla-soli-permiso-autorizar|R")]
		public async Task<List<V_GEN_EMPLEADO>> GetEmpleadosAutorizar([FromQuery]DATA_GEN_EMPLEADO Data)
		{
			Data.TIPO_CONSULTA = 2;
      		Data.OPCION_CONSULTA = 6;
      		Data.LOGIN_SISTEMA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repoEmpleado.GetAll(Data);
		}

		[HttpGet("GetAllSoliPermisoDetaAutorizar")]
		[Authorize(Policy = "/pla-soli-permiso-autorizar|R")]
		public async Task<List<V_PLA_SOLI_PERMISO_DETA>> GetAllAutorizar([FromQuery]DATA_PLA_SOLI_PERMISO_DETA Data)
		{
			Data.TIPO_CONSULTA = 1;
      		Data.OPCION_CONSULTA = 0;
			return await _repoSoli.GetAll(Data);
		}

		[HttpPut("Autorizar")]
		[Authorize(Policy = "/pla-soli-permiso-autorizar|R")]
		public async Task<IActionResult> Autorizar(PLA_SOLI_PERMISO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.Autorizar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
			  return StatusCode(201, (Int32)(resultado.CodeHelper));
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

    	[HttpPut("Denegar")]
		[Authorize(Policy = "/pla-soli-permiso-autorizar|R")]
		public async Task<IActionResult> Denegar(PLA_SOLI_PERMISO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.Denegar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
			  return StatusCode(201, (Int32)(resultado.CodeHelper));
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}