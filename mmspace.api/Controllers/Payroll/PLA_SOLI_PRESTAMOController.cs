using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Payroll;
using mmspace.api.Data.Parameters.Payroll;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Data.Parameters.General;
using mmspace.api.Models.General;

namespace mmspace.api.Data.Controllers.Payroll
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class PLA_SOLI_PRESTAMOController : ControllerBase
	{
		private readonly IPLA_SOLI_PRESTAMORepository _repo;
		private readonly IGEN_LISTA_DETARepository _repoEstado;
		private readonly IGEN_EMPLEADORepository _repoEmpleado;
		private readonly IPLA_RUBRORepository _repoRubro;
		private readonly IGEN_PERIODICIDADRepository _repoPeriodicidad;
		private readonly IGEN_TIPO_MONEDARepository _repoTipoMoneda;
		private readonly IGEN_EMPRESARepository _repoEmpresa;
		private readonly IPLA_PARAMETRORepository _repoParametro;
		public PLA_SOLI_PRESTAMOController(IPLA_SOLI_PRESTAMORepository repo,
										   IGEN_LISTA_DETARepository repoEstado,
										   IGEN_EMPLEADORepository repoEmpleado, 
										   IPLA_RUBRORepository repoRubro, 
										   IGEN_PERIODICIDADRepository repoPeriodicidad, 
										   IGEN_TIPO_MONEDARepository repoTipoMoneda, 
										   IGEN_EMPRESARepository repoEmpresa,
										   IPLA_PARAMETRORepository repoParametro)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
			_repoEstado = repoEstado ?? throw new ArgumentNullException(nameof(_repoEstado));
			_repoEmpleado = repoEmpleado ?? throw new ArgumentNullException(nameof(_repoEmpleado));
			_repoRubro = repoRubro ?? throw new ArgumentNullException(nameof(_repoRubro));
			_repoPeriodicidad = repoPeriodicidad ?? throw new ArgumentNullException(nameof(_repoPeriodicidad));
			_repoTipoMoneda = repoTipoMoneda ?? throw new ArgumentNullException(nameof(_repoTipoMoneda));
			_repoEmpresa = repoEmpresa ?? throw new ArgumentNullException(nameof(_repoEmpresa));
			_repoParametro = repoParametro ?? throw new ArgumentNullException(nameof(_repoParametro));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<List<V_PLA_SOLI_PRESTAMO>> GetAll([FromQuery]DATA_PLA_SOLI_PRESTAMO Data)
		{
			Data.USUARIO_SOLICITA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_PRESTAMO}")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<V_PLA_SOLI_PRESTAMO> Get(Int32 CORR_PRESTAMO,[FromQuery]DATA_PLA_SOLI_PRESTAMO Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_PRESTAMO);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pla-soli-prestamo|C")]
		public async Task<IActionResult> Post(PLA_SOLI_PRESTAMO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA,Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pla-soli-prestamo|U")]
		public async Task<IActionResult> Put(PLA_SOLI_PRESTAMO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA,Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_PRESTAMO}")]
		[Authorize(Policy = "/pla-soli-prestamo|D")]
		public async Task<IActionResult> Delete(Int32 CORR_PRESTAMO,[FromQuery]DATA_PLA_SOLI_PRESTAMO Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.CORR_EMPLEADO,CORR_PRESTAMO, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpGet("GetEstados")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstados([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			return await _repoEstado.GetAll(Data);
		} 

		[HttpGet("GetEmpleados")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<List<V_GEN_EMPLEADO>> GetEmpleados([FromQuery]DATA_GEN_EMPLEADO Data)
		{
			Data.LOGIN_SISTEMA= User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			return await _repoEmpleado.GetAll(Data);
		}

		[HttpGet("GetPeriodicidadEmpleado")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<GEN_PERIODICIDAD_EMPLEADO> GetPeriodicidadEmpleado([FromQuery]DATA_GEN_EMPLEADO Data)
		{
			return await _repoEmpleado.GetPeriodicidadEmpleado(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO);
		}

		[HttpGet("GetRubros")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<List<V_PLA_RUBRO>> GetAllRubros([FromQuery]DATA_PLA_RUBRO Data)
		{
			return await _repoRubro.GetAll(Data);
		}

		[HttpGet("GetPeriodicidades")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<List<V_GEN_PERIODICIDAD>> GetAllPeriodicidades([FromQuery]DATA_GEN_PERIODICIDAD Data)
		{
			return await _repoPeriodicidad.GetAll(Data);
		}

		[HttpGet("GetTipoMonedas")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<List<V_GEN_TIPO_MONEDA>> GetAllTipoMonedas([FromQuery]DATA_GEN_TIPO_MONEDA Data)
		{
			return await _repoTipoMoneda.GetAll(Data);
		}

		[HttpGet("GetMonedaEmpresa")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<List<V_GEN_EMPRESA>> GetMonedaEmpresa([FromQuery]DATA_GEN_EMPRESA Data)
		{
			return await _repoEmpresa.GetAll(Data);
		}

		[HttpGet("GetMontoMaximo")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<Decimal> GetMontoMaximo([FromQuery]DATA_PLA_PARAMETRO Data)
		{
			return (Decimal) await _repoParametro.GetScalar(Data, 14);
		}

		[HttpPut("Solicitar")]
		[Authorize(Policy = "/pla-soli-prestamo|R")]
		public async Task<IActionResult> Solicitar(PLA_SOLI_PRESTAMO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;

			var resultado = await _repo.Solicitar(Data, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, Data.CORR_EMPLEADO, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}


	}
}