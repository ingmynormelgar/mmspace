using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Possale;
using mmspace.api.Data.Parameters.Possale;
using mmspace.api.Models.Possale;

namespace mmspace.api.Data.Controllers.Possale
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class POS_CLIENTEController : ControllerBase
	{
		private readonly IPOS_CLIENTERepository _repo;
		
		public POS_CLIENTEController(IPOS_CLIENTERepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pos-cliente|R")]
		public async Task<List<V_POS_CLIENTE>> GetAll([FromQuery]DATA_POS_CLIENTE Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_CLIENTE}")]
		[Authorize(Policy = "/pos-cliente|R")]
		public async Task<V_POS_CLIENTE> Get(Int32 CORR_CLIENTE,[FromQuery]DATA_POS_CLIENTE Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_CLIENTE);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pos-cliente|C")]
		public async Task<IActionResult> Post(POS_CLIENTE Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pos-cliente|U")]
		public async Task<IActionResult> Put(POS_CLIENTE Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_CLIENTE}")]
		[Authorize(Policy = "/pos-cliente|D")]
		public async Task<IActionResult> Delete(Int32 CORR_CLIENTE,[FromQuery]DATA_POS_CLIENTE Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_CLIENTE, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}