using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Possale;
using mmspace.api.Data.Parameters.Possale;
using mmspace.api.Models.Possale;

namespace mmspace.api.Data.Controllers.Possale
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class POS_COMPRAController : ControllerBase
	{
		private readonly IPOS_COMPRARepository _repo;
		
		public POS_COMPRAController(IPOS_COMPRARepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pos-compra|R")]
		public async Task<List<V_POS_COMPRA>> GetAll([FromQuery]DATA_POS_COMPRA Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_COMPRA}")]
		[Authorize(Policy = "/pos-compra|R")]
		public async Task<V_POS_COMPRA> Get(Int32 CORR_COMPRA,[FromQuery]DATA_POS_COMPRA Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_COMPRA);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pos-compra|C")]
		public async Task<IActionResult> Post(POS_COMPRA Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pos-compra|U")]
		public async Task<IActionResult> Put(POS_COMPRA Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_COMPRA}")]
		[Authorize(Policy = "/pos-compra|D")]
		public async Task<IActionResult> Delete(Int32 CORR_COMPRA,[FromQuery]DATA_POS_COMPRA Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_COMPRA, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}