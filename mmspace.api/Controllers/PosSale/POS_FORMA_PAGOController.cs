using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Possale;
using mmspace.api.Data.Parameters.Possale;
using mmspace.api.Models.Possale;

namespace mmspace.api.Data.Controllers.Possale
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class POS_FORMA_PAGOController : ControllerBase
	{
		private readonly IPOS_FORMA_PAGORepository _repo;
		
		public POS_FORMA_PAGOController(IPOS_FORMA_PAGORepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pos-forma-pago|R")]
		public async Task<List<V_POS_FORMA_PAGO>> GetAll([FromQuery]DATA_POS_FORMA_PAGO Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_FORMA_PAGO}")]
		[Authorize(Policy = "/pos-forma-pago|R")]
		public async Task<V_POS_FORMA_PAGO> Get(Int32 CORR_FORMA_PAGO,[FromQuery]DATA_POS_FORMA_PAGO Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_FORMA_PAGO);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pos-forma-pago|C")]
		public async Task<IActionResult> Post(POS_FORMA_PAGO Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pos-forma-pago|U")]
		public async Task<IActionResult> Put(POS_FORMA_PAGO Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_FORMA_PAGO}")]
		[Authorize(Policy = "/pos-forma-pago|D")]
		public async Task<IActionResult> Delete(Int32 CORR_FORMA_PAGO,[FromQuery]DATA_POS_FORMA_PAGO Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_FORMA_PAGO, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}