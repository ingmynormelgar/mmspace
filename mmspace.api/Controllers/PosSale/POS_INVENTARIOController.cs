using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Possale;
using mmspace.api.Data.Parameters.Possale;
using mmspace.api.Models.Possale;

namespace mmspace.api.Data.Controllers.Possale
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class POS_INVENTARIOController : ControllerBase
	{
		private readonly IPOS_INVENTARIORepository _repo;
		
		public POS_INVENTARIOController(IPOS_INVENTARIORepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pos-inventario|R")]
		public async Task<List<V_POS_INVENTARIO>> GetAll([FromQuery]DATA_POS_INVENTARIO Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_INVENTARIO}")]
		[Authorize(Policy = "/pos-inventario|R")]
		public async Task<V_POS_INVENTARIO> Get(Int32 CORR_INVENTARIO,[FromQuery]DATA_POS_INVENTARIO Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.PERIODO_ANIO,Data.PERIODO_MES,CORR_INVENTARIO);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pos-inventario|C")]
		public async Task<IActionResult> Post(POS_INVENTARIO Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA,Data.PERIODO_ANIO,Data.PERIODO_MES, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pos-inventario|U")]
		public async Task<IActionResult> Put(POS_INVENTARIO Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA,Data.PERIODO_ANIO,Data.PERIODO_MES, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_INVENTARIO}")]
		[Authorize(Policy = "/pos-inventario|D")]
		public async Task<IActionResult> Delete(Int32 CORR_INVENTARIO,[FromQuery]DATA_POS_INVENTARIO Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.PERIODO_ANIO,Data.PERIODO_MES,CORR_INVENTARIO, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}