using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Possale;
using mmspace.api.Data.Parameters.Possale;
using mmspace.api.Models.Possale;

namespace mmspace.api.Data.Controllers.Possale
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class POS_PRODUCTOController : ControllerBase
	{
		private readonly IPOS_PRODUCTORepository _repo;
		
		public POS_PRODUCTOController(IPOS_PRODUCTORepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pos-producto|R")]
		public async Task<List<V_POS_PRODUCTO>> GetAll([FromQuery]DATA_POS_PRODUCTO Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_PRODUCTO}")]
		[Authorize(Policy = "/pos-producto|R")]
		public async Task<V_POS_PRODUCTO> Get(Int32 CORR_PRODUCTO,[FromQuery]DATA_POS_PRODUCTO Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_PRODUCTO);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pos-producto|C")]
		public async Task<IActionResult> Post(POS_PRODUCTO Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pos-producto|U")]
		public async Task<IActionResult> Put(POS_PRODUCTO Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_PRODUCTO}")]
		[Authorize(Policy = "/pos-producto|D")]
		public async Task<IActionResult> Delete(Int32 CORR_PRODUCTO,[FromQuery]DATA_POS_PRODUCTO Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_PRODUCTO, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}