using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Possale;
using mmspace.api.Data.Parameters.Possale;
using mmspace.api.Models.Possale;

namespace mmspace.api.Data.Controllers.Possale
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class POS_TIPO_FACTURAController : ControllerBase
	{
		private readonly IPOS_TIPO_FACTURARepository _repo;
		
		public POS_TIPO_FACTURAController(IPOS_TIPO_FACTURARepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pos-tipo-factura|R")]
		public async Task<List<V_POS_TIPO_FACTURA>> GetAll([FromQuery]DATA_POS_TIPO_FACTURA Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_TIPO_FACTURA}")]
		[Authorize(Policy = "/pos-tipo-factura|R")]
		public async Task<V_POS_TIPO_FACTURA> Get(Int32 CORR_TIPO_FACTURA,[FromQuery]DATA_POS_TIPO_FACTURA Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_TIPO_FACTURA);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pos-tipo-factura|C")]
		public async Task<IActionResult> Post(POS_TIPO_FACTURA Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pos-tipo-factura|U")]
		public async Task<IActionResult> Put(POS_TIPO_FACTURA Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_TIPO_FACTURA}")]
		[Authorize(Policy = "/pos-tipo-factura|D")]
		public async Task<IActionResult> Delete(Int32 CORR_TIPO_FACTURA,[FromQuery]DATA_POS_TIPO_FACTURA Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,CORR_TIPO_FACTURA, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}