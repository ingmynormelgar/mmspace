using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Possale;
using mmspace.api.Data.Parameters.Possale;
using mmspace.api.Models.Possale;

namespace mmspace.api.Data.Controllers.Possale
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class POS_VENTAController : ControllerBase
	{
		private readonly IPOS_VENTARepository _repo;
		
		public POS_VENTAController(IPOS_VENTARepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/pos-venta|R")]
		public async Task<List<V_POS_VENTA>> GetAll([FromQuery]DATA_POS_VENTA Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CORR_VENTA}")]
		[Authorize(Policy = "/pos-venta|R")]
		public async Task<V_POS_VENTA> Get(Int32 CORR_VENTA,[FromQuery]DATA_POS_VENTA Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.PERIODO_ANIO,Data.PERIODO_MES,CORR_VENTA);
		}
		
		[HttpPost]
		[Authorize(Policy = "/pos-venta|C")]
		public async Task<IActionResult> Post(POS_VENTA Data)
		{
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA,Data.PERIODO_ANIO,Data.PERIODO_MES, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/pos-venta|U")]
		public async Task<IActionResult> Put(POS_VENTA Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, Data.CORR_EMPRESA,Data.PERIODO_ANIO,Data.PERIODO_MES, (Int32)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CORR_VENTA}")]
		[Authorize(Policy = "/pos-venta|D")]
		public async Task<IActionResult> Delete(Int32 CORR_VENTA,[FromQuery]DATA_POS_VENTA Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CORR_EMPRESA,Data.PERIODO_ANIO,Data.PERIODO_MES,CORR_VENTA, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}