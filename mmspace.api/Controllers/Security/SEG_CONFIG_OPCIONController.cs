using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Security;
using mmspace.api.Data.Parameters.Security;
using mmspace.api.Models.Security;

namespace mmspace.api.Data.Controllers.Security
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class SEG_CONFIG_OPCIONController : ControllerBase
	{
		private readonly ISEG_CONFIG_OPCIONRepository _repo;
		private readonly ISEG_SISTEMARepository _repoSistema;
		private readonly ISEG_MENU_SISTEMARepository _repoMenuSistema;
		private readonly ISEG_OPCION_SISTEMARepository _repoOpcionSistema;
		
		public SEG_CONFIG_OPCIONController(ISEG_CONFIG_OPCIONRepository repo,
										   ISEG_SISTEMARepository repoSistema,
										   ISEG_MENU_SISTEMARepository repoMenuSistema,
										   ISEG_OPCION_SISTEMARepository repoOpcionSistema)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
			_repoSistema = repoSistema ?? throw new ArgumentNullException(nameof(_repoSistema));
			_repoMenuSistema = repoMenuSistema ?? throw new ArgumentNullException(nameof(_repoMenuSistema));
			_repoOpcionSistema = repoOpcionSistema ?? throw new ArgumentNullException(nameof(_repoOpcionSistema));
		}
		
		[HttpGet]
		[Authorize(Policy = "/seg-config-opcion|R")]
		public async Task<List<V_SEG_CONFIG_OPCION>> GetAll([FromQuery]DATA_SEG_CONFIG_OPCION Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CODIGO_OPCION}")]
		[Authorize(Policy = "/seg-config-opcion|R")]
		public async Task<V_SEG_CONFIG_OPCION> Get(String CODIGO_OPCION,[FromQuery]DATA_SEG_CONFIG_OPCION Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CODIGO_SISTEMA,Data.CODIGO_MENU,CODIGO_OPCION);
		}
		
		[HttpPost]
		[Authorize(Policy = "/seg-config-opcion|C")]
		public async Task<IActionResult> Post(SEG_CONFIG_OPCION Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS,Data.CODIGO_SISTEMA,Data.CODIGO_MENU, (string)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/seg-config-opcion|U")]
		public async Task<IActionResult> Put(SEG_CONFIG_OPCION Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS,Data.CODIGO_SISTEMA,Data.CODIGO_MENU, (string)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CODIGO_OPCION}")]
		[Authorize(Policy = "/seg-config-opcion|D")]
		public async Task<IActionResult> Delete(String CODIGO_OPCION,[FromQuery]DATA_SEG_CONFIG_OPCION Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CODIGO_SISTEMA,Data.CODIGO_MENU,CODIGO_OPCION, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

		[HttpGet("GetSistema")]
		[Authorize(Policy = "/seg-config-opcion|R")]
		public async Task<List<V_SEG_SISTEMA>> GetSistema([FromQuery]DATA_SEG_SISTEMA Data)
		{
			return await _repoSistema.GetAll(Data);
		}

		[HttpGet("GetMenus")]
		[Authorize(Policy = "/seg-config-opcion|R")]
		public async Task<List<V_SEG_MENU_SISTEMA>> GetMenus([FromQuery]DATA_SEG_MENU_SISTEMA Data)
		{
			return await _repoMenuSistema.GetAll(Data);
		}

		[HttpGet("GetOpciones")]
		[Authorize(Policy = "/seg-config-opcion|R")]
		public async Task<List<V_SEG_OPCION_SISTEMA>> GetOpciones([FromQuery]DATA_SEG_OPCION_SISTEMA Data)
		{
			return await _repoOpcionSistema.GetAll(Data);
		}
	}
}