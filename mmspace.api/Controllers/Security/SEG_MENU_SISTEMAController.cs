using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Security;
using mmspace.api.Data.Parameters.Security;
using mmspace.api.Models.Security;

namespace mmspace.api.Data.Controllers.Security
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class SEG_MENU_SISTEMAController : ControllerBase
	{
		private readonly ISEG_MENU_SISTEMARepository _repo;
		
		public SEG_MENU_SISTEMAController(ISEG_MENU_SISTEMARepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/seg-config-opcion|R")]
		public async Task<List<V_SEG_MENU_SISTEMA>> GetAll([FromQuery]DATA_SEG_MENU_SISTEMA Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CODIGO_MENU}")]
		[Authorize(Policy = "/seg-config-opcion|R")]
		public async Task<V_SEG_MENU_SISTEMA> Get(String CODIGO_MENU,[FromQuery]DATA_SEG_MENU_SISTEMA Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,CODIGO_MENU);
		}
		
		[HttpPost]
		[Authorize(Policy = "/seg-menu-sistema|C")]
		public async Task<IActionResult> Post(SEG_MENU_SISTEMA Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, (string)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/seg-menu-sistema|U")]
		public async Task<IActionResult> Put(SEG_MENU_SISTEMA Data)
		{
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, (string)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CODIGO_MENU}")]
		[Authorize(Policy = "/seg-menu-sistema|D")]
		public async Task<IActionResult> Delete(String CODIGO_MENU,[FromQuery]DATA_SEG_MENU_SISTEMA Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,CODIGO_MENU, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}