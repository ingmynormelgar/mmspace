using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Security.Claims;
using mmspace.api.Data.Repositories.Security;
using mmspace.api.Data.Parameters.Security;
using mmspace.api.Models.Security;

namespace mmspace.api.Data.Controllers.Security
{
	[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	
	public class SEG_OPCION_SISTEMAController : ControllerBase
	{
		private readonly ISEG_OPCION_SISTEMARepository _repo;
		
		public SEG_OPCION_SISTEMAController(ISEG_OPCION_SISTEMARepository repo)
		{
			_repo = repo ?? throw new ArgumentNullException(nameof(_repo));
		}
		
		[HttpGet]
		[Authorize(Policy = "/seg-opcion-sistema|R")]
		public async Task<List<V_SEG_OPCION_SISTEMA>> GetAll([FromQuery]DATA_SEG_OPCION_SISTEMA Data)
		{
			return await _repo.GetAll(Data);
		}
		
		[HttpGet("{CODIGO_OPCION}")]
		[Authorize(Policy = "/seg-opcion-sistema|R")]
		public async Task<V_SEG_OPCION_SISTEMA> Get(String CODIGO_OPCION,[FromQuery]DATA_SEG_OPCION_SISTEMA Data)
		{
			return await _repo.Get(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,CODIGO_OPCION);
		}
		
		[HttpPost]
		[Authorize(Policy = "/seg-opcion-sistema|C")]
		public async Task<IActionResult> Post(SEG_OPCION_SISTEMA Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, (String)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/seg-opcion-sistema|U")]
		public async Task<IActionResult> Put(SEG_OPCION_SISTEMA Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.FECHA_ACTU = Data.FECHA_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.CORR_SUSCRIPCION, Data.CORR_CONFI_PAIS, (String)(resultado.CodeHelper));
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{CODIGO_OPCION}")]
		[Authorize(Policy = "/seg-opcion-sistema|D")]
		public async Task<IActionResult> Delete(String CODIGO_OPCION,[FromQuery]DATA_SEG_OPCION_SISTEMA Data)
		{
			var resultado = await _repo.Delete(Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,CODIGO_OPCION, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
	}
}