using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using mmspace.api.Models.Security;
using mmspace.api.Data.Parameters.Security;
using mmspace.api.Data.Repositories.Security;
using System.Linq;
using System.Security.Claims;
using System.Collections.Generic;
using mmspace.api.Data.Repositories.General;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;

namespace mmspace.api.Controllers.Security
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SEG_USUARIOController: ControllerBase
    {
        private readonly ISEG_USUARIORepository _repo; 
        private readonly ISEG_USUARIO_OPCIONRepository _repoOpciones;
        private readonly IGEN_LISTA_DETARepository _repoListaDeta;
        public IConfiguration _config;
        public SEG_USUARIOController(ISEG_USUARIORepository repo,
                                     ISEG_USUARIO_OPCIONRepository repoOpciones,
                                     IGEN_LISTA_DETARepository repoListaDeta,
                                     IConfiguration config)
        {
            _repo = repo ?? throw new ArgumentNullException(nameof(_repo));
            _repoOpciones = repoOpciones ?? throw new ArgumentNullException(nameof(_repoOpciones));            
            _repoListaDeta = repoListaDeta ?? throw new ArgumentNullException(nameof(_repoListaDeta));
            _config = config;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(SEG_USUARIO_LOGIN Data)
        {
            var UserForRepo = await _repo.Login(Data.LOGIN_SISTEMA.ToLower(), Data.CLAVE_USUARIO);
            
            if (UserForRepo == null)
                return BadRequest("Usuario o Clave Inválida!");

            // Agregado los permisos del usuario al token
            var vParamOpcion = new DATA_SEG_USUARIO_OPCION();
            vParamOpcion.TIPO_CONSULTA=1;
            vParamOpcion.LOGIN_SISTEMA=Data.LOGIN_SISTEMA.ToLower();
            vParamOpcion.OPCION_CONSULTA=1;
            var userOpcion = await _repoOpciones.GetAll(vParamOpcion);

            return Ok(new
            {
                token = _repo.GenerateToken(UserForRepo, userOpcion)
            });
        }
        
        [HttpPost("CambioClave")]
        public async Task<IActionResult> CambioClave(SEG_USUARIO_CAMBIO_CLAVE Data)
        {
            var UserForRepo = await _repo.Login(Data.LOGIN_SISTEMA.ToLower(), Data.CLAVE_USUARIO);

            if (UserForRepo == null)
                return Unauthorized();    

            var resultado = await _repo.CambioClave(Data, "Admin", "e-coffee");
            if (resultado.ErrorCode != 0)
            {
                return BadRequest(resultado.ErrorMessage);
            }
            
            // Agregado los permisos del usuario al token
            var vParamOpcion = new DATA_SEG_USUARIO_OPCION();
            vParamOpcion.TIPO_CONSULTA=1;
            vParamOpcion.LOGIN_SISTEMA=Data.LOGIN_SISTEMA.ToLower();
            vParamOpcion.OPCION_CONSULTA=1;            
            var userOpcion = await _repoOpciones.GetAll(vParamOpcion);

            return Ok(new
            {
                token = _repo.GenerateToken(UserForRepo, userOpcion)
            });
        }

        [HttpPost("ReasignarClave")]
        [Authorize(Policy = "/seg-usuario|U")]
        public async Task<IActionResult> ReasignarClave(SEG_USUARIO_CAMBIO_CLAVE Data)
        {
            var UserForRepo = await _repo.Get(Data.LOGIN_SISTEMA.ToLower());

            if (UserForRepo == null)
                return BadRequest("Usuario No Existe");

            var resultado = await _repo.CambioClave(Data, "MmSpace", "prueba22");
            if (resultado.ErrorCode != 0)
            {
                return BadRequest(resultado.ErrorMessage);
            }
                       
            // Agregado los permisos del usuario al token
            var vParamOpcion = new DATA_SEG_USUARIO_OPCION();
            vParamOpcion.TIPO_CONSULTA=1;
            vParamOpcion.LOGIN_SISTEMA=Data.LOGIN_SISTEMA.ToLower();
            vParamOpcion.OPCION_CONSULTA=1;     
            var userOpcion = await _repoOpciones.GetAll(vParamOpcion);

            return Ok(new
            {
                token = _repo.GenerateToken(UserForRepo, userOpcion)
            });
        }
        
        [HttpGet("menu")]
        public async Task<List<V_SEG_USUARIO_MENU>> GetMenu()
        {
            var LOGIN_SISTEMA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
            
            return await _repo.getMenu(LOGIN_SISTEMA);
        }

        [HttpGet]
		[Authorize(Policy = "/seg-usuario|R")]
		public async Task<List<V_SEG_USUARIO>> GetAll([FromQuery]DATA_SEG_USUARIO Data)
		{
			return await _repo.GetAll(Data);
		}

        [HttpGet("GetAllUsuarioOpcion")]
        [Authorize(Policy = "/seg-usuario|R")]
		public async Task<List<V_SEG_USUARIO_OPCION>> GetAllUsuarioOpcion([FromQuery]DATA_SEG_USUARIO_OPCION Data)
		{
			return await _repoOpciones.GetAll(Data);
		}
		
		[HttpGet("{LOGIN_SISTEMA}")]
		[Authorize(Policy = "/seg-usuario|R")]
		public async Task<V_SEG_USUARIO> Get(String LOGIN_SISTEMA,[FromQuery]DATA_SEG_USUARIO Data)
		{
			return await _repo.Get(LOGIN_SISTEMA);
		}
		
		[HttpPost]          
		[Authorize(Policy = "/seg-usuario|C")]
		public async Task<IActionResult> Post(SEG_USUARIO Data)
		{
			Data.USUARIO_CREA =User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Add, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.LOGIN_SISTEMA);
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);;
			}
		}
		
		[HttpPut]
		[Authorize(Policy = "/seg-usuario|U")]
		public async Task<IActionResult> Put(SEG_USUARIO Data)
		{
			Data.USUARIO_CREA = User.Claims.ToList().SingleOrDefault(e => e.Type == ClaimTypes.NameIdentifier).Value.ToLower();
			Data.FECHA_CREA = DateTime.Now;
			Data.ESTACION_CREA = Data.USUARIO_CREA;
			Data.USUARIO_ACTU = Data.USUARIO_CREA;
			Data.ESTACION_ACTU = Data.ESTACION_CREA;
			
			var resultado = await _repo.Save(Data, api.Data.Fx.UpdateType.Update, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				var newData = await _repo.Get(Data.LOGIN_SISTEMA);
				return StatusCode(201, newData);
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}
		
		[HttpDelete("{LOGIN_SISTEMA}")]
		[Authorize(Policy = "/seg-usuario|D")]
		public async Task<IActionResult> Delete(String LOGIN_SISTEMA,[FromQuery]DATA_SEG_USUARIO Data)
		{
			var resultado = await _repo.Delete(LOGIN_SISTEMA, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

        [HttpDelete("DeleteUsuarioOpcion/{CODIGO_OPCION}")]
        [Authorize(Policy = "/seg-usuario|D")]
		public async Task<IActionResult> DeleteUsuarioOpcion(String CODIGO_OPCION,[FromQuery]DATA_SEG_USUARIO_OPCION Data)
		{
			var resultado = await _repoOpciones.Delete(Data.LOGIN_SISTEMA,Data.CORR_SUSCRIPCION,Data.CORR_CONFI_PAIS,Data.CODIGO_SISTEMA,Data.CODIGO_MENU,CODIGO_OPCION, "Admin", "e-coffee");
			if (resultado.ErrorCode == 0) {
				return Ok();
			} else {
				return BadRequest(resultado.ErrorMessage);
			}
		}

        [HttpGet("GetTipoUsuario")]
		[Authorize(Policy = "/seg-usuario|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetTipoUsuario([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			return await _repoListaDeta.GetAll(Data);
		}

        [HttpGet("GetEstadoUsuario")]
		[Authorize(Policy = "/seg-usuario|R")]
		public async Task<List<V_GEN_LISTA_DETA>> GetEstadoUsuario([FromQuery]DATA_GEN_LISTA_DETA Data)
		{
			return await _repoListaDeta.GetAll(Data);
		}
    }
}