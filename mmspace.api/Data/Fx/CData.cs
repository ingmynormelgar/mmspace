using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace mmspace.api.Data.Fx
{
    public class CData
    {
        private string _connectiongString;
        private DbProviderFactory dataFactory;
        public DbCommand objCommand { get; set; }
        private DbParameter objParameter;
        public DbConnection objConnection;
        private DbDataReader objReader;
        private byte[] _key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
        private byte[] _iv = new byte[] {65, 110, 68, 26, 69, 178, 200, 219};


        public CData(string connectionString, string providerName)    
        {            
            _connectiongString = connectionString;              
            DbProviderFactories.RegisterFactory(providerName, SqlClientFactory.Instance);
            dataFactory = DbProviderFactories.GetFactory(providerName);            
            
            objConnection = dataFactory.CreateConnection();
            objConnection.ConnectionString = _connectiongString;
        }

        public async Task<DbDataReader> GetDataReader(CommandType xCommandType, 
                                          string  xQry,
                                          List<CParameter> xParametros = null){
        try
            {                
                if (objConnection.State != ConnectionState.Open)
                    await objConnection.OpenAsync();
                objCommand = dataFactory.CreateCommand();
                objCommand.Connection = objConnection;
                objCommand.CommandType = xCommandType;
                objCommand.CommandText = xQry;
                objCommand.CommandTimeout = 0;

                foreach (CParameter p in xParametros)
                {
                    if (p.ParameterName != "")
                    {
                        objParameter = dataFactory.CreateParameter();
                        objParameter.DbType = p.DbType;  
                        objParameter.ParameterName = p.ParameterName;
                        objParameter.Value = p.Value;
                        objParameter.Direction = p.Direction;

                        objCommand.Parameters.Add(objParameter);
                    }
                }                

               objReader  = await objCommand.ExecuteReaderAsync();

            }
            catch (System.Exception)
            {                
                throw;
            }
            
            return objReader;
        }

        public async Task<object> ExecCmd(CommandType xCommandType, 
                                          string  xQry,
                                          bool xRowsAfected=true,
                                          List<CParameter> xParametros = null)
        {   
            object sqlrows = 0;

            try
            {
                if (objConnection.State != ConnectionState.Open)
                    await objConnection.OpenAsync();
                    
                objCommand = dataFactory.CreateCommand();
                objCommand.Connection = objConnection;
                objCommand.CommandType = xCommandType;
                objCommand.CommandText = xQry;
                objCommand.CommandTimeout = 0;
                
                foreach (CParameter p in xParametros)
                {   
                    if (p.ParameterName != "")
                    {
                        objParameter = dataFactory.CreateParameter();
                        objParameter.DbType = p.DbType;  
                        objParameter.ParameterName = p.ParameterName;
                        objParameter.Value = p.Value;
                        objParameter.Direction = p.Direction;
                        objParameter.Size = p.Size;

                        objCommand.Parameters.Add(objParameter);
                    }                        
                }

                if(xRowsAfected)
                {
                    sqlrows = await objCommand.ExecuteNonQueryAsync();
                }
                else
                {
                    sqlrows = await objCommand.ExecuteScalarAsync();
                }                             
            }
            catch (System.Exception)
            {                
                throw;
            }    
            finally
            {
                objConnection.Close();
            }
            return sqlrows;
        }  

        public object IsNull(object vDato, object vDefecto)
        {
            if (vDato == System.DBNull.Value)
                return vDefecto;

            return vDato;
        }

        public string Encrypt(string plainText)
        {
            // Declare a UTF8Encoding object so we may use the GetByte 
            // method to transform the plainText into a Byte array. 
            UTF8Encoding utf8encoder = new UTF8Encoding();
            byte[] inputInBytes = utf8encoder.GetBytes(plainText);

            // Create a new TripleDES service provider 
            TripleDESCryptoServiceProvider tdesProvider = new TripleDESCryptoServiceProvider();

            // The ICryptTransform interface uses the TripleDES 
            // crypt provider along with encryption key and init vector 
            // information 
            ICryptoTransform cryptoTransform = tdesProvider.CreateEncryptor(_key, _iv);

            // All cryptographic functions need a stream to output the 
            // encrypted information. Here we declare a memory stream 
            // for this purpose. 
            MemoryStream encryptedStream = new MemoryStream();
            CryptoStream cryptStream = new CryptoStream(encryptedStream, cryptoTransform, CryptoStreamMode.Write);

            // Write the encrypted information to the stream. Flush the information 
            // when done to ensure everything is out of the buffer. 
            cryptStream.Write(inputInBytes, 0, inputInBytes.Length);
            cryptStream.FlushFinalBlock();
            encryptedStream.Position = 0;

            // Read the stream back into a Byte array and return it to the calling 
            // method. 
            byte[] result = new byte[encryptedStream.Length - 1 + 1];
            encryptedStream.Read(result, 0, (int)encryptedStream.Length);
            cryptStream.Close();

            UTF8Encoding myutf = new UTF8Encoding();
            return myutf.GetString(result);
        }

        public string ToBase64(string Mensaje)
        {
            UTF8Encoding utf8encoder = new UTF8Encoding();
            byte[] data = utf8encoder.GetBytes(Mensaje);

            if (data == null)
                throw new ArgumentNullException("data");

            return Convert.ToBase64String(data);
        }
    }
    
    public enum UpdateType {Add=1, Update, Delete, Browse, Not_Defined};
    public enum QueryType {AllRowsAllColumns=1, AllRowsComboBox, OneRow, OneRowOneColumn};

}