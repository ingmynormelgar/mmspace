using System;

namespace mmspace.api.Data.Fx
{
    public class CReflection
    {
        public void FillObjectWithProperty(ref object objectTo, string propertyName, object propertyValue)
        {
            Type tOb2 = objectTo.GetType();
            try //Para tolerar el fallo cuando en el objeto no exista la propiedad
            {
                tOb2.GetProperty(propertyName).SetValue(objectTo, propertyValue);   
            }
            catch (System.Exception)
            {            
            }            
        }
    }
}