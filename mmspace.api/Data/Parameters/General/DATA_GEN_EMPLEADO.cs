using System;

namespace mmspace.api.Data.Parameters.General
{
	public class DATA_GEN_EMPLEADO
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } = 0;
		public String ESTADO_EMPLEADO { get; set; } = "A";
		public String LOGIN_SISTEMA { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}