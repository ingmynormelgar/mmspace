using System;

namespace mmspace.api.Data.Parameters.General
{
	public class DATA_GEN_EMPRESA
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public String LOGIN_USUARIO { get; set; } 
		public Int32 CORR_CONFI_PAIS_ORIGEN { get; set; } 
		public Int32 CORR_EMPRESA_ORIGEN { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}