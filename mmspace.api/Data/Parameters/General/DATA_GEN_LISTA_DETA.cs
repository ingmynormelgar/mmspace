using System;

namespace mmspace.api.Data.Parameters.General
{
	public class DATA_GEN_LISTA_DETA
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_LISTA { get; set; } 
		public String CODIGO { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}