using System;

namespace mmspace.api.Data.Parameters.General
{
	public class DATA_GEN_MONEDA
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_MONEDA { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}