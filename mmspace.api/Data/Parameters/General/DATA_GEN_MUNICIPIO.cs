using System;

namespace mmspace.api.Data.Parameters.AssetManagement
{
	public class DATA_GEN_MUNICIPIO
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_PAIS { get; set; } 
		public Int32 CORR_DEPTO { get; set; } 
		public Int32 CORR_MUNICIPIO { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}