using System;

namespace mmspace.api.Data.Parameters.General
{
	public class DATA_GEN_PERIODICIDAD
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_PERIODICIDAD { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}