using System;

namespace mmspace.api.Data.Parameters.General
{
	public class DATA_GEN_TIPO_DIP
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_TIPO_DIP { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}