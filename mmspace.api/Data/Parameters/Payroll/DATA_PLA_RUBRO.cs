using System;

namespace mmspace.api.Data.Parameters.Payroll
{
	public class DATA_PLA_RUBRO
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_RUBRO { get; set; } 
		public Boolean ES_IMPUESTO { get; set; } 
		public Int32 INGRESO_DEDUCCION { get; set; } 
		public String CLASE_RUBRO { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}