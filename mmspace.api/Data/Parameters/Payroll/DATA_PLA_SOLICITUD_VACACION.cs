using System;

namespace mmspace.api.Data.Parameters.Payroll
{
	public class DATA_PLA_SOLICITUD_VACACION
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public Int32 CORR_SOLI_VACACION { get; set; } 
		public String ESTADO_SOLICITUD { get; set; } 
		public DateTime FECHA_INICIAL { get; set; } 
		public DateTime FECHA_FINAL { get; set; } 
		public String LOGIN_SISTEMA { get; set; }
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}