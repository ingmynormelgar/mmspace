using System;

namespace mmspace.api.Data.Parameters.Payroll
{
	public class DATA_PLA_SOLI_HORA_EXTRA
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public Int32 CORR_SOLICITUD { get; set; } 
		public String ESTADO_SOLICITUD { get; set; } 
		public DateTime FECHA_INICIAL { get; set; } 
		public DateTime FECHA_FINAL { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
		public string USUARIO_SOLICITA { get; set; } 
	}
}