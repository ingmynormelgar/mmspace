using System;

namespace mmspace.api.Data.Parameters.Payroll
{
	public class DATA_PLA_SOLI_HORA_EXTRA_DETA
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public Int32 CORR_SOLICITUD { get; set; } 
		public Int32 CORR_SOLICITUD_DETA { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}