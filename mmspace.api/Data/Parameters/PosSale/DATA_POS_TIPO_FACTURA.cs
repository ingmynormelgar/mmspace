using System;

namespace mmspace.api.Data.Parameters.Possale
{
	public class DATA_POS_TIPO_FACTURA
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_TIPO_FACTURA { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}