using System;

namespace mmspace.api.Data.Parameters.Security
{
	public class DATA_SEG_CONFIG_OPCION
	{
		public Int32 TIPO_CONSULTA { get; set; } 
		public Int32 CORR_SUSCRIPCION { get; set; }  = 7;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 7;
		public String CODIGO_SISTEMA { get; set; } 
		public String CODIGO_MENU { get; set; } 
		public String CODIGO_OPCION { get; set; } 
		public Int32 OPCION_CONSULTA { get; set; } 
	}
}