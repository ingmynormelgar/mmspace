namespace mmspace.api.Data.Parameters.Security
{
    public class DATA_SEG_USUARIO
    {
        public int TIPO_CONSULTA { get; set; }
	    public string LOGIN_SISTEMA { get; set; }
	    public int OPCION_CONSULTA { get; set; } = 0;
    }
}