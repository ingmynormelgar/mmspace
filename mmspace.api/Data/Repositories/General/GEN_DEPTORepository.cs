using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using mmspace.api.Data.Fx;
using mmspace.api.Models.AssetManagement;
using mmspace.api.Data.Parameters.AssetManagement;

namespace mmspace.api.Data.Repositories.AssetManagement
{
	public class GEN_DEPTORepository : IGEN_DEPTORepository
	{
		private const string _TableName = "GEN_DEPTO";
		private CData _Data;
		public GEN_DEPTORepository(IConfiguration _config)
		{
			_Data = new CData(_config.GetConnectionString("defaultConnection"),
					  _config.GetSection("DbProvider:defaultProvider").Value);
		}
		
		public async Task<CResult> Save(GEN_DEPTO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PAIS",Value=Data.CORR_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_DEPTO",Value=Data.CORR_DEPTO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NOMBRE_DEPTO",Value=Data.NOMBRE_DEPTO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_DEPTO",Value=Data.CODIGO_DEPTO,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_DEPTO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
			
			return objResultado;
		}
		public async Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, Int32 vCORR_DEPTO, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PAIS",Value=vCORR_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_DEPTO",Value=vCORR_DEPTO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NOMBRE_DEPTO",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_DEPTO",Value="",DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_DEPTO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";
			
			return objResultado;
		}
		
		public async Task<V_GEN_DEPTO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, Int32 vCORR_DEPTO)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PAIS",Value=vCORR_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_DEPTO",Value=vCORR_DEPTO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_GEN_DEPTO>().FromDataReader(reader).FirstOrDefault();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, Int32 vCORR_DEPTO)
		{
			Boolean existe;
			var response = new V_GEN_DEPTO();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PAIS",Value=vCORR_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_DEPTO",Value=vCORR_DEPTO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			
			if (reader.HasRows)
				existe=true;
			else
				existe=false;
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return existe;
		}
		public async Task<List<V_GEN_DEPTO>> GetAll(DATA_GEN_DEPTO Data)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PAIS",Value=Data.CORR_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_DEPTO",Value=Data.CORR_DEPTO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_GEN_DEPTO>().FromDataReader(reader).ToList();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<Object> GetScalar(DATA_GEN_DEPTO Data, int vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PAIS",Value=Data.CORR_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_DEPTO",Value=Data.CORR_DEPTO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        
			
			return response;
		}
	}
}