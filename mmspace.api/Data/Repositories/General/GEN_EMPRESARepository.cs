using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using mmspace.api.Data.Fx;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;

namespace mmspace.api.Data.Repositories.General
{
	public class GEN_EMPRESARepository : IGEN_EMPRESARepository
	{
		private const string _TableName = "GEN_EMPRESA";
		private CData _Data;
		public GEN_EMPRESARepository(IConfiguration _config)
		{
			_Data = new CData(_config.GetConnectionString("defaultConnection"),
					  _config.GetSection("DbProvider:defaultProvider").Value);
		}
		
		public async Task<CResult> Save(GEN_EMPRESA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NOMBRE_EMPRESA",Value=Data.NOMBRE_EMPRESA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NOMBRE_COMERCIAL",Value=Data.NOMBRE_COMERCIAL,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@GIRO_EMPRESA",Value=Data.GIRO_EMPRESA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@DIRECCION_EMPRESA",Value=Data.DIRECCION_EMPRESA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NUMERO_NIT",Value=Data.NUMERO_NIT,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NUMERO_NRC",Value=Data.NUMERO_NRC,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NOMBRE_CONTACTO",Value=Data.NOMBRE_CONTACTO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@TELEFONO_1",Value=Data.TELEFONO_1,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@TELEFONO_2",Value=Data.TELEFONO_2,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FAX",Value=Data.FAX,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORREO_ELECTRONICO",Value=Data.CORREO_ELECTRONICO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@TAMANO_EMPRESA",Value=Data.TAMANO_EMPRESA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NATURAL_JURIDICO",Value=Data.NATURAL_JURIDICO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_SUSCRIPCION",Value=Data.CODIGO_SUSCRIPCION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value=Data.USUARIO_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=Data.FECHA_CREA,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value=Data.ESTACION_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=Data.USUARIO_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=Data.FECHA_ACTU,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=Data.ESTACION_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_GRUPO",Value=Data.CORR_GRUPO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CODIGO_EMPRESA",Value=Data.CODIGO_EMPRESA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_MONEDA",Value=Data.CORR_MONEDA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PAIS",Value=Data.CORR_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_DEPTO",Value=Data.CORR_DEPTO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_MUNICIPIO",Value=Data.CORR_MUNICIPIO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@DIRECCION_EMPRESA_LARGO",Value=Data.DIRECCION_EMPRESA_LARGO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NOMBRE_EMPRESA_LARGO",Value=Data.NOMBRE_EMPRESA_LARGO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_POSTAL",Value=Data.CODIGO_POSTAL,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_EMPRESA"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
			
			return objResultado;
		}
		public async Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NOMBRE_EMPRESA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NOMBRE_COMERCIAL",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@GIRO_EMPRESA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@DIRECCION_EMPRESA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NUMERO_NIT",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NUMERO_NRC",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NOMBRE_CONTACTO",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@TELEFONO_1",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@TELEFONO_2",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FAX",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORREO_ELECTRONICO",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@TAMANO_EMPRESA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NATURAL_JURIDICO",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_SUSCRIPCION",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_GRUPO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CODIGO_EMPRESA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_MONEDA",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PAIS",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_DEPTO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_MUNICIPIO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@DIRECCION_EMPRESA_LARGO",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@NOMBRE_EMPRESA_LARGO",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_POSTAL",Value="",DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_EMPRESA"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";
			
			return objResultado;
		}
		
		public async Task<V_GEN_EMPRESA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_GEN_EMPRESA>().FromDataReader(reader).FirstOrDefault();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA)
		{
			Boolean existe;
			var response = new V_GEN_EMPRESA();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			
			if (reader.HasRows)
				existe=true;
			else
				existe=false;
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return existe;
		}
		public async Task<List<V_GEN_EMPRESA>> GetAll(DATA_GEN_EMPRESA Data)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_GEN_EMPRESA>().FromDataReader(reader).ToList();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<Object> GetScalar(DATA_GEN_EMPRESA Data, int vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@LOGIN_USUARIO",Value=Data.LOGIN_USUARIO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS_ORIGEN",Value=Data.CORR_CONFI_PAIS_ORIGEN,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA_ORIGEN",Value=Data.CORR_EMPRESA_ORIGEN,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        
			
			return response;
		}
	}
}