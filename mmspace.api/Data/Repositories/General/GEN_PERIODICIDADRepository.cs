using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using mmspace.api.Data.Fx;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;

namespace mmspace.api.Data.Repositories.General
{
	public class GEN_PERIODICIDADRepository : IGEN_PERIODICIDADRepository
	{
		private const string _TableName = "GEN_PERIODICIDAD";
		private CData _Data;
		public GEN_PERIODICIDADRepository(IConfiguration _config)
		{
			_Data = new CData(_config.GetConnectionString("defaultConnection"),
					  _config.GetSection("DbProvider:defaultProvider").Value);
		}
		
		public async Task<CResult> Save(GEN_PERIODICIDAD Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PERIODICIDAD",Value=Data.CORR_PERIODICIDAD,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NOMBRE_PERIODICIDAD",Value=Data.NOMBRE_PERIODICIDAD,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CUOTAS_ANIO",Value=Data.CUOTAS_ANIO,DbType=System.Data.DbType.Int32});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_PERIODICIDAD"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
			
			return objResultado;
		}
		public async Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PERIODICIDAD, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PERIODICIDAD",Value=vCORR_PERIODICIDAD,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NOMBRE_PERIODICIDAD",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CUOTAS_ANIO",Value=0,DbType=System.Data.DbType.Int32});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_PERIODICIDAD"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";
			
			return objResultado;
		}
		
		public async Task<V_GEN_PERIODICIDAD> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PERIODICIDAD)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PERIODICIDAD",Value=vCORR_PERIODICIDAD,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_GEN_PERIODICIDAD>().FromDataReader(reader).FirstOrDefault();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PERIODICIDAD)
		{
			Boolean existe;
			var response = new V_GEN_PERIODICIDAD();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PERIODICIDAD",Value=vCORR_PERIODICIDAD,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			
			if (reader.HasRows)
				existe=true;
			else
				existe=false;
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return existe;
		}
		public async Task<List<V_GEN_PERIODICIDAD>> GetAll(DATA_GEN_PERIODICIDAD Data)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PERIODICIDAD",Value=Data.CORR_PERIODICIDAD,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_GEN_PERIODICIDAD>().FromDataReader(reader).ToList();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<Object> GetScalar(DATA_GEN_PERIODICIDAD Data, int vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PERIODICIDAD",Value=Data.CORR_PERIODICIDAD,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        
			
			return response;
		}
	}
}