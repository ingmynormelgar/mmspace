using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.AssetManagement;
using mmspace.api.Data.Parameters.AssetManagement;

namespace mmspace.api.Data.Repositories.AssetManagement
{
	public interface IGEN_DEPTORepository
	{
		Task<CResult> Save(GEN_DEPTO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, Int32 vCORR_DEPTO, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_GEN_DEPTO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, Int32 vCORR_DEPTO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, Int32 vCORR_DEPTO);
		Task<List<V_GEN_DEPTO>> GetAll(DATA_GEN_DEPTO Data);
		Task<object> GetScalar(DATA_GEN_DEPTO Data, int vOPCION_CONSULTA);
	}
}