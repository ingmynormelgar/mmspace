using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;

namespace mmspace.api.Data.Repositories.General
{
	public interface IGEN_EMPLEADORepository
	{			
		Task<V_GEN_EMPLEADO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO);
		Task<List<V_GEN_EMPLEADO>> GetAll(DATA_GEN_EMPLEADO Data);
		Task<object> GetScalar(DATA_GEN_EMPLEADO Data, int vOPCION_CONSULTA);
		Task<GEN_PERIODICIDAD_EMPLEADO> GetPeriodicidadEmpleado(Int32 vCORR_SUSCRIPCION, 
																			 Int32 vCORR_CONFI_PAIS, 
																			 Int32 vCORR_EMPRESA, 
																			 Int32 vCORR_EMPLEADO);
	}
}