using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;

namespace mmspace.api.Data.Repositories.General
{
	public interface IGEN_EMPRESARepository
	{
		Task<CResult> Save(GEN_EMPRESA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_GEN_EMPRESA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA);
		Task<List<V_GEN_EMPRESA>> GetAll(DATA_GEN_EMPRESA Data);
		Task<object> GetScalar(DATA_GEN_EMPRESA Data, int vOPCION_CONSULTA);
	}
}