using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;

namespace mmspace.api.Data.Repositories.General
{
	public interface IGEN_LISTA_DETARepository
	{				
		Task<V_GEN_LISTA_DETA> Get(Int32 vCORR_LISTA, String vCODIGO);
		Task<bool> Exists(Int32 vCORR_LISTA, String vCODIGO);
		Task<List<V_GEN_LISTA_DETA>> GetAll(DATA_GEN_LISTA_DETA Data);
		Task<object> GetScalar(DATA_GEN_LISTA_DETA Data, int vOPCION_CONSULTA);
	}
}