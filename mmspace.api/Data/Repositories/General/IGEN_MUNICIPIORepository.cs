using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.AssetManagement;
using mmspace.api.Data.Parameters.AssetManagement;

namespace mmspace.api.Data.Repositories.AssetManagement
{
	public interface IGEN_MUNICIPIORepository
	{
		Task<CResult> Save(GEN_MUNICIPIO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, Int32 vCORR_DEPTO, Int32 vCORR_MUNICIPIO, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_GEN_MUNICIPIO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, Int32 vCORR_DEPTO, Int32 vCORR_MUNICIPIO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, Int32 vCORR_DEPTO, Int32 vCORR_MUNICIPIO);
		Task<List<V_GEN_MUNICIPIO>> GetAll(DATA_GEN_MUNICIPIO Data);
		Task<object> GetScalar(DATA_GEN_MUNICIPIO Data, int vOPCION_CONSULTA);
	}
}