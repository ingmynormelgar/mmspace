using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.AssetManagement;
using mmspace.api.Data.Parameters.AssetManagement;

namespace mmspace.api.Data.Repositories.AssetManagement
{
	public interface IGEN_PAISRepository
	{
		Task<CResult> Save(GEN_PAIS Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_GEN_PAIS> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_PAIS);
		Task<List<V_GEN_PAIS>> GetAll(DATA_GEN_PAIS Data);
		Task<object> GetScalar(DATA_GEN_PAIS Data, int vOPCION_CONSULTA);
	}
}