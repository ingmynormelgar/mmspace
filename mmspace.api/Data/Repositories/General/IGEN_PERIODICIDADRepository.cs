using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;

namespace mmspace.api.Data.Repositories.General
{
	public interface IGEN_PERIODICIDADRepository
	{
		Task<CResult> Save(GEN_PERIODICIDAD Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PERIODICIDAD, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_GEN_PERIODICIDAD> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PERIODICIDAD);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PERIODICIDAD);
		Task<List<V_GEN_PERIODICIDAD>> GetAll(DATA_GEN_PERIODICIDAD Data);
		Task<object> GetScalar(DATA_GEN_PERIODICIDAD Data, int vOPCION_CONSULTA);
	}
}