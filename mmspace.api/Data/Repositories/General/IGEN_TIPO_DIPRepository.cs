using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;

namespace mmspace.api.Data.Repositories.General
{
	public interface IGEN_TIPO_DIPRepository
	{
		Task<CResult> Save(GEN_TIPO_DIP Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_TIPO_DIP, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_GEN_TIPO_DIP> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_TIPO_DIP);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_TIPO_DIP);
		Task<List<V_GEN_TIPO_DIP>> GetAll(DATA_GEN_TIPO_DIP Data);
		Task<object> GetScalar(DATA_GEN_TIPO_DIP Data, int vOPCION_CONSULTA);
	}
}