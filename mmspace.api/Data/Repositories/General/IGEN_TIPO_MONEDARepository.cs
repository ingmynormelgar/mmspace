using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.General;
using mmspace.api.Data.Parameters.General;

namespace mmspace.api.Data.Repositories.General
{
	public interface IGEN_TIPO_MONEDARepository
	{
		Task<CResult> Save(GEN_TIPO_MONEDA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_MONEDA, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_GEN_TIPO_MONEDA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_MONEDA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_MONEDA);
		Task<List<V_GEN_TIPO_MONEDA>> GetAll(DATA_GEN_TIPO_MONEDA Data);
		Task<object> GetScalar(DATA_GEN_TIPO_MONEDA Data, int vOPCION_CONSULTA);
	}
}