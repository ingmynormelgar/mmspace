using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public interface IPLA_PARAMETRORepository
	{
		Task<V_PLA_PARAMETRO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA);
		Task<List<V_PLA_PARAMETRO>> GetAll(DATA_PLA_PARAMETRO Data);
		Task<object> GetScalar(DATA_PLA_PARAMETRO Data, int vOPCION_CONSULTA);
	}
}