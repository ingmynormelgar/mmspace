using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public interface IPLA_PLAN_VACACION_EMPLEADORepository
	{
		Task<CResult> Save(PLA_PLAN_VACACION_EMPLEADO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vANIO_PERIODO, Int32 vCORR_EMPLEADO, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_PLA_PLAN_VACACION_EMPLEADO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vANIO_PERIODO, Int32 vCORR_EMPLEADO, Int32 vOPCION_CONSULTA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vANIO_PERIODO, Int32 vCORR_EMPLEADO);
		Task<List<V_PLA_PLAN_VACACION_EMPLEADO>> GetAll(DATA_PLA_PLAN_VACACION_EMPLEADO Data);
		Task<object> GetScalar(DATA_PLA_PLAN_VACACION_EMPLEADO Data, int vOPCION_CONSULTA);
		Task<CResult> SolicitarPro(PLA_PLAN_VACACION_EMPLEADO Data, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Autorizar(List<PLA_PLAN_VACACION_EMPLEADO> Data, String vLOGIN_SISTEMA, String vESTACION);
	}
}