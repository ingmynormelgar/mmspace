using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public interface IPLA_RUBRORepository
	{
		Task<CResult> Save(PLA_RUBRO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_RUBRO, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_PLA_RUBRO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_RUBRO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_RUBRO);
		Task<List<V_PLA_RUBRO>> GetAll(DATA_PLA_RUBRO Data);
		Task<object> GetScalar(DATA_PLA_RUBRO Data, int vOPCION_CONSULTA);
	}
}