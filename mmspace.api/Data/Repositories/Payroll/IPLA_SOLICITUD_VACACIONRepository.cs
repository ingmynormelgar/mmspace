using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public interface IPLA_SOLICITUD_VACACIONRepository
	{
		Task<CResult> Save(PLA_SOLICITUD_VACACION Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_SOLI_VACACION, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_PLA_SOLICITUD_VACACION> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_SOLI_VACACION, Int32 vCORR_EMPLEADO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_SOLI_VACACION, Int32 vCORR_EMPLEADO);
		Task<List<V_PLA_SOLICITUD_VACACION>> GetAll(DATA_PLA_SOLICITUD_VACACION Data);
	
		Task<object> GetScalar(DATA_PLA_SOLICITUD_VACACION Data, int vOPCION_CONSULTA);
		Task<CResult> Solicitar(PLA_SOLICITUD_VACACION Data, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Aplicar(PLA_SOLICITUD_VACACION Data, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Rechazar (PLA_SOLICITUD_VACACION Data, String vLOGIN_SISTEMA, String vESTACION);
	}
}