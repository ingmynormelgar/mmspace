using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public interface IPLA_SOLI_CONVENIORepository
	{
		Task<CResult> Save(PLA_SOLI_CONVENIO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_CONVENIO, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_PLA_SOLI_CONVENIO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_CONVENIO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_CONVENIO);
		Task<List<V_PLA_SOLI_CONVENIO>> GetAll(DATA_PLA_SOLI_CONVENIO Data);
		Task<object> GetScalar(DATA_PLA_SOLI_CONVENIO Data, int vOPCION_CONSULTA);
		Task<CResult> Solicitar(PLA_SOLI_CONVENIO Data, String vLOGIN_SISTEMA, String vESTACION);
	}
}