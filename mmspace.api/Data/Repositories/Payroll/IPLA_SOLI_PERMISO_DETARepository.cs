using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public interface IPLA_SOLI_PERMISO_DETARepository
	{
		Task<CResult> Save(PLA_SOLI_PERMISO_DETA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_PERMISO, Int32 vCORR_PERMISO_DETA, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_PLA_SOLI_PERMISO_DETA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_PERMISO, Int32 vCORR_PERMISO_DETA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_PERMISO, Int32 vCORR_PERMISO_DETA);
		Task<List<V_PLA_SOLI_PERMISO_DETA>> GetAll(DATA_PLA_SOLI_PERMISO_DETA Data);
		Task<object> GetScalar(DATA_PLA_SOLI_PERMISO_DETA Data, int vOPCION_CONSULTA);
	}
}