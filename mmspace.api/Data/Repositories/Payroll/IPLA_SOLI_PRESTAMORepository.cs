using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public interface IPLA_SOLI_PRESTAMORepository
	{
		Task<CResult> Save(PLA_SOLI_PRESTAMO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_PRESTAMO, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_PLA_SOLI_PRESTAMO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_PRESTAMO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_PRESTAMO);
		Task<List<V_PLA_SOLI_PRESTAMO>> GetAll(DATA_PLA_SOLI_PRESTAMO Data);
		Task<object> GetScalar(DATA_PLA_SOLI_PRESTAMO Data, int vOPCION_CONSULTA);
		Task<CResult> Solicitar(PLA_SOLI_PRESTAMO Data, String vLOGIN_SISTEMA, String vESTACION);
	}
}