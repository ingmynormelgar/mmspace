using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public interface IPLA_TIPO_CONSTANCIARepository
	{
		Task<CResult> Save(PLA_TIPO_CONSTANCIA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_TIPO_CONSTANCIA, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_PLA_TIPO_CONSTANCIA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_TIPO_CONSTANCIA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_TIPO_CONSTANCIA);
		Task<List<V_PLA_TIPO_CONSTANCIA>> GetAll(DATA_PLA_TIPO_CONSTANCIA Data);
		Task<object> GetScalar(DATA_PLA_TIPO_CONSTANCIA Data, int vOPCION_CONSULTA);
	}
}