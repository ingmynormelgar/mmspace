using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public class PLA_PLAN_VACACION_EMPLEADORepository : IPLA_PLAN_VACACION_EMPLEADORepository
	{
		private const string _TableName = "PLA_PLAN_VACACION_EMPLEADO";
		private CData _Data;
		public PLA_PLAN_VACACION_EMPLEADORepository(IConfiguration _config)
		{
			_Data = new CData(_config.GetConnectionString("defaultConnection"),
					  _config.GetSection("DbProvider:defaultProvider").Value);
		}
		
		public async Task<CResult> Save(PLA_PLAN_VACACION_EMPLEADO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ANIO_PERIODO",Value=Data.ANIO_PERIODO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@CORR_DEPARTAMENTO",Value=Data.CORR_DEPARTAMENTO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CENTRO_COSTO",Value=Data.CORR_CENTRO_COSTO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@FECHA_INGRESO_EMPLEADO",Value=Data.FECHA_INGRESO_EMPLEADO,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@DIAS_SALDO",Value=Data.DIAS_SALDO,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_1",Value=Data.DIAS_PROG_1,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_2",Value=Data.DIAS_PROG_2,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_3",Value=Data.DIAS_PROG_3,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_4",Value=Data.DIAS_PROG_4,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_5",Value=Data.DIAS_PROG_5,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_6",Value=Data.DIAS_PROG_6,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_7",Value=Data.DIAS_PROG_7,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_8",Value=Data.DIAS_PROG_8,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_9",Value=Data.DIAS_PROG_9,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_10",Value=Data.DIAS_PROG_10,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_11",Value=Data.DIAS_PROG_11,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_12",Value=Data.DIAS_PROG_12,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_1",Value=Data.DIAS_ACUM_1,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_2",Value=Data.DIAS_ACUM_2,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_3",Value=Data.DIAS_ACUM_3,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_4",Value=Data.DIAS_ACUM_4,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_5",Value=Data.DIAS_ACUM_5,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_6",Value=Data.DIAS_ACUM_6,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_7",Value=Data.DIAS_ACUM_7,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_8",Value=Data.DIAS_ACUM_8,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_9",Value=Data.DIAS_ACUM_9,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_10",Value=Data.DIAS_ACUM_10,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_11",Value=Data.DIAS_ACUM_11,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_12",Value=Data.DIAS_ACUM_12,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_1",Value=Data.DIAS_DESC_1,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_2",Value=Data.DIAS_DESC_2,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_3",Value=Data.DIAS_DESC_3,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_4",Value=Data.DIAS_DESC_4,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_5",Value=Data.DIAS_DESC_5,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_6",Value=Data.DIAS_DESC_6,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_7",Value=Data.DIAS_DESC_7,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_8",Value=Data.DIAS_DESC_8,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_9",Value=Data.DIAS_DESC_9,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_10",Value=Data.DIAS_DESC_10,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_11",Value=Data.DIAS_DESC_11,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_12",Value=Data.DIAS_DESC_12,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PAGO",Value=Data.DIAS_PAGO,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@ESTADO_PLAN",Value=Data.ESTADO_PLAN,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value=Data.USUARIO_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=Data.FECHA_CREA,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value=Data.ESTACION_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=Data.USUARIO_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=Data.FECHA_ACTU,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=Data.ESTACION_ACTU,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_EMPLEADO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
			
			return objResultado;
		}
		public async Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vANIO_PERIODO, Int32 vCORR_EMPLEADO, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ANIO_PERIODO",Value=vANIO_PERIODO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vCORR_EMPLEADO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@CORR_DEPARTAMENTO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CENTRO_COSTO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@FECHA_INGRESO_EMPLEADO",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@DIAS_SALDO",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_1",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_2",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_3",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_4",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_5",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_6",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_7",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_8",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_9",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_10",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_11",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PROG_12",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_1",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_2",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_3",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_4",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_5",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_6",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_7",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_8",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_9",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_10",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_11",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_ACUM_12",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_1",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_2",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_3",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_4",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_5",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_6",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_7",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_8",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_9",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_10",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_11",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_DESC_12",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@DIAS_PAGO",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@ESTADO_PLAN",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value="",DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_EMPLEADO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";
			
			return objResultado;
		}
		
		public async Task<V_PLA_PLAN_VACACION_EMPLEADO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vANIO_PERIODO, Int32 vCORR_EMPLEADO, Int32 vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ANIO_PERIODO",Value=vANIO_PERIODO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vCORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_PLA_PLAN_VACACION_EMPLEADO>().FromDataReader(reader).FirstOrDefault();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vANIO_PERIODO, Int32 vCORR_EMPLEADO)
		{
			Boolean existe;
			var response = new V_PLA_PLAN_VACACION_EMPLEADO();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ANIO_PERIODO",Value=vANIO_PERIODO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vCORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			
			if (reader.HasRows)
				existe=true;
			else
				existe=false;
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return existe;
		}
		public async Task<List<V_PLA_PLAN_VACACION_EMPLEADO>> GetAll(DATA_PLA_PLAN_VACACION_EMPLEADO Data)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ANIO_PERIODO",Value=Data.ANIO_PERIODO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=Data.LOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_PLA_PLAN_VACACION_EMPLEADO>().FromDataReader(reader).ToList();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<Object> GetScalar(DATA_PLA_PLAN_VACACION_EMPLEADO Data, int vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ANIO_PERIODO",Value=Data.ANIO_PERIODO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        
			
			return response;
		}

		public async Task<CResult> SolicitarPro(PLA_PLAN_VACACION_EMPLEADO Data, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ANIO_PERIODO",Value=Data.ANIO_PERIODO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=vESTACION,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName+"_SOLICITAR_PROGRAMACION",true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_EMPLEADO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".SolicitarPro";
			
			return objResultado;
		}

		public async Task<CResult> Autorizar(List<PLA_PLAN_VACACION_EMPLEADO> Data, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			foreach ( var vVacacionEmpleado in Data)
			{
				p.Clear();
				p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vVacacionEmpleado.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vVacacionEmpleado.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vVacacionEmpleado.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@ANIO_PERIODO",Value=vVacacionEmpleado.ANIO_PERIODO,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vVacacionEmpleado.CORR_EMPLEADO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
				p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
				p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
				p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=vESTACION,DbType=System.Data.DbType.String});
				
				//Parametros para gestionar la operación
				p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
				p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
				p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
				p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
				p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
				
				await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName+"_AUTORIZAR",true,p);
			}

			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_EMPLEADO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Autorizar";

			return objResultado;
		}
	}
}