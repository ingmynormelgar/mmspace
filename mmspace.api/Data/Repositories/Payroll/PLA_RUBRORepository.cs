using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public class PLA_RUBRORepository : IPLA_RUBRORepository
	{
		private const string _TableName = "PLA_RUBRO";
		private CData _Data;
		public PLA_RUBRORepository(IConfiguration _config)
		{
			_Data = new CData(_config.GetConnectionString("defaultConnection"),
					  _config.GetSection("DbProvider:defaultProvider").Value);
		}
		
		public async Task<CResult> Save(PLA_RUBRO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO",Value=Data.CORR_RUBRO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NOMBRE_RUBRO",Value=Data.NOMBRE_RUBRO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@INGRESO_DEDUCCION",Value=Data.INGRESO_DEDUCCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CLASE_RUBRO",Value=Data.CLASE_RUBRO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@ES_IMPUESTO",Value=Data.ES_IMPUESTO,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@CUENTA_CONTABLE",Value=Data.CUENTA_CONTABLE,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@MOSTRAR_IMPRESION",Value=Data.MOSTRAR_IMPRESION,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@ORDEN_RUBRO",Value=Data.ORDEN_RUBRO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@POR_RUBRO",Value=Data.POR_RUBRO,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@CLASE_TIPO_FACTOR",Value=Data.CLASE_TIPO_FACTOR,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO_ADELANTO",Value=Data.CORR_RUBRO_ADELANTO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@INCLUYE_CALC_SUELDO_PROMEDIO",Value=Data.INCLUYE_CALC_SUELDO_PROMEDIO,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@BASE_PROVISION",Value=Data.BASE_PROVISION,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_RUBRO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
			
			return objResultado;
		}
		public async Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_RUBRO, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO",Value=vCORR_RUBRO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NOMBRE_RUBRO",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@INGRESO_DEDUCCION",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CLASE_RUBRO",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@ES_IMPUESTO",Value=0,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@CUENTA_CONTABLE",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@MOSTRAR_IMPRESION",Value=0,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@ORDEN_RUBRO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@POR_RUBRO",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@CLASE_TIPO_FACTOR",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO_ADELANTO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@INCLUYE_CALC_SUELDO_PROMEDIO",Value=0,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@BASE_PROVISION",Value="",DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_RUBRO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";
			
			return objResultado;
		}
		
		public async Task<V_PLA_RUBRO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_RUBRO)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO",Value=vCORR_RUBRO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_PLA_RUBRO>().FromDataReader(reader).FirstOrDefault();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_RUBRO)
		{
			Boolean existe;
			var response = new V_PLA_RUBRO();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO",Value=vCORR_RUBRO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			
			if (reader.HasRows)
				existe=true;
			else
				existe=false;
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return existe;
		}
		public async Task<List<V_PLA_RUBRO>> GetAll(DATA_PLA_RUBRO Data)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO",Value=Data.CORR_RUBRO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CLASE_RUBRO",Value=Data.CLASE_RUBRO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_PLA_RUBRO>().FromDataReader(reader).ToList();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<Object> GetScalar(DATA_PLA_RUBRO Data, int vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO",Value=Data.CORR_RUBRO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ES_IMPUESTO",Value=Data.ES_IMPUESTO,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@INGRESO_DEDUCCION",Value=Data.INGRESO_DEDUCCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CLASE_RUBRO",Value=Data.CLASE_RUBRO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        
			
			return response;
		}
	}
}