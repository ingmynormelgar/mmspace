using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public class PLA_SOLI_HORA_EXTRARepository : IPLA_SOLI_HORA_EXTRARepository
	{
		private const string _TableName = "PLA_SOLI_HORA_EXTRA";
		private CData _Data;
		public PLA_SOLI_HORA_EXTRARepository(IConfiguration _config)
		{
			_Data = new CData(_config.GetConnectionString("defaultConnection"),
					  _config.GetSection("DbProvider:defaultProvider").Value);
		}
		
		public async Task<CResult> Save(PLA_SOLI_HORA_EXTRA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=Data.CORR_SOLICITUD,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@FECHA_SOLICITUD",Value=Data.FECHA_SOLICITUD,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTADO_SOLICITUD",Value=Data.ESTADO_SOLICITUD,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OBSERVACIONES",Value=Data.OBSERVACIONES,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value=Data.USUARIO_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=Data.FECHA_CREA,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value=Data.ESTACION_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=Data.USUARIO_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=Data.FECHA_ACTU,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=Data.ESTACION_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_MONEDA",Value=Data.CORR_MONEDA,DbType=System.Data.DbType.Int32});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_SOLICITUD"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
			
			foreach(var Deta in Data.SOLI_HORA_EXTRA_DETA)
			{
				p.Clear();
				p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Deta.MTTO,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=objResultado.CodeHelper,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD_DETA",Value=Deta.CORR_SOLICITUD_DETA,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
				p.Add(new CParameter() {ParameterName="@CORR_RUBRO",Value=Deta.CORR_RUBRO,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@FECHA_INICIAL_SOLI",Value=Deta.FECHA_INICIAL_SOLI.ToLocalTime(),DbType=System.Data.DbType.DateTimeOffset});
				p.Add(new CParameter() {ParameterName="@FECHA_FINAL_SOLI",Value=Deta.FECHA_FINAL_SOLI.ToLocalTime(),DbType=System.Data.DbType.DateTimeOffset});
				p.Add(new CParameter() {ParameterName="@TOTAL_HORAS_SOLI",Value=Deta.TOTAL_HORAS_SOLI,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@TOTAL_MINUTOS_SOLI",Value=Deta.TOTAL_MINUTOS_SOLI,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@FECHA_INICIAL_AUT",Value=Deta.FECHA_INICIAL_AUT.ToLocalTime(),DbType=System.Data.DbType.DateTimeOffset});
				p.Add(new CParameter() {ParameterName="@FECHA_FINAL_AUT",Value=Deta.FECHA_FINAL_AUT.ToLocalTime(),DbType=System.Data.DbType.DateTimeOffset});
				p.Add(new CParameter() {ParameterName="@TOTAL_HORAS_AUT",Value=Deta.TOTAL_HORAS_AUT,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@TOTAL_MINUTOS_AUT",Value=Deta.TOTAL_MINUTOS_AUT,DbType=System.Data.DbType.Int32});
        		p.Add(new CParameter() {ParameterName="@FECHA_INICIAL",Value=Deta.FECHA_INICIAL.ToLocalTime(),DbType=System.Data.DbType.DateTimeOffset});
				p.Add(new CParameter() {ParameterName="@FECHA_FINAL",Value=Deta.FECHA_FINAL.ToLocalTime(),DbType=System.Data.DbType.DateTimeOffset});
				p.Add(new CParameter() {ParameterName="@TOTAL_HORAS",Value=Deta.TOTAL_HORAS,DbType=System.Data.DbType.Int32});
				p.Add(new CParameter() {ParameterName="@TOTAL_MINUTOS",Value=Deta.TOTAL_MINUTOS,DbType=System.Data.DbType.Int32});
				//Parametros para gestionar la operación
				p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
				p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
				p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
				p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
				p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
				
				await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName+"_DETA",true,p);
			
				objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
				objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
				objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
				objResultado.ErrorSource = "C" + _TableName + ".SaveDeta(" + Tipo.ToString() + ")";
			}

			return objResultado;
		}
		public async Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_SOLICITUD, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vCORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=vCORR_SOLICITUD,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@FECHA_SOLICITUD",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTADO_SOLICITUD",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OBSERVACIONES",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_MONEDA",Value=0,DbType=System.Data.DbType.Int32});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_SOLICITUD"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";
			
			return objResultado;
		}
		
		public async Task<V_PLA_SOLI_HORA_EXTRA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_SOLICITUD)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vCORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=vCORR_SOLICITUD,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_PLA_SOLI_HORA_EXTRA>().FromDataReader(reader).FirstOrDefault();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_SOLICITUD)
		{
			Boolean existe;
			var response = new V_PLA_SOLI_HORA_EXTRA();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vCORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=vCORR_SOLICITUD,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			
			if (reader.HasRows)
				existe=true;
			else
				existe=false;
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return existe;
		}
		public async Task<List<V_PLA_SOLI_HORA_EXTRA>> GetAll(DATA_PLA_SOLI_HORA_EXTRA Data)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=Data.CORR_SOLICITUD,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@USUARIO_SOLICITA",Value=Data.USUARIO_SOLICITA,DbType=System.Data.DbType.String});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_PLA_SOLI_HORA_EXTRA>().FromDataReader(reader).ToList();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<Object> GetScalar(DATA_PLA_SOLI_HORA_EXTRA Data, int vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=Data.CORR_SOLICITUD,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ESTADO_SOLICITUD",Value=Data.ESTADO_SOLICITUD,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_INICIAL",Value=Data.FECHA_INICIAL,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@FECHA_FINAL",Value=Data.FECHA_FINAL,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        
			
			return response;
		}

		public async Task<CResult> Solicitar(PLA_SOLI_HORA_EXTRA Data, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=Data.CORR_SOLICITUD,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=Data.USUARIO_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=Data.FECHA_ACTU,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=Data.ESTACION_ACTU,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName+"_SOLICITAR",true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_SOLICITUD"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Permiso(" + UpdateType.Update.ToString() + ")";
			
			return objResultado;
		}

		public async Task<CResult> Autorizar(PLA_SOLI_HORA_EXTRA Data, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=Data.CORR_SOLICITUD,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=Data.USUARIO_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=Data.FECHA_ACTU,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=Data.ESTACION_ACTU,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName+"_AUTORIZAR",true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_SOLICITUD"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Permiso(" + UpdateType.Update.ToString() + ")";
			
			return objResultado;
		}

		public async Task<CResult> Denegar(PLA_SOLI_HORA_EXTRA Data, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SOLICITUD",Value=Data.CORR_SOLICITUD,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@OBSERVACIONES",Value=Data.OBSERVACIONES,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName+"_DENEGAR",true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_SOLICITUD"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Permiso(" + UpdateType.Update.ToString() + ")";
			
			return objResultado;
		}
	}
}