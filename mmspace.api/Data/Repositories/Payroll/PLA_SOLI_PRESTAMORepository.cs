using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Payroll;
using mmspace.api.Data.Parameters.Payroll;

namespace mmspace.api.Data.Repositories.Payroll
{
	public class PLA_SOLI_PRESTAMORepository : IPLA_SOLI_PRESTAMORepository
	{
		private const string _TableName = "PLA_SOLI_PRESTAMO";
		private CData _Data;
		public PLA_SOLI_PRESTAMORepository(IConfiguration _config)
		{
			_Data = new CData(_config.GetConnectionString("defaultConnection"),
					  _config.GetSection("DbProvider:defaultProvider").Value);
		}
		
		public async Task<CResult> Save(PLA_SOLI_PRESTAMO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PRESTAMO",Value=Data.CORR_PRESTAMO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@FECHA_SOLICITUD",Value=Data.FECHA_SOLICITUD,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO_PRESTAMO",Value=Data.CORR_RUBRO_PRESTAMO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@FECHA_INICIO",Value=Data.FECHA_INICIO,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@CORR_PERIODICIDAD",Value=Data.CORR_PERIODICIDAD,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@MONTO_PRESTAMO",Value=Data.MONTO_PRESTAMO,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@NUMERO_CUOTAS",Value=Data.NUMERO_CUOTAS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@MONTO_CUOTA",Value=Data.MONTO_CUOTA,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@USUARIO_SOLICITA",Value=Data.USUARIO_SOLICITA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_AUTORIZA",Value=Data.USUARIO_AUTORIZA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_MONEDA",Value=Data.CORR_MONEDA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ESTADO_PRESTAMO",Value=Data.ESTADO_PRESTAMO,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OBSERVACIONES",Value=Data.OBSERVACIONES,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value=Data.USUARIO_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=Data.FECHA_CREA,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value=Data.ESTACION_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=Data.USUARIO_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=Data.FECHA_ACTU,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=Data.ESTACION_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO_FIJO",Value=Data.CORR_RUBRO_FIJO,DbType=System.Data.DbType.Int32});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_PRESTAMO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
			
			return objResultado;
		}
		public async Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_PRESTAMO, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vCORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PRESTAMO",Value=vCORR_PRESTAMO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@FECHA_SOLICITUD",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO_PRESTAMO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@FECHA_INICIO",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@CORR_PERIODICIDAD",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@MONTO_PRESTAMO",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@NUMERO_CUOTAS",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@MONTO_CUOTA",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@USUARIO_SOLICITA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_AUTORIZA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_MONEDA",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@ESTADO_PRESTAMO",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OBSERVACIONES",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_RUBRO_FIJO",Value=0,DbType=System.Data.DbType.Int32});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_PRESTAMO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";
			
			return objResultado;
		}
		
		public async Task<V_PLA_SOLI_PRESTAMO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_PRESTAMO)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vCORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PRESTAMO",Value=vCORR_PRESTAMO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_PLA_SOLI_PRESTAMO>().FromDataReader(reader).FirstOrDefault();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_EMPLEADO, Int32 vCORR_PRESTAMO)
		{
			Boolean existe;
			var response = new V_PLA_SOLI_PRESTAMO();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=vCORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PRESTAMO",Value=vCORR_PRESTAMO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			
			if (reader.HasRows)
				existe=true;
			else
				existe=false;
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return existe;
		}
		public async Task<List<V_PLA_SOLI_PRESTAMO>> GetAll(DATA_PLA_SOLI_PRESTAMO Data)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PRESTAMO",Value=Data.CORR_PRESTAMO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@FECHA_INICIAL",Value=Data.FECHA_INICIAL,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@FECHA_FINAL",Value=Data.FECHA_FINAL,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@USUARIO_SOLICITA",Value=Data.USUARIO_SOLICITA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_PLA_SOLI_PRESTAMO>().FromDataReader(reader).ToList();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<Object> GetScalar(DATA_PLA_SOLI_PRESTAMO Data, int vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PRESTAMO",Value=Data.CORR_PRESTAMO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@FECHA_INICIAL",Value=Data.FECHA_INICIAL,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@FECHA_FINAL",Value=Data.FECHA_FINAL,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@USUARIO_SOLICITA",Value=Data.USUARIO_SOLICITA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        
			
			return response;
		}

		public async Task<CResult> Solicitar(PLA_SOLI_PRESTAMO Data, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_PRESTAMO",Value=Data.CORR_PRESTAMO,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=vESTACION,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName+"_SOLICITAR",true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_PRESTAMO"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Prestamo(" + UpdateType.Update.ToString() + ")";
			
			return objResultado;
		}
	}
}