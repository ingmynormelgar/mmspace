using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public interface IPOS_BODEGARepository
	{
		Task<CResult> Save(POS_BODEGA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_BODEGA, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_POS_BODEGA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_BODEGA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_BODEGA);
		Task<List<V_POS_BODEGA>> GetAll(DATA_POS_BODEGA Data);
		Task<object> GetScalar(DATA_POS_BODEGA Data, int vOPCION_CONSULTA);
	}
}