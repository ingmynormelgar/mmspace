using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public interface IPOS_CLIENTERepository
	{
		Task<CResult> Save(POS_CLIENTE Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_CLIENTE, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_POS_CLIENTE> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_CLIENTE);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_CLIENTE);
		Task<List<V_POS_CLIENTE>> GetAll(DATA_POS_CLIENTE Data);
		Task<object> GetScalar(DATA_POS_CLIENTE Data, int vOPCION_CONSULTA);
	}
}