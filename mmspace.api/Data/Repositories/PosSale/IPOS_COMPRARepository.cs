using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public interface IPOS_COMPRARepository
	{
		Task<CResult> Save(POS_COMPRA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_COMPRA, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_POS_COMPRA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_COMPRA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_COMPRA);
		Task<List<V_POS_COMPRA>> GetAll(DATA_POS_COMPRA Data);
		Task<object> GetScalar(DATA_POS_COMPRA Data, int vOPCION_CONSULTA);
	}
}