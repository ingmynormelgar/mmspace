using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public interface IPOS_FORMA_PAGORepository
	{
		Task<CResult> Save(POS_FORMA_PAGO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_FORMA_PAGO, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_POS_FORMA_PAGO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_FORMA_PAGO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_FORMA_PAGO);
		Task<List<V_POS_FORMA_PAGO>> GetAll(DATA_POS_FORMA_PAGO Data);
		Task<object> GetScalar(DATA_POS_FORMA_PAGO Data, int vOPCION_CONSULTA);
	}
}