using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public interface IPOS_INVENTARIORepository
	{
		Task<CResult> Save(POS_INVENTARIO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vPERIODO_ANIO, Int32 vPERIODO_MES, Int32 vCORR_INVENTARIO, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_POS_INVENTARIO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vPERIODO_ANIO, Int32 vPERIODO_MES, Int32 vCORR_INVENTARIO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vPERIODO_ANIO, Int32 vPERIODO_MES, Int32 vCORR_INVENTARIO);
		Task<List<V_POS_INVENTARIO>> GetAll(DATA_POS_INVENTARIO Data);
		Task<object> GetScalar(DATA_POS_INVENTARIO Data, int vOPCION_CONSULTA);
	}
}