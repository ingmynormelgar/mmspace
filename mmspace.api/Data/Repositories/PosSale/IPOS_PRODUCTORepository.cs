using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public interface IPOS_PRODUCTORepository
	{
		Task<CResult> Save(POS_PRODUCTO Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PRODUCTO, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_POS_PRODUCTO> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PRODUCTO);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PRODUCTO);
		Task<List<V_POS_PRODUCTO>> GetAll(DATA_POS_PRODUCTO Data);
		Task<object> GetScalar(DATA_POS_PRODUCTO Data, int vOPCION_CONSULTA);
	}
}