using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public interface IPOS_PROVEEDORRepository
	{
		Task<CResult> Save(POS_PROVEEDOR Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PROVEEDOR, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_POS_PROVEEDOR> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PROVEEDOR);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_PROVEEDOR);
		Task<List<V_POS_PROVEEDOR>> GetAll(DATA_POS_PROVEEDOR Data);
		Task<object> GetScalar(DATA_POS_PROVEEDOR Data, int vOPCION_CONSULTA);
	}
}