using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public interface IPOS_TIPO_FACTURARepository
	{
		Task<CResult> Save(POS_TIPO_FACTURA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_TIPO_FACTURA, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_POS_TIPO_FACTURA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_TIPO_FACTURA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vCORR_TIPO_FACTURA);
		Task<List<V_POS_TIPO_FACTURA>> GetAll(DATA_POS_TIPO_FACTURA Data);
		Task<object> GetScalar(DATA_POS_TIPO_FACTURA Data, int vOPCION_CONSULTA);
	}
}