using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public interface IPOS_VENTARepository
	{
		Task<CResult> Save(POS_VENTA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vPERIODO_ANIO, Int32 vPERIODO_MES, Int32 vCORR_VENTA, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_POS_VENTA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vPERIODO_ANIO, Int32 vPERIODO_MES, Int32 vCORR_VENTA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vPERIODO_ANIO, Int32 vPERIODO_MES, Int32 vCORR_VENTA);
		Task<List<V_POS_VENTA>> GetAll(DATA_POS_VENTA Data);
		Task<object> GetScalar(DATA_POS_VENTA Data, int vOPCION_CONSULTA);
	}
}