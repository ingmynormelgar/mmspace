using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Security;
using mmspace.api.Data.Parameters.Security;

namespace mmspace.api.Data.Repositories.Security
{
	public interface ISEG_CONFIG_OPCIONRepository
	{
		Task<CResult> Save(SEG_CONFIG_OPCION Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_SISTEMA, String vCODIGO_MENU, String vCODIGO_OPCION, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_SEG_CONFIG_OPCION> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_SISTEMA, String vCODIGO_MENU, String vCODIGO_OPCION);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_SISTEMA, String vCODIGO_MENU, String vCODIGO_OPCION);
		Task<List<V_SEG_CONFIG_OPCION>> GetAll(DATA_SEG_CONFIG_OPCION Data);
		Task<object> GetScalar(DATA_SEG_CONFIG_OPCION Data, int vOPCION_CONSULTA);
	}
}