using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Security;
using mmspace.api.Data.Parameters.Security;

namespace mmspace.api.Data.Repositories.Security
{
	public interface ISEG_MENU_SISTEMARepository
	{
		Task<CResult> Save(SEG_MENU_SISTEMA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_MENU, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_SEG_MENU_SISTEMA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_MENU);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_MENU);
		Task<List<V_SEG_MENU_SISTEMA>> GetAll(DATA_SEG_MENU_SISTEMA Data);
		Task<object> GetScalar(DATA_SEG_MENU_SISTEMA Data, int vOPCION_CONSULTA);
	}
}