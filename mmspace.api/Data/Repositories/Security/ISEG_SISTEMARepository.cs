using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Security;
using mmspace.api.Data.Parameters.Security;

namespace mmspace.api.Data.Repositories.Security
{
	public interface ISEG_SISTEMARepository
	{
		Task<CResult> Save(SEG_SISTEMA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION);
		Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_SISTEMA, String vLOGIN_SISTEMA, String vESTACION);
		Task<V_SEG_SISTEMA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_SISTEMA);
		Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_SISTEMA);
		Task<List<V_SEG_SISTEMA>> GetAll(DATA_SEG_SISTEMA Data);
		Task<object> GetScalar(DATA_SEG_SISTEMA Data, int vOPCION_CONSULTA);
	}
}