using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using mmspace.api.Data.Fx;
using mmspace.api.Data.Parameters.Security;
using mmspace.api.Models.Security;

namespace mmspace.api.Data.Repositories.Security
{
    public interface ISEG_USUARIORepository
    {
        Task<CResult> Save(SEG_USUARIO Data, UpdateType Tipo, string vLOGIN_SISTEMA, string vESTACION);
        Task<CResult> Delete(string LOGIN_SISTEMA, string vLOGIN_SISTEMA, string vESTACION);
        Task<V_SEG_USUARIO> Get(string LOGIN_SISTEMA);         
        Task<bool> Exists(string LOGIN_SISTEMA);
        Task<List<V_SEG_USUARIO>> GetAll(DATA_SEG_USUARIO Data);
        Task<object> GetScalar(DATA_SEG_USUARIO Data, int vOPCION_CONSULTA);
        Task<V_SEG_USUARIO> Login(string LOGIN_SISTEMA, string CLAVE_USUARIO);         
        string GenerateToken(V_SEG_USUARIO Usuario, List<V_SEG_USUARIO_OPCION> Opciones);
        string GenerateRptToken(string LOGIN_SISTEMA);
        Task<CResult> CambioClave(SEG_USUARIO_CAMBIO_CLAVE Data, string vLOGIN_SISTEMA, string vESTACION);
        Task<V_SEG_USUARIO> GetCompleto(String LOGIN_SISTEMA); 
        Task<List<V_SEG_USUARIO_MENU>> getMenu(string LOGIN_SISTEMA);
    }
}