using System.Collections.Generic;
using System.Threading.Tasks;
using mmspace.api.Data.Fx;
using mmspace.api.Data.Parameters.Security;
using mmspace.api.Models.Security;

namespace mmspace.api.Data.Repositories.Security
{
    public interface ISEG_USUARIO_OPCIONRepository
    {
        Task<CResult> Save(SEG_USUARIO_OPCION Data, UpdateType Tipo, string vLOGIN_SISTEMA, string vESTACION);
		Task<CResult> Delete(string vLOGIN_SISTEMA, int vCORR_SUSCRIPCION, int vCORR_CONFI_PAIS, string vCODIGO_SISTEMA, string vCODIGO_MENU, string vCODIGO_OPCION, string LOGIN_SISTEMA, string ESTACION);
		Task<V_SEG_USUARIO_OPCION> Get(string vLOGIN_SISTEMA, int vCORR_SUSCRIPCION, int vCORR_CONFI_PAIS, string vCODIGO_SISTEMA, string vCODIGO_MENU, string vCODIGO_OPCION);
		Task<bool> Exists(string vLOGIN_SISTEMA, int vCORR_SUSCRIPCION, int vCORR_CONFI_PAIS, string vCODIGO_SISTEMA, string vCODIGO_MENU, string vCODIGO_OPCION);
        Task<List<V_SEG_USUARIO_OPCION>> GetAll(DATA_SEG_USUARIO_OPCION Data);
        Task<object> GetScalar(DATA_SEG_USUARIO_OPCION Data, int vOPCION_CONSULTA);
    }
}