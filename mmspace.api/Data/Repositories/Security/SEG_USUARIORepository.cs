using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Security;
using mmspace.api.Data.Parameters.Security;
using System.Linq;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace mmspace.api.Data.Repositories.Security
{
    public class SEG_USUARIORepository: ISEG_USUARIORepository
    {
        private const string _TableName = "SEG_USUARIO";
        private CData _Data;
        private readonly IConfiguration _config;        
        public SEG_USUARIORepository(IConfiguration config)
        {            
            _Data = new CData(config.GetConnectionString("defaultConnection"),
                              config.GetSection("DbProvider:defaultProvider").Value);
            _config = config ?? throw new ArgumentNullException(nameof(_config));            
        }

        public async Task<CResult> Save(SEG_USUARIO Data, UpdateType Tipo, string vLOGIN_SISTEMA, string vESTACION)
        {
            CResult objResultado = new CResult();
            var p = new List<CParameter>();

            if (Tipo == UpdateType.Add ) {
                byte[] CLAVE_USUARIOHash, CLAVE_USUARIO_SAL;
                CreatePasswordHash(Data.LOGIN_SISTEMA, out CLAVE_USUARIOHash, out CLAVE_USUARIO_SAL);

                Data.CLAVE_USUARIO = CLAVE_USUARIOHash;
                Data.CLAVE_USUARIO_SAL = CLAVE_USUARIO_SAL;   
            }

            Data.FECHA_CREA = DateTime.Now;
            Data.FECHA_ACTU = DateTime.Now;
            p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
            p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=Data.LOGIN_SISTEMA,DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput});
            p.Add(new CParameter() {ParameterName="@NOMBRE_USUARIO",Value=Data.NOMBRE_USUARIO,DbType=System.Data.DbType.String});                       
            p.Add(new CParameter() {ParameterName="@CLAVE_USUARIO",Value=Data.CLAVE_USUARIO,DbType=System.Data.DbType.Binary});
            p.Add(new CParameter() {ParameterName="@CLAVE_USUARIO_SAL",Value=Data.CLAVE_USUARIO_SAL,DbType=System.Data.DbType.Binary});            
            p.Add(new CParameter() {ParameterName="@CORREO_ELECTRONICO",Value=Data.CORREO_ELECTRONICO,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@TIPO_USUARIO",Value=Data.TIPO_USUARIO,DbType=System.Data.DbType.Int32});
            p.Add(new CParameter() {ParameterName="@ESTADO_USUARIO",Value=Data.ESTADO_USUARIO,DbType=System.Data.DbType.Int32});            
            p.Add(new CParameter() {ParameterName="@IDIOMA",Value=Data.IDIOMA,DbType=System.Data.DbType.String});            
            p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value=Data.USUARIO_CREA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=Data.FECHA_CREA,DbType=System.Data.DbType.DateTime});
            p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value=Data.ESTACION_CREA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=Data.USUARIO_ACTU,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=Data.FECHA_ACTU,DbType=System.Data.DbType.DateTime});
            p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=Data.ESTACION_ACTU,DbType=System.Data.DbType.String});            
            
            //Parametros para gestionar la operación
            p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
            p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
            p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});

            await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);            
            
            objResultado.CodeHelper =  _Data.objCommand.Parameters["@LOGIN_SISTEMA"].Value;
            objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
            objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
		    objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
		    objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
            
            foreach (var deta in Data.DETALLE)
            {   deta.FECHA_CREA = DateTime.Now;
                deta.FECHA_ACTU = DateTime.Now;
                p.Clear();
                if (deta.SELECCION) {                     
                    p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=deta.MTTO, DbType=System.Data.DbType.Int32});
                } else {
                    p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete, DbType=System.Data.DbType.Int32});
                }                
                p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=Data.LOGIN_SISTEMA,DbType=System.Data.DbType.String});
                p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=deta.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
                p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=deta.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
                p.Add(new CParameter() {ParameterName="@CODIGO_SISTEMA",Value=deta.CODIGO_SISTEMA,DbType=System.Data.DbType.String});
                p.Add(new CParameter() {ParameterName="@CODIGO_MENU",Value=deta.CODIGO_MENU,DbType=System.Data.DbType.String});
                p.Add(new CParameter() {ParameterName="@CODIGO_OPCION",Value=deta.CODIGO_OPCION,DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput});
                p.Add(new CParameter() {ParameterName="@NUEVO",Value=deta.NUEVO,DbType=System.Data.DbType.Boolean});
                p.Add(new CParameter() {ParameterName="@MODIFICAR",Value=deta.MODIFICAR,DbType=System.Data.DbType.Boolean});
                p.Add(new CParameter() {ParameterName="@ELIMINAR",Value=deta.ELIMINAR,DbType=System.Data.DbType.Boolean});
                p.Add(new CParameter() {ParameterName="@IMPRIMIR",Value=deta.IMPRIMIR,DbType=System.Data.DbType.Boolean});
                p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value=Data.USUARIO_CREA,DbType=System.Data.DbType.String});
                p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=Data.FECHA_CREA,DbType=System.Data.DbType.DateTime});
                p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value=Data.ESTACION_CREA,DbType=System.Data.DbType.String});
                p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=Data.USUARIO_ACTU,DbType=System.Data.DbType.String});
                p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=Data.FECHA_ACTU,DbType=System.Data.DbType.DateTime});
                p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=Data.ESTACION_ACTU,DbType=System.Data.DbType.String});

                //Parametros para gestionar la operación
				p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
				p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
				p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
				p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
				p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
				
				await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_SEG_USUARIO_OPCION",true,p);	
            }
            
            return objResultado;
        }

        public async Task<CResult> Delete(string LOGIN_SISTEMA, string vLOGIN_SISTEMA, string vESTACION)
        {
            CResult objResultado = new CResult();
            var p = new List<CParameter>();
            p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});            
            p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=LOGIN_SISTEMA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@NOMBRE_USUARIO",Value="",DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@CLAVE_USUARIO",Value=null,DbType=System.Data.DbType.Binary});
            p.Add(new CParameter() {ParameterName="@CLAVE_USUARIO_SAL",Value=null,DbType=System.Data.DbType.Binary});
            p.Add(new CParameter() {ParameterName="@CORREO_ELECTRONICO",Value="",DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@TIPO_USUARIO",Value=0,DbType=System.Data.DbType.Int32});
            p.Add(new CParameter() {ParameterName="@ESTADO_USUARIO",Value=0,DbType=System.Data.DbType.Int32});            
            p.Add(new CParameter() {ParameterName="@IDIOMA",Value="",DbType=System.Data.DbType.String});            
            p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value="",DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
            p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value="",DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value="",DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
            p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value="",DbType=System.Data.DbType.String});
           

            //Parametros para gestionar la operación
            p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
            p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
            p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});

            await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);            
            
            objResultado.CodeHelper =  _Data.objCommand.Parameters["@LOGIN_SISTEMA"].Value;
            objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
            objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
		    objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
		    objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";

            return objResultado;
        }

        public async Task<V_SEG_USUARIO> Get(String LOGIN_SISTEMA)
        { 
            var p = new List<CParameter>();
            p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});            
            p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=LOGIN_SISTEMA,DbType=System.Data.DbType.String});            
            p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});

            var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
            var response = new List<V_SEG_USUARIO>().FromDataReader(reader).FirstOrDefault();
            
            reader.Close();
            reader = null;
            _Data.objConnection.Close();

            return response;
        }

        public async Task<V_SEG_USUARIO> GetCompleto(string LOGIN_SISTEMA)
        { 
            var p = new List<CParameter>();
            p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
            p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=LOGIN_SISTEMA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=1,DbType=System.Data.DbType.Int32});

            var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
            var response = new List<V_SEG_USUARIO>().FromDataReader(reader).FirstOrDefault();
            
            reader.Close();
            reader = null;
            _Data.objConnection.Close();

            return response;
        }

        public async Task<bool> Exists(string LOGIN_SISTEMA)
        {
            bool existe;
            var response = new SEG_USUARIO();
            var p = new List<CParameter>();
            p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});            
            p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=LOGIN_SISTEMA,DbType=System.Data.DbType.String});            
            p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});

            var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);            
            
            if (reader.HasRows)      
                existe=true;
            else
                existe=false;

            reader.Close();
            reader = null;
            _Data.objConnection.Close();

            return existe;
        }

        public async Task<List<V_SEG_USUARIO>> GetAll(DATA_SEG_USUARIO Data)
        {            
            var p = new List<CParameter>();
            p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});
            p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=Data.LOGIN_SISTEMA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});

            var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
            var response = new List<V_SEG_USUARIO>().FromDataReader(reader).ToList();

            reader.Close();
            reader = null;
            _Data.objConnection.Close();

            return response;
        }

        public async Task<object> GetScalar(DATA_SEG_USUARIO Data, int vOPCION_CONSULTA)
        {           
            var p = new List<CParameter>();
            p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
            p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=Data.LOGIN_SISTEMA,DbType=System.Data.DbType.String});            
            p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});

            var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        

            return response;
        }

        private void CreatePasswordHash(string CLAVE_USUARIO, out byte[] CLAVE_USUARIOHash, out byte[] CLAVE_USUARIO_SAL)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())  
            {
                CLAVE_USUARIO_SAL = hmac.Key;
                CLAVE_USUARIOHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(CLAVE_USUARIO));
            }
        }

        public async Task<V_SEG_USUARIO> Login(string LOGIN_SISTEMA, string CLAVE_USUARIO)
        {
            var Data = await GetCompleto(LOGIN_SISTEMA);

            if (Data == null)
                return null;

            if (!VerifyPasswordHash(CLAVE_USUARIO, Data.CLAVE_USUARIO, Data.CLAVE_USUARIO_SAL))
                return null;

            return Data;
        }        

        private bool VerifyPasswordHash(string CLAVE_USUARIO, byte[] CLAVE_USUARIOHash, byte[] CLAVE_USUARIO_SAL)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(CLAVE_USUARIO_SAL))  
            {                
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(CLAVE_USUARIO));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != CLAVE_USUARIOHash[i]) return false;
                }

                return true;
            }
        }  
        
        public string GenerateToken(V_SEG_USUARIO Usuario, List<V_SEG_USUARIO_OPCION> Opciones)
        {
            var claims = new[]
            {                
                new Claim(ClaimTypes.NameIdentifier, Usuario.LOGIN_SISTEMA),
                new Claim(ClaimTypes.Name, Usuario.NOMBRE_USUARIO),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                // new Claim("URL_FOTO_PERFIL",Usuario.URL_FOTO_PERFIL),                
            };
            var appIdentity = new ClaimsIdentity(claims);
            
            foreach (var vOpcion in Opciones)
            {               
                if (vOpcion.URL_OPCION != null && vOpcion.PERMISO != null) {
                    appIdentity.AddClaim(new Claim(vOpcion.URL_OPCION,vOpcion.PERMISO));  
                }                
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(_config.GetSection("AppSetting:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);           

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = appIdentity,
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        public string GenerateRptToken(string username)
        {
            // appsetting for Token JWT
            var secretKey = _config.GetSection("AppSetting:TokenRpt").Value;
            var audienceToken = _config.GetSection("AppSetting:apiRptURL").Value; //ConfigurationManager.AppSettings["JWT_AUDIENCE_TOKEN"];
            var issuerToken = _config.GetSection("AppSetting:apiRptURL").Value;; //ConfigurationManager.AppSettings["JWT_ISSUER_TOKEN"];
            var expireTime = 30; //ConfigurationManager.AppSettings["JWT_EXPIRE_MINUTES"];

            var securityKey = new SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(secretKey));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            // create a claimsIdentity
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, username) });

            // create token to the user
            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
            var jwtSecurityToken = tokenHandler.CreateJwtSecurityToken(
                audience: audienceToken,
                issuer: issuerToken,
                subject: claimsIdentity,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(Convert.ToInt32(expireTime)),
                signingCredentials: signingCredentials);

            var jwtTokenString = tokenHandler.WriteToken(jwtSecurityToken);
            return jwtTokenString;
        }   
                
        public async Task<CResult> CambioClave(SEG_USUARIO_CAMBIO_CLAVE Data, string vLOGIN_SISTEMA, string vESTACION)
        {
            CResult objResultado = new CResult();
            var p = new List<CParameter>();            
            byte[] CLAVE_USUARIOHash, CLAVE_USUARIO_SAL;

            CreatePasswordHash(Data.CLAVE_USUARIO_NUEVA, out CLAVE_USUARIOHash, out CLAVE_USUARIO_SAL);
                       
            p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=Data.LOGIN_SISTEMA,DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput});            
            p.Add(new CParameter() {ParameterName="@CLAVE_USUARIO",Value=CLAVE_USUARIOHash,DbType=System.Data.DbType.Binary});
            p.Add(new CParameter() {ParameterName="@CLAVE_USUARIO_SAL",Value=CLAVE_USUARIO_SAL,DbType=System.Data.DbType.Binary});            
            p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=DateTime.Now,DbType=System.Data.DbType.DateTime});
            p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=vESTACION,DbType=System.Data.DbType.String});            

            //Parametros para gestionar la operación
            p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
            p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
            p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});

            await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName+"_CAMBIO_CLAVE",true,p);            
            
            objResultado.CodeHelper =  _Data.objCommand.Parameters["@LOGIN_SISTEMA"].Value;
            objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
            objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
		    objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
		    objResultado.ErrorSource = "C" + _TableName + ".CambioClave()";
            
            return objResultado;
        }

		public async Task<List<V_SEG_USUARIO_MENU>> getMenu(string LOGIN_SISTEMA)
        {
            var p = new List<CParameter>();
            p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.AllRowsAllColumns,DbType=System.Data.DbType.Int32});
            p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=LOGIN_SISTEMA,DbType=System.Data.DbType.String});
            p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=4,DbType=System.Data.DbType.Int32}); //@OPCION_CONSULTA = 5 (Sistemas)
            
            var readerSistema = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_SEG_USUARIO_OPCION",p);
            var responseSistema = new List<V_SEG_USUARIO_MENU>().FromDataReader(readerSistema).ToList();
            readerSistema.Close();
            readerSistema = null;

            //@OPCION_CONSULTA = 4 (Menus)
            p[2].Value=5;
            var readerMenu = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_SEG_USUARIO_OPCION",p);
            var responseMenu = new List<V_SEG_USUARIO_MENU_OPCION>().FromDataReader(readerMenu).ToList();
            readerMenu.Close();
            readerMenu = null;            

            //@OPCION_CONSULTA = 3 (Opciones)
            p[2].Value=3;
            var readerOpcion = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_SEG_USUARIO_OPCION",p);
            var responseOpcion = new List<V_SEG_USUARIO_MENU_OPCION>().FromDataReader(readerOpcion).ToList();
            readerOpcion.Close();
            readerOpcion = null;

            foreach (var sistema in responseSistema)
            {
                sistema.items = responseMenu.Where(x => x.codeSistema == sistema.codeSistema).ToList();

                foreach(var menu in sistema.items) 
                {
                    menu.items = responseOpcion.Where(x => x.codeSistema == menu.codeSistema && x.codeMenu == menu.codeMenu).ToList();
                }                
            }
            
            _Data.objConnection.Close();

            return responseSistema;
        }

    }
}