using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Security;
using mmspace.api.Data.Parameters.Security;

namespace mmspace.api.Data.Repositories.Security
{
    public class SEG_USUARIO_OPCIONRepository : ISEG_USUARIO_OPCIONRepository
    {
        private const string _TableName = "SEG_USUARIO_OPCION";
        private CData _Data;
		public SEG_USUARIO_OPCIONRepository(IConfiguration _config)
		{
			_Data = new CData(_config.GetConnectionString("defaultConnection"),
					  _config.GetSection("DbProvider:defaultProvider").Value);
		}

		public async Task<CResult> Save(SEG_USUARIO_OPCION Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=Data.LOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CODIGO_SISTEMA",Value=Data.CODIGO_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_MENU",Value=Data.CODIGO_MENU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_OPCION",Value=Data.CODIGO_OPCION,DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NUEVO",Value=Data.NUEVO,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@MODIFICAR",Value=Data.MODIFICAR,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@ELIMINAR",Value=Data.ELIMINAR,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@IMPRIMIR",Value=Data.IMPRIMIR,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value=Data.USUARIO_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=Data.FECHA_CREA,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value=Data.ESTACION_CREA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value=Data.USUARIO_ACTU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=Data.FECHA_ACTU,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value=Data.ESTACION_ACTU,DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CODIGO_OPCION"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
			
			return objResultado;
		}
		public async Task<CResult> Delete(String vLOGIN_SISTEMA, Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_SISTEMA, String vCODIGO_MENU, String vCODIGO_OPCION, String LOGIN_SISTEMA, String ESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CODIGO_SISTEMA",Value=vCODIGO_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_MENU",Value=vCODIGO_MENU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_OPCION",Value=vCODIGO_OPCION,DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@NUEVO",Value=0,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@MODIFICAR",Value=0,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@ELIMINAR",Value=0,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@IMPRIMIR",Value=0,DbType=System.Data.DbType.Boolean});
			p.Add(new CParameter() {ParameterName="@USUARIO_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_CREA",Value=null,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_CREA",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@USUARIO_ACTU",Value="",DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@FECHA_ACTU",Value=null,DbType=System.Data.DbType.DateTime});
			p.Add(new CParameter() {ParameterName="@ESTACION_ACTU",Value="",DbType=System.Data.DbType.String});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=LOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=ESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CODIGO_OPCION"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";
			
			return objResultado;
		}
		
		public async Task<V_SEG_USUARIO_OPCION> Get(String vLOGIN_SISTEMA, Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_SISTEMA, String vCODIGO_MENU, String vCODIGO_OPCION)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CODIGO_SISTEMA",Value=vCODIGO_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_MENU",Value=vCODIGO_MENU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_OPCION",Value=vCODIGO_OPCION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_SEG_USUARIO_OPCION>().FromDataReader(reader).FirstOrDefault();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<bool> Exists(String vLOGIN_SISTEMA, Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, String vCODIGO_SISTEMA, String vCODIGO_MENU, String vCODIGO_OPCION)
		{
			Boolean existe;
			var response = new V_SEG_USUARIO_OPCION();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CODIGO_SISTEMA",Value=vCODIGO_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_MENU",Value=vCODIGO_MENU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_OPCION",Value=vCODIGO_OPCION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			
			if (reader.HasRows)
				existe=true;
			else
				existe=false;
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return existe;
		}
        public async Task<List<V_SEG_USUARIO_OPCION>> GetAll(DATA_SEG_USUARIO_OPCION Data)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});			
			p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=Data.LOGIN_SISTEMA,DbType=System.Data.DbType.String});			
			p.Add(new CParameter() {ParameterName="@CODIGO_SISTEMA",Value=Data.CODIGO_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_MENU",Value=Data.CODIGO_MENU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_OPCION",Value=Data.CODIGO_OPCION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_SUITE",Value=Data.CODIGO_SUITE,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_SEG_USUARIO_OPCION>().FromDataReader(reader).ToList();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}

		public async Task<Object> GetScalar(DATA_SEG_USUARIO_OPCION Data, int vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@LOGIN_SISTEMA",Value=Data.LOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CODIGO_SISTEMA",Value=Data.CODIGO_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_MENU",Value=Data.CODIGO_MENU,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_OPCION",Value=Data.CODIGO_OPCION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@CODIGO_SUITE",Value=Data.CODIGO_SUITE,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        
			
			return response;
		}
        
    }
}