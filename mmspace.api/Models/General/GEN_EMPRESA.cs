using System;

namespace mmspace.api.Models.General
{
	public class GEN_EMPRESA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public String NOMBRE_EMPRESA { get; set; } 
		public String NOMBRE_COMERCIAL { get; set; } 
		public String GIRO_EMPRESA { get; set; } 
		public String DIRECCION_EMPRESA { get; set; } 
		public String NUMERO_NIT { get; set; } 
		public String NUMERO_NRC { get; set; } 
		public String NOMBRE_CONTACTO { get; set; } 
		public String TELEFONO_1 { get; set; } 
		public String TELEFONO_2 { get; set; } 
		public String FAX { get; set; } 
		public String CORREO_ELECTRONICO { get; set; } 
		public Byte[] LOGO_1 { get; set; } 
		public Byte[] LOGO_2 { get; set; } 
		public String TAMANO_EMPRESA { get; set; } 
		public String NATURAL_JURIDICO { get; set; } 
		public String CODIGO_SUSCRIPCION { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
		public Int32 CORR_GRUPO { get; set; } 
		public String CODIGO_EMPRESA { get; set; } 
		public Int32 CORR_MONEDA { get; set; } 
		public Int32 CORR_PAIS { get; set; } 
		public Int32 CORR_DEPTO { get; set; } 
		public Int32 CORR_MUNICIPIO { get; set; } 
		public String DIRECCION_EMPRESA_LARGO { get; set; } 
		public String NOMBRE_EMPRESA_LARGO { get; set; } 
		public Byte[] SELLO { get; set; } 
		public String CODIGO_POSTAL { get; set; } 
	}
}