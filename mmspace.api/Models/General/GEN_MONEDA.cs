using System;

namespace mmspace.api.Models.General
{
	public class GEN_MONEDA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_MONEDA { get; set; } 
		public String NOMBRE_MONEDA { get; set; } 
		public String SIMBOLO { get; set; } 
		public String DESCRIPCION_MONEDA { get; set; } 
	}
}