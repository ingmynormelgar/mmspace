using System;

namespace mmspace.api.Models.AssetManagement
{
	public class GEN_MUNICIPIO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_PAIS { get; set; } 
		public Int32 CORR_DEPTO { get; set; } 
		public Int32 CORR_MUNICIPIO { get; set; } 
		public String NOMBRE_MUNICIPIO { get; set; } 
		public String CODIGO_MUNICIPIO { get; set; } 
	}
}