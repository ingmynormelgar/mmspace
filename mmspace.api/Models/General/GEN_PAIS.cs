using System;

namespace mmspace.api.Models.AssetManagement
{
	public class GEN_PAIS
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_PAIS { get; set; } 
		public String NOMBRE_PAIS { get; set; } 
		public String CODIGO_PAIS { get; set; } 
		public String NACIONALIDAD { get; set; } 
	}
}