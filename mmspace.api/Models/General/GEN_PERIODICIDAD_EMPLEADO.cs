using System;

namespace mmspace.api.Models.General
{
    public class GEN_PERIODICIDAD_EMPLEADO
    {
        public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_PERIODICIDAD { get; set; } 
        public Int32 NUMERO_CUOTAS { get; set; } 

    }
}