using System;

namespace mmspace.api.Models.General
{
	public class GEN_TIPO_MONEDA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_MONEDA { get; set; } 
		public String NOMBRE_MONEDA { get; set; } 
		public String SIMBOLO { get; set; } 
		public String DESCRIPCION_MONEDA { get; set; } 
		public String NOMBRE_CORTO { get; set; } 
		public String CODIGO_MONEDA { get; set; } 
	}
}