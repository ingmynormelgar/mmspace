using System;

namespace mmspace.api.Models.AssetManagement
{
	public class V_GEN_DEPTO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_PAIS { get; set; } 
		public String NOMBRE_PAIS { get; set; } 
		public Int32 CORR_DEPTO { get; set; } 
		public String NOMBRE_DEPTO { get; set; } 
		public String CODIGO_DEPTO { get; set; } 
	}
}