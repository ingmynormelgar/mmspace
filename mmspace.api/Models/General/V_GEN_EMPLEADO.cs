using System;

namespace mmspace.api.Models.General
{
	public class V_GEN_EMPLEADO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public String NOMBRE_EMPLEADO { get; set; } 
		public String DUI { get; set; }
		// public DateTime FECHA_INGRESO { get; set; } 
		// public DateTime FECHA_ULTIMA_INDEMNIZACION { get; set; } 
		// public DateTime FECHA_ULTIMA_VACACION { get; set; } 
		// public DateTime FECHA_ULTIMO_AGUINALDO { get; set; }  
		// public DateTime FECHA_RETIRO { get; set; }
		// public Decimal SALARIO_MENSUAL { get; set; }
		// public Decimal SALARIO_DIARIO { get; set; } 
		// public Decimal SALARIO_HORA { get; set; } 
		// public Int32 CORR_DEPARTAMENTO { get; set; } 
		// public String NOMBRE_DEPARTAMENTO { get; set; } 
		// public Int32 CORR_PUESTO { get; set; } 
		// public String NOMBRE_PUESTO { get; set; } 
		// public Int32 CORR_EMPLEADO_JEFE { get; set; } 
		// public String NOMBRE_EMPLEADO_JEFE { get; set; } 
		// public String LOGIN_SISTEMA { get; set; }
		// public Int32 CORR_CENTRO_COSTO { get; set; } 
		// public String NOMBRE_CENTRO { get; set; } 
	}
}