using System;

namespace mmspace.api.Models.General
{
	public class V_GEN_LISTA_DETA
	{
		public Int32 CORR_LISTA { get; set; } 
		public Object CODIGO { get; set; } 
		public String DESCRIPCION { get; set; } 
	}
}