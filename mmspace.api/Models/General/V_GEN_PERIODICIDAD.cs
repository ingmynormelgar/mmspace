using System;

namespace mmspace.api.Models.General
{
	public class V_GEN_PERIODICIDAD
	{
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_PERIODICIDAD { get; set; } 
		public String NOMBRE_PERIODICIDAD { get; set; } 
		public Int32 CUOTAS_ANIO { get; set; } 
	}
}