using System;

namespace mmspace.api.Models.General
{
	public class V_GEN_TIPO_DIP
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_TIPO_DIP { get; set; } 
		public String NOMBRE_TIPO_DIP { get; set; } 
		public Int32 LONGITUD { get; set; }
	}
}