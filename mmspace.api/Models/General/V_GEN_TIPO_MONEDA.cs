using System;

namespace mmspace.api.Models.General
{
	public class V_GEN_TIPO_MONEDA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_MONEDA { get; set; } 
		public String NOMBRE_MONEDA { get; set; } 
		public String SIMBOLO { get; set; } 
		public String DESCRIPCION_MONEDA { get; set; } 
		public String NOMBRE_CORTO { get; set; } 
	}
}