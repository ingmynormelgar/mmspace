using System;

namespace mmspace.api.Models.Payroll
{
	public class PLA_MOTIVO_PERMISO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 7;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 7;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_MOTIVO_PERMISO { get; set; } 
		public String NOMBRE_MOTIVO_PERMISO { get; set; } 
		public Boolean CON_GOCE_SALARIAL { get; set; } 
		public String CLASE_PERMISO { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
	}
}