using System;
using mmspace.api.Data.Fx;

namespace mmspace.api.Models.Payroll
{
	public class PLA_PLAN_VACACION_EMPLEADO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 ANIO_PERIODO { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public Int32 CORR_DEPARTAMENTO { get; set; } 
		public Int32 CORR_CENTRO_COSTO { get; set; } 
		public DateTime FECHA_INGRESO_EMPLEADO { get; set; } 
		public Decimal DIAS_SALDO { get; set; } 
		public Decimal DIAS_PROG_1 { get; set; } 
		public Decimal DIAS_PROG_2 { get; set; } 
		public Decimal DIAS_PROG_3 { get; set; } 
		public Decimal DIAS_PROG_4 { get; set; } 
		public Decimal DIAS_PROG_5 { get; set; } 
		public Decimal DIAS_PROG_6 { get; set; } 
		public Decimal DIAS_PROG_7 { get; set; } 
		public Decimal DIAS_PROG_8 { get; set; } 
		public Decimal DIAS_PROG_9 { get; set; } 
		public Decimal DIAS_PROG_10 { get; set; } 
		public Decimal DIAS_PROG_11 { get; set; } 
		public Decimal DIAS_PROG_12 { get; set; } 
		public Decimal DIAS_ACUM_1 { get; set; } 
		public Decimal DIAS_ACUM_2 { get; set; } 
		public Decimal DIAS_ACUM_3 { get; set; } 
		public Decimal DIAS_ACUM_4 { get; set; } 
		public Decimal DIAS_ACUM_5 { get; set; } 
		public Decimal DIAS_ACUM_6 { get; set; } 
		public Decimal DIAS_ACUM_7 { get; set; } 
		public Decimal DIAS_ACUM_8 { get; set; } 
		public Decimal DIAS_ACUM_9 { get; set; } 
		public Decimal DIAS_ACUM_10 { get; set; } 
		public Decimal DIAS_ACUM_11 { get; set; } 
		public Decimal DIAS_ACUM_12 { get; set; } 
		public Decimal DIAS_DESC_1 { get; set; } 
		public Decimal DIAS_DESC_2 { get; set; } 
		public Decimal DIAS_DESC_3 { get; set; } 
		public Decimal DIAS_DESC_4 { get; set; } 
		public Decimal DIAS_DESC_5 { get; set; } 
		public Decimal DIAS_DESC_6 { get; set; } 
		public Decimal DIAS_DESC_7 { get; set; } 
		public Decimal DIAS_DESC_8 { get; set; } 
		public Decimal DIAS_DESC_9 { get; set; } 
		public Decimal DIAS_DESC_10 { get; set; } 
		public Decimal DIAS_DESC_11 { get; set; } 
		public Decimal DIAS_DESC_12 { get; set; } 
		public Decimal DIAS_PAGO { get; set; } 
		public String ESTADO_PLAN { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
		public UpdateType MTTO { get; set; } = UpdateType.Browse;
	}
}