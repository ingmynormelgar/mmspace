using System;

namespace mmspace.api.Models.Payroll
{
	public class PLA_RUBRO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_RUBRO { get; set; } 
		public String NOMBRE_RUBRO { get; set; } 
		public Int32 INGRESO_DEDUCCION { get; set; } 
		public String CLASE_RUBRO { get; set; } 
		public Boolean ES_IMPUESTO { get; set; } 
		public String CUENTA_CONTABLE { get; set; } 
		public Boolean MOSTRAR_IMPRESION { get; set; } 
		public Int32 ORDEN_RUBRO { get; set; } 
		public Decimal POR_RUBRO { get; set; } 
		public String CLASE_TIPO_FACTOR { get; set; } 
		public Int32 CORR_RUBRO_ADELANTO { get; set; } 
		public Boolean INCLUYE_CALC_SUELDO_PROMEDIO { get; set; } 
		public String BASE_PROVISION { get; set; } 
	}
}