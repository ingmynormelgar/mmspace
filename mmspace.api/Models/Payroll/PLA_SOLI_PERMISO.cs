using System;
using System.Collections.Generic;

namespace mmspace.api.Models.Payroll
{
	public class PLA_SOLI_PERMISO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public Int32 CORR_PERMISO { get; set; } 
		public DateTime FECHA_SOLICITUD { get; set; } 
		public Int32 CORR_MOTIVO_PERMISO { get; set; } 
		public Boolean CON_GOCE_SALARIAL { get; set; } 
		public String USUARIO_SOLICITA { get; set; } 
		public String USUARIO_AUTORIZA { get; set; } 
		public String ESTADO_PERMISO { get; set; } 
		public String OBSERVACIONES { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
		public List<PLA_SOLI_PERMISO_DETA> SOLI_PERMISO_DETA { get; set; }
	}
}