using System;

namespace mmspace.api.Models.Payroll
{
	public class V_PLA_PARAMETRO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Boolean DESCUENTA_INCAPACIDAD_DE_SALARIO { get; set; } 
		public String NOMBRE_REPORTE_PLANILLA { get; set; } 
		public String NOMBRE_REPORTE_RECIBO { get; set; } 
		public String PERIODO_MOSTRAR { get; set; } 
		public String CORREO_REMITENTE_RECIBO_PAGO { get; set; } 
		public String CONTRASENA_REMITENTE_RECIBO_PAGO { get; set; } 
		public String SERVIDOR_CORREO_RECIBO_PAGO { get; set; } 
		public Int32 PUERTO_CORREO_RECIBO_PAGO { get; set; } 
		public Boolean USA_SSL_CORREO_RECIBO_PAGO { get; set; } 
		public String CORREO_USUARIO_RECIBO_PAGO { get; set; } 
		public Boolean IMPRESION_PLANILLA_PIVOTE { get; set; } 
		public Int32 CORR_PERIODICIDAD_DEFAULT { get; set; } 
		public Boolean AUTOCALCULA_FECHA_PLANILLA { get; set; } 
		public String EXTENSION_ARCHIVO_BANCO_AGRICOLA { get; set; } 
	}
}