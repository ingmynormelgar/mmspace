using System;

namespace mmspace.api.Models.Payroll
{
	public class V_PLA_SOLICITUD_VACACION
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_SOLI_VACACION { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public String NOMBRE_EMPLEADO { get; set; } 
		public DateTime FECHA_SOLICITUD { get; set; } 
		public DateTime FECHA_INICIA_VACACION { get; set; } 
		public DateTime FECHA_FINAL_VACACION { get; set; } 
		public DateTime FECHA_INICIA_LABORES { get; set; } 
		public Int32 DIAS_VACACION { get; set; } 
		public Int32 DIAS_GOZAR { get; set; } 
		public Int32 DIAS_PAGO { get; set; } 
		public String OBSERVACIONES { get; set; } 
		public DateTime FECHA_APROBADA { get; set; } 
		public String ESTADO_SOLICITUD { get; set; } 
		public String NOMBRE_ESTADO_SOLICITUD { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
		public Int32 CORR_MONEDA { get; set; } 
		public String NOMBRE_MONEDA { get; set; } 
		public String SIMBOLO { get; set; } 
		public DateTime FECHA_CALCULO_VACACION { get; set; } 
	}
}