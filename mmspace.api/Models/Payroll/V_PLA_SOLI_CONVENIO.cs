using System;

namespace mmspace.api.Models.Payroll
{
	public class V_PLA_SOLI_CONVENIO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public String NOMBRE_EMPLEADO { get; set; } 
		public Int32 CORR_CONVENIO { get; set; } 
		public DateTime FECHA_SOLICITUD { get; set; } 
		public Int32 CORR_RUBRO_CONVENIO { get; set; } 
		public String NOMBRE_RUBRO { get; set; } 
		public Decimal MONTO_CREDITO { get; set; } 
		public String USUARIO_SOLICITA { get; set; } 
		public String USUARIO_AUTORIZA { get; set; } 
		public DateTime FECHA_ENTREGA { get; set; } 
		public DateTime FECHA_VENCIMIENTO { get; set; } 
		public Int32 CORR_MONEDA { get; set; } 
		public String NOMBRE_MONEDA { get; set; } 
		public String ESTADO_CONVENIO { get; set; } 
		public String NOMBRE_ESTADO_CONVENIO { get; set; } 
		public String OBSERVACIONES { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
		public Int32 CORR_RUBRO_FIJO { get; set; } 
		public String CLASE_CONVENIO { get; set; } 
		public String NOMBRE_CLASE_CONVENIO { get; set; } 
		public String NOMBRE_BENEFICIARIO { get; set; } 
		public String NUMERO_IDENTIFICACION_BENEFICIARIO { get; set; } 
		public Int32 CORR_PERIODICIDAD { get; set; }
		public String NOMBRE_PERIODICIDAD { get; set; } 
		public Decimal MONTO_CUOTA { get; set; } 
		public Int32 NUMERO_CUOTAS { get; set; } 
		public DateTime FECHA_INICIO { get; set; } 
		public Decimal MONTO_RUBRO_TOTAL { get; set; } 

	}
}