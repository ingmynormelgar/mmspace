using System;
using System.Collections.Generic;

namespace mmspace.api.Models.Payroll
{
	public class V_PLA_SOLI_HORA_EXTRA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public String NOMBRE_EMPLEADO { get; set; } 
		public Int32 CORR_SOLICITUD { get; set; } 
		public DateTime FECHA_SOLICITUD { get; set; } 
		public String USUARIO_AUTORIZA { get; set; } 
		public String ESTADO_SOLICITUD { get; set; } 
		public String NOMBRE_ESTADO_SOLICITUD { get; set; } 
		public String OBSERVACIONES { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
		public Int32 CORR_MONEDA { get; set; } 
		public String NOMBRE_MONEDA { get; set; } 
		public String SIMBOLO { get; set; } 
		public List<PLA_SOLI_HORA_EXTRA_DETA> SOLI_HORA_EXTRA_DETA { get; set; }
	}
}