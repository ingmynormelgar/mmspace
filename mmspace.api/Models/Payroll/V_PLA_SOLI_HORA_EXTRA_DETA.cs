using System;
using mmspace.api.Data.Fx;

namespace mmspace.api.Models.Payroll
{
	public class V_PLA_SOLI_HORA_EXTRA_DETA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public Int32 CORR_SOLICITUD { get; set; } 
		public Int32 CORR_SOLICITUD_DETA { get; set; } 
		public Int32 CORR_RUBRO { get; set; } 
		public String NOMBRE_RUBRO { get; set; } 
		public DateTime FECHA_INICIAL_SOLI { get; set; } 
		public DateTime FECHA_INICIAL_SOLI_H { get; set; } 
		public DateTime FECHA_FINAL_SOLI { get; set; } 
		public Int32 TOTAL_HORAS_SOLI { get; set; } 
		public Int32 TOTAL_MINUTOS_SOLI { get; set; } 
		public DateTime FECHA_INICIAL_AUT { get; set; } 
		public DateTime FECHA_INICIAL_AUT_H { get; set; } 
		public DateTime FECHA_FINAL_AUT { get; set; } 
		public Int32 TOTAL_HORAS_AUT { get; set; } 
		public Int32 TOTAL_MINUTOS_AUT { get; set; } 
		public DateTime FECHA_INICIAL { get; set; } 
		public DateTime FECHA_INICIAL_H { get; set; } 
		public DateTime FECHA_FINAL { get; set; } 
		public Int32 TOTAL_HORAS { get; set; } 
		public Int32 TOTAL_MINUTOS { get; set; } 
		public Int32 ANIO_PERIODO { get; set; } 
		public Int32 MES_PERIODO { get; set; } 
		public Int32 CORR_RUBRO_MES { get; set; } 
		public UpdateType MTTO { get; set; } = UpdateType.Browse;
	}
}