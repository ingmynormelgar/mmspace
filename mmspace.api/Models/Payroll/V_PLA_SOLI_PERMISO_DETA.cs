using System;
using mmspace.api.Data.Fx;

namespace mmspace.api.Models.Payroll
{
	public class V_PLA_SOLI_PERMISO_DETA
	{
		public Int32 CORR_SUSCRIPCION { get; set; } 
		public Int32 CORR_CONFI_PAIS { get; set; }
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public Int32 CORR_PERMISO { get; set; } 
		public Int32 CORR_PERMISO_DETA { get; set; } 
		public DateTime FECHA_INICIAL { get; set; } 
		public DateTime FECHA_INICIAL_H { get; set; } 
		public DateTime FECHA_FINAL { get; set; } 
		public Decimal TOTAL_DIAS { get; set; } 
		public Decimal TOTAL_HORAS { get; set; } 
		public Decimal TOTAL_MIN { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
		public Int32 CORR_ACCION { get; set; } 
		public String CLASE_SOLICITUD_PERMISO_DETA { get; set; } 
		public String NOMBRE_CLASE_SOLICITUD_PERMISO_DETA { get; set; } 
		public Boolean VISIBLE_DIA { get; set; }
		public UpdateType MTTO { get; set; } = UpdateType.Browse;
	}
}