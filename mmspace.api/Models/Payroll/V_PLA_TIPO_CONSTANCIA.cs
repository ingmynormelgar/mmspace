using System;

namespace mmspace.api.Models.Payroll
{
	public class V_PLA_TIPO_CONSTANCIA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_TIPO_CONSTANCIA { get; set; } 
		public String NOMBRE_TIPO_CONSTANCIA { get; set; } 
		public String CLASE_CONSTANCIA { get; set; } 
		public String NOMBRE_CLASE_CONSTANCIA { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
	}
}