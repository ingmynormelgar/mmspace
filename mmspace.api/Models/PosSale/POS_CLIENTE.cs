using System;

namespace mmspace.api.Models.Possale
{
	public class POS_CLIENTE
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_CLIENTE { get; set; } 
		public String NOMBRE_CLIENTE { get; set; } 
		public String NRC { get; set; } 
	}
}