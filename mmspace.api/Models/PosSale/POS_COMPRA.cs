using System;

namespace mmspace.api.Models.Possale
{
	public class POS_COMPRA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_COMPRA { get; set; } 
		public Int32 CORR_PROVEEDOR { get; set; } 
		public Int32 FECHA_COMPRA { get; set; } 
		public String ESTADO_COMPRA { get; set; } 
	}
}