using System;

namespace mmspace.api.Models.Possale
{
	public class POS_EMPLEADO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public String PRIMER_NOMBRE { get; set; } 
		public String SEGUNDO_NOMBRE { get; set; } 
		public String PRIMER_APELLIDO { get; set; } 
		public String SEGUNDO_APELLIDO { get; set; } 
		public String DUI { get; set; } 
		public Int32 TELEFONO { get; set; } 
		public String CARGO { get; set; } 
	}
}