using System;

namespace mmspace.api.Models.Possale
{
	public class POS_INVENTARIO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 PERIODO_ANIO { get; set; } 
		public Int32 PERIODO_MES { get; set; } 
		public Int32 CORR_INVENTARIO { get; set; } 
		public Int32 CORR_BODEGA { get; set; } 
	}
}