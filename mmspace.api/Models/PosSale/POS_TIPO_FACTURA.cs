using System;

namespace mmspace.api.Models.Possale
{
	public class POS_TIPO_FACTURA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_TIPO_FACTURA { get; set; } 
		public String NOMBRE_TIPO_FACTURA { get; set; } 
		public Int32 NUMERO_ULT_RECIBO { get; set; } 
	}
}