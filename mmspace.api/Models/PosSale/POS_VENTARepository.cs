using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;
using mmspace.api.Data.Fx;
using mmspace.api.Models.Possale;
using mmspace.api.Data.Parameters.Possale;

namespace mmspace.api.Data.Repositories.Possale
{
	public class POS_VENTARepository : IPOS_VENTARepository
	{
		private const string _TableName = "POS_VENTA";
		private CData _Data;
		public POS_VENTARepository(IConfiguration _config)
		{
			_Data = new CData(_config.GetConnectionString("defaultConnection"),
					  _config.GetSection("DbProvider:defaultProvider").Value);
		}
		
		public async Task<CResult> Save(POS_VENTA Data, UpdateType Tipo, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=Tipo,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_ANIO",Value=Data.PERIODO_ANIO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_MES",Value=Data.PERIODO_MES,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_VENTA",Value=Data.CORR_VENTA,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=Data.CORR_EMPLEADO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_FORMA_PAGO",Value=Data.CORR_FORMA_PAGO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CLIENTE",Value=Data.CORR_CLIENTE,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_TIPO_FACTURA",Value=Data.CORR_TIPO_FACTURA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@NUMERO_RECIBO",Value=Data.NUMERO_RECIBO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@TOTAL_VENTA",Value=Data.TOTAL_VENTA,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@TOTAL_DESCUENTO",Value=Data.TOTAL_DESCUENTO,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@TOTAL_IVA",Value=Data.TOTAL_IVA,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@EXENTO_IVA",Value=Data.EXENTO_IVA,DbType=System.Data.DbType.Boolean});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_VENTA"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Save(" + Tipo.ToString() + ")";
			
			return objResultado;
		}
		public async Task<CResult> Delete(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vPERIODO_ANIO, Int32 vPERIODO_MES, Int32 vCORR_VENTA, String vLOGIN_SISTEMA, String vESTACION)
		{
			CResult objResultado = new CResult();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_ACTUALIZA",Value=UpdateType.Delete,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_ANIO",Value=vPERIODO_ANIO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_MES",Value=vPERIODO_MES,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_VENTA",Value=vCORR_VENTA,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@CORR_EMPLEADO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_FORMA_PAGO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CLIENTE",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_TIPO_FACTURA",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@NUMERO_RECIBO",Value=0,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@TOTAL_VENTA",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@TOTAL_DESCUENTO",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@TOTAL_IVA",Value=0,DbType=System.Data.DbType.Decimal});
			p.Add(new CParameter() {ParameterName="@EXENTO_IVA",Value=0,DbType=System.Data.DbType.Boolean});
			
			//Parametros para gestionar la operación
			p.Add(new CParameter() {ParameterName="@SYS_LOGIN_USUARIO",Value=vLOGIN_SISTEMA,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_ESTACION",Value=vESTACION,DbType=System.Data.DbType.String});
			p.Add(new CParameter() {ParameterName="@SYS_FILAS_AFECTADAS",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_NUMERO_ERROR",Value=0,DbType=System.Data.DbType.Int32,Direction=System.Data.ParameterDirection.InputOutput});
			p.Add(new CParameter() {ParameterName="@SYS_MENSAJE_ERROR",Value="",DbType=System.Data.DbType.String,Direction=System.Data.ParameterDirection.InputOutput,Size=4000});
			
			await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_MTTO_"+_TableName,true,p);
			
			objResultado.CodeHelper =  _Data.objCommand.Parameters["@CORR_VENTA"].Value;
			objResultado.RowsAffected = (Int32) _Data.objCommand.Parameters["@SYS_FILAS_AFECTADAS"].Value;
			objResultado.ErrorCode =  (Int32) _Data.objCommand.Parameters["@SYS_NUMERO_ERROR"].Value;
			objResultado.ErrorMessage = (String) _Data.objCommand.Parameters["@SYS_MENSAJE_ERROR"].Value;
			objResultado.ErrorSource = "C" + _TableName + ".Delete(" + UpdateType.Delete.ToString() + ")";
			
			return objResultado;
		}
		
		public async Task<V_POS_VENTA> Get(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vPERIODO_ANIO, Int32 vPERIODO_MES, Int32 vCORR_VENTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=3,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_ANIO",Value=vPERIODO_ANIO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_MES",Value=vPERIODO_MES,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_VENTA",Value=vCORR_VENTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_POS_VENTA>().FromDataReader(reader).FirstOrDefault();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<bool> Exists(Int32 vCORR_SUSCRIPCION, Int32 vCORR_CONFI_PAIS, Int32 vCORR_EMPRESA, Int32 vPERIODO_ANIO, Int32 vPERIODO_MES, Int32 vCORR_VENTA)
		{
			Boolean existe;
			var response = new V_POS_VENTA();
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRow,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=vCORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=vCORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=vCORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_ANIO",Value=vPERIODO_ANIO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_MES",Value=vPERIODO_MES,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_VENTA",Value=vCORR_VENTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=0,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			
			if (reader.HasRows)
				existe=true;
			else
				existe=false;
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return existe;
		}
		public async Task<List<V_POS_VENTA>> GetAll(DATA_POS_VENTA Data)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=Data.TIPO_CONSULTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_ANIO",Value=Data.PERIODO_ANIO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_MES",Value=Data.PERIODO_MES,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_VENTA",Value=Data.CORR_VENTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=Data.OPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var reader = await _Data.GetDataReader(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,p);
			var response = new List<V_POS_VENTA>().FromDataReader(reader).ToList();
			
			reader.Close();
			reader = null;
			_Data.objConnection.Close();
			
			return response;
		}
		
		public async Task<Object> GetScalar(DATA_POS_VENTA Data, int vOPCION_CONSULTA)
		{
			var p = new List<CParameter>();
			p.Add(new CParameter() {ParameterName="@TIPO_CONSULTA",Value=QueryType.OneRowOneColumn,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_SUSCRIPCION",Value=Data.CORR_SUSCRIPCION,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_CONFI_PAIS",Value=Data.CORR_CONFI_PAIS,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_EMPRESA",Value=Data.CORR_EMPRESA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_ANIO",Value=Data.PERIODO_ANIO,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@PERIODO_MES",Value=Data.PERIODO_MES,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@CORR_VENTA",Value=Data.CORR_VENTA,DbType=System.Data.DbType.Int32});
			p.Add(new CParameter() {ParameterName="@OPCION_CONSULTA",Value=vOPCION_CONSULTA,DbType=System.Data.DbType.Int32});
			
			var response = await _Data.ExecCmd(System.Data.CommandType.StoredProcedure,"PRAL_DATA_"+_TableName,false,p);        
			
			return response;
		}
	}
}