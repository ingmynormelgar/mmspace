using System;

namespace mmspace.api.Models.Possale
{
	public class V_POS_PRODUCTO
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_PRODUCTO { get; set; } 
		public Int32 CODIGO_PRODUCTO { get; set; } 
		public String NOMBRE_PRODUCTO { get; set; } 
		public Decimal PRECIO_VENTA { get; set; } 
	}
}