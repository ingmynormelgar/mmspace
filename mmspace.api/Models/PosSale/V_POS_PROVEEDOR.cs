using System;

namespace mmspace.api.Models.Possale
{
	public class V_POS_PROVEEDOR
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 CORR_PROVEEDOR { get; set; } 
		public String NOMBRE_PROVEEDOR { get; set; } 
		public String TELEFONO_PROVEEDOR { get; set; } 
		public String DIRECCION_PROVEEDOR { get; set; } 
	}
}