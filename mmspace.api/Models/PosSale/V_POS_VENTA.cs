using System;

namespace mmspace.api.Models.Possale
{
	public class V_POS_VENTA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 1;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 1;
		public Int32 CORR_EMPRESA { get; set; } 
		public Int32 PERIODO_ANIO { get; set; } 
		public Int32 PERIODO_MES { get; set; } 
		public Int32 CORR_VENTA { get; set; } 
		public Int32 CORR_EMPLEADO { get; set; } 
		public Int32 CORR_FORMA_PAGO { get; set; } 
		public Int32 CORR_CLIENTE { get; set; } 
		public Int32 CORR_TIPO_FACTURA { get; set; } 
		public Int32 NUMERO_RECIBO { get; set; } 
		public Decimal TOTAL_VENTA { get; set; } 
		public Decimal TOTAL_DESCUENTO { get; set; } 
		public Decimal TOTAL_IVA { get; set; } 
		public Boolean EXENTO_IVA { get; set; } 
	}
}