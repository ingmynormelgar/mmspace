using System;

namespace mmspace.api.Models.Security
{
	public class SEG_CONFIG_OPCION
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 7;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 7;
		public String CODIGO_SISTEMA { get; set; } 
		public String CODIGO_MENU { get; set; } 
		public String CODIGO_OPCION { get; set; } 
		public Int32 ORDEN_SISTEMA { get; set; } 
		public Int32 ORDEN_MENU { get; set; } 
		public Int32 ORDEN_OPCION { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
	}
}