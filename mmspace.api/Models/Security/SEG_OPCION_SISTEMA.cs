using System;

namespace mmspace.api.Models.Security
{
	public class SEG_OPCION_SISTEMA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 7;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 7;
		public String CODIGO_OPCION { get; set; } 
		public String NOMBRE_OPCION { get; set; } 
		public String URL_OPCION { get; set; } 
		public String IMAGEN_OPCION { get; set; } 
		public String USUARIO_CREA { get; set; } 
		public DateTime FECHA_CREA { get; set; } 
		public String ESTACION_CREA { get; set; } 
		public String USUARIO_ACTU { get; set; } 
		public DateTime FECHA_ACTU { get; set; } 
		public String ESTACION_ACTU { get; set; } 
	}
}