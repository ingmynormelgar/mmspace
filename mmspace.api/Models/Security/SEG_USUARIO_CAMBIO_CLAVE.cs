using System.ComponentModel.DataAnnotations;

namespace mmspace.api.Models.Security
{
    public class SEG_USUARIO_CAMBIO_CLAVE
    {
        [Required(ErrorMessage = "Debe especificar el Login")]
		[StringLength(30, MinimumLength = 4, ErrorMessage = "Debe especificar entre 4 y 30 caracteres")]
	    public string LOGIN_SISTEMA { get; set; }        
        public string CLAVE_USUARIO { get; set; }	
        [Required(ErrorMessage = "Debe especificar la nueva clave")]
        public string CLAVE_USUARIO_NUEVA { get; set; }	
    }
}