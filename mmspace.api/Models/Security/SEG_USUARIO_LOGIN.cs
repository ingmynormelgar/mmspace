using System;
using System.ComponentModel.DataAnnotations;

namespace mmspace.api.Models.Security
{
    public class SEG_USUARIO_LOGIN
    {
        [Required(ErrorMessage = "Debe especificar el Login")]
		[StringLength(30, MinimumLength = 4, ErrorMessage = "Debe especificar entre 4 y 30 caracteres")]	            
        public string LOGIN_SISTEMA { get; set; }
        [Required(ErrorMessage = "Debe especificar una clave")]
        public string CLAVE_USUARIO { get; set; }	

    }
}