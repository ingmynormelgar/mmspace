using System;

namespace mmspace.api.Models.Security
{
	public class V_SEG_MENU_SISTEMA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 7;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 7;
		public String CODIGO_MENU { get; set; } 
		public String NOMBRE_MENU { get; set; } 
		public String IMAGEN_MENU { get; set; } 
	}
}