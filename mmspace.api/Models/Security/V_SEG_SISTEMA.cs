using System;

namespace mmspace.api.Models.Security
{
	public class V_SEG_SISTEMA
	{
		public Int32 CORR_SUSCRIPCION { get; set; }  = 7;
		public Int32 CORR_CONFI_PAIS { get; set; }  = 7;
		public String CODIGO_SISTEMA { get; set; } 
		public String NOMBRE_SISTEMA { get; set; } 
		public String IMAGEN_SISTEMA { get; set; } 
		public String PREFIJO { get; set; } 
		public String NOMBRE_MODULO { get; set; } 
	}
}