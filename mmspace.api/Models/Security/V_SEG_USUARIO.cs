using System;
using System.Collections.Generic;

namespace mmspace.api.Models.Security
{
    public class V_SEG_USUARIO
    {        
	    public string LOGIN_SISTEMA { get; set; }		
        public string NOMBRE_USUARIO { get; set; }
		public byte[] CLAVE_USUARIO { get; set; }
	    public byte[] CLAVE_USUARIO_SAL { get; set; }	
        public string CORREO_ELECTRONICO { get; set; }
	    public int TIPO_USUARIO { get; set; }
        public string NOMBRE_TIPO_USUARIO { get; set; }
	    public int ESTADO_USUARIO { get; set; }
        public string NOMBRE_ESTADO_USUARIO { get; set; }
	    public string IDIOMA { get; set; }		
	    public string USUARIO_CREA { get; set; }
	    public DateTime FECHA_CREA { get; set; }
	    public string ESTACION_CREA { get; set; }
	    public string USUARIO_ACTU { get; set; }
	    public DateTime FECHA_ACTU { get; set; }
	    public string ESTACION_ACTU { get; set; }
        public int CORR_EMPRESA { get; set; }
        public string NOMBRE_EMPRESA { get; set; }
        public int CORR_TIENDA { get; set; }
        public string NOMBRE_TIENDA { get; set; }	
		public Int32 CORR_EMPLEADO { get; set; }
		public String NOMBRE_EMPLEADO { get; set; }
		public byte[] FOTO_PERFIL { get; set; }

		public List<SEG_USUARIO_OPCION> DETALLE {get; set;}
    }
}