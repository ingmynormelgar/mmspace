using System.Collections.Generic;

namespace mmspace.api.Models.Security
{
    public class V_SEG_USUARIO_MENU_OPCION
    {
        public string codeSistema { get; set; }
        public string codeMenu { get; set; }
        public string code { get; set; }
        public string text { get; set; }
        public string path { get; set; }
        public string icon { get; set; }
        public bool expanded { get; set; } = true;
        public List<V_SEG_USUARIO_MENU_OPCION> items { get; set; }
    }
}