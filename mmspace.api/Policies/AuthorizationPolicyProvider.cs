using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace mmspace.api.Policies
{
    public class AuthorizationPolicyProvider : DefaultAuthorizationPolicyProvider
    {
        private readonly AuthorizationOptions _options;
        private readonly IConfiguration _configuration;

        public AuthorizationPolicyProvider(IOptions<AuthorizationOptions> options, IConfiguration configuration) : base(options)
        {
            _options = options.Value;
            _configuration = configuration;
        }

        public override async Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {

            // Check static policies first
            var policy = await base.GetPolicyAsync(policyName);

            if (policy == null)
            {
                string policyValue;
                if (policyName.Contains("|"))
                {
                    policyValue = policyName.Split("|")[1].ToString();
                }
                else
                {
                    policyValue = "R";
                }
                policy = new AuthorizationPolicyBuilder()
                    .AddRequirements(new HasScopeRequirement(policyName, policyValue))
                    .Build();

                // Add policy to the AuthorizationOptions, so we don't have to re-create it each time
                try {
                    _options.AddPolicy(policyName, policy);                
                }       
                catch (NullReferenceException ex) {
                    Console.Write(ex);
                }                
            }

            return policy;
        }
    }
}