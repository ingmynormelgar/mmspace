SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PRAL_DATA_GEN_LISTA_DETA]
(
	@TIPO_CONSULTA int,
	@CORR_LISTA int=NULL,
	@CODIGO varchar(10)=NULL,
	@OPCION_CONSULTA int=0
)
AS
BEGIN
	SET NOCOUNT ON

	IF @TIPO_CONSULTA=1 --Todos los campos, todos los registrosl (Mtto)
	BEGIN
		IF @OPCION_CONSULTA=0 --CODIGO Varchar
		BEGIN
			SELECT * FROM V_GEN_LISTA_DETA
			WHERE CORR_LISTA=@CORR_LISTA
		END ELSE
		IF @OPCION_CONSULTA=1 --CODIGO Int
		BEGIN
			SELECT A.CORR_LISTA
			,CONVERT(INT, A.CODIGO) CODIGO
			,A.DESCRIPCION
			FROM V_GEN_LISTA_DETA A
			WHERE CORR_LISTA=@CORR_LISTA
		END
	END ELSE
	IF @TIPO_CONSULTA=2 --Algunos campos, todos los registrosl (Combos)
	BEGIN
		IF @OPCION_CONSULTA = 0
		BEGIN
			SELECT * FROM V_GEN_LISTA_DETA
			WHERE CORR_LISTA=@CORR_LISTA
		END ELSE
		IF @OPCION_CONSULTA = 1
		BEGIN
			SELECT A.CORR_LISTA
			,CONVERT(INT, A.CODIGO) CODIGO
			,A.DESCRIPCION
			FROM V_GEN_LISTA_DETA A
			WHERE CORR_LISTA=@CORR_LISTA
		END
	END ELSE
	IF @TIPO_CONSULTA=3 --Todos los campos, un solo registro
	BEGIN
		SELECT * FROM V_GEN_LISTA_DETA
		WHERE CORR_LISTA=@CORR_LISTA
		AND CODIGO=@CODIGO
	END ELSE
	IF @TIPO_CONSULTA=4 --Un campo, un solo registro
	BEGIN
		IF @OPCION_CONSULTA=0
		BEGIN
			SELECT DESCRIPCION FROM V_GEN_LISTA_DETA
			WHERE CORR_LISTA=@CORR_LISTA
			AND CODIGO=@CODIGO
		END
	END

END
GO
