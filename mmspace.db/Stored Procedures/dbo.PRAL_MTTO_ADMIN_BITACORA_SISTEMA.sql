SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PRAL_MTTO_ADMIN_BITACORA_SISTEMA]
(
@TIPO_ACTUALIZA INT,
@CORR_SUSCRIPCION int,
@CORR_CONFI_PAIS int,
@CORR_BITACORA numeric(38,0) output,
@TIPO_BITACORA varchar(2),
@NUMERO_ERROR int,
@SEVERITY_ERROR int,
@ESTADO_ERROR int,
@ORIGEN_ERROR nvarchar(126),
@LINEA_ERROR int,
@MENSAJE_ERROR nvarchar(4000),
@USUARIO_CREA varchar(30),
@FECHA_CREA datetime,
@ESTACION_CREA varchar(50)
)
AS
BEGIN
	SET NOCOUNT ON
	
	IF @TIPO_ACTUALIZA=1 --Adicionar
	BEGIN
		INSERT INTO ADMIN_BITACORA_SISTEMA
		(CORR_SUSCRIPCION,CORR_CONFI_PAIS,TIPO_BITACORA,NUMERO_ERROR,SEVERITY_ERROR,ESTADO_ERROR,ORIGEN_ERROR,LINEA_ERROR,MENSAJE_ERROR,USUARIO_CREA,FECHA_CREA,ESTACION_CREA)
		VALUES
		(@CORR_SUSCRIPCION,@CORR_CONFI_PAIS,@TIPO_BITACORA,@NUMERO_ERROR,@SEVERITY_ERROR,@ESTADO_ERROR,@ORIGEN_ERROR,@LINEA_ERROR,@MENSAJE_ERROR,@USUARIO_CREA,@FECHA_CREA,@ESTACION_CREA)	
	
	END ELSE
	IF @TIPO_ACTUALIZA=2 --Adicionar
	BEGIN
		UPDATE ADMIN_BITACORA_SISTEMA
		SET TIPO_BITACORA=@TIPO_BITACORA,
		NUMERO_ERROR=@NUMERO_ERROR,
		SEVERITY_ERROR=@SEVERITY_ERROR,
		ESTADO_ERROR=@ESTADO_ERROR,
		ORIGEN_ERROR=@ORIGEN_ERROR,
		LINEA_ERROR=@LINEA_ERROR,
		MENSAJE_ERROR=@MENSAJE_ERROR
		--USUARIO_CREA=@USUARIO_CREA,
		--FECHA_CREA=@FECHA_CREA,
		--ESTACION_CREA=@ESTACION_CREA
		WHERE CORR_SUSCRIPCION=@CORR_SUSCRIPCION
		AND CORR_CONFI_PAIS=@CORR_CONFI_PAIS
		AND CORR_BITACORA=@CORR_BITACORA 
	END ELSE
	IF @TIPO_ACTUALIZA=3 --Adicionar
	BEGIN
		DELETE ADMIN_BITACORA_SISTEMA
		WHERE CORR_SUSCRIPCION=@CORR_SUSCRIPCION
		AND CORR_CONFI_PAIS=@CORR_CONFI_PAIS
		AND CORR_BITACORA=@CORR_BITACORA
	END
			
	SELECT @CORR_BITACORA= @@IDENTITY
END
RETURN 0
GO
