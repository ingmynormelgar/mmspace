SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PRAL_MTTO_GEN_EMPRESA]
(
	@TIPO_ACTUALIZA INT,
	@CORR_SUSCRIPCION int,
    @CORR_CONFI_PAIS int,
	@CORR_EMPRESA int OUTPUT,
	@NOMBRE_EMPRESA varchar(100),
	@NOMBRE_COMERCIAL varchar(100),
	@GIRO_EMPRESA varchar(255),
	@DIRECCION_EMPRESA varchar(100),
	@NUMERO_NIT varchar(30),
	@NUMERO_NRC varchar(30),
	@NOMBRE_CONTACTO varchar(100),
	@TELEFONO_1 varchar(30),
	@TELEFONO_2 varchar(30),
	@FAX varchar(30),
	@CORREO_ELECTRONICO varchar(100),
	@TAMANO_EMPRESA varchar(1),
	@NATURAL_JURIDICO varchar(1),
	@CODIGO_SUSCRIPCION varchar(50),
	@CORR_GRUPO int,
	@CODIGO_EMPRESA varchar(10),
	@USUARIO_CREA varchar(30),
	@FECHA_CREA datetime,
	@ESTACION_CREA varchar(50),
	@USUARIO_ACTU varchar(30),
	@FECHA_ACTU datetime,
	@ESTACION_ACTU varchar(50),
	@CORR_MONEDA int=NULL,
	@CORR_PAIS int=NULL,
	@CORR_DEPTO int=NULL,
	@CORR_MUNICIPIO int=NULL,
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) output
)
AS
BEGIN

	EXECUTE [MmSpace].dbo.PRAL_MTTO_GEN_EMPRESA 
		@TIPO_ACTUALIZA,                               
	    @CORR_SUSCRIPCION,                             
	    @CORR_CONFI_PAIS,                              
	    @CORR_EMPRESA  OUTPUT,              
	    @NOMBRE_EMPRESA,                              
	    @NOMBRE_COMERCIAL,                            
	    @GIRO_EMPRESA,                                
	    @DIRECCION_EMPRESA,                           
	    @NUMERO_NIT,                                  
	    @NUMERO_NRC,                                  
	    @NOMBRE_CONTACTO,                             
	    @TELEFONO_1,                                  
	    @TELEFONO_2,                                  
	    @FAX,                                         
	    @CORREO_ELECTRONICO,                          
	    @TAMANO_EMPRESA,                              
	    @NATURAL_JURIDICO,                            
	    @CODIGO_SUSCRIPCION,                          
	    @CORR_GRUPO,                                   
	    @CODIGO_EMPRESA,                              
	    @USUARIO_CREA,                                
	    @FECHA_CREA,               
	    @ESTACION_CREA,                               
	    @USUARIO_ACTU,                                
	    @FECHA_ACTU,               
	    @ESTACION_ACTU,                               
	    @CORR_MONEDA,                                  
	    @CORR_PAIS,                                    
	    @CORR_DEPTO,                                   
	    @CORR_MUNICIPIO,                               
	    @SYS_LOGIN_USUARIO,                           
	    @SYS_ESTACION ,                                
	    @SYS_FILAS_AFECTADAS  OUTPUT,
	    @SYS_NUMERO_ERROR  OUTPUT,      
	    @SYS_MENSAJE_ERROR  OUTPUT     
	
	
END
GO
