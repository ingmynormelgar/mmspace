SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PRAL_MTTO_GEN_PERIODICIDAD]
(
	@TIPO_ACTUALIZA INT,
	@CORR_SUSCRIPCION int,
	@CORR_CONFI_PAIS int,
	@CORR_EMPRESA int,
	@CORR_PERIODICIDAD int OUTPUT,
	@NOMBRE_PERIODICIDAD varchar(50),
	@CUOTAS_ANIO int,
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) output
)
AS
BEGIN

	EXECUTE [MmSpace].dbo.PRAL_MTTO_GEN_PERIODICIDAD 
		 @TIPO_ACTUALIZA,                                
	     @CORR_SUSCRIPCION,                              
	     @CORR_CONFI_PAIS,                               
	     @CORR_EMPRESA,                                  
	     @CORR_PERIODICIDAD OUTPUT,     
	     @NOMBRE_PERIODICIDAD,                          
	     @CUOTAS_ANIO,                                   
	     @SYS_LOGIN_USUARIO,                            
	     @SYS_ESTACION,                                 
	     @SYS_FILAS_AFECTADAS OUTPUT, 
	     @SYS_NUMERO_ERROR OUTPUT,       
	     @SYS_MENSAJE_ERROR OUTPUT      
	
END
GO
