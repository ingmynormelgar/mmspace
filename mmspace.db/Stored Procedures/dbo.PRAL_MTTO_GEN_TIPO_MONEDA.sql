SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PRAL_MTTO_GEN_TIPO_MONEDA]
(
	@TIPO_ACTUALIZA INT,
	@CORR_SUSCRIPCION int,
	@CORR_CONFI_PAIS INT,
	@CORR_MONEDA int OUTPUT,
	@NOMBRE_MONEDA varchar(100),
	@SIMBOLO varchar(15),
	@DESCRIPCION_MONEDA VARCHAR(255),
	@NOMBRE_CORTO VARCHAR(5),
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) output
)
AS
BEGIN
	
	EXECUTE [MmSpace].dbo.PRAL_MTTO_GEN_TIPO_MONEDA
		@TIPO_ACTUALIZA,                                
		@CORR_SUSCRIPCION,                              
		@CORR_CONFI_PAIS,                               
		@CORR_MONEDA OUTPUT,                 
		@NOMBRE_MONEDA,                                
		@SIMBOLO,                                      
		@DESCRIPCION_MONEDA,                           
		@NOMBRE_CORTO,                                 
		@SYS_LOGIN_USUARIO,                            
		@SYS_ESTACION,                                 
		@SYS_FILAS_AFECTADAS OUTPUT, 
		@SYS_NUMERO_ERROR OUTPUT,       
		@SYS_MENSAJE_ERROR OUTPUT      
	
END
GO
