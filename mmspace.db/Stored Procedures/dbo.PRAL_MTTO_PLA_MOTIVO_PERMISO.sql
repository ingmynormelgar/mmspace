SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PRAL_MTTO_PLA_MOTIVO_PERMISO]
(
	@TIPO_ACTUALIZA INT,
	@CORR_SUSCRIPCION int,
	@CORR_CONFI_PAIS int,
	@CORR_EMPRESA int,
	@CORR_MOTIVO_PERMISO int OUTPUT,
	@NOMBRE_MOTIVO_PERMISO varchar(150),
	@CON_GOCE_SALARIAL bit,
	@CLASE_PERMISO varchar(3),
	@USUARIO_CREA varchar(30),
	@FECHA_CREA datetime,
	@ESTACION_CREA varchar(50),
	@USUARIO_ACTU varchar(30),
	@FECHA_ACTU datetime,
	@ESTACION_ACTU varchar(50),
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) output
)
AS
BEGIN
	
	EXECUTE [MmSpace].dbo.PRAL_MTTO_PLA_MOTIVO_PERMISO 
	@TIPO_ACTUALIZA,                              
	@CORR_SUSCRIPCION,                           
	@CORR_CONFI_PAIS,                      
	@CORR_EMPRESA,                               
	@CORR_MOTIVO_PERMISO OUTPUT,
	@NOMBRE_MOTIVO_PERMISO,              
	@CON_GOCE_SALARIAL,                       
	@CLASE_PERMISO,                             
	@USUARIO_CREA,                           
	@FECHA_CREA,              
	@ESTACION_CREA,                            
	@USUARIO_ACTU,                             
	@FECHA_ACTU,             
	@ESTACION_ACTU,                          
	@SYS_LOGIN_USUARIO,                      
	@SYS_ESTACION,                             
	@SYS_FILAS_AFECTADAS OUTPUT,
	@SYS_NUMERO_ERROR OUTPUT,      
	@SYS_MENSAJE_ERROR OUTPUT     
	
END
GO
