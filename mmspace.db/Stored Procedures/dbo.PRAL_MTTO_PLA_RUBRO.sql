SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PRAL_MTTO_PLA_RUBRO]
(
	@TIPO_ACTUALIZA INT,
	@CORR_SUSCRIPCION int,
	@CORR_CONFI_PAIS int,
	@CORR_EMPRESA int,
	@CORR_RUBRO int OUTPUT,
	@NOMBRE_RUBRO varchar(100),
	@INGRESO_DEDUCCION int,
	@CLASE_RUBRO varchar(3),
	@ES_IMPUESTO bit,
	@POR_RUBRO DECIMAL(12,2),
	@CUENTA_CONTABLE VARCHAR(30)='',
	@MOSTRAR_IMPRESION BIT,
	@ORDEN_RUBRO INT,
	@CORR_RUBRO_ADELANTO INT=NULL,
	@CLASE_TIPO_FACTOR varchar(4)='POR',
	@INCLUYE_CALC_SUELDO_PROMEDIO bit=NULL,
	@BASE_PROVISION VARCHAR(3)=NULL,
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) output
)
AS
BEGIN

	EXECUTE [MmSpace].dbo.PRAL_MTTO_PLA_RUBRO 
		@TIPO_ACTUALIZA,                               
		@CORR_SUSCRIPCION,                              
		@CORR_CONFI_PAIS,                               
		@CORR_EMPRESA,                                  
		@CORR_RUBRO OUTPUT,                   
		@NOMBRE_RUBRO,                                 
		@INGRESO_DEDUCCION,                             
		@CLASE_RUBRO,                                  
		@ES_IMPUESTO,                                
		@POR_RUBRO,                                  
		@CUENTA_CONTABLE,                              
		@MOSTRAR_IMPRESION,                          
		@ORDEN_RUBRO,                                   
		@CORR_RUBRO_ADELANTO,                           
		@CLASE_TIPO_FACTOR,                            
		@INCLUYE_CALC_SUELDO_PROMEDIO,               
		@BASE_PROVISION,                               
		@SYS_LOGIN_USUARIO,                            
		@SYS_ESTACION ,                                 
		@SYS_FILAS_AFECTADAS  OUTPUT, 
		@SYS_NUMERO_ERROR  OUTPUT,       
		@SYS_MENSAJE_ERROR  OUTPUT      

END
GO
