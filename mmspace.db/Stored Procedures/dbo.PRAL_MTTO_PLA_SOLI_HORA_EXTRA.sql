SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[PRAL_MTTO_PLA_SOLI_HORA_EXTRA]
(
	@TIPO_ACTUALIZA INT,
	@CORR_SUSCRIPCION int,
	@CORR_CONFI_PAIS int,
	@CORR_EMPRESA int,
	@CORR_EMPLEADO int,
	@CORR_SOLICITUD int OUTPUT,
	@FECHA_SOLICITUD date,
	@ESTADO_SOLICITUD varchar(2),
	@OBSERVACIONES varchar(255),
	@USUARIO_CREA varchar(30),
	@FECHA_CREA datetime,
	@ESTACION_CREA varchar(50),
	@USUARIO_ACTU varchar(30),
	@FECHA_ACTU datetime,
	@ESTACION_ACTU varchar(50),
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) output,
	@CORR_MONEDA int=NULL
)
AS
BEGIN

	EXECUTE [MmSpace].dbo.PRAL_MTTO_PLA_SOLI_HORA_EXTRA 
	@TIPO_ACTUALIZA,                               
	@CORR_SUSCRIPCION,                             
	@CORR_CONFI_PAIS,                              
	@CORR_EMPRESA,                                 
	@CORR_EMPLEADO,                                
	@CORR_SOLICITUD OUTPUT,          
	@FECHA_SOLICITUD,                   
	null,                            
	@ESTADO_SOLICITUD,                            
	@OBSERVACIONES,                               
	@USUARIO_CREA,                                
	@FECHA_CREA,               
	@ESTACION_CREA,                               
	@USUARIO_ACTU,                                
	@FECHA_ACTU,               
	@ESTACION_ACTU,                               
	@SYS_LOGIN_USUARIO,                           
	@SYS_ESTACION,                                
	@SYS_FILAS_AFECTADAS  OUTPUT,
	@SYS_NUMERO_ERROR  OUTPUT,      
	@SYS_MENSAJE_ERROR  OUTPUT,    
	@CORR_MONEDA
	
END
GO
