SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[PRAL_MTTO_PLA_SOLI_HORA_EXTRA_DETA]
(
	@TIPO_ACTUALIZA INT,
	@CORR_SUSCRIPCION int,
	@CORR_CONFI_PAIS int,
	@CORR_EMPRESA int,
	@CORR_EMPLEADO int,
	@CORR_SOLICITUD int,
	@CORR_SOLICITUD_DETA int OUTPUT,
	@CORR_RUBRO int,
	@FECHA_INICIAL_SOLI datetime,
	@FECHA_FINAL_SOLI datetime,
	@TOTAL_HORAS_SOLI INT = NULL,
	@TOTAL_MINUTOS_SOLI INT = NULL,
	@FECHA_INICIAL_AUT datetime= NULL,
	@FECHA_FINAL_AUT datetime= NULL,
	@TOTAL_HORAS_AUT INT = NULL,
	@TOTAL_MINUTOS_AUT INT = NULL,
	@FECHA_INICIAL datetime= NULL,
	@FECHA_FINAL datetime= NULL,
	@TOTAL_HORAS INT = NULL,
	@TOTAL_MINUTOS INT = NULL,
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) OUTPUT
	
)
AS
BEGIN

	EXECUTE [MmSpace].dbo.PRAL_MTTO_PLA_SOLI_HORA_EXTRA_DETA
	@TIPO_ACTUALIZA,                               
	@CORR_SUSCRIPCION,                             
	@CORR_CONFI_PAIS,                              
	@CORR_EMPRESA,                                 
	@CORR_EMPLEADO,                                
	@CORR_SOLICITUD,                               
	@CORR_SOLICITUD_DETA OUTPUT,
	@CORR_RUBRO,                                   
	@FECHA_INICIAL_SOLI,       
	@FECHA_FINAL_SOLI,         
	@TOTAL_HORAS_SOLI,                             
	@TOTAL_MINUTOS_SOLI,                           
	@FECHA_INICIAL_AUT,        
	@FECHA_FINAL_AUT,          
	@TOTAL_HORAS_AUT,                              
	@TOTAL_MINUTOS_AUT,
	@FECHA_INICIAL,        
	@FECHA_FINAL,          
	@TOTAL_HORAS,                              
	@TOTAL_MINUTOS, 
	null,                                 
	null,                                  
	null,                               
	@SYS_LOGIN_USUARIO,                           
	@SYS_ESTACION,                                
	@SYS_FILAS_AFECTADAS  OUTPUT,
	@SYS_NUMERO_ERROR  OUTPUT,
	@SYS_MENSAJE_ERROR OUTPUT     
	
	
END
GO
