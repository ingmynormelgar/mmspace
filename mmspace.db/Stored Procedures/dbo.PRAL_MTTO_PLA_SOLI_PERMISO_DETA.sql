SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[PRAL_MTTO_PLA_SOLI_PERMISO_DETA]
(
	@TIPO_ACTUALIZA INT,
	@CORR_SUSCRIPCION int,
	@CORR_CONFI_PAIS int,
	@CORR_EMPRESA int,
	@CORR_EMPLEADO int,
	@CORR_PERMISO int,
	@CORR_PERMISO_DETA int OUTPUT,
	@FECHA_INICIAL datetime,
	@FECHA_FINAL DATETIME,
	@TOTAL_DIAS decimal(12,2),
	@TOTAL_HORAS decimal(12,2),
	@TOTAL_MIN decimal(12,2) ,
	@USUARIO_CREA varchar(30),
	@FECHA_CREA datetime,
	@ESTACION_CREA varchar(50),
	@USUARIO_ACTU varchar(30),
	@FECHA_ACTU datetime,
	@ESTACION_ACTU varchar(50),
	@CORR_ACCION INT=NULL,
	@CLASE_SOLICITUD_PERMISO_DETA varchar(2),
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) output
)
AS
BEGIN

	EXECUTE [MmSpace].dbo.PRAL_MTTO_PLA_SOLI_PERMISO_DETA 
		@TIPO_ACTUALIZA,                               
	    @CORR_SUSCRIPCION,                             
	    @CORR_CONFI_PAIS,                              
	    @CORR_EMPRESA,                                 
	    @CORR_EMPLEADO,                                
	    @CORR_PERMISO,                                 
	    @CORR_PERMISO_DETA OUTPUT,    
	    @FECHA_INICIAL,            
	    @FECHA_FINAL,              
	    @TOTAL_DIAS,                                
	    @TOTAL_HORAS,                               
	    @TOTAL_MIN,                                 
	    @USUARIO_CREA,                                
	    @FECHA_CREA,               
	    @ESTACION_CREA,                               
	    @USUARIO_ACTU,                                
	    @FECHA_ACTU,               
	    @ESTACION_ACTU,                               
	    @CORR_ACCION,                                  
	    @CLASE_SOLICITUD_PERMISO_DETA,                
	    @SYS_LOGIN_USUARIO,                           
	    @SYS_ESTACION,                                
	    @SYS_FILAS_AFECTADAS OUTPUT,
	    @SYS_NUMERO_ERROR OUTPUT,      
	    @SYS_MENSAJE_ERROR OUTPUT     
	
END
GO
