SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PRAL_MTTO_PLA_SOLI_PRESTAMO]
(
	@TIPO_ACTUALIZA INT,
	@CORR_SUSCRIPCION int,
	@CORR_CONFI_PAIS int,
	@CORR_EMPRESA int,
	@CORR_EMPLEADO int,
	@CORR_PRESTAMO int OUTPUT,
	@FECHA_SOLICITUD date,
	@CORR_RUBRO_PRESTAMO int,
	@FECHA_INICIO date,
	@CORR_PERIODICIDAD int,
	@MONTO_PRESTAMO decimal(12,2),
	@NUMERO_CUOTAS int,
	@MONTO_CUOTA decimal(12,2),
	@USUARIO_SOLICITA varchar(30),
	@USUARIO_AUTORIZA varchar(30),
	@CORR_MONEDA int,
	@ESTADO_PRESTAMO varchar(2),
	@OBSERVACIONES varchar(255),
	@USUARIO_CREA varchar(30),
	@FECHA_CREA datetime,
	@ESTACION_CREA varchar(50),
	@USUARIO_ACTU varchar(30),
	@FECHA_ACTU datetime,
	@ESTACION_ACTU varchar(50),
	@CORR_RUBRO_FIJO int,
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) output
)
AS
BEGIN
	
	EXECUTE [MmSpace].dbo.PRAL_MTTO_PLA_SOLI_PRESTAMO
		@TIPO_ACTUALIZA,
		@CORR_SUSCRIPCION,
		@CORR_CONFI_PAIS,
		@CORR_EMPRESA,
		@CORR_EMPLEADO,
		@CORR_PRESTAMO OUTPUT,
		@FECHA_SOLICITUD,
		@CORR_RUBRO_PRESTAMO,
		@FECHA_INICIO,
		@CORR_PERIODICIDAD,
		@MONTO_PRESTAMO,
		@NUMERO_CUOTAS,
		@MONTO_CUOTA,
		@USUARIO_SOLICITA,
		@USUARIO_AUTORIZA,
		@CORR_MONEDA,
		@ESTADO_PRESTAMO,
		@OBSERVACIONES,
		@USUARIO_CREA,
		@FECHA_CREA,
		@ESTACION_CREA,
		@USUARIO_ACTU,
		@FECHA_ACTU,
		@ESTACION_ACTU,
		@CORR_RUBRO_FIJO,
		@SYS_LOGIN_USUARIO,
		@SYS_ESTACION,
		@SYS_FILAS_AFECTADAS output,
		@SYS_NUMERO_ERROR output,
		@SYS_MENSAJE_ERROR output
END
GO
