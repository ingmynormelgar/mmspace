SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[PRAL_MTTO_SEG_USUARIO_EMPRESA]
(
	@TIPO_ACTUALIZA INT,
	@LOGIN_SISTEMA varchar(30),
	@CORR_SUSCRIPCION int,
	@CORR_CONFI_PAIS int,
	@CORR_EMPRESA int OUTPUT,
	@POR_DEFECTO int,
	@SYS_LOGIN_USUARIO Varchar(30),
	@SYS_ESTACION Varchar(50),
	@SYS_FILAS_AFECTADAS int output,
	@SYS_NUMERO_ERROR numeric(38,0) output,
	@SYS_MENSAJE_ERROR nvarchar(4000) output
)
AS
BEGIN
	
	SET NOCOUNT ON

	SELECT @SYS_NUMERO_ERROR=0,@SYS_MENSAJE_ERROR='',@SYS_FILAS_AFECTADAS=0

	IF NOT EXISTS(SELECT 1 FROM SEG_USUARIO_EMPRESA
				  WHERE CORR_SUSCRIPCION=@CORR_SUSCRIPCION
				  AND CORR_CONFI_PAIS=@CORR_CONFI_PAIS
				  AND CORR_EMPRESA=@CORR_EMPRESA
				  AND LOGIN_SISTEMA=@LOGIN_SISTEMA)
	BEGIN
		SELECT @TIPO_ACTUALIZA=1
	END 

	BEGIN TRY
		IF @TIPO_ACTUALIZA=1 --Adicionar
		BEGIN
			SELECT @CORR_EMPRESA=ISNULL(MAX(CORR_EMPRESA),0)+1
			FROM SEG_USUARIO_EMPRESA
			WHERE CORR_SUSCRIPCION=@CORR_SUSCRIPCION
			AND CORR_CONFI_PAIS=@CORR_CONFI_PAIS
			AND LOGIN_SISTEMA=@LOGIN_SISTEMA


			INSERT INTO SEG_USUARIO_EMPRESA
			(LOGIN_SISTEMA,CORR_SUSCRIPCION,CORR_CONFI_PAIS,CORR_EMPRESA,POR_DEFECTO)
			VALUES
			(@LOGIN_SISTEMA,@CORR_SUSCRIPCION,@CORR_CONFI_PAIS,@CORR_EMPRESA,@POR_DEFECTO)
		END ELSE
		IF @TIPO_ACTUALIZA=2 --Actualizar
		BEGIN
			UPDATE SEG_USUARIO_EMPRESA SET
			POR_DEFECTO=@POR_DEFECTO
			WHERE CORR_SUSCRIPCION=@CORR_SUSCRIPCION
			AND CORR_CONFI_PAIS=@CORR_CONFI_PAIS
			AND LOGIN_SISTEMA=@LOGIN_SISTEMA
			AND CORR_EMPRESA=@CORR_EMPRESA
		END ELSE
		IF @TIPO_ACTUALIZA=3 --Eliminar
		BEGIN
			DELETE FROM SEG_USUARIO_EMPRESA
			WHERE CORR_SUSCRIPCION=@CORR_SUSCRIPCION
			AND CORR_CONFI_PAIS=@CORR_CONFI_PAIS
			AND LOGIN_SISTEMA=@LOGIN_SISTEMA
			AND CORR_EMPRESA=@CORR_EMPRESA
		END
		
		SELECT @SYS_FILAS_AFECTADAS=@@ROWCOUNT
	
	END TRY

	BEGIN CATCH
		DECLARE
		@CORR_BITACORA  int,
		@SEVERITY_ERROR  int,
		@ESTADO_ERROR     int,
		@ORIGEN_ERROR nvarchar(126),
		@LINEA_ERROR      int,
		@SYS_FECHA DateTime

		SELECT @CORR_BITACORA=0,
		@SYS_NUMERO_ERROR=ERROR_NUMBER(),
		@SEVERITY_ERROR=ERROR_SEVERITY(),
		@ESTADO_ERROR=ERROR_STATE(),
		@ORIGEN_ERROR=ERROR_PROCEDURE(),
		@LINEA_ERROR=ERROR_LINE(),
		@SYS_MENSAJE_ERROR=ERROR_MESSAGE(),
		@SYS_FECHA=GETDATE()

		EXEC PRAL_MTTO_ADMIN_BITACORA_SISTEMA 1,@CORR_SUSCRIPCION,@CORR_CONFI_PAIS,@CORR_BITACORA output,'PM',@SYS_NUMERO_ERROR,@SEVERITY_ERROR,@ESTADO_ERROR,@ORIGEN_ERROR,@LINEA_ERROR,@SYS_MENSAJE_ERROR,@SYS_LOGIN_USUARIO,@SYS_FECHA,@SYS_ESTACION

	END CATCH;
FINA:
END
GO
