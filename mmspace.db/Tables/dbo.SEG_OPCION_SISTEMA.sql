CREATE TABLE [dbo].[SEG_OPCION_SISTEMA]
(
[CORR_SUSCRIPCION] [int] NOT NULL,
[CORR_CONFI_PAIS] [int] NOT NULL,
[CODIGO_OPCION] [varchar] (30) COLLATE Latin1_General_CS_AS NOT NULL,
[NOMBRE_OPCION] [varchar] (100) COLLATE Latin1_General_CS_AS NOT NULL,
[URL_OPCION] [nvarchar] (4000) COLLATE Latin1_General_CS_AS NOT NULL,
[IMAGEN_OPCION] [varchar] (25) COLLATE Latin1_General_CS_AS NOT NULL,
[USUARIO_CREA] [varchar] (30) COLLATE Latin1_General_CS_AS NOT NULL,
[FECHA_CREA] [datetime] NOT NULL,
[ESTACION_CREA] [varchar] (30) COLLATE Latin1_General_CS_AS NOT NULL,
[USUARIO_ACTU] [varchar] (30) COLLATE Latin1_General_CS_AS NULL,
[FECHA_ACTU] [datetime] NULL,
[ESTACION_ACTU] [varchar] (30) COLLATE Latin1_General_CS_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEG_OPCION_SISTEMA] ADD CONSTRAINT [PK_SEG_OPCIONES_SISTEMA] PRIMARY KEY CLUSTERED  ([CORR_SUSCRIPCION], [CORR_CONFI_PAIS], [CODIGO_OPCION]) ON [PRIMARY]
GO
