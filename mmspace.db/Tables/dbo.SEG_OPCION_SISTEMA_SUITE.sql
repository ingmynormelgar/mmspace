CREATE TABLE [dbo].[SEG_OPCION_SISTEMA_SUITE]
(
[CORR_SUSCRIPCION] [int] NOT NULL,
[CORR_CONFI_PAIS] [int] NOT NULL,
[CODIGO_OPCION] [varchar] (30) COLLATE Latin1_General_CS_AS NOT NULL,
[CODIGO_SUITE] [varchar] (30) COLLATE Latin1_General_CS_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEG_OPCION_SISTEMA_SUITE] ADD CONSTRAINT [PK_SEG_OPCION_SISTEMA_SUITE] PRIMARY KEY CLUSTERED  ([CORR_SUSCRIPCION], [CORR_CONFI_PAIS], [CODIGO_OPCION], [CODIGO_SUITE]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEG_OPCION_SISTEMA_SUITE] ADD CONSTRAINT [FK_SEG_OPCION_SISTEMA_SUITE_SEG_OPCION_SISTEMA] FOREIGN KEY ([CORR_SUSCRIPCION], [CORR_CONFI_PAIS], [CODIGO_OPCION]) REFERENCES [dbo].[SEG_OPCION_SISTEMA] ([CORR_SUSCRIPCION], [CORR_CONFI_PAIS], [CODIGO_OPCION])
GO
ALTER TABLE [dbo].[SEG_OPCION_SISTEMA_SUITE] ADD CONSTRAINT [FK_SEG_OPCION_SISTEMA_SUITE_SEG_SUITE] FOREIGN KEY ([CORR_SUSCRIPCION], [CORR_CONFI_PAIS], [CODIGO_SUITE]) REFERENCES [dbo].[SEG_SUITE] ([CORR_SUSCRIPCION], [CORR_CONFI_PAIS], [CODIGO_SUITE])
GO
