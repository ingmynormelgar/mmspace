CREATE TABLE [dbo].[SEG_USUARIO_EMPRESA]
(
[LOGIN_SISTEMA] [varchar] (30) COLLATE Latin1_General_CS_AS NOT NULL,
[CORR_SUSCRIPCION] [int] NOT NULL,
[CORR_CONFI_PAIS] [int] NOT NULL,
[CORR_EMPRESA] [int] NOT NULL,
[POR_DEFECTO] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEG_USUARIO_EMPRESA] ADD CONSTRAINT [PK_SEG_USUARIO_EMPRESA] PRIMARY KEY CLUSTERED  ([CORR_SUSCRIPCION], [CORR_CONFI_PAIS], [LOGIN_SISTEMA], [CORR_EMPRESA]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEG_USUARIO_EMPRESA] ADD CONSTRAINT [FK_SEG_USUARIO_EMPRESA_SEG_USUARIO] FOREIGN KEY ([LOGIN_SISTEMA]) REFERENCES [dbo].[SEG_USUARIO] ([LOGIN_SISTEMA])
GO
