CREATE TABLE [dbo].[SEG_USUARIO_SESION]
(
[LOGIN_SISTEMA] [varchar] (30) COLLATE Latin1_General_CS_AS NOT NULL,
[CORR_SUSCRIPCION] [int] NOT NULL,
[CORR_CONFI_PAIS] [int] NOT NULL,
[CODIGO_SUITE] [varchar] (30) COLLATE Latin1_General_CS_AS NOT NULL,
[IP_ESTACION] [varchar] (100) COLLATE Latin1_General_CS_AS NOT NULL,
[NOMBRE_ESTACION] [varchar] (50) COLLATE Latin1_General_CS_AS NOT NULL,
[FECHA_INICIO] [datetime] NOT NULL,
[FECHA_FIN] [datetime] NULL,
[ESTADO_SESION] [varchar] (2) COLLATE Latin1_General_CS_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEG_USUARIO_SESION] ADD CONSTRAINT [PK_SEG_USUARIO_SESION] PRIMARY KEY CLUSTERED  ([LOGIN_SISTEMA], [CORR_SUSCRIPCION], [CORR_CONFI_PAIS], [CODIGO_SUITE], [IP_ESTACION]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SEG_USUARIO_SESION] ADD CONSTRAINT [FK_SEG_USUARIO_SESION_SEG_USUARIO] FOREIGN KEY ([LOGIN_SISTEMA]) REFERENCES [dbo].[SEG_USUARIO] ([LOGIN_SISTEMA])
GO
