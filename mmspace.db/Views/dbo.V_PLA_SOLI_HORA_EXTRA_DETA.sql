SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[V_PLA_SOLI_HORA_EXTRA_DETA]
AS
SELECT A.CORR_SUSCRIPCION
      ,A.CORR_CONFI_PAIS
      ,A.CORR_EMPRESA
      ,A.CORR_EMPLEADO
      ,A.CORR_SOLICITUD
      ,A.CORR_SOLICITUD_DETA
      ,A.CORR_RUBRO
      ,A.NOMBRE_RUBRO 
      ,A.FECHA_INICIAL
      ,A.FECHA_FINAL
      ,A.TOTAL_HORAS_SOLICITA
      ,A.ANIO_PERIODO
      ,A.MES_PERIODO
      ,A.CORR_RUBRO_MES
	  ,A.TOTAL_HORAS_AUTORIZA
	  ,A.TOTAL_HORAS_REAL
  FROM [MmSpace].dbo.V_PLA_SOLI_HORA_EXTRA_DETA A
GO
