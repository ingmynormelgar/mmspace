SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[V_SEG_USUARIO]
AS
SELECT A.LOGIN_SISTEMA
      ,A.NOMBRE_USUARIO
      ,A.CLAVE_USUARIO
	  ,A.CLAVE_USUARIO_SAL
      ,A.CORREO_ELECTRONICO
      ,A.TIPO_USUARIO
      ,CASE A.TIPO_USUARIO
       WHEN 1 THEN 'Solo consulta'
       WHEN 2 THEN 'Normal'
       WHEN 3 THEN 'Supervisor o Auditor'
       WHEN 4 THEN 'Administrador del sistema'
       WHEN 5 THEN 'Super Administrador'
       ELSE '' END NOMBRE_TIPO_USUARIO
      ,A.ESTADO_USUARIO
      ,CASE A.ESTADO_USUARIO
       WHEN 0 THEN 'Pendiente de Activar'
       WHEN 1 THEN 'Usuario Activo'
       WHEN 2 THEN 'Usuario Suspendido'
       WHEN 3 THEN 'Usuario Cancelado'       
       ELSE '' END NOMBRE_ESTADO_USUARIO
	  ,A.IDIOMA	    
      ,A.USUARIO_CREA
      ,A.FECHA_CREA
      ,A.ESTACION_CREA
      ,A.USUARIO_ACTU
      ,A.FECHA_ACTU
      ,A.ESTACION_ACTU
      ,ISNULL(B.CORR_EMPRESA,0) CORR_EMPRESA
      ,ISNULL(B.NOMBRE_EMPRESA,'') NOMBRE_EMPRESA      
	  ,C.FOTO FOTO_PERFIL
	  ,D.FOTO FOTO_FIRMA
	  ,ISNULL(A.CORR_EMPLEADO,0) CORR_EMPLEADO
	  ,E.NOMBRE_EMPLEADO
FROM SEG_USUARIO A
LEFT OUTER JOIN (SELECT A.LOGIN_SISTEMA,A.CORR_SUSCRIPCION,A.CORR_CONFI_PAIS,A.CORR_EMPRESA,A.NOMBRE_EMPRESA, ROW_NUMBER() OVER(PARTITION BY A.LOGIN_SISTEMA,A.CORR_SUSCRIPCION,A.CORR_CONFI_PAIS ORDER BY A.LOGIN_SISTEMA,A.CORR_SUSCRIPCION,A.CORR_CONFI_PAIS,A.CORR_EMPRESA) ORDEN_EMPRESA
                 FROM V_SEG_USUARIO_EMPRESA A WHERE A.POR_DEFECTO=1) B ON A.LOGIN_SISTEMA=B.LOGIN_SISTEMA AND B.ORDEN_EMPRESA=1
LEFT OUTER JOIN SEG_USUARIO_FOTO C ON A.LOGIN_SISTEMA=C.LOGIN_SISTEMA AND C.CLASE_FOTO='PE'
LEFT OUTER JOIN SEG_USUARIO_FOTO D ON A.LOGIN_SISTEMA=D.LOGIN_SISTEMA AND D.CLASE_FOTO='FI'
LEFT OUTER JOIN [MmSpace].dbo.GEN_EMPLEADO E ON E.CORR_SUSCRIPCION = 7 AND E.CORR_CONFI_PAIS = 7 AND E.CORR_EMPRESA = 1 AND E.CORR_EMPLEADO = A.CORR_EMPLEADO
GO
